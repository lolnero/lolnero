// Copyright (c) 2014-2019, The Monero Project
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this list of
//    conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this list
//    of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may be
//    used to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
// THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
// THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <array>
#include <boost/predef/other/endian.h>
#include <boost/endian/conversion.hpp>
#include <boost/range/algorithm/equal.hpp>
#include <boost/range/algorithm_ext/iota.hpp>
#include <cstdint>
#include <gtest/gtest.h>
#include <iterator>
#include <string>
#include <sstream>
#include <vector>

#include "math/crypto/functional/key.hpp"
#include "tools/epee/functional/hex.hpp"
#include "tools/epee/functional/span.hpp"
#include "tools/epee/include/net/net_utils_base.h"
#include "tools/epee/include/net/local_ip.h"
#include "tools/epee/include/string_tools.h"

namespace
{
  template<typename Destination, typename Source>
  bool can_construct()
  {
    const unsigned count =
      unsigned(std::is_constructible<Destination, Source>()) +
      unsigned(std::is_constructible<Destination, Source&>()) +
      unsigned(std::is_convertible<Source, Destination>()) +
      unsigned(std::is_convertible<Source&, Destination>()) +
      unsigned(std::is_assignable<Destination, Source>()) +
      unsigned(std::is_assignable<Destination, Source&>());
    EXPECT_TRUE(count == 6 || count == 0) <<
      "Mismatch on construction results - " << count << " were true";
    return count == 6;
  }

  std::string std_to_hex(const std::vector<unsigned char>& source)
  {
    std::stringstream out;
    out << std::hex;
    for (const unsigned char byte : source)
    {
      out << std::setw(2) << std::setfill('0') << int(byte);
    }
    return out.str();
  }

  std::vector<unsigned char> get_all_bytes()
  {
    std::vector<unsigned char> out;
    out.resize(256);
    boost::range::iota(out, 0);
    return out;
  }

  #define CHECK_EQUAL(lhs, rhs) \
    EXPECT_TRUE( lhs == rhs );  \
    EXPECT_TRUE( rhs == lhs );  \
    EXPECT_FALSE( lhs != rhs ); \
    EXPECT_FALSE( rhs != lhs ); \
    EXPECT_FALSE( lhs < rhs );  \
    EXPECT_FALSE( rhs < lhs );

  #define CHECK_LESS(lhs, rhs)   \
    EXPECT_FALSE( lhs == rhs );  \
    EXPECT_FALSE( rhs == lhs );  \
    EXPECT_TRUE( lhs != rhs );   \
    EXPECT_TRUE( rhs != lhs );   \
    EXPECT_TRUE( lhs < rhs );    \
    EXPECT_FALSE( rhs < lhs );

  #if BOOST_ENDIAN_LITTLE_BYTE
    #define CHECK_LESS_ENDIAN(lhs, rhs) CHECK_LESS( rhs , lhs )
  #else
    #define CHECK_LESS_ENDIAN(lhs, rhs) CHECK_LESS( lhs , rhs )
  #endif
}

TEST(ToHex, String)
{
  EXPECT_EQ(
    std::string{"ffab0100"},
    epee::hex::encode_to_hex(epee::pod_to_span("\xff\xab\x01"))
  );

  const std::vector<unsigned char> all_bytes = get_all_bytes();
  EXPECT_EQ(
    std_to_hex(all_bytes), epee::hex::encode_to_hex(all_bytes)
  );

}

TEST(ToHex, Ostream)
{
  std::stringstream out;

  {
    const std::uint8_t source[] = {0xff, 0xab, 0x01, 0x00};
    out << epee::hex::encode_to_hex(source);
  }

  std::string expected{"ffab0100"};
  EXPECT_EQ(expected, out.str());

  const std::vector<unsigned char> all_bytes = get_all_bytes();

  expected.append(std_to_hex(all_bytes));
  out << epee::hex::encode_to_hex(all_bytes);
  EXPECT_EQ(expected, out.str());
}

TEST(ToHex, Formatted)
{
  std::stringstream out;
  std::string expected{};

  expected.append("<ffab0100>");
  out << epee::hex::encode_to_hex_formatted(epee::pod_to_span("\xFF\xAB\x01"));
  EXPECT_EQ(expected, out.str());

  const std::vector<unsigned char> all_bytes = get_all_bytes();

  expected.append("<").append(std_to_hex(all_bytes)).append(">");
  out << epee::hex::encode_to_hex_formatted(all_bytes);
  EXPECT_EQ(expected, out.str());
}

TEST(FromHex, ToString)
{
  static constexpr const char hex[] = "deadbeeffY";
  static constexpr const char binary[] = {
    char(0xde), char(0xad), char(0xbe), char(0xef), 0x00
  };

  EXPECT_FALSE(epee::hex::decode_from_hex_to_string(hex));

  std::string_view portion{hex};
  portion.remove_suffix(1);
  EXPECT_FALSE(epee::hex::decode_from_hex_to_string(portion));

  portion.remove_suffix(1);

  const auto maybe_str = epee::hex::decode_from_hex_to_string(portion);
  EXPECT_TRUE(maybe_str);

  EXPECT_EQ(std::string{binary}, *maybe_str);
}

TEST(FromHex, ToBuffer)
{
  static constexpr const char hex[] = "deadbeeffY";
  static constexpr const std::uint8_t binary[] = {0xde, 0xad, 0xbe, 0xef};

  EXPECT_FALSE(epee::hex::decode_from_hex_to_blob(hex));

  std::string_view portion{hex};
  portion.remove_suffix(1);

  EXPECT_FALSE(epee::hex::decode_from_hex_to_blob(portion));

  portion.remove_suffix(1);

  const auto maybe_decoded = epee::hex::decode_from_hex_to_blob(portion);
  EXPECT_TRUE(maybe_decoded);

  const epee::blob::data expected{std::begin(binary), std::end(binary)};
  EXPECT_EQ(*maybe_decoded, expected);
}

TEST(StringTools, BuffToHex)
{
  const std::vector<unsigned char> all_bytes = get_all_bytes();

  EXPECT_EQ(
    std_to_hex(all_bytes),
    (epee::string_tools::buff_to_hex_nodelimer(
      std::string{reinterpret_cast<const char*>(all_bytes.data()), all_bytes.size()}
    ))
  );
}

TEST(StringTools, PodToHex)
{
  struct some_pod { unsigned char data[4]; };
  EXPECT_EQ(
    std::string{"ffab0100"},
    (epee::string_tools::pod_to_hex(some_pod{{0xFF, 0xAB, 0x01, 0x00}}))
  );
}

TEST(StringTools, ParseHex)
{
  static const char data[] = "a10b68c2";
  for (size_t i = 0; i < sizeof(data); i += 2)
  {
    std::string res;
    ASSERT_TRUE(epee::string_tools::parse_hexstr_to_binbuff(std::string(data, i), res));
    std::string hex = epee::string_tools::buff_to_hex_nodelimer(res);
    ASSERT_EQ(hex.size(), i);
    ASSERT_EQ(memcmp(data, hex.data(), i), 0);
  }
}

TEST(StringTools, ParseNotHex)
{
  std::string res;
  for (size_t i = 0; i < 256; ++i)
  {
    std::string inputHexString = std::string(2, static_cast<char>(i));
    if ((i >= '0' && i <= '9') || (i >= 'A' && i <= 'F') || (i >= 'a' && i <= 'f')) {
      ASSERT_TRUE(epee::string_tools::parse_hexstr_to_binbuff(inputHexString, res));
    } else {
      ASSERT_FALSE(epee::string_tools::parse_hexstr_to_binbuff(inputHexString, res));
    }
  }

  ASSERT_FALSE(epee::string_tools::parse_hexstr_to_binbuff(std::string("a"), res));
}

TEST(StringTools, GetIpString)
{
  EXPECT_EQ(
    std::string{"0.0.0.0"}, epee::string_tools::get_ip_string_from_int32(0)
  );
  EXPECT_EQ(
    std::string{"255.0.255.0"},
    epee::string_tools::get_ip_string_from_int32(htonl(0xff00ff00))
  );
  EXPECT_EQ(
    std::string{"255.255.255.255"},
    epee::string_tools::get_ip_string_from_int32(htonl(0xffffffff))
  );
}

TEST(StringTools, GetIpInt32)
{
  std::uint32_t ip = 0;
  EXPECT_FALSE(epee::string_tools::get_ip_int32_from_string(ip, ""));
  EXPECT_FALSE(epee::string_tools::get_ip_int32_from_string(ip, "1."));
  EXPECT_FALSE(epee::string_tools::get_ip_int32_from_string(ip, "1.1."));
  EXPECT_FALSE(epee::string_tools::get_ip_int32_from_string(ip, "1.1.1."));
  EXPECT_FALSE(epee::string_tools::get_ip_int32_from_string(ip, "ff.0.ff.0"));
  EXPECT_FALSE(epee::string_tools::get_ip_int32_from_string(ip, "1.1.1.256"));

  EXPECT_TRUE(epee::string_tools::get_ip_int32_from_string(ip, "1"));
  EXPECT_EQ(htonl(1), ip);

  EXPECT_TRUE(epee::string_tools::get_ip_int32_from_string(ip, "1.1"));
  EXPECT_EQ(htonl(0x1000001), ip);

  EXPECT_TRUE(epee::string_tools::get_ip_int32_from_string(ip, "1.1.1"));
  EXPECT_EQ(htonl(0x1010001), ip);

  EXPECT_TRUE(epee::string_tools::get_ip_int32_from_string(ip, "0.0.0.0"));
  EXPECT_EQ(0, ip);

  EXPECT_TRUE(epee::string_tools::get_ip_int32_from_string(ip, "1.1.1.1"));
  EXPECT_EQ(htonl(0x01010101), ip);

/*
  The existing epee conversion function does not work with 255.255.255.255, for
  the reasons specified in the inet_addr documentation. Consider fixing in a
  future patch. This address is not likely to be used for purposes within
  monero.
  EXPECT_TRUE(epee::string_tools::get_ip_int32_from_string(ip, "255.255.255.255"));
  EXPECT_EQ(htonl(0xffffffff), ip);
*/

  EXPECT_TRUE(epee::string_tools::get_ip_int32_from_string(ip, "10.0377.0.0377"));
  EXPECT_EQ(htonl(0xaff00ff), ip);

  EXPECT_TRUE(epee::string_tools::get_ip_int32_from_string(ip, "0xff.10.0xff.0"));
  EXPECT_EQ(htonl(0xff0aff00), ip);
}

TEST(NetUtils, IPv4NetworkAddress)
{
  static_assert(epee::net_utils::ipv4_network_address::get_type_id() == epee::net_utils::address_type::ipv4, "bad ipv4 type id");

  const auto ip1 = boost::endian::native_to_big(0x330012FFu);
  const auto ip_loopback = boost::endian::native_to_big(0x7F000001u);
  const auto ip_local = boost::endian::native_to_big(0x0A000000u);

  epee::net_utils::ipv4_network_address address1{ip1, 65535};
  CHECK_EQUAL(address1, address1);
  EXPECT_STREQ("51.0.18.255:65535", address1.str().c_str());
  EXPECT_STREQ("51.0.18.255", address1.host_str().c_str());
  EXPECT_FALSE(address1.is_loopback());
  EXPECT_FALSE(address1.is_local());
  EXPECT_EQ(epee::net_utils::ipv4_network_address::get_type_id(), address1.get_type_id());
  EXPECT_EQ(ip1, address1.ip());
  EXPECT_EQ(65535, address1.port());
  EXPECT_TRUE(epee::net_utils::ipv4_network_address{std::move(address1)} == address1);
  EXPECT_TRUE(epee::net_utils::ipv4_network_address{address1} == address1);

  const epee::net_utils::ipv4_network_address loopback{ip_loopback, 0};
  CHECK_EQUAL(loopback, loopback);
  CHECK_LESS_ENDIAN(address1, loopback);
  EXPECT_STREQ("127.0.0.1:0", loopback.str().c_str());
  EXPECT_STREQ("127.0.0.1", loopback.host_str().c_str());
  EXPECT_TRUE(loopback.is_loopback());
  EXPECT_FALSE(loopback.is_local());
  EXPECT_EQ(epee::net_utils::ipv4_network_address::get_type_id(), address1.get_type_id());
  EXPECT_EQ(ip_loopback, loopback.ip());
  EXPECT_EQ(0, loopback.port());

  const epee::net_utils::ipv4_network_address local{ip_local, 8080};
  CHECK_EQUAL(local, local);
  CHECK_LESS(local, address1);
  CHECK_LESS(local, loopback);
  EXPECT_FALSE(local.is_loopback());
  EXPECT_TRUE(local.is_local());

  epee::net_utils::ipv4_network_address address2{ip1, 55};
  CHECK_EQUAL(address2, address2);
  CHECK_LESS_ENDIAN(address2, loopback);
  CHECK_LESS(local, address2);
  EXPECT_STREQ("51.0.18.255:55", address2.str().c_str());
  EXPECT_STREQ("51.0.18.255", address2.host_str().c_str());


  address2 = std::move(address1);
  CHECK_EQUAL(address2, address1);

  address2 = local;
  CHECK_EQUAL(address2, local);
  CHECK_LESS(address2, address1);
}

TEST(NetUtils, NetworkAddress)
{
  const auto ip1 = boost::endian::native_to_big(0x330012FFu);
  const auto ip_loopback = boost::endian::native_to_big(0x7F000001u);
  const auto ip_local = boost::endian::native_to_big(0x0A000000u);

  struct custom_address {
    constexpr static bool equal(const custom_address&) noexcept { return false; }
    constexpr static bool less(const custom_address&) noexcept { return false; }
    constexpr static bool is_same_host(const custom_address&) noexcept { return false; }
    constexpr static bool is_loopback() noexcept { return false; }
    constexpr static bool is_local() noexcept { return false; }
    static std::string str() { return {}; }
    static std::string host_str() { return {}; }
    constexpr static epee::net_utils::address_type get_type_id() noexcept { return epee::net_utils::address_type(-1); }
    constexpr static epee::net_utils::zone get_zone() noexcept { return epee::net_utils::zone::invalid; }
    constexpr static bool is_blockable() noexcept { return false; }
  };

  const epee::net_utils::network_address empty;
  CHECK_EQUAL(empty, empty);
  EXPECT_TRUE(empty.is_same_host(empty));
  EXPECT_STREQ("<none>", empty.str().c_str());
  EXPECT_STREQ("<none>", empty.host_str().c_str());
  EXPECT_FALSE(empty.is_loopback());
  EXPECT_FALSE(empty.is_local());
  EXPECT_EQ(epee::net_utils::address_type::invalid, empty.get_type_id());
  EXPECT_EQ(epee::net_utils::zone::invalid, empty.get_zone());
  EXPECT_FALSE(empty.is_blockable());
  EXPECT_THROW(empty.as<custom_address>(), std::bad_cast);

  epee::net_utils::network_address address1{
    epee::net_utils::ipv4_network_address{ip1, 65535}
  };
  CHECK_EQUAL(address1, address1);
  CHECK_EQUAL(epee::net_utils::network_address{address1}, address1);
  CHECK_LESS(empty, address1);
  EXPECT_TRUE(address1.is_same_host(address1));
  EXPECT_FALSE(empty.is_same_host(address1));
  EXPECT_FALSE(address1.is_same_host(empty));
  EXPECT_STREQ("51.0.18.255:65535", address1.str().c_str());
  EXPECT_STREQ("51.0.18.255", address1.host_str().c_str());
  EXPECT_FALSE(address1.is_loopback());
  EXPECT_FALSE(address1.is_local());
  EXPECT_EQ(epee::net_utils::ipv4_network_address::get_type_id(), address1.get_type_id());
  EXPECT_EQ(epee::net_utils::zone::public_, address1.get_zone());
  EXPECT_TRUE(address1.is_blockable());
  EXPECT_NO_THROW(address1.as<epee::net_utils::ipv4_network_address>());
  EXPECT_THROW(address1.as<custom_address>(), std::bad_cast);

  const epee::net_utils::network_address loopback{
    epee::net_utils::ipv4_network_address{ip_loopback, 0}
  };
  CHECK_EQUAL(loopback, loopback);
  CHECK_LESS(empty, loopback);
  CHECK_LESS_ENDIAN(address1, loopback);
  EXPECT_TRUE(loopback.is_same_host(loopback));
  EXPECT_FALSE(loopback.is_same_host(address1));
  EXPECT_FALSE(address1.is_same_host(loopback));
  EXPECT_STREQ("127.0.0.1:0", loopback.str().c_str());
  EXPECT_STREQ("127.0.0.1", loopback.host_str().c_str());
  EXPECT_TRUE(loopback.is_loopback());
  EXPECT_FALSE(loopback.is_local());
  EXPECT_EQ(epee::net_utils::ipv4_network_address::get_type_id(), address1.get_type_id());
  EXPECT_EQ(epee::net_utils::zone::public_, address1.get_zone());
  EXPECT_EQ(epee::net_utils::ipv4_network_address::get_type_id(), address1.get_type_id());

  const epee::net_utils::network_address local{
    epee::net_utils::ipv4_network_address{ip_local, 8080}
  };
  CHECK_EQUAL(local, local);
  CHECK_LESS(local, loopback);
  CHECK_LESS(local, address1);
  EXPECT_FALSE(local.is_loopback());
  EXPECT_TRUE(local.is_local());

  epee::net_utils::network_address address2{
    epee::net_utils::ipv4_network_address{ip1, 55}
  };
  CHECK_EQUAL(address2, address2);
  CHECK_LESS(address2, address1);
  CHECK_LESS(local, address2);
  CHECK_LESS_ENDIAN(address2, loopback);
  EXPECT_TRUE(address1.is_same_host(address2));
  EXPECT_TRUE(address2.is_same_host(address1));
  EXPECT_STREQ("51.0.18.255:55", address2.str().c_str());
  EXPECT_STREQ("51.0.18.255", address2.host_str().c_str());

  address2 = std::move(address1);
  CHECK_EQUAL(address1, address1);
  CHECK_EQUAL(empty, address1);
  CHECK_LESS(address1, address2);
  EXPECT_FALSE(address1.is_same_host(address2));
  EXPECT_FALSE(address2.is_same_host(address1));
  EXPECT_STREQ("51.0.18.255:65535", address2.str().c_str());
  EXPECT_STREQ("51.0.18.255", address2.host_str().c_str());
  EXPECT_FALSE(address1.is_loopback());
  EXPECT_FALSE(address1.is_local());
  EXPECT_THROW(address1.as<epee::net_utils::ipv4_network_address>(), std::bad_cast);
  EXPECT_NO_THROW(address2.as<epee::net_utils::ipv4_network_address>());

  address2 = local;
  CHECK_EQUAL(address2, local);
  CHECK_LESS(address1, address2);
  EXPECT_TRUE(address2.is_same_host(local));
  EXPECT_TRUE(local.is_same_host(address2));
  EXPECT_FALSE(address2.is_same_host(address1));
  EXPECT_FALSE(address1.is_same_host(address2));

}

static bool is_local(const char *s)
{
  uint32_t ip;
  LOG_ERROR_AND_THROW_UNLESS(epee::string_tools::get_ip_int32_from_string(ip, s), std::string("Invalid IP address: ") + s);
  return epee::net_utils::is_ip_local(ip);
}

TEST(NetUtils, PrivateRanges)
{
  ASSERT_EQ(is_local("10.0.0.0"), true);
  ASSERT_EQ(is_local("10.255.0.0"), true);
  ASSERT_EQ(is_local("127.0.0.0"), false); // loopback is not considered local
  ASSERT_EQ(is_local("192.167.255.255"), false);
  ASSERT_EQ(is_local("192.168.0.0"), true);
  ASSERT_EQ(is_local("192.168.255.255"), true);
  ASSERT_EQ(is_local("192.169.0.0"), false);
  ASSERT_EQ(is_local("172.0.0.0"), false);
  ASSERT_EQ(is_local("172.15.255.255"), false);
  ASSERT_EQ(is_local("172.16.0.0"), true);
  ASSERT_EQ(is_local("172.16.255.255"), true);
  ASSERT_EQ(is_local("172.31.255.255"), true);
  ASSERT_EQ(is_local("172.32.0.0"), false);
  ASSERT_EQ(is_local("0.0.0.0"), false);
  ASSERT_EQ(is_local("255.255.255.254"), false);
  ASSERT_EQ(is_local("11.255.255.255"), false);
  ASSERT_EQ(is_local("0.0.0.10"), false);
  ASSERT_EQ(is_local("0.0.168.192"), false);
  ASSERT_EQ(is_local("0.0.30.172"), false);
  ASSERT_EQ(is_local("0.0.30.127"), false);
}
