/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include <gtest/gtest.h>

#include "math/crypto/controller/keyGen.hpp"
#include "math/crypto/controller/random.hpp"

#include "math/blockchain/controller/blockchain_mem.hpp"

#include "tools/epee/include/logging.hpp"
#include "cryptonote/basic/functional/format_utils.hpp"

#include "math/blockchain/functional/block.hpp"
#include "math/blockchain/functional/ringct.hpp"

using namespace cryptonote;

class mem_db : public ::testing::Test {
 protected:
  void SetUp() override {
    global_setting_verify_ringct = false;

    epee::set_log_level(4);
  }
};

blockchain_t make_chain() {
  blockchain_t chain;

  chain.m_fixed_difficulty = 0;

  return chain;
}

TEST_F(mem_db, init)
{
  auto chain = make_chain();

  // EXPECT_TRUE(chain.get_size() == 1);

  // const block_t genesis =
  //   chain.get_block_by_index(0).value_or(block_t{});

  // EXPECT_EQ
  //   (
  //    lol_genesis_block()
  //    , review_block(genesis)
  //    );
}

TEST_F(mem_db, add_empty_block)
{
  auto chain = make_chain();

  block_t b{};

  EXPECT_FALSE(chain.push_block(b));

  b.prev_hash = lol_genesis_block().hash();
  b.miner_tx = {};
  b.miner_tx.index = 2;

  EXPECT_TRUE(chain.push_block(b));

  EXPECT_EQ
    (
     b
     , chain.get_block_by_index(1)
     );

  EXPECT_FALSE
    (
     chain.get_block_by_hash({})
     );

  EXPECT_EQ
    (
     chain.get_block_by_hash(b.hash())
     , b
     );

  EXPECT_FALSE
    (
     chain.get_coinbase_tx_by_hash({})
     );

  EXPECT_EQ
    (
     chain.get_coinbase_tx_by_hash(b.miner_tx.hash())
     , b.miner_tx
     );
}

TEST_F(mem_db, push_block_with_empty_ringct)
{
  auto chain = make_chain();

  block_t b{};
  b.prev_hash = lol_genesis_block().hash();
  b.miner_tx = {};
  b.miner_tx.index = 2;

  ringct tx{};

  const auto dummy_output = crypto::p2pk(crypto::randomPoint());
  const auto dummy_commit = crypto::randomPoint();

  tx.outputs = {{dummy_output, dummy_commit}};

  b.txs = {tx};

  EXPECT_TRUE(chain.push_block(b));

  EXPECT_EQ
    (
     b
     , chain.get_block_by_index(1)
     );

  EXPECT_FALSE
    (
     chain.get_block_by_hash({})
     );

  EXPECT_EQ
    (
     chain.get_block_by_hash(b.hash())
     , b
     );

  EXPECT_FALSE
    (
     chain.get_coinbase_tx_by_hash({})
     );

  EXPECT_EQ
    (
     chain.get_coinbase_tx_by_hash(b.miner_tx.hash())
     , b.miner_tx
     );

  EXPECT_FALSE
    (
     chain.get_ringct_by_hash({})
     );

  EXPECT_EQ
    (
     chain.get_ringct_by_hash(tx.hash())
     , tx
     );

  EXPECT_TRUE
    (
     chain.get_output_by_index(0)
     );

  EXPECT_TRUE
    (
     chain.get_output_by_index(1)
     );

  EXPECT_FALSE
    (
     chain.get_output_by_index(2)
     );

  EXPECT_EQ
    (
     chain.get_output_by_index(1)->public_key
     , dummy_output
     );
}


TEST_F(mem_db, pop_block)
{
  auto chain = make_chain();

  block_t b{};
  b.prev_hash = lol_genesis_block().hash();
  b.miner_tx = {};
  b.miner_tx.index = 2;

  ringct tx{};

  // const auto dummy_key_image = crypto::p2ki(crypto::randomPoint());
  // const ringct_input dummy_input = {dummy_key_image, {}};

  // tx.inputs = {dummy_input};


  const auto dummy_output = crypto::p2pk(crypto::randomPoint());
  const auto dummy_commit = crypto::randomPoint();

  tx.outputs = {{dummy_output, dummy_commit}};

  b.txs = {tx};

  EXPECT_TRUE(chain.push_block(b));

  EXPECT_EQ
    (
     b
     , chain.get_block_by_index(1)
     );

  EXPECT_EQ
    (
     chain.get_block_by_hash(b.hash())
     , b
     );

  EXPECT_EQ
    (
     chain.get_coinbase_tx_by_hash(b.miner_tx.hash())
     , b.miner_tx
     );

  EXPECT_EQ
    (
     chain.get_ringct_by_hash(tx.hash())
     , tx
     );

  EXPECT_TRUE
    (
     chain.get_output_by_index(1)
     );

  EXPECT_EQ
    (
     chain.get_output_by_index(1)->public_key
     , dummy_output
     );

  // EXPECT_TRUE
  //   (
  //    chain.is_output_key_image_spent(dummy_key_image)
  //    );


  const auto maybe_popped = chain.pop_block();

  EXPECT_TRUE(maybe_popped);

  const auto x = *maybe_popped;

  EXPECT_EQ
    (
     x
     , b
     );

  EXPECT_FALSE
    (
     chain.get_block_by_index(1)
     );

  EXPECT_FALSE
    (
     chain.get_block_by_hash(b.hash())
     );

  EXPECT_FALSE
    (
     chain.get_coinbase_tx_by_hash(b.miner_tx.hash())
     );

  EXPECT_FALSE
    (
     chain.get_ringct_by_hash(tx.hash())
     );


  EXPECT_TRUE
    (
     chain.get_output_by_index(0)
     );

  EXPECT_FALSE
    (
     chain.get_output_by_index(1)
     );

  // EXPECT_FALSE
  //   (
  //    chain.is_output_key_image_spent(dummy_key_image)
  //    );


  // const json j = chain;
  // LOG_DEBUG("Chain: \n");
  // LOG_DEBUG(j.dump(4));
}

TEST_F(mem_db, init_tree)
{
  blocktree_t tree;

  EXPECT_TRUE(tree.get_size() == 1);

  const block_t genesis =
    tree.get_block_by_index(0).value_or(block_t{});

  EXPECT_EQ
    (
     lol_genesis_block()
     , genesis
     );
}


TEST_F(mem_db, chain_sync)
{
  auto chain = make_chain();

  const auto [empty_hashes, common_idx] = chain.get_hashes_by_short_chain_history({});

  EXPECT_TRUE
    (
     empty_hashes.empty()
     );

  const auto genesis_hash = lol_genesis_block().hash();

  const auto [hashes, common_idx_2] = chain.get_hashes_by_short_chain_history
    ({genesis_hash});

  EXPECT_EQ
    (
     hashes.size()
     , 1
     );

  EXPECT_EQ
    (
     hashes.front()
     , genesis_hash
     );
}
