/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include <gtest/gtest.h>

#include "tools/epee/include/logging.hpp"

#include "math/blockchain/functional/difficulty.hpp"

using namespace cryptonote;

TEST(blockchain_difficulty, simple)
{

  // block 146 147
  const std::vector<uint64_t> timestamps =
    {
      1604824862ull
      , 939580649ull
    };

  const std::vector<diff_t> diffs =
    {
      40133435783157ull
      , 40134375363806ull
    };

  const auto next_diff =
    next_difficulty_pure(timestamps, diffs, 148);

  EXPECT_EQ(65044747, next_diff);
}


TEST(blockchain_difficulty, simple_3)
{

  // block 146 147 148
  const std::vector<uint64_t> timestamps =
    {
      1604824862ull
      , 939580649ull
      , 1604824916ull
      , 
    };

  const std::vector<diff_t> diffs =
    {
      40133435783157ull
      , 40134375363806ull
      , 40134983649770ull
    };

  const auto next_diff =
    next_difficulty_pure(timestamps, diffs, 149);

  EXPECT_EQ(107154821, next_diff);
}
