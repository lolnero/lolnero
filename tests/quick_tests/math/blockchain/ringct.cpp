/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include <gtest/gtest.h>

#include "math/crypto/controller/keyGen.hpp"
#include "math/crypto/controller/random.hpp"

#include "tools/epee/include/logging.hpp"
#include "cryptonote/basic/functional/format_utils.hpp"

#include "math/blockchain/functional/ringct.hpp"

#include "ringct.hpp"

#include <ranges>

using namespace cryptonote;

TEST(blockchain_ringct, lens)
{
  // std::cout << tx_blob << std::endl;
  const auto tx_blob =
    epee::hex::decode_from_hex_to_string(tx_blob_hex)
    .value_or(std::string{});

  const auto maybe_parsed = preview_ringct_from_blob(tx_blob);

  EXPECT_TRUE(maybe_parsed);

  const auto ringct = *maybe_parsed;

  EXPECT_EQ(tx_blob, review_ringct_to_blob(ringct));
}


TEST(blockchain_ringct, json)
{
  const auto tx_blob =
    epee::hex::decode_from_hex_to_string(tx_blob_hex)
    .value_or(std::string{});

  const ringct tx =
    preview_ringct_from_blob(tx_blob).value_or(ringct{});

  const json j = tx;
  const ringct tx_2 = j;
  const json j_2 = tx_2;

  for (const auto& x: tx.inputs) {
    std::cout << x.key_image.to_str() << std::endl;
  }

  EXPECT_EQ(tx.inputs, tx_2.inputs);

  EXPECT_EQ
    (
     tx.bulletproof
     , tx_2.bulletproof
     );

  EXPECT_EQ
    (
     tx.outputs
     , tx_2.outputs
     );


  EXPECT_EQ(tx.hash(), tx_2.hash());
}
