/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include <gtest/gtest.h>

#include "math/crypto/controller/keyGen.hpp"
#include "math/crypto/controller/random.hpp"

#include "tools/epee/include/logging.hpp"
#include "tools/epee/functional/base64.hpp"

#include "cryptonote/basic/functional/format_utils.hpp"

#include "math/blockchain/functional/block.hpp"

#include "math/consensus/consensus.hpp"

#include <ranges>

using namespace cryptonote;

TEST(blockchain_block, genesis)
{

  const crypto::hash genesis_hash = 
    crypto::preview_hash_from_base64
    (
     "CCJ/8DPFxx+C/yHs4P+odveqXw0Sjnh0WNJgUhUqmC8="
     ).value_or(crypto::hash{});

  const auto genesis = lol_genesis_block();

  EXPECT_EQ(genesis.hash(), genesis_hash);
}
