/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include <gtest/gtest.h>

#include "math/hash/functional/hash.hpp"
#include "math/group/functional/group.hpp"

#include "math/hash/opencl/sha3.hpp"
#include "math/hash/opencl/sha3_native.hpp"

using namespace crypto;

TEST(quick_sha3, empty_crypto_data) {

  const auto empty_data = crypto_data{};
  const auto sha3_data = sha3(empty_data.data);
  const auto expected_sha3_data =
    preview_hash_from_hex 
    ("9e6291970cb44dd94008c79bcaf9d86f18b4b49ba5b2a04781db7199ed3b9e4e");

  EXPECT_EQ(expected_sha3_data, sha3_data);
}

TEST(quick_sha3_opencl, empty_crypto_data) {

  using namespace opencl;

  const auto maybeGpu = getGPU();

  if (!maybeGpu) {
    std::cerr << "No OpenCL capable GPUs" << std::endl;
    return;
  }

  const auto device = *maybeGpu;
  const cl::Context context(device);

  const auto maybeProgram = getSha3Program(device, context);

  if (!maybeProgram) {
    std::cerr << "Failed to build sha3 OpenCL kernel" << std::endl;
    return;
  }

  const auto [program, queue] = *maybeProgram;

  const auto empty_data = crypto_data{};

  // const auto sha3_data = sha3(empty_data.data);
  const auto sha3_data = opencl_sha3_single
    (
     empty_data.data
     , context
     , program
     , queue
     );

	std::cout << "finished!" << std::endl;

  const auto expected_sha3_data = sha3(empty_data.data);

  EXPECT_EQ(expected_sha3_data, sha3_data);

}


TEST(quick_sha3_opencl_with_context, empty_crypto_data) {

  using namespace opencl;

  const auto maybeGpu = getGPU();

  if (!maybeGpu) {
    std::cerr << "No OpenCL capable GPUs" << std::endl;
    return;
  }

  const auto device = *maybeGpu;
  const cl::Context context(device);

  const auto maybeProgram = getSha3Program(device, context);

  if (!maybeProgram) {
    std::cerr << "Failed to build sha3 OpenCL kernel" << std::endl;
    return;
  }

  const auto [program, queue] = *maybeProgram;

  const auto empty_data = crypto_data{};

  using namespace opencl;


  using blobBS = std::basic_string<uint8_t>;
  blobBS data1 = {empty_data.data.begin(), empty_data.data.end()};
  blobBS data2 = data1;

  sha3_ctx_t sha3_ctx;
  sha3_init(&sha3_ctx);
  sha3_update(&sha3_ctx, data1.data(), data1.size());

  const auto sha3_data = opencl_sha3_single_with_context
    (
     sha3_ctx
     , empty_data.data
     , context
     , program
     , queue
     );

	std::cout << "finished!" << std::endl;

  const auto data_full = data1 + data2;

  const auto expected_sha3_data = sha3(data_full);

  EXPECT_EQ(expected_sha3_data, sha3_data);

}
