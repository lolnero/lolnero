/*

Copyright (c) 2020-2021, The Lolnero Project

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include <gtest/gtest.h>

#include "math/crypto/functional/key.hpp"
#include "math/crypto/controller/keyGen.hpp"
#include "math/crypto/controller/random.hpp"

#include "math/ringct/functional/rctTypes.hpp"
#include "math/ringct/functional/rctOps.hpp"
#include "math/ringct/functional/clsag_verify.hpp"
#include "math/ringct/functional/clsag_gen.hpp"

#include "math/json/all.hpp"

#include "config/lol.hpp"

using namespace rct;
using namespace crypto;


struct clsagInput
{
  crypto::hash message;
  crypto::ec_scalar signer_sk;
  crypto::ec_scalar signer_blinding_factor;
  size_t index_in_decoys;
  crypto::ec_scalar pseudo_input_blinding_factor;
  crypto::ec_point pseudo_input_commit;
  output_public_dataV decoys;
  crypto::ec_scalar random_a;
  scalarV random_s_V;
}; 

JSON_LAYOUT
(
 clsagInput
 , message
 , signer_sk
 , signer_blinding_factor
 , index_in_decoys
 , pseudo_input_blinding_factor
 , pseudo_input_commit
 , decoys
 , random_a
 , random_s_V
 )

clsagInput randomClsagInput() {
  const crypto::hash message = d2h(randomCryptoData());
  const crypto::ec_scalar signer_sk = randomScalar();
  const crypto::ec_scalar signer_blinding_factor = randomScalar();
  const size_t index_in_decoys = rand_idx(config::lol::ring_size);
  const crypto::ec_scalar pseudo_input_blinding_factor =
    randomScalar();

  const amount_t some_amount = randomAmount();
  const crypto::ec_point pseudo_input_commit =
    commit(some_amount, pseudo_input_blinding_factor);

  const crypto::ec_point real_input_commit =
    commit(some_amount, signer_blinding_factor);

  output_public_dataV decoys;
  std::generate_n
    (
     std::back_inserter(decoys)
     , config::lol::ring_size
     , [] -> output_public_data {
       return {
         randomPoint()
         , randomPoint()
       };
     }
     );

  decoys[index_in_decoys] =
    {to_pk(s2sk(signer_sk)), real_input_commit};

  const crypto::ec_scalar random_a = randomScalar();
  const scalarV random_s_V = randomScalars(decoys.size());

  return {
    message
    , signer_sk
    , signer_blinding_factor
    , index_in_decoys
    , pseudo_input_blinding_factor
    , pseudo_input_commit
    , decoys
    , random_a
    , random_s_V
  };
}

TEST(quick_clsag, random_input)
{
  const auto i = randomClsagInput();
  const auto sig = generate_clsag_signature
    (
     i.message
     , i.signer_sk
     , i.signer_blinding_factor
     , i.index_in_decoys
     , i.pseudo_input_blinding_factor
     , i.pseudo_input_commit
     , i.decoys
     , i.random_a
     , i.random_s_V
     );

  EXPECT_TRUE
    (
     verify_clsag_signature
     (i.message, sig, i.decoys, i.pseudo_input_commit)
     );
}


TEST(quick_clsag, wrong_message)
{
  const auto i = randomClsagInput();
  const auto sig = generate_clsag_signature
    (
     i.message
     , i.signer_sk
     , i.signer_blinding_factor
     , i.index_in_decoys
     , i.pseudo_input_blinding_factor
     , i.pseudo_input_commit
     , i.decoys
     , i.random_a
     , i.random_s_V
     );

  EXPECT_TRUE
    (
     verify_clsag_signature
     (i.message, sig, i.decoys, i.pseudo_input_commit)
     );

  const crypto::hash message = d2h(randomCryptoData());
  EXPECT_FALSE
    (
     verify_clsag_signature
     (message, sig, i.decoys, i.pseudo_input_commit)
     );
}


TEST(quick_clsag, wrong_decoys_1)
{
  const auto i = randomClsagInput();
  const auto sig = generate_clsag_signature
    (
     i.message
     , i.signer_sk
     , i.signer_blinding_factor
     , i.index_in_decoys
     , i.pseudo_input_blinding_factor
     , i.pseudo_input_commit
     , i.decoys
     , i.random_a
     , i.random_s_V
     );

  EXPECT_TRUE
    (
     verify_clsag_signature
     (i.message, sig, i.decoys, i.pseudo_input_commit)
     );

  auto decoys = i.decoys;
  const size_t index_in_decoys = rand_idx(config::lol::ring_size);
  const auto decoy = decoys[index_in_decoys];

  const output_public_data wrong_decoy_1 =
    {randomPoint(), decoy.commit};

  decoys[index_in_decoys] = wrong_decoy_1;

  EXPECT_FALSE
    (
     verify_clsag_signature
     (i.message, sig, decoys, i.pseudo_input_commit)
     );
}

TEST(quick_clsag, wrong_decoys_2)
{
  const auto i = randomClsagInput();
  const auto sig = generate_clsag_signature
    (
     i.message
     , i.signer_sk
     , i.signer_blinding_factor
     , i.index_in_decoys
     , i.pseudo_input_blinding_factor
     , i.pseudo_input_commit
     , i.decoys
     , i.random_a
     , i.random_s_V
     );

  EXPECT_TRUE
    (
     verify_clsag_signature
     (i.message, sig, i.decoys, i.pseudo_input_commit)
     );

  auto decoys = i.decoys;
  const size_t index_in_decoys = rand_idx(config::lol::ring_size);
  const auto decoy = decoys[index_in_decoys];

  const output_public_data wrong_decoy_2 =
    {decoy.public_key, randomPoint()};

  decoys[index_in_decoys] = wrong_decoy_2;

  EXPECT_FALSE
    (
     verify_clsag_signature
     (i.message, sig, decoys, i.pseudo_input_commit)
     );
}


TEST(quick_clsag, wrong_pseudo_input_commit)
{
  const auto i = randomClsagInput();
  const auto sig = generate_clsag_signature
    (
     i.message
     , i.signer_sk
     , i.signer_blinding_factor
     , i.index_in_decoys
     , i.pseudo_input_blinding_factor
     , i.pseudo_input_commit
     , i.decoys
     , i.random_a
     , i.random_s_V
     );

  EXPECT_TRUE
    (
     verify_clsag_signature
     (i.message, sig, i.decoys, i.pseudo_input_commit)
     );

  const auto pseudo_input_commit = randomPoint();

  EXPECT_FALSE
    (
     verify_clsag_signature
     (i.message, sig, i.decoys, pseudo_input_commit)
     );
}


TEST(quick_clsag, wrong_sig_c1)
{
  const auto i = randomClsagInput();
  const auto sig = generate_clsag_signature
    (
     i.message
     , i.signer_sk
     , i.signer_blinding_factor
     , i.index_in_decoys
     , i.pseudo_input_blinding_factor
     , i.pseudo_input_commit
     , i.decoys
     , i.random_a
     , i.random_s_V
     );

  EXPECT_TRUE
    (
     verify_clsag_signature
     (i.message, sig, i.decoys, i.pseudo_input_commit)
     );

  clsag sig1 = sig;

  sig1.c1 = randomScalar();

  EXPECT_FALSE
    (
     verify_clsag_signature
     (i.message, sig1, i.decoys, i.pseudo_input_commit)
     );
}


TEST(quick_clsag, wrong_sig_signer_key_image)
{
  const auto i = randomClsagInput();
  const auto sig = generate_clsag_signature
    (
     i.message
     , i.signer_sk
     , i.signer_blinding_factor
     , i.index_in_decoys
     , i.pseudo_input_blinding_factor
     , i.pseudo_input_commit
     , i.decoys
     , i.random_a
     , i.random_s_V
     );

  EXPECT_TRUE
    (
     verify_clsag_signature
     (i.message, sig, i.decoys, i.pseudo_input_commit)
     );

  clsag sig1 = sig;

  sig1.signer_key_image = randomPoint();

  EXPECT_FALSE
    (
     verify_clsag_signature
     (i.message, sig1, i.decoys, i.pseudo_input_commit)
     );
}

TEST(quick_clsag, wrong_sig_blinding_factor_surplus_ki)
{
  const auto i = randomClsagInput();
  const auto sig = generate_clsag_signature
    (
     i.message
     , i.signer_sk
     , i.signer_blinding_factor
     , i.index_in_decoys
     , i.pseudo_input_blinding_factor
     , i.pseudo_input_commit
     , i.decoys
     , i.random_a
     , i.random_s_V
     );

  EXPECT_TRUE
    (
     verify_clsag_signature
     (i.message, sig, i.decoys, i.pseudo_input_commit)
     );

  clsag sig1 = sig;

  sig1.blinding_factor_surplus_ki = randomPoint();

  EXPECT_FALSE
    (
     verify_clsag_signature
     (i.message, sig1, i.decoys, i.pseudo_input_commit)
     );
}


TEST(quick_clsag, wrong_sig_s)
{
  const auto i = randomClsagInput();
  const auto sig = generate_clsag_signature
    (
     i.message
     , i.signer_sk
     , i.signer_blinding_factor
     , i.index_in_decoys
     , i.pseudo_input_blinding_factor
     , i.pseudo_input_commit
     , i.decoys
     , i.random_a
     , i.random_s_V
     );

  EXPECT_TRUE
    (
     verify_clsag_signature
     (i.message, sig, i.decoys, i.pseudo_input_commit)
     );

  clsag sig1 = sig;

  const auto index_to_change = rand_idx(sig1.s.size());
  sig1.s[index_to_change] = randomScalar();

  EXPECT_FALSE
    (
     verify_clsag_signature
     (i.message, sig1, i.decoys, i.pseudo_input_commit)
     );
}

constexpr auto pp = preview_point_from_base64;
constexpr auto ps = preview_scalar_from_base64;

clsagInput get_sample_clsag_input() {
  const output_public_dataV decoys =
    {
      {
        *pp("lZ56K72kY1wlPiT78JyJgHqZkGlLH4ZLqondlLGf2HY=")
        , *pp("zrCq7lPuMakpNIClgziCkecjY9z3y1vd06A3GgxlvMk=")
      }
      , {
        *pp("/CHzquNJNXQRLcOyy0UDcnFYleYyXml0VeLBEoENV/o=")
        , *pp("T2P9pK7rYsO13HuknLs1hVf9AHWFEH/R7G9rdk7Qnxg=")
      }
      , {
        *pp("GvCwhGxpwOLdcivzAKEsZo5JgC0lqjqPPksbN4u3Ilc=")
        , *pp("1ruDQK3VFN3JnFr817DaMkZDQZeG9VUlAaclK1PTsjo=")
      }
      , {
        *pp("KdpsDpfUtKLdUzrN9v/jzn1zc55ltKwNcxe6MreM5xc=")
        , *pp("Q1xlwa22l7EwAfMzjtBHGg0s5LItm4i3Z26DdyJLBdA=")
      }
      , {
        *pp("ySExfpMmyWrQRhqtH8ndoRA2QraWlPMA/sx+3vtahLA=")
        , *pp("U7AYw8lGxl3kJL6d1K9aXsHlfV/zkeK7zjd3nV2D5sE=")
      }
      , {
        *pp("3GtVQO8cwam4Ev77ckXn99yBh/VhmjhOKjWtnshmix4=")
        , *pp("1FaIwvyN3mmODKQmOfjBFT6FWJIUK9FuhUOXXkozf+w=")
      }
      , {
        *pp("0H4t8hBnFv0J1TuX69KS63vYRvS+AO7P4uckZlbn1ME=")
        , *pp("srEgnVAe360Q4STuMgTvPHSk0753h1HtViKXkl1BFR8=")
      }
      , {
        *pp("x6HyvHjafUHdYfpO10QbdopKEJUfwmoCEkIV5ZIqqZE=")
        , *pp("j+z2d9Gzv8xf+0pbyL5ajegmCOQN0nxfal2jynwlyCE=")
      }
      , {
        *pp("dbgK+pTDvwP6X/12TNSZKTl98AF4bt3ePsSl5evC0lM=")
        , *pp("lf5RRXuvtA03zbAwtJuolljuN/PabpgbPe6UUQX0yuU=")
      }
      , {
        *pp("BeNZQc8uyduFTbqPW5eKRITH1uN/GctrnLnyno2AAXc=")
        , *pp("MOPe+D2kw0/U5uUl2qYRyAuh5cmwgk0NAfT/KeBrYxA=")
      }
      , {
        *pp("paIve7m07JYf8k4pv5oBj2dyZTu8qaIZNloJEHJ4X1s=")
        , *pp("5dxhGmTcmv0jsa7QVUpEvi19J9fIhvEVomdQAX+yNV4=")
      }
      , {
        *pp("2dGLg2CQ6POe5TCvMlgoLMfDAAQ3KhpUaRbdOMhIkog=")
        , *pp("I8MCz/No/tWTb49TwPaAcizy3Kl0+D0Ii9Rgj7JvJ+4=")
      }
      , {
        *pp("4pId7RFR/w3JoET2llRDOwWmiPwKhNs6UwE+sQOxBD0=")
        , *pp("//2vLoztO+RHRnGtcqgbwRHZamtC2c4TDGAhLiDL79k=")
      }
      , {
        *pp("2pJmxGndYSlGsLKAnohRKG0PNyajNUd9pYWbSEklM6U=")
        , *pp("Z59t36qK2PYXuuK6m86hhPNWNsW5ZgjQnem7tRXd+0A=")
      }
      , {
        *pp("ARcNpUs6IsDXABuoWlEDML8SooLT7C1euouSkh0JxIk=")
        , *pp("WDvINHJs6+OA05jNlk0QAL2Dr0/ooEhTyyrG/SXodRg=")
      }
      , {
        *pp("wJejjDzjUXDgfVkBxd/Plpdkvz+cXKeoTMzmUFDOIVg=")
        , *pp("ZpfnruZ1lFliUiYCXL7vN4eplDFgKNvkBQJeyZxhE3w=")
      }
      , {
        *pp("wzH1Fh4M3ltiUQ9PB68YNKrX8fFaWEUBRT9JYQbln8U=")
        , *pp("UFD4oh+Y0FSvgtCYcnfiwVYCOdd2RqTWI+/Xidj/t80=")
      }
      , {
        *pp("RDWy7T1DcUBgKu4y41JKi+8H3blJ1Yz6WUaLqdPa7yw=")
        , *pp("cRH3x6PgsaCd0BVq+SdB6CbWNSKpDM5Q5uOuzHcGstI=")
      }
      , {
        *pp("Sa3ReDEK5kG3Ytrn1srppWAr361ssfHHQDUlwOV0Adc=")
        , *pp("JDX6hgDITylmerpzSBswdnqTrN1zr31BhsecAJeLaXc=")
      }
      , {
        *pp("Ni8fV0d0Rvabt9O7HilKcC0hjaYATBLwcy2vKfor8G8=")
        , *pp("6dCJlVonqytgGzHBCcgEdOki1fCODiqyHlGsJEIrQm8=")
      }
      , {
        *pp("9/+DYftAsTO4zckgMa97+moTKaMDuv34TdO5LmEKW8k=")
        , *pp("jjwgmSapJdnC/4lnkluAb3tubottNNNz9nXU4RC/ctc=")
      }
      , {
        *pp("i0YhUOYy0+uoRj2G5a/fQpRCSQNoAVYaFoISDgab1eo=")
        , *pp("1zsliiZ+ecLVUMB/jgeg+3XOW9pvv+5kcMGMMHjqAXU=")
      }
      , {
        *pp("bIixdO29HkYgpqPdHKptaAaeYDOsabtfAWj4eS6hurY=")
        , *pp("8/054JcZFWHct90FbN4TUPbLvWcMO8gSIrq7mDHqwmM=")
      }
      , {
        *pp("p8e2xG/5+f+QcXhEXaXzjELxzuDJsPdjzs9Vt7hS4T8=")
        , *pp("7U0JdiQw4Ge5XfG+vB9gD7AOR02xrKljfPgi40LAiks=")
      }
      , {
        *pp("tZbuESRZJxN13t8gIypejaVghMHH6ELtukEQ44DIbUM=")
        , *pp("pn2XJfqN7yLTl+ibERIpiYOKn4gFfntrEfYCJ7nj8J8=")
      }
      , {
        *pp("dIJLrjCtZ7PBYIhbB+6tv3QBN+D6ph/uJICIyvhdOWA=")
        , *pp("DuDBqOw3tZa5HXW5O+SC5clZDrx3gWspnizhcayad3s=")
      }
      , {
        *pp("LwIkfYYgeA0Y2hsZ7rjURfQBRG4p30dMeIu/waJk/E4=")
        , *pp("NuMB+J76ovnFyTjOAbqqtrM5QOKumO1Bv2AUFQ78yFg=")
      }
      , {
        *pp("ZLcf8V9/9hLDe+mDQNutz/n53RfaU5b+UDYuOnW9GtQ=")
        , *pp("TED1HbqCcgAMdDKUJ76YTRf8fgtItLGkyJI6iygO/+E=")
      }
      , {
        *pp("iW0baAxEvP5M/T0yIdphI6qjzTutVnaXhXC6TGngpDQ=")
        , *pp("suM1s1QlVvMHMgnA+geQknuTF9fsLoEwV1hJP/X9BYo=")
      }
      , {
        *pp("lZXIqF7Yt8JSBMvx4lCkLxIpOoP9mJmpCuz/+TQh1Kk=")
        , *pp("SregOav3Pd+yp2WgkZKFwJD6IfFV4F9pa4jw29jdP5c=")
      }
      , {
        *pp("4dS8ZeZCoTozybootXaoHoiBhoQpJ2FOdCSrtmwKaeA=")
        , *pp("oXda6imWs3lMb+tf9VKp0gzjghpyY+4M+DrNdRa514o=")
      }
      , {
        *pp("/T5qV1OSlPi+X5E2K1kkKTNZL/m1mihZj1gHXXRdhbA=")
        , *pp("7t01Qr7pSiDWnCpmUi965bjZDp6qE1+MK9WoR6eSdPs=")
      }
    };

  const scalarV random_s_V =
    {
      *ps("4FW/YgHtB0eM+4NHYciU0xBYu7wbkvB8zL/sH7af7QA=")
      , *ps("h3QdMyGx8uPGgcYJL/ygBSLVPgwK31JL+WfbuRQN/Ao=")
      , *ps("vkIDfII9m+UScLJeOCeQSiQZ4p93LFfpMhBWkGwkSws=")
      , *ps("1QOX2HtQR46fIWxsEhyEotE6ieBEJEnXxDt/YPwVAAY=")
      , *ps("oeBpGgogF+J/hFBBq9TKdwylYM8JlOJvI+BDCBSghQQ=")
      , *ps("yp/mfnXv096JxywAciFDpNHdn3Ad0d1CsnJPFR9lwQw=")
      , *ps("51sX6Q2UNykxfH/n8MSis84exVzpWG3ZbxYraJ26cg8=")
      , *ps("LE2zcdM3xEfulHoQkclP9ZjWvY0UW0Y36DMosasAVgM=")
      , *ps("DPyag4PL/xInobpMIgrhQlLUd71oi374xN95LcCxtgY=")
      , *ps("RnOEdoEB6ZsIiYpOU3+tKdXlztkvpJKVWXBY3hDj+gY=")
      , *ps("6H372i2iAgoe9NiepIZeeS4H8ZcJpSchXlnE/ypQoA4=")
      , *ps("uIz3faTdem44mrGbMLkMHPXfizQiNN3NBBimTRITTA0=")
      , *ps("NQ5jbqa7vdllvPpPXXLANfc10dR0M1XLzGkRqrTVyww=")
      , *ps("q2QLqSCQx/lNkbve86/ZKBTyVfzu+xaRGi8i+jQGNgE=")
      , *ps("F+PMuSQB26GWAMMP4VCrIfQbTMHTjVdamXhvK7rfSQs=")
      , *ps("doWtVhm533hdAZ8afB6NKMgUoBHRiMwhuY8tpCu3BA0=")
      , *ps("mavCsJ9VZ2IEecKJIW/fep0rhgnPmZs5JH0eB0A/tAc=")
      , *ps("0iHHTlaP3/WgY1diyupRKIA7Wa/oIHlRcg2YVboZZwk=")
      , *ps("QD9Pa9XoCAGUFhy9fPFAH8QUC/TDAxU3NSu4lDGkIQs=")
      , *ps("WoyzUWNZKmsxEA7E+iJRfluhbGquurqJU2il+EyoHwQ=")
      , *ps("4sQQSD8r2mvKNXflyD+ZQDuc/L6h7x0sBADNXHcQYgo=")
      , *ps("MctoFId7ITR5GjKbklFmZJQ241qlTEMcPB3ZH0Zy6g8=")
      , *ps("rlxVAUisyNHuueQOLG/+mzhmESwLy4k41ivKulVOXQI=")
      , *ps("DOhYswTBB5800EOF07Ha58E3zPATcZ5t71+iDfC2JA0=")
      , *ps("fhUXcJbbgwdEgxYgq6v+emrSYXyvQJwxuA/C21FkTQM=")
      , *ps("fPz9tZI4O+mShh2WbqzvH4i8039FgBTtNqIJc1N7gw0=")
      , *ps("KSL0VPH6FfmS0/tO9wGrSHMHQwNFYgJ6M3bbQ+2TYgc=")
      , *ps("EnJZKWY5f81ltQSPWeUGv6NWh5C0bF/MryF27CML4Qw=")
      , *ps("PcBZxeRekwy4KFhE9oFBNj82QDV0I22ds6cHX18gVQE=")
      , *ps("T+n05OXSsHPqkK/My4wilGPUKwWBSVFoH8tn1k/qZwQ=")
      , *ps("ArZQPGdwcp5oe9KoRaojZiLvEQT0LqBm4vsnHmiayAs=")
      , *ps("yuC/XOEh1645gGBIm2IkTdqFAK1t1mCIVfxBzr80mQk=")
    };


  const auto index_in_decoys = 20uz;
  const auto message = *preview_hash_from_base64
    ("3LiNGeIF+JlilE2Ib/UFHYBv1lDloNbjcNpuieQkDgc=");

  const auto pseudo_input_blinding_factor =
    *ps("aAbdAPDuZCYKY1eT60ywI6ixd+uLoHhh0SO7NF2ZxAw=");

  const auto pseudo_input_commit =
    *pp("MN9FCZbVlR7XbTFsPk/M9GpBIFxxxUMZ6ejxpm1StgQ=");

  const auto random_a =
    *ps("aqCBYNvdfBBoRTZohL68X6tgJQ+mInQ95nG0S+ed3gA=");

  const auto signer_blinding_factor =
    *ps("sEjMMgBXTFnMkRpAHPvMOl10iBNMxnWvWSXbgjbmEwQ=");

  const auto signer_sk =
    *ps("wuME3IT5YvrzdXkITvRHixnK7Zzxirbh9+QHHeuIygE=");

  return 
    {
      message
      , signer_sk
      , signer_blinding_factor
      , index_in_decoys
      , pseudo_input_blinding_factor
      , pseudo_input_commit
      , decoys
      , random_a
      , random_s_V
    };
}


TEST(quick_clsag, sample_0)
{
  auto i = get_sample_clsag_input();
  const auto sig = generate_clsag_signature
    (
     i.message
     , i.signer_sk
     , i.signer_blinding_factor
     , i.index_in_decoys
     , i.pseudo_input_blinding_factor
     , i.pseudo_input_commit
     , i.decoys
     , i.random_a
     , i.random_s_V
     );

  EXPECT_TRUE
    (
     verify_clsag_signature
     (i.message, sig, i.decoys, i.pseudo_input_commit)
     );
}

TEST(quick_clsag, get_hash_data_from_decoys_with_key)
{
  const auto hash_data_bs =
    get_hash_data_from_decoys_with_key
    (config::HASH_KEY_CLSAG_AGG_0, {});

  const auto hash_data_span = std::span(hash_data_bs);

  const epee::blob::span hash_data =
    {
      (const uint8_t*)hash_data_span.data()
      , hash_data_span.size_bytes()
    };

  EXPECT_EQ
    (
     "434c5341475f6167675f30000000000000000000000000000000000000000000"
     , epee::hex::encode_to_hex(hash_data)
     );
}
