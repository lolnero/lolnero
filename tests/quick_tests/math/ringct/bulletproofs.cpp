/*

Copyright (c) 2020-2021, The Lolnero Project

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include <gtest/gtest.h>

#include "math/crypto/controller/keyGen.hpp"
#include "math/crypto/controller/random.hpp"

#include "math/ringct/functional/rctTypes.hpp"
#include "math/ringct/functional/rctOps.hpp"
#include "math/ringct/functional/bulletproofs_verify.hpp"
#include "math/ringct/controller/bulletproofs_gen.hpp"

#include <tuple>

using namespace rct;
using namespace crypto;

bp_input_t random_bp_input() {
  return { randomAmount(), randomScalar() };
}

std::pair<pointV, std::vector<bp_input_t>>
random_bp_inputs_for_size(const size_t i) {
  if (i < 1) return {};

  std::vector<bp_input_t> xs;
  std::generate_n
    (
     std::back_inserter(xs)
     , i
     , random_bp_input
     );

  pointV commits;
  std::transform
    (
     xs.begin()
     , xs.end()
     , std::back_inserter(commits)
     , [](const auto& x) { return std::apply(commit, x); }
     );

  return {commits, xs};
}

  
TEST(quick_bulletproofs, pick_amount_size_1_to_16)
{
  const size_t i = rand_range(1, 16);
  const auto x = random_bp_inputs_for_size(i);
  const auto proof = bulletproof_MAKE(x.second);
  EXPECT_TRUE(bulletproof_VERIFY(x.first, proof));
}

// struct Bulletproof
// {
//   rct::pointV commits;
//   crypto::ec_point A, S;
//   crypto::ec_point T1, T2;
//   crypto::ec_scalar tau, mu;
//   LR_V LR;
//   crypto::ec_scalar a, b, t;
// };

std::pair<pointV, Bulletproof> randomProof() {
  const size_t i = rand_range(1, 16);
  const auto x = random_bp_inputs_for_size(i);
  const auto proof = bulletproof_MAKE(x.second);
  return {x.first, proof};
}

TEST(quick_bulletproofs, wrong_A)
{
  const auto input = randomProof();
  const auto proof = input.second;
  EXPECT_TRUE(bulletproof_VERIFY(input.first, proof));

  auto altered_proof = proof;
  altered_proof.A = randomPoint();
  EXPECT_FALSE(bulletproof_VERIFY(input.first,altered_proof));
}


TEST(quick_bulletproofs, wrong_S)
{
  const auto input = randomProof();
  const auto proof = input.second;
  EXPECT_TRUE(bulletproof_VERIFY(input.first, proof));

  auto altered_proof = proof;
  altered_proof.S = randomPoint();
  EXPECT_FALSE(bulletproof_VERIFY(input.first, altered_proof));
}


TEST(quick_bulletproofs, wrong_T1)
{
  const auto input = randomProof();
  const auto proof = input.second;
  EXPECT_TRUE(bulletproof_VERIFY(input.first, proof));

  auto altered_proof = proof;
  altered_proof.T1 = randomPoint();
  EXPECT_FALSE(bulletproof_VERIFY(input.first, altered_proof));
}

TEST(quick_bulletproofs, wrong_T2)
{
  const auto input = randomProof();
  const auto proof = input.second;
  EXPECT_TRUE(bulletproof_VERIFY(input.first, proof));

  auto altered_proof = proof;
  altered_proof.T2 = randomPoint();
  EXPECT_FALSE(bulletproof_VERIFY(input.first, altered_proof));
}

TEST(quick_bulletproofs, wrong_tau)
{
  const auto input = randomProof();
  const auto proof = input.second;
  EXPECT_TRUE(bulletproof_VERIFY(input.first, proof));

  auto altered_proof = proof;
  altered_proof.tau = randomScalar();
  EXPECT_FALSE(bulletproof_VERIFY(input.first, altered_proof));
}


TEST(quick_bulletproofs, wrong_mu)
{
  const auto input = randomProof();
  const auto proof = input.second;
  EXPECT_TRUE(bulletproof_VERIFY(input.first, proof));

  auto altered_proof = proof;
  altered_proof.mu = randomScalar();
  EXPECT_FALSE(bulletproof_VERIFY(input.first, altered_proof));
}

TEST(quick_bulletproofs, wrong_a)
{
  const auto input = randomProof();
  const auto proof = input.second;
  EXPECT_TRUE(bulletproof_VERIFY(input.first, proof));

  auto altered_proof = proof;
  altered_proof.a = randomScalar();
  EXPECT_FALSE(bulletproof_VERIFY(input.first, altered_proof));
}

TEST(quick_bulletproofs, wrong_b)
{
  const auto input = randomProof();
  const auto proof = input.second;
  EXPECT_TRUE(bulletproof_VERIFY(input.first, proof));

  auto altered_proof = proof;
  altered_proof.b = randomScalar();
  EXPECT_FALSE(bulletproof_VERIFY(input.first, altered_proof));
}

TEST(quick_bulletproofs, wrong_t)
{
  const auto input = randomProof();
  const auto proof = input.second;
  EXPECT_TRUE(bulletproof_VERIFY(input.first, proof));

  auto altered_proof = proof;
  altered_proof.t = randomScalar();
  EXPECT_FALSE(bulletproof_VERIFY(input.first, altered_proof));
}

TEST(quick_bulletproofs, wrong_LR_first)
{
  const auto input = randomProof();
  const auto proof = input.second;
  EXPECT_TRUE(bulletproof_VERIFY(input.first, proof));

  auto altered_proof = proof;

  const auto index_to_change = rand_idx(proof.LR.size());
  auto lr = proof.LR[index_to_change];
  lr.L = randomPoint();

  altered_proof.LR[index_to_change] = lr;
  EXPECT_FALSE(bulletproof_VERIFY(input.first, altered_proof));
}


TEST(quick_bulletproofs, wrong_LR_second)
{
  const auto input = randomProof();
  const auto proof = input.second;
  EXPECT_TRUE(bulletproof_VERIFY(input.first, proof));

  auto altered_proof = proof;

  const auto index_to_change = rand_idx(proof.LR.size());
  auto lr = proof.LR[index_to_change];
  lr.R = randomPoint();

  altered_proof.LR[index_to_change] = lr;
  EXPECT_FALSE(bulletproof_VERIFY(input.first, altered_proof));
}


TEST(quick_bulletproofs_generator_H, idx_0)
{
  const auto h_0 = get_bp_generator_H(0);
  ASSERT_EQ
    (
     h_0
     , preview_point_from_base64
     ("U7TC46zROI4VrfMrDvA89ixNTuaE1OD0T5t2zFqujnQ=")
     );
}


TEST(quick_bulletproofs_generator_G, idx_0)
{
  const auto g_0 = get_bp_generator_G(0);
  ASSERT_EQ
    (
     g_0
     , preview_point_from_base64
     ("zB0dJXcVqMxvu7i//YVAxQS/8DjdQrxC5EPGTHT7UPU=")
     );
}


constexpr auto pp = preview_point_from_base64;
constexpr auto ps = preview_scalar_from_base64;

TEST(quick_bulletproofs_verify, sample_0)
{
  const auto A = *pp("gKj7RE2z248hMb0LYoWA5vjooGlululBaBTZnx+DsMg=");
  const auto S = *pp("Thy0QnEJCsYSsOfacBYLGv5M95V1AQsn4nHZ51PqmL8=");
  const auto T1 = *pp("JmfnExRnkjFD3K4Ix7iHY1Jl0LgoVa6yDoCWqe9Zp40=");
  const auto T2 = *pp("JaG6vXkZJ9uXuRM/MhaUEg3pFhokMW4pn44AjFrket8=");
  const auto tau = *ps("ZJ+A5ByBx5aoVICtLETNx1qUZJJu+lDrqpuueOttUQI=");
  const auto mu = *ps("hSnVjW0J5Jamn4KSKWkdj5rR8am2d7IHAgOvBJQ0yAo=");

  const LR_V LR =
    {
      { *pp("1QGlhuqolNVSgh106qux/BbFviKWxfvTfRpHxWrn3sg=")
        , *pp("vJpYM4H5xgonhYwDjjRDRJC/UVnNbFVThLykf5OUDP8=")
      }
      , {
        *pp("MolgN9YdiOEVyeQajv5nbw2c/9rChM9XPEHH1i6kwUY=")
        , *pp("NG9fG4e0FNPdTvF43Y8+cTtg8C8XX8XxK8zK/S1G/S8=")
      }
      , {
        *pp("7iknqpIZclu/7F+DM4Zy3d/+sUlyBWXO6UtzZdgP4aQ=")
        , *pp("Np08/Xl9JuDsMs80EF8lZ6EHi2dc5fxvH47Hw7lc/rc=")
      }
      , {
        *pp("MXSyOQ6FZPeeF5t5dPOVOQtWF5eHDyDHNBbynFTmMRc=")
        , *pp("4ekMvs/AzPdj5aF091E+TO6EdONeiY7xOUtJa+hiZRw=")
      }
      , {
        *pp("yh22O3ADpgfTXYTvOrFCqF3j/oCJpYsl5Cxex1zbUnE=")
        , *pp("wTiysp/naDtjYl5gQCQXDZ5khSm+Y3tVVV8nwIbXzhM=")
      }
      , {
        *pp("zC440ZSjPCfdY/EIlItM2+UXkY8u6FRX2RvkF9Uu9eM=")
        , *pp("IGkRyH4P9vZnxfrauuFdLI4c0Xraq0hj6B58kA7Keak=")
      }
      , {
        *pp("0kqd1ZGoCj9o8oMbhgg3GxNrI1cfnAhtk8HuLp8tWn4=")
        , *pp("X+EnIcLofUkPcD6NrvLDF1/xAkjKne/M1qyY+qhJZ/g=")
      }
    };
  const auto a = *ps("ONw+iTtgF9d100g8qaUSnZrQR/IhJFIXlyfDEA0Xewk=");
  const auto b = *ps("tpTsgLcP6iorNLpZnEKs2zoYHSe36SMw8/Tp/bPBLQI=");
  const auto t = *ps("aoftNXMgxQbn4912bdGnMDVeFxYJrWiHqXM2Mld6jQ0=");

  const auto bp = Bulletproof
    {
      A, S
      , T1, T2
      , tau, mu
      , LR
      , a, b, t
    };

  const pointV commits =
    {
      *pp("z2/XwCqfquga59LIROCl132Q/dtEZDZNPNYhyLzNjeM=")
      , *pp("xgg8a963xgGeDXDVgiDn87mVFijwkJFfTQBLf/eyeeo=")
    };

  ASSERT_TRUE
    (
     bulletproof_VERIFY(commits, bp)
     );
}
