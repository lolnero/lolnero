1. Private
2. ASIC friendly
3. Linear emission

There is no premine and no dev tax.

Source code: https://gitlab.com/lolnero/lolnero

Lolnero was launched on Oct 10th, 2020.


Updates:

Aut 26th, 2022

The dev will be out of coverage until all the fast mined coins
(e.g. monero and aeon) are dead.

Good luck.

BTW, the dev is unsophisticated, uneducated, ignorant, and
doesn't even speak English. You've been warned.


Aug 24th, 2022

So when lolnero-hs?

Believe it or not, the development is going well.

But there's a precondition, that is we, or I, need to wait for every
cryptonote fork to die before releasing anything. Because, for now,
they are all fast mined or pre mined scams.

If scams don't die, all we are doing is giving them a better
implementation.

Yes, that means, we need to wait until monero is dead before releasing
anything.

You might think that it's impossible. Sure, but if shitty code base
with scammy emission can last, I shouldn't be programming, and lolnero
should be dead sooner rather than later.


Aug 23th, 2022

What can we learn from Grin.

1. Hardforks are bad.
2. Donation is not a bless.
3. Early succuess is a disaster.
4. Competition of ASIC market is important.
5. Rust is not that good.



Aug 19th, 2022

Some of the preliminary upcoming changes are documented in the RFC
issues:

<https://gitlab.com/lolnero/lolnero/-/issues>


Aug 18th, 2022

The C++ code base will be abandoned soon. Make sure you can fork this
project to lolnero classic if you want to stick with C++. It's now a
good time to get yourself familiar with NixOS since it will be the
only supported OS.


Aug 15th, 2022

Why is lolnero the only cryptonote fork using libsodium?

Well, let's walk through the analysis together. The fact that nobody
uses libsodium just means that libsodium sucks. It has to be, since
otherwise, everyone would be using it. Right?

Unless, of course, extremely unlikely, there's another
possibility. That the only reason nobody besides lolnero uses
libsodium is that every cryptonote fork sucks. That's just non sense,
I personally don't believe it. It's just impossible.

There, so by our careful analysis, the reason that lolnero is the only
cryptonote fork that uses libsodium is because libsodium sucks, not
that everyone else sucks.


Jul 31th, 2022

There's still some bugs in the reorg code that can falsely block some
supposedly valid nodes.

1. They probably won't be fixed like forever.
2. A manual restart of the service is recommended.
3. If there is a fix, it has to be a complete overhaul of the p2p
   code.
4. Current p2p code is a joke and is not workable. Just ask any
   cryptonote dev: what's their favorite part of the project.

Everyone can be a sys admin.


Jul 28th, 2022

How to keep your privacy without having to be a bitch?

Fork lolnero.


Jul 20th, 2022

What can we learn from cruzbit:

Pros:

1. Written in Go, tiny implementation that can be easily ported to
other languages.

2. SHA-3 as PoW, which enables future industrial mining.

3. Conservative on design and cryptography, kinda of natural for any
serious cryptographer.

Cons:

1. Open ledger. Bitcoin can get away with it since it's the first one,
but it's pretty clear now that open ledger won't cut it, and there's
no way you can compete with Bitcoin with an open ledger.

2. Adding privacy features to an open ledger probably won't work.

3. FPGA / ASIC need to be thought about and dealt with from the very
beginning.

4. Miners are really not your friends, being nice won't get things
   done.

Can lolnero learn from these mistakes?


Jul 17th, 2022

Why won't lolnero use bulletproofs+ or ++ or +++?

1. There's no need. The significance of bulletproofs is the shape,
   log(n), not size. Anyone who misses this is either a salesperson or
   clueless.

2. It doesn't come without a cost, a maintenance cost that nobody is
   willing to pay.

3. Almost no peer reviews, hello?

4. Programmers tend to significantly underestimate how difficult a
   proof like bulletproofs is. I'm not a programmer, I know what I'm
   dealing with.

Lolnero is a cryptocurrency, a tool, it has to be simple and
maintainable. Let them play with all the range proofs that they
probably will never understand.


Jul 17th, 2022

Why is "if it ain't broke, don't fix it" wrong?

Because it's not scientific.

Why brush your teeth? Why maintain your car? Why reject communism? Why
fix your shitty C/C++?

Common, grow up.


Jul 17th, 2022

Why is lolnero easy to fork?

Because, unlike other coins, I want a better fork. Seriously, who
wants to maintain this shit?


Jul 7th, 2022

What about the all new mem db? There are several things.

Cons.

1. Memory usage

This is not a real database, since everything is in ram, or heap. When
a reorg happens, the ram usage pretty much doubles since it's just a
simple copy of the vector of the main db. That sucks, yes. In practice
though, with enough swap, it's fine. Since most of the blockchain can
be swapped to disk and won't be touched again like forever in
computer's time.

https://en.wikipedia.org/wiki/Memory_paging

2. Scalability

It won't scale at least for low end PCs because they don't have enough
ram, but for now it's not a problem.


Pros.

1. New code in modern C++

We are on C++23, with ranges, parallel algorithms and monadic
optionals. If you don't know what that means. It's like guns vs
spears. No there's no nuclear, I assure you. You can trust me.

2. New design with a single mutex for the entire database

This is really a lazy design, but I'm kind of used to it.

3. Performance

This doesn't necessarily have to be true, since lmdb shouldn't be a
bottleneck. So the only explanation here is that we have better design
or better code. I don't know. But syncing is comparably speaking a lot
faster.

I explained earlier that the reason for a new db is not to fix the
old, but to add consensus changes which would be otherwise impossible.

So there, new db.



Jul 7th, 2022

Why I never optimize C++?

There are many reasons, but most importantly, I don't need it.

Before talking about optimizations, we need to establish what is the
goal of this project.

Well the goal of lolnero is to show the world that you don't need to
be bitch to be a privacy coin. You can just be a fucking coin.

Am I ever gonna optimize C++? Never.




Jul 6th, 2022

What happens when hackers are blinded by greed?

Well, just look around you. Or more easily, look at the mirror.



Jul 6th, 2022

Why do your output public keys need to be in the main prime subgroup?

If you understand group theory, you already knew. What if you don't?

Let me try to explain. There are 12 keys in common music theory. You
can play a song at pretty much any key, and it will sound right. But
you can't play it at two different keys at the same time, it will be a
complete mess, in most cases.

The same is true for any elliptic curve cryptography, you can't mix
points from different subgroup, and if you do, everything breaks.

Now why am I talking about an issue that doesn't exist in lolnero?



Jul 4th, 2022

What happens when you have a broken gamma picker? You don't talk about
it.

Fortunately for lolnero, our decoy selection has been completely
rewritten. The hardfork was moved earlier than expected since why
not. This hardfork removed transaction unlock time, which has some
interesting implications.

Previously, decoy selection is not mathematical, by that I mean you
can't just get a gamma sample and expect it to work, since the unlock
time might block some of your selected outputs from being available by
consensus.

Now, we don't have that restriction, which means every gamma sample is
a valid sample. This significantly increases the performance of sample
generation since we don't have to query the database for unlock time
anymore, and it opens the door for deterministic sample generation.

Try sending a transaction in lolnero, it's instant. Since decoy
selection is trivial.

By the way, our decoy selection follows the true gamma distribution,
and it can be done on either the client or the server, there's
absolutely no difference, because, again, every sample is a valid
sample. Not necessarily the case for some other coins.




Jul 2nd, 2022

Recently lolnero switched from lmdb to in memory db that persists to a
json file. It's definitely a downgrade, but it turned out better than
I expected, since it kind of works OK given enough swap :D

The reason for this downgrade is that this is the only way I can
implement some of the changes that involve the database.

For example, we disallowed duplicated output keys at the consensus
level, it's a softfork that requires additional check from the
database.

We support arbitrary rollback to make removing unlock time
works. Maybe the previous code can also work, but I don't know, and I
can never fix it if it doesn't.

We also simplified the p2p layer that requires precise understand of how
the data layer works.

So it is a necessary downgrade to make some positive changes possible,
at least for now.


Oct 21th, 2021

Lolnero moved to GPU mining, since there's a built-in OpenCL miner
now. What's interesting is the speed this miner got developed. It
literally only took 4 days. Thanks to
[cruzbit](https://github.com/cruzbit/cruzbit) which provided a solid
opencl code base to work on.


Sep 11th, 2021

Tx extra has been reworked to only include tx output public keys. It's
probably how sub-address was intended to be used. More refactoring has
been done that made further work on wallet code possible. This is
actually huge.


Sep 6th, 2021

We changed our payment proof, which is what one uses to settle a
dispute out of band, from 64 bytes to 128 bytes. Monero/wownero uses a
single schnorr signature for two public keys, we use two.

Our implementation is just dumb but there's no room for mistakes, two
signatures, two keys, easy.


Sep 1st, 2021

Twitter blocked this account, and I'm [i]not[/i] interested in getting
it back. We are too good for it.


Aug 14th, 2021

A lot happened behind the scene. We dropped most of the internally
maintained curve25519 code and instead relies on libsodium for curve
operations. We unified the data types from ringct and crypto, and at
the same time split the key type into key (point) and scalar, the way
the original cryptonote developers structured their types.

We took advantage of the overloading capability of C++ to make the
point and scalar type aware of the + - * operators so we can use these
in the code to better match how we think about algorithms. We start to
replace raw for loops with std algorithms because they have the
potential in the future to be parallelized. Currently android NDK is
the blocker for adding parallelization. Hopefully google can put their
shit together and not suck on making NDK anymore.


Jul 26th, 2021

No hardfork is kind of boring but that's the way it is. We reduced the
code size down to about 72k lines of code, which gave us a build time
less than 2 minutes with clang on a 6 cores ryzen.

On the haskell side, I tried to share some development screenshot on
twitter, but some miner started to panic thinking I was able to add
blocks with a terminal. So I'll try not to share development on
twitter mindlessly. Anyway, we are able to talk the levin protocol
now, but only tested with handshake. The next step, only brainstorming
obviously, is to probably somehow build a state machine that can
capture the entire p2p protocol, so we can discard the http
server. Because we already have full parsing of all the blockchain
data, we should be able to reconstruct the entire database acting only
as a  regular node.

The next thing is probably to build something useful with it, like an
explorer or a backup service, or something.


May 4th, 2021

Nothing really happened, but since wownero mooned, it's probably
worthwhile to see where we can fit in this new landscape.

Out primary competitor wownero is a legitimate coin. It's pretty much
impossible to beat a legitimate coin once it mooned, assuming you all
agree? So where do we go from here. The maintenance cost of lolnero is
pretty low, so the chain will likely stay with pretty much no
maintenance needed. The parallel implementation in Haskell is still on
the roadmap, but I need to find some time to do it as a hobby, cuz
there's no funding, it has to be a hobby ;) I'm still pretty
interested nevertheless.

So yes, lolnero will just be like before, happy CPU mining.


April 18th, 2021

So we had an integer overflow bug in our difficulty adjustment
algorithm (DAA). There are good news and bad news, which do yo want to
hear?

Since we didn't die, let's start with the bad news. Out of the first
278 blocks, the first half have hard coded constant difficulty so
these are fine, the rest all have overflowed difficulty with no
exception  :-X

OK, the good news. Because our relatively low hashrate, after the
first 278 blocks, the difficulty never overflowed, and since we fixed
the algorithm it shouldn't overflow ever again. We were a little bit
luckier than wownero, since its difficulty was at the edge of
overflowing, which made hot fixing a nightmare.

There you have it. Happy mining, until the next bug kills us all.


April 10th 2021

We have a website now, legit as fuck.

https://lolnero.org/


Marth 31th, 2021

We simplified the daemon CLI even more, I know. We removed another
boost dependency, it's actually harder than it seems, but it was done.


Marth 29th, 2021

We enabled pretty much all the unit tests and they are auto run on
rebuild, well, with our build script. The default cmake configuration
does not build tests. We enabled C++20 just for fun, I mean we are
allowed to have some fun right? We cleaned up the interface for wallet
cli even more, so it's less of a mess at least from the outside.


Marth 25th, 2021

We finally brought back some unit tests, so from now on, every new
release will be guarded by these revived tests. We are not sure if
more, like the core tests and functional tests, can be added, since we
need completely different test data. For now, unit tests are what we
have. It's better than nothing right?


Marth 20th, 2021

After observing some cryptocurrency got spammed, we decided to make
the block size limit a linear function of the block height to prevent
spams. Lolnero tries to target mobile users, so we want running a node
to be as cheap as possible, and a large blockchain which might be
acceptable on the server but too costly on mobile is not something we
want.

The linear function is the identity function, with a minimum value of
128k.


Marth 12th, 2021

Android node:
https://play.google.com/store/apps/details?id=org.lolnero.node

Android seed generator:
https://play.google.com/store/apps/details?id=org.lolnero.lolnero_seed

Android wallet:
https://play.google.com/store/apps/details?id=org.lolnero.lolnero_wallet

We never really explained the technical details of why SHA3 or any
light weight POW is important for lolnero, now it seems to be a good
time.

The current set up of a mobile wallet requires three processes to work
simultaneously, specifically the flutter rendering process, the
wallet-rpc server and the node. They all need to be relatively
responsive for an acceptable user experience, in particularly the
daemon can not be unresponsive for several seconds trying to validate
pow for several blocks, which currently is the case for wownero and
monero. To be fair, this set up, a full validating node on mobile, is
not strictly required for a privacy coin to work, but this is what we
are after, and what makes lolnero special.


Marth 9th, 2021

We submitted the mobile seed generator and the mobile wallet to the
play store, they are both waiting for review.

https://gitlab.com/fuwa/lolnero-seed
https://gitlab.com/fuwa/lolnero-wallet

From out testing, they work alright, so lolnero should be mobile
ready, hopefully.


Marth 6th, 2021

We've been hacking on some C++. Latest cmake 3.20 beta and android ndk
23 beta allow us to build for android again, so "lolnero node" is back
on the play store:

https://play.google.com/store/apps/details?id=org.lolnero.node

These tools also allow us to build the wallet, which has not been
ported yet. We were worried that we might have to build a seed
generator using the C++ code, but after looking a bit deeper on how
the seed is converted into keys, we are confident that this is
unnecessary, since a "scaler_reduce" is part of the key generation
process, which means that any value, no matter how large, is a valid
key thanks to modular arithmetic, math basically.

So a mobile seed generator is also on the road map.

We made the wallet cli even simpler to use, but again by removing some
features which aren't essential for a wallet, like the view only
feature. This can be added back with a new wallet, or a custom 3rd
party wallet for a specific user group. We want to keep C++ to the
bare minimal, right?


Feb 28th, 2021

So we are working on a rewrite. So far we have implemented

* tx (de)serialization
* block (de)serialization
* block pow validation
* an in ram database that can persist to disk

Pretty much half of monero-rs and a little bit more.

We won't post anything on twitter until there's a working daemon, but
we'd like to share with you some findings. The entire thing is in 2k
lines of haskell, and half of it is unit test. Varint parsing /
building was like 6 lines of code. We use libraries like crazy. We
don't plan to implement bulletproof validation, we want to wrap the
C++ with a C API, and just call it from haskell. We don't care about
batched verification, so we'll just iterate over it. We are
experimenting with a new build system, since it requires some heavy
duty tooling to manage this C++ beast.

There's still a lot to be done, so stay tuned :D


Feb 19th, 2021

After some heavy refactoring of the original code base, mostly
removing non-essential features. We are finally ready to start to work
on the real goal of this project, a rewrite.

The goal is to have as many implementations as possible, but for now,
we focus on a rewrite in haskell, for only one reason: that is the
only language our current developer is willing to work with for free.

Anyway, haskell isn't completely useless and pretty much anything is
better than C++.

We made the C++ implementation as maintainable as possible, so it
should give us enough time to work on this. Good luck with your other
investments.

P.S. Yes, we implemented tree-hash in haskell. I know, what a pathetic
project.


Feb 13th, 2021

We made some further improvements to the code base by reducing the
Lines of Code in the src/ folder from the original 159k down to 86k,
which should gives us better future maintainability.

Some simplification to the consensus and protocol were also
introduced, for example, a limited block size of 4MB and a removal of
the some what unconventional block weight formula. In lolnero, block
weight = block size, so these terms are interchangeable. We simplified
the pseudo protocol involving the fields in tx_extra, by reduing the
number of them from five or something down to two, so other wallet
implementations can have less things to worry about to be compatible
with the reference implementation.

We cleaned up the cmake build system to use only standard cmake
functions, to make maintenance easier for the future. It also makes
using other build system possible by having a somewhat cleaner
reference build. This might be beneficial when porting to other
platforms, which might not be well supported by cmake, like the
current situation on android.

We refactored the wallet2.(h/cpp) module significantly,  but that's
just another taste on how code should be organized :p

To wrap up, we really want to have maintainable code, so we have
something we are relatively comfortable to store some value in.


Feb 5th, 2021

We were ready to pull the plug, but there was this miner that refused
to leave  >:(

Anyway, guess this network will keep running for some unspecified
extended period of time ...


Jan 10th, 2021

It seems this project failed to attract any attention, the seed node
will be taken down once most miners left, thanks for not contributing
anything really. It's a fun experiment anyway.


Dec 24th, 2020

How's everyone? We made some huge "improvements" to the code base by
reducing its size. But unlike other projects, you don't need to
upgrade if you don't feel like it, since there's no hardforks  ;)

Have a nice holiday.


Dec 6th, 2020

Enabled socks4a for clear net. To use Tor for everything (clear net
and hidden services), start the daemon like this:

[code] lolnerod \ --proxy public,127.0.0.1:9050 \ --proxy
tor,127.0.0.1:9050 [/code]

So you don't need to run it under torsocks :D


Dec 5th, 2020

Full Tor support has been added / restored with the latest code, use

[code]--proxy tor,127.0.0.1:9050,10[/code]

to enable access to the built-in seed node.

You can block clear net at the firewall level and the blockchain
should continue to work :D


Nov 4th, 2020

Mobile node is available:
https://play.google.com/store/apps/details?id=org.lolnero.node


Oct 11th, 2020 - 3

We are online at height 1, join us!


Oct 11th, 2020 - 2

We will launch again at Oct 11th, 2020, 16:00 (UTC), see you there!


Oct 11th, 2020

It seems the seed node isn't accepting connections, we'll try to fix
it later, and possibly relaunch. Thanks :D


Oct 10th, 2020

We'll likely launch on Oct 10th, 2020, 21:00 (UTC) !

