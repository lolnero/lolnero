{
  description = "A private ASIC friendly cryptocurrency"

  ; inputs.nixpkgs.url = "nixpkgs/nixos-unstable"

  ; outputs = { self, nixpkgs }:
      let
        supportedSystems =
          [ "x86_64-linux" "aarch64-linux" "riscv64-linux" ]

        ; forAllSystems =
            f: nixpkgs.lib.genAttrs supportedSystems
              (system: f system)

        ; nixpkgsFor = forAllSystems
          (system: import nixpkgs
            { inherit system; overlays = [ self.overlay ]; })
        ;
      in
        {
          # A Nixpkgs overlay.
          overlay = final: prev:
            with final
            ;

            let
              stdenvLatest = gcc12Stdenv
              ; version = builtins.substring 0 8 self.lastModifiedDate
              ; boostLatest = boost179

              ; lolnero-template =
                  {
                    stdenv
                  , opencl ? false
                  , name ? "lolnero"
                  , patchScript ? true
                  }: stdenv.mkDerivation {
                    pname = name
                    ; inherit version
                    ; src = ./.

                    ; nativeBuildInputs = [ cmake ]

                    ; buildInputs = [
                        boostLatest libsodium
                        nlohmann_json
                        tbb
                      ]
                      ++
                      (
                        nixpkgs.lib.optionals opencl
                          [
                            
                            opencl-headers
                            ocl-icd
                            opencl-clhpp
                          ]
                      )

                    ; postPatch = lib.optional patchScript
                      ''
                        for f in doc/examples/lolnerod-rpc/*; do
                            substituteInPlace $f \
                                --replace "#!/usr/bin/env bash" "#!${pkgs.bash}/bin/bash" \
                                --replace "/usr/bin/env http" "${pkgs.httpie}/bin/http" \
                                --replace "/usr/bin/env jq" "${pkgs.jq}/bin/jq" \
                                --replace "/usr/bin/env printf" "${pkgs.coreutils}/bin/printf" \
                                --replace "/usr/bin/env xargs" "${pkgs.findutils}/bin/xargs" \

                        done

                        for f in doc/examples/lolnero-wallet-rpc/*; do
                            substituteInPlace $f \
                                --replace "#!/usr/bin/env bash" "#!${pkgs.bash}/bin/bash" \
                                --replace "/usr/bin/env http" "${pkgs.httpie}/bin/http" \
                                --replace "/usr/bin/env jq" "${pkgs.jq}/bin/jq" \
                                --replace "/usr/bin/env printf" "${pkgs.coreutils}/bin/printf" \
                                --replace "/usr/bin/env xargs" "${pkgs.findutils}/bin/xargs" \

                        done
                      ''

                    ; cmakeFlags = [
                        "--no-warn-unused-cli"
                        "-DVERSIONTAG=${version}"
                      ]
                      ++
                      (
                        nixpkgs.lib.optionals opencl
                          [
                            "-DUSE_OPENCL=ON"
                          ]
                      )
                    ;
                  }
              ; in
              {
                lolnero = lolnero-template { stdenv = stdenvLatest; }

                ; lolnero-lite = lolnero-template
                  { name = "lolnero-lite "
                    ; stdenv = stdenvLatest
                    ; patchScript = false
                    ;
                  }

                ; lolnero-opencl = lolnero-template
                  {
                    name = "lolnero-opencl"
                    ; stdenv = stdenvLatest
                    ; opencl = true
                    ;
                  }

                ; lolnero-lib = stdenvLatest.mkDerivation {
                    pname = "lolnero-lib"
                    ; inherit version
                    ; src = ./.

                    ; nativeBuildInputs = [ cmake ]

                    ; buildInputs = [
                        boostLatest libsodium
                        nlohmann_json
                      ]

                    ; cmakeFlags = [
                        "--no-warn-unused-cli"
                        "-DVERSIONTAG=${version}"
                        "-DONLY_LIB=ON"
                        "-DBUILD_SHARED_LIBS=ON"
                      ]
                    ;
                  }

                ; lolnero-with-tests = stdenvLatest.mkDerivation {
                    pname = "lolnero-with-tests"
                    ; inherit version
                    ; src = ./.

                    ; nativeBuildInputs = [ cmake ]

                    ; buildInputs = [
                        boostLatest libsodium
                        opencl-headers
                        ocl-icd
                        opencl-clhpp
                        gmock
                        nlohmann_json
                        tbb
                      ]

                    ; doCheck = true

                    ; checkPhase =
                        ''
                            ${cmake}/bin/ctest
                        ''
                          
                    ; cmakeFlags = [
                        "--no-warn-unused-cli"
                        "-DVERSIONTAG=${version}"
                        "-DUSE_OPENCL=ON"
                        "-DBUILD_TESTING=ON"
                      ]
                    ;
                  }
                ;
              }
                
          ; nixosModules.lolnero =
              { pkgs, ... }:
              {
                nixpkgs.overlays = [ self.overlay ]
                ;
              }
                
          ; checks = forAllSystems (system:
              {
                inherit (nixpkgsFor.${system}) lolnero-with-tests
                ;
              })
            
          ; packages = forAllSystems (system:
              {
                inherit (nixpkgsFor.${system}) lolnero
                ; inherit (nixpkgsFor.${system}) lolnero-lite
                ; inherit (nixpkgsFor.${system}) lolnero-opencl
                ; inherit (nixpkgsFor.${system}) lolnero-lib
                ;
              })
            
          ; defaultPackage = forAllSystems
            (system: self.packages.${system}.lolnero)
            
          ; apps = forAllSystems
            (
              system:
              {
                lolnerod-rpc =
                  {
                    type = "app"
                    ; program = "${self.defaultPackage.${system}}"
                      + "/bin/lolnerod-rpc"
                    ;
                  }

                ; lolnero-wallet-rpc =
                    {
                      type = "app"
                      ; program = "${self.defaultPackage.${system}}"
                        + "/bin/lolnero-wallet-rpc"
                      ;
                    }
                ;
                
              }
            )
            
          ; devShell = forAllSystems
            (
              system:
              let
                pkgs = nixpkgs.legacyPackages.${system}
                ; CMakeFlags_Lolnero =
                    ''
                    ''

                ; CMakeFlags_Lolnero_OpenCL =
                    ''
                        -DUSE_OPENCL=ON
                    ''

                ; CMakeDevFlags =
                    ''
                        -DBUILD_SHARED_LIBS=ON
                        -DCMAKE_BUILD_TYPE=Debug
                    ''

                # CMakeCCacheFlags = "";

                ; CMakeCCacheFlags =
                    ''
                        -DCMAKE_CXX_COMPILER_LAUNCHER=${pkgs.ccache}/bin/ccache
                        -DCMAKE_C_COMPILER_LAUNCHER=${pkgs.ccache}/bin/ccache
                    ''

                ; CMakeTestFlags =
                    ''
                        -DBUILD_TESTING=ON
                    ''

                ; configureReleaseCommon =
                    ''
                        ${pkgs.cmake}/bin/cmake ${CMakeFlags_Lolnero} ${CMakeCCacheFlags}
                    ''

                ; configureCommon = configureReleaseCommon
                                    + CMakeDevFlags
                                    # + CMakeFlags_Lolnero_OpenCL

                ; configureGCC = configureCommon
                ; configureGCCRelease = configureReleaseCommon

                ; configure = configureGCC
                ; configureRelease = configureGCCRelease

                ;
              in
                pkgs.gcc12Stdenv.mkDerivation {
                  name = "lolnero-dev-shell"
                  ; hardeningDisable = [ "all" ]
                  ; buildInputs =
                      (
                        with pkgs
                        ;
                        [
                          cmake git

                          boost179 libsodium
                          gmock
                          ccache

                          opencl-headers
                          ocl-icd
                          opencl-clhpp
                          httpie
                          jq
                          nlohmann_json
                          tbb
                        ]
                      )

                  ; inherit CMakeFlags_Lolnero
                  ; inherit CMakeCCacheFlags
                  ; inherit CMakeTestFlags

                  ; inherit configureGCC
                  ; inherit configureGCCRelease
                  ; inherit configure
                  ; inherit configureRelease

                  ; configureTest = configure + CMakeTestFlags
                  ; configureTestRelease =
                      configureRelease + CMakeTestFlags
                  ;
                }
            )
          ;
        }
  ;
}
