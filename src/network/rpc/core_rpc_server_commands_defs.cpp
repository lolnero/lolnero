/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "core_rpc_server_commands_defs.hpp"

namespace cryptonote
{
  void to_json(json& j, const block_and_txs_data_t& x) {
    const std::string block_hex = epee::hex::encode_to_hex
      (epee::string_tools::string_to_blob(x.block));

    std::vector<std::string> txs_as_hex;
    std::transform
      (
       x.txs.begin()
       , x.txs.end()
       , std::back_inserter(txs_as_hex)
       , [](const auto& x) {
         return epee::hex::encode_to_hex
           (epee::string_tools::string_to_blob(x));
       }
       );

    j = json
      {
        {"block", block_hex}
        , {"txs", txs_as_hex}
      };
  }

  void from_json(const json& j, block_and_txs_data_t& x) {
    const std::string block_hex = j.at("block").get<std::string>();
    x.block =
      epee::hex::decode_from_hex_to_string(block_hex).value_or("");

    std::vector<std::string> txs_as_hex;
    j.at("txs").get_to(txs_as_hex);

    std::vector<std::string> txs;
    std::transform
      (
       txs_as_hex.begin()
       , txs_as_hex.end()
       , std::back_inserter(txs)
       , [](const auto& x) {
         return epee::hex::decode_from_hex_to_string(x).value_or("");
       }
       );

    x.txs = txs;
  }


}
