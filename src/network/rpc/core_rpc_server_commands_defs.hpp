/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/


#pragma once

#include "cryptonote/protocol/cryptonote_protocol_defs.h"

#include "tools/epee/include/string_tools.h"
#include "math/blockchain/controller/gamma_ring.hpp"

#include "cryptonote/basic/functional/base_json.hpp"
#include "math/blockchain/controller/tx_pool.hpp"
#include "math/ringct/functional/rctTypes.hpp"

#include "math/json/all.hpp"


using namespace constant;

constexpr std::string_view CORE_RPC_STATUS_OK = "OK";
constexpr std::string_view CORE_RPC_STATUS_BUSY = "BUSY";
constexpr std::string_view CORE_RPC_STATUS_NOT_MINING = "NOT MINING";

#define EMPTY_REQUEST                                   \
  struct request {};                                    \
  inline void from_json(const json& j, request& p) {};  \
  inline void to_json(json& j, const request& p) {};    \

#define EMPTY_RESPONSE                          \
  struct response : rpc_response_base {};       \
                                                \
  JSON_LAYOUT                                   \
  ( response                                    \
    , status                                    \
    )

namespace cryptonote
{
  constexpr uint16_t CORE_RPC_VERSION_MAJOR = 4;
  constexpr uint16_t CORE_RPC_VERSION_MINOR = 0;

  consteval uint32_t MAKE_CORE_RPC_VERSION
  (uint16_t major, uint16_t minor)
  {
    return (major<<16)|minor;
  }

  constexpr uint32_t CORE_RPC_VERSION =
    MAKE_CORE_RPC_VERSION
    (CORE_RPC_VERSION_MAJOR, CORE_RPC_VERSION_MINOR);


  void to_json(json& j, const block_and_txs_data_t& x);
  void from_json(const json& j, block_and_txs_data_t& x);

  JSON_LAYOUT
  (
   connection_info
   , incoming
   // , localhost
   // , local_ip
   // , address
   , host
   // , ip
   , port
   // , rpc_port
   , peer_id
   // , recv_count
   // , recv_idle_time
   // , send_count
   // , send_idle_time
   , state
   , live_time
   // , avg_download
   // , current_download
   // , avg_upload
   // , current_upload
   // , support_flags
   // , connection_id
   , top_block_index
   // , address_type
   );


  struct rpc_response_base
  {
    std::string status;
  };

  namespace COMMAND_RPC_GET_BLOCKS_FAST
  {

    struct request
    {
      /*
        first 10 blocks id goes sequential,
        next goes in pow(2,n) offset,
        like 2, 4, 8, 16, 32, 64 and so on,
        and the last one is always genesis block
      */
      std::list<crypto::hash> block_ids;
    };

    JSON_LAYOUT
    (
     request
     , block_ids
     );

    struct response: rpc_response_base
    {
      std::vector<block_t> blocks;
      uint64_t    start_block_index;
      uint64_t    blockchain_size;
    };

    JSON_LAYOUT
    (
     response
     , status
     , blocks
     , start_block_index
     , blockchain_size
     );

  };

  //-----------------------------------------------
  namespace COMMAND_RPC_GET_TRANSACTION
  {
    struct request
    {
      crypto::hash tx_hash;
    };

    JSON_LAYOUT
    (
     request
     , tx_hash
     );


    struct tx_meta
    {
      crypto::hash hash;
      // uint64_t block_index;
      // uint64_t timestamp;
      // std::vector<uint64_t> output_indices;
    };

    JSON_LAYOUT
    (
     tx_meta
     , hash
     // , block_index
     // , timestamp
     // , output_indices
     );
      
    struct response: rpc_response_base
    {
      std::string as_hex;
      cryptonote::transaction tx;
      tx_meta meta;
    };

    JSON_LAYOUT
    (
     response
     , status
     , as_hex
     , tx
     , meta
     );

  };

  namespace COMMAND_RPC_GET_RING_CT
  {
    struct request
    {
      crypto::hash tx_hash;
    };

    JSON_LAYOUT
    (
     request
     , tx_hash
     );


    struct tx_meta
    {
      crypto::hash hash;
    };

    JSON_LAYOUT
    (
     tx_meta
     , hash
     );
      
    struct response: rpc_response_base
    {
      std::string as_hex;
      cryptonote::ringct tx;
      tx_meta meta;
    };

    JSON_LAYOUT
    (
     response
     , status
     , as_hex
     , tx
     , meta
     );

  };


  //-----------------------------------------------
  namespace COMMAND_RPC_IS_KEY_IMAGE_SPENT
  {
    struct request
    {
      crypto::crypto_data output_key_image;
    };

    JSON_LAYOUT
    (
     request
     , output_key_image
     );


    struct response: rpc_response_base
    {
      bool spent;
    };

    JSON_LAYOUT
    (
     response
     , status
     , spent
     );

  };

  namespace ring
  {
    JSON_LAYOUT
    (
     decoy
     , public_key
     , commit
     , block_index
     , output_index
     );
  }

  namespace COMMAND_RPC_GET_DECOYS
  {
    struct request
    {
      crypto::public_key output_public_key;
      uint64_t gamma_config = 0;
    };

    JSON_LAYOUT
    (
     request
     , output_public_key
     , gamma_config
     );

    struct response: rpc_response_base
    {
      std::vector<cryptonote::ring::decoy> decoys;
    };

    JSON_LAYOUT
    (
     response
     , status
     , decoys
     );
  };


  //-----------------------------------------------
  namespace COMMAND_RPC_GET_OUTPUTS
  {
    struct request
    {
      uint64_t index;
    };

    JSON_LAYOUT
    (
     request
     , index
     );

    struct response: rpc_response_base
    {
      ring::decoy output;
    };

    JSON_LAYOUT
    (
     response
     , status
     , output
     );
  };
  //-----------------------------------------------
  namespace COMMAND_RPC_SEND_RAW_TX
  {
    struct request
    {
      ringct tx;
    };

    JSON_LAYOUT
    (
     request
     , tx
     );


    struct response: rpc_response_base
    {
      std::string reason;
      bool not_relayed;
      bool low_mixin;
      bool double_spend;
      bool invalid_input;
      bool invalid_output;
      bool too_big;
      bool overspend;
      bool fee_too_low;
      bool too_few_outputs;
      bool sanity_check_failed;
      bool rejected_by_pool;
    };

    JSON_LAYOUT
    (
     response
     , status
     , reason
     , not_relayed
     , low_mixin
     , double_spend
     , invalid_input
     , invalid_output
     , too_big
     , overspend
     , fee_too_low
     , too_few_outputs
     , sanity_check_failed
     , rejected_by_pool
     );

  };
  //-----------------------------------------------
  namespace COMMAND_RPC_START_MINING
  {
    struct request
    {
      std::string miner_address;
      uint64_t    threads_count;
    };

    JSON_LAYOUT
    (
     request
     , miner_address
     , threads_count
     );

    EMPTY_RESPONSE;
  };

  //-----------------------------------------------
  namespace COMMAND_RPC_GET_INFO
  {
    EMPTY_REQUEST;

    struct response: rpc_response_base
    {
      uint64_t top_block_index;
      uint64_t target_top_block_index;
      diff_t difficulty;
      uint64_t difficulty_top64;
      diff_t target;
      uint64_t tx_count;
      uint64_t tx_pool_size;
      uint64_t alt_blocks_count;
      uint64_t outgoing_connections_count;
      uint64_t incoming_connections_count;
      uint64_t white_peerlist_size;
      uint64_t grey_peerlist_size;
      bool mainnet = true;
      bool testnet = false;
      std::string nettype;
      crypto::hash top_block_hash;
      diff_t cumulative_difficulty;
      bool offline = false;
      bool busy_syncing = false;
      std::string version;
    };

    JSON_LAYOUT
    (
     response
     , status
     , top_block_index
     , target_top_block_index
     , difficulty
     , tx_count
     , tx_pool_size
     , outgoing_connections_count
     , incoming_connections_count
     , white_peerlist_size
     , grey_peerlist_size
     , nettype
     , top_block_hash
     , cumulative_difficulty
     , busy_syncing
     , version
     );

  };

  //-----------------------------------------------
  namespace COMMAND_RPC_STOP_MINING
  {
    EMPTY_REQUEST;
    EMPTY_RESPONSE;
  };

  //-----------------------------------------------
  namespace COMMAND_RPC_MINING_STATUS
  {
    EMPTY_REQUEST;

    struct response: rpc_response_base
    {
      bool active = false;
      uint64_t speed;
      uint32_t threads_count;
      std::string address;
      std::string pow_algorithm;
      uint32_t block_target;
      uint64_t block_reward;
      diff_t difficulty;
    };

    JSON_LAYOUT
    (
     response
     , status
     , active
     , speed
     , threads_count
     , address
     , pow_algorithm
     , block_target
     , block_reward
     , difficulty
     );

  };

  struct block_meta
  {
    uint64_t index;
    crypto::hash hash;
    diff_t difficulty;
    diff_t cumulative_difficulty;
    uint64_t reward;
    uint64_t num_txes;
    crypto::hash pow_hash;
    crypto::hash miner_tx_hash;
  };

  JSON_LAYOUT
  (
   block_meta
   , index
   , hash
   , difficulty
   , cumulative_difficulty
   , pow_hash
   , miner_tx_hash
   );

  namespace COMMAND_RPC_GET_BLOCK
  {
    struct request
    {
      crypto::hash hash;
      uint64_t block_index;
    };

    JSON_LAYOUT
    (
     request
     , hash
     , block_index
     );


    struct response: rpc_response_base
    {
      block_meta meta;
      std::string blob;
      cryptonote::raw_block block;
    };

    JSON_LAYOUT
    (
     response
     , status
     , meta
     , blob
     , block
     );

  };


  struct peer {
    uint64_t id;
    std::string host;
    uint32_t ip;
    uint16_t port;
    uint64_t last_seen;
  };

  JSON_LAYOUT
  (
   peer
   , id
   , host
   , ip
   , port
   , last_seen
   );

  namespace COMMAND_RPC_GET_PEER_LIST
  {
    EMPTY_REQUEST;

    struct response: rpc_response_base
    {
      std::vector<peer> white_list;
      std::vector<peer> gray_list;
    };

    JSON_LAYOUT
    (
     response
     , status
     , white_list
     , gray_list
     );

  };


  JSON_LAYOUT
  (
   pool_tx_t
   , tx
   , size
   , receive_time
   );

  struct pool_tx_rpc_t
  {
    ringct tx;
    crypto::hash hash;
  };

  JSON_LAYOUT
  (
   pool_tx_rpc_t
   , tx
   , hash
   );


  namespace COMMAND_RPC_GET_TRANSACTION_POOL
  {
    EMPTY_REQUEST;

    struct response: rpc_response_base
    {
      std::vector<pool_tx_rpc_t> transactions;
    };

    JSON_LAYOUT
    (
     response
     , status
     , transactions
     );
  };

  namespace COMMAND_RPC_GET_CONNECTIONS
  {
    EMPTY_REQUEST;

    struct response: rpc_response_base
    {
      std::list<connection_info> connections;
    };

    JSON_LAYOUT
    (
     response
     , status
     , connections
     );
  };

  namespace COMMAND_RPC_GETBANS
  {
    struct ban
    {
      std::string host;
      uint32_t ip;
      uint32_t seconds;
    };

    JSON_LAYOUT
    (
     ban
     , host
     , ip
     , seconds
     );

    EMPTY_REQUEST;


    struct response: rpc_response_base
    {
      std::vector<ban> bans;
    };

    JSON_LAYOUT
    (
     response
     , status
     , bans
     );

  };

  namespace COMMAND_RPC_SETBANS
  {
    struct ban
    {
      std::string host;
      bool ban = true;
      uint32_t seconds;
    };

    JSON_LAYOUT
    (
     ban
     , host
     , ban
     , seconds
     );

    struct request
    {
      std::vector<ban> bans;
    };

    JSON_LAYOUT
    (
     request
     , bans
     );

    EMPTY_RESPONSE;
  };

  namespace COMMAND_RPC_BANNED
  {
    struct request
    {
      std::string address;
    };

    JSON_LAYOUT
    (
     request
     , address
     );

    struct response
    {
      std::string status;
      bool banned = false;
      uint32_t seconds;
    };

    JSON_LAYOUT
    (
     response
     , status
     , banned
     , seconds
     );
  };

  namespace COMMAND_RPC_FLUSH_TRANSACTION_POOL
  {
    struct request
    {
      std::vector<crypto::hash> txids;
    };

    JSON_LAYOUT
    (
     request
     , txids
     );

    EMPTY_RESPONSE;

  };

  namespace COMMAND_RPC_GET_VERSION
  {
    EMPTY_REQUEST;

    struct response: rpc_response_base
    {
      uint32_t version;
    };

    JSON_LAYOUT
    (
     response
     , status
     , version
     );

  };

  namespace COMMAND_RPC_RELAY_TX
  {
    struct request
    {
      std::vector<crypto::hash> txids;
    };

    JSON_LAYOUT
    (
     request
     , txids
     );

    EMPTY_RESPONSE;
  };


  namespace COMMAND_RPC_POP_BLOCKS
  {
    struct request
    {
      uint64_t nblocks;
    };

    JSON_LAYOUT
    (
     request
     , nblocks
     );

    struct response: rpc_response_base
    {
      uint64_t top_block_index;
    };

    JSON_LAYOUT
    (
     response
     , status
     , top_block_index
     );
  };
}
