// Copyright (c) 2014-2020, The Monero Project
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this list of
//    conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this list
//    of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may be
//    used to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
// THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
// THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Parts of this file are originally copyright (c) 2012-2013 The Cryptonote developers

#include "core_rpc_server.h"
#include "core_rpc_server_error_codes.h"

#include "network/type/parse.h"

#include "math/blockchain/functional/tx_check.hpp"
#include "cryptonote/tx/functional/tx_utils.hpp"
#include "cryptonote/basic/functional/tx_extra.hpp"
#include "cryptonote/basic/functional/format_utils.hpp"

#include "math/blockchain/controller/gamma_ring.hpp"

#include "config/version/version.hpp"

#include <cmath>





using namespace constant;

namespace cryptonote
{
  //------------------------------------------------------------------------------------------------------------------------------
  core_rpc_server::core_rpc_server(
      core& cr
    , nodetool::node_server& p2p
    )
    : m_core(cr)
    , m_p2p(p2p)
  {}
  //------------------------------------------------------------------------------------------------------------------------------
  bool core_rpc_server::on_get_info(const COMMAND_RPC_GET_INFO::request& req, COMMAND_RPC_GET_INFO::response& res)
  {
    res.top_block_hash = m_core.get_tree().get_top_block().hash();
    res.top_block_index = m_core.get_tree().get_top_block_index();

    res.target_top_block_index =
      m_p2p.get_payload_object().is_synchronized()
      ? 0 : m_core.get_tree().get_top_block_index();

    res.difficulty = m_core.get_tree().get_top_diff();

    res.target = m_core.get_tree().get_next_diff();

    res.tx_pool_size = m_core.get_tree().pool_get_all_txs().size();
    res.tx_count = m_core.get_tree().get_txs_size();

    uint64_t total_conn = m_p2p.get_public_connections_count();
    res.outgoing_connections_count = m_p2p.get_public_outgoing_connections_count();
    res.incoming_connections_count = (total_conn - res.outgoing_connections_count);
    res.white_peerlist_size = m_p2p.get_public_white_peers_count();
    res.grey_peerlist_size = m_p2p.get_public_gray_peers_count();

    network_type net_type = nettype();
    res.mainnet = net_type == MAINNET;
    res.testnet = net_type == TESTNET;
    res.nettype =
      net_type == MAINNET
      ? "mainnet"
      : net_type == TESTNET
      ? "testnet"
      : "fakechain"
      ;

    res.cumulative_difficulty = m_core.get_tree().get_top_cumulative_diff();

    res.offline = m_core.m_offline;
    res.version = LOLNERO_VERSION_FULL;
    res.busy_syncing = m_p2p.get_payload_object().is_busy_syncing();

    res.status = CORE_RPC_STATUS_OK;
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool core_rpc_server::on_get_blocks(const COMMAND_RPC_GET_BLOCKS_FAST::request& req, COMMAND_RPC_GET_BLOCKS_FAST::response& res)
  {
    // quick check for noop
    const auto& block_ids = req.block_ids;

    if (!block_ids.empty())
    {
      const auto top_block_hash =
        m_core.get_tree().get_top_block().hash();

      if (top_block_hash == block_ids.front())
      {
        res.start_block_index = 0;
        res.blockchain_size =
          m_core.get_tree().get_size();

        res.status = CORE_RPC_STATUS_OK;
        return true;
      }
    }

    constexpr size_t max_blocks =
      constant::COMMAND_RPC_GET_BLOCKS_FAST_MAX_COUNT;

    std::vector
      <std::pair
       <std::pair<string_blob, crypto::hash>
        , std::vector<std::pair<crypto::hash, string_blob> > > > bs;

    const auto truncted_ids =
      std::vector<crypto::hash>{block_ids.begin(), block_ids.end()}
      | std::views::take(max_blocks)
      ;

    const auto blocks =
      m_core.get_tree().get_blocks_by_short_chain_history
      ({truncted_ids.begin(), truncted_ids.end()})
      ;

    if (blocks.empty()) {
      res.status = "Failed";
      return true;
    }

    res.start_block_index = blocks.front().get_index();
    res.blockchain_size = m_core.get_tree().get_size();


    const auto& block_blobs =
      blocks
      ;

    res.blocks = {block_blobs.begin(), block_blobs.end()};

    LOG_DEBUG
      (
       "on_get_blocks: "
       + std::to_string(blocks.size())
       + " blocks"
       );
    res.status = CORE_RPC_STATUS_OK;
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool core_rpc_server::on_get_tx_outputs(const COMMAND_RPC_GET_OUTPUTS::request& req, COMMAND_RPC_GET_OUTPUTS::response& res)
  {

    const auto maybe_output =
      m_core.get_tree().get_decoy_by_index(req.index);

    if(!maybe_output)
    {
      res.status = "Failed";
      return true;
    }

    res.output = *maybe_output;
    res.status = CORE_RPC_STATUS_OK;
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool core_rpc_server::on_get_transaction
  (
   const COMMAND_RPC_GET_TRANSACTION::request& req
   , COMMAND_RPC_GET_TRANSACTION::response& res
   )
  {
    const auto tx_hash = req.tx_hash;

    const auto maybe_ringct =
      m_core.get_tree().get_tx_by_hash(tx_hash);

    if (maybe_ringct) {
      LOG_VERBOSE
        (
         "Found ringct: "
         + req.tx_hash.to_str()
         + " on the blockchain"
         );

      const auto tx = *maybe_ringct;

      COMMAND_RPC_GET_TRANSACTION::tx_meta meta{};

      auto& e = res;

      meta.hash = tx.hash();

      e.as_hex = epee::hex::encode_to_hex
        (epee::string_tools::string_to_blob
         (review_ringct_to_blob(tx)));

      e.tx = review_ringct(tx);

      e.meta = meta;

      res.status = CORE_RPC_STATUS_OK;
      return true;
    }

    const auto maybe_coinbase_tx =
      m_core.get_tree().get_coinbase_tx_by_hash(tx_hash);

    if (maybe_coinbase_tx) {
      LOG_VERBOSE
        (
         "Found coinbase tx: "
         + req.tx_hash.to_str()
         + " on the blockchain"
         );

      const auto tx = *maybe_coinbase_tx;

      COMMAND_RPC_GET_TRANSACTION::tx_meta meta{};

      auto& e = res;

      meta.hash = tx.hash();

      e.as_hex = epee::hex::encode_to_hex
        (epee::string_tools::string_to_blob
         (review_coinbase_tx_to_blob(tx)));

      e.tx = review_coinbase_tx(tx);

      e.meta = meta;

      res.status = CORE_RPC_STATUS_OK;
      return true;
    }

    res.status = "Failed";
    return true;
  }

  //------------------------------------------------------------------------------------------------------------------------------
  bool core_rpc_server::on_get_ringct
  (
   const COMMAND_RPC_GET_RING_CT::request& req
   , COMMAND_RPC_GET_RING_CT::response& res
   )
  {
    const auto tx_hash = req.tx_hash;

    const auto maybe_ringct =
      m_core.get_tree().get_tx_by_hash(tx_hash);

    if (maybe_ringct) {
      LOG_VERBOSE
        (
         "Found ringct: "
         + req.tx_hash.to_str()
         + " on the blockchain"
         );

      const auto tx = *maybe_ringct;

      COMMAND_RPC_GET_RING_CT::tx_meta meta{};

      auto& e = res;

      meta.hash = tx.hash();

      e.as_hex = epee::hex::encode_to_hex
        (epee::string_tools::string_to_blob
         (review_ringct_to_blob(tx)));

      e.tx = tx;

      e.meta = meta;

      res.status = CORE_RPC_STATUS_OK;
      return true;
    }

    res.status = "Failed";
    return true;
  }

  //------------------------------------------------------------------------------------------------------------------------------
  bool core_rpc_server::on_is_output_key_image_spent(const COMMAND_RPC_IS_KEY_IMAGE_SPENT::request& req, COMMAND_RPC_IS_KEY_IMAGE_SPENT::response& res)
  {
    const auto maybe_point =
      crypto::preview_safe_point(d2p(req.output_key_image));

    if (maybe_point) {
      res.spent = m_core.get_tree()
        .is_output_key_image_spent(p2ki(*maybe_point));

      res.status = CORE_RPC_STATUS_OK;
    } else {
      res.status = "Invalid key image";
    }


    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool core_rpc_server::on_send_raw_tx(const COMMAND_RPC_SEND_RAW_TX::request& req, COMMAND_RPC_SEND_RAW_TX::response& res)
  {

    // this breaks the lens law, but whatever.
    const auto maybe_tx =
      preview_ringct_from_blob(review_ringct_to_blob(req.tx));

    if (!maybe_tx)
    {
      LOG_ERROR("failed to parse tx");
      res.status = "Failed";
      res.reason = "Tx parsing failed";
      res.sanity_check_failed = true;
      return true;
    }

    const auto tx = *maybe_tx;

    const auto tx_added = m_core.get_tree().pool_add_tx(tx);

    if (!tx_added) {
      LOG_ERROR("failed to add tx");
      res.status = "Failed";
      res.reason = "Tx adding failed";
      res.rejected_by_pool = true;
      return true;
    }

    NOTIFY_NEW_TRANSACTIONS::request r{};
    r.txs.push_back(review_ringct_to_blob(tx));
    m_core.get_protocol().relay_transactions
      (
       r
       , boost::uuids::nil_uuid()
       );
    //TODO: make sure that tx has reached other nodes here, probably wait to receive reflections from other nodes
    res.status = CORE_RPC_STATUS_OK;
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool core_rpc_server::on_start_mining(const COMMAND_RPC_START_MINING::request& req, COMMAND_RPC_START_MINING::response& res)
  {
    if(!m_p2p.get_payload_object().is_synchronized())
      {
        res.status = CORE_RPC_STATUS_BUSY;
        return true;
      }

    const auto maybe_info =
      get_account_address_from_str(nettype(), req.miner_address);

    if(!maybe_info)
    {
      res.status = "Failed, wrong address";
      LOG_PRINT_L0(res.status);
      return true;
    }

    const auto info = *maybe_info;

    if (info.is_subaddress)
    {
      res.status = "Mining to subaddress isn't supported yet";
      LOG_PRINT_L0(res.status);
      return true;
    }

    miner &miner= m_core.get_miner();
    if (miner.is_mining())
    {
      res.status = "Already mining";
      return true;
    }
    if(!miner.start(info.address, static_cast<size_t>(req.threads_count)))
    {
      res.status = "Failed, mining not started";
      LOG_PRINT_L0(res.status);
      return true;
    }
    res.status = CORE_RPC_STATUS_OK;
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool core_rpc_server::on_stop_mining(const COMMAND_RPC_STOP_MINING::request& req, COMMAND_RPC_STOP_MINING::response& res)
  {
    miner &miner= m_core.get_miner();
    if(!miner.is_mining())
    {
      res.status = "Mining never started";
      LOG_PRINT_L0(res.status);
      return true;
    }
    if(!miner.stop())
    {
      res.status = "Failed, mining not stopped";
      LOG_PRINT_L0(res.status);
      return true;
    }
    res.status = CORE_RPC_STATUS_OK;
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool core_rpc_server::on_mining_status(const COMMAND_RPC_MINING_STATUS::request& req, COMMAND_RPC_MINING_STATUS::response& res)
  {
    const miner& lMiner = m_core.get_miner();
    res.active = lMiner.is_mining();
    res.difficulty = m_core.get_tree().get_next_diff();


    res.block_target = DIFFICULTY_TARGET_IN_SECONDS;
    if ( lMiner.is_mining() ) {
      res.speed = lMiner.get_speed();
      res.threads_count = lMiner.get_threads_count();
      res.block_reward = lMiner.get_block_reward();
    }
    const spend_view_public_keys& lMiningAdr = lMiner.get_mining_address();
    if (lMiner.is_mining())
      res.address = get_account_address_as_str(nettype(), false, lMiningAdr);

    res.pow_algorithm = "SHA-3";
    res.status = CORE_RPC_STATUS_OK;
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool core_rpc_server::on_get_peer_list(const COMMAND_RPC_GET_PEER_LIST::request& req, COMMAND_RPC_GET_PEER_LIST::response& res)
  {
    std::vector<nodetool::peerlist_entry> white_list;
    std::vector<nodetool::peerlist_entry> gray_list;

    m_p2p.get_peerlist(gray_list, white_list);

    for (auto & entry : white_list)
    {

      const uint64_t last_seen = static_cast<uint64_t>(entry.last_seen);

      if (entry.adr.get_type_id() == epee::net_utils::ipv4_network_address::get_type_id()) {
        const auto ip =
          entry.adr.as<epee::net_utils::ipv4_network_address>().ip();

        const peer peer =
          {
            entry.id
            , epee::string_tools::get_ip_string_from_int32(ip)
            , ip
            , entry.adr.as<epee::net_utils::ipv4_network_address>().port()
            , last_seen
          };
        res.white_list.emplace_back(peer);
      }
      else if (entry.adr.get_type_id() == epee::net_utils::ipv6_network_address::get_type_id()) {
        const peer peer =
          {
           entry.id
           , entry.adr.as<epee::net_utils::ipv6_network_address>().host_str()
           , 0
           , entry.adr.as<epee::net_utils::ipv6_network_address>().port()
           , last_seen
          };

        res.white_list.emplace_back(peer);
      }
      else {
        const peer peer =
          {
           entry.id
           , entry.adr.str()
           , 0
           , 0
           , last_seen
          };
        res.white_list.emplace_back(peer);
      }
    }

    for (auto & entry : gray_list)
    {
      const uint64_t last_seen = static_cast<uint64_t>(entry.last_seen);

      if (entry.adr.get_type_id() == epee::net_utils::ipv4_network_address::get_type_id()) {

        const auto ip =
          entry.adr.as<epee::net_utils::ipv4_network_address>().ip();

        const peer peer =
          {
            entry.id
            , epee::string_tools::get_ip_string_from_int32(ip)
            , ip
            , entry.adr.as<epee::net_utils::ipv4_network_address>().port()
            , last_seen
          };
        res.gray_list.emplace_back(peer);
      }
      else if (entry.adr.get_type_id() == epee::net_utils::ipv6_network_address::get_type_id()) {
        const peer peer =
          {
           entry.id
           , entry.adr.as<epee::net_utils::ipv6_network_address>().host_str()
           , 0
           , entry.adr.as<epee::net_utils::ipv6_network_address>().port()
           , last_seen
          };
        res.gray_list.emplace_back(peer);
      }
      else {
        const peer peer =
          {
           entry.id
           , entry.adr.str()
           , 0
           , 0
           , last_seen
          };
        res.gray_list.emplace_back(peer);
      }
    }

    res.status = CORE_RPC_STATUS_OK;
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool core_rpc_server::on_get_transaction_pool(const COMMAND_RPC_GET_TRANSACTION_POOL::request& req, COMMAND_RPC_GET_TRANSACTION_POOL::response& res)
  {
    const auto txs = m_core.get_tree().pool_get_all_txs();
    std::transform
      (
       txs.begin()
       , txs.end()
       , std::back_inserter(res.transactions)
       , [](const auto& x) {
         return pool_tx_rpc_t
           {
             x.second.tx
             , x.first
           };
       }
       );

    res.status = CORE_RPC_STATUS_OK;
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  uint64_t core_rpc_server::get_block_reward(const raw_block& blk)
  {
    uint64_t reward = 0;
    for(const tx_out& out: blk.miner_tx.vout)
    {
      reward += out.amount;
    }
    return reward;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool core_rpc_server::fill_block_header_response
  (
   const block_t& b, block_meta& response
   )
  {
    const auto block_index = b.get_index();
    const auto blk = review_block(b);

    response.index = block_index;
    response.hash = b.hash();
    response.reward = get_block_reward(blk);
    response.num_txes = blk.tx_hashes.size();
    response.pow_hash = get_mining_hash(blk);
    response.miner_tx_hash = get_transaction_hash(blk.miner_tx);

    response.difficulty =
      m_core.get_tree().get_diff_by_index(block_index)
      .value_or(0);

    response.cumulative_difficulty =
      m_core.get_tree().get_cumulative_diff_by_index(block_index)
      .value_or(0);

    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool core_rpc_server::on_get_block(const COMMAND_RPC_GET_BLOCK::request& req, COMMAND_RPC_GET_BLOCK::response& res, epee::json_rpc::json_error& error_resp)
  {
    const std::optional<block_t> maybe_block
      = req.hash == crypto::null_hash
      ? m_core.get_tree().get_block_by_index(req.block_index)
      : m_core.get_tree().get_block_by_hash(req.hash);

    if (!maybe_block)
    {
      error_resp.code = CORE_RPC_ERROR_CODE_INTERNAL_ERROR;
      error_resp.message = "Internal error: can't get block by hash. Hash = " + req.hash.to_str() + '.';
      return false;
    }

    const auto block = *maybe_block;
    bool response_filled =
      fill_block_header_response(block, res.meta);

    if (!response_filled)
    {
      error_resp.code = CORE_RPC_ERROR_CODE_INTERNAL_ERROR;
      error_resp.message = "Internal error: can't produce valid response.";
      return false;
    }

    res.blob = epee::string_tools::buff_to_hex_nodelimer
      (block_to_blob(review_block(block)));

    res.block = review_block(block);
    res.status = CORE_RPC_STATUS_OK;
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool core_rpc_server::on_get_connections(const COMMAND_RPC_GET_CONNECTIONS::request& req, COMMAND_RPC_GET_CONNECTIONS::response& res, epee::json_rpc::json_error& error_resp)
  {
    res.connections = m_p2p.get_payload_object().get_connections();

    res.status = CORE_RPC_STATUS_OK;

    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool core_rpc_server::on_get_info_json(const COMMAND_RPC_GET_INFO::request& req, COMMAND_RPC_GET_INFO::response& res, epee::json_rpc::json_error& error_resp)
  {
    if (!on_get_info(req, res) || res.status != CORE_RPC_STATUS_OK)
    {
      error_resp.code = CORE_RPC_ERROR_CODE_INTERNAL_ERROR;
      error_resp.message = res.status;
      return false;
    }
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool core_rpc_server::on_get_bans(const COMMAND_RPC_GETBANS::request& req, COMMAND_RPC_GETBANS::response& res, epee::json_rpc::json_error& error_resp)
  {
    auto now = time(nullptr);
    std::map<std::string, time_t> blocked_hosts = m_p2p.get_blocked_hosts();
    for (std::map<std::string, time_t>::const_iterator i = blocked_hosts.begin(); i != blocked_hosts.end(); ++i)
    {
      if (i->second > now) {
        COMMAND_RPC_GETBANS::ban b;
        b.host = i->first;
        b.ip = 0;

        const auto maybe_ip =
          epee::string_tools::get_ip_int32_from_string(b.host);

        if (maybe_ip) {
          b.ip = *maybe_ip;
        }

        b.seconds = i->second - now;
        res.bans.push_back(b);
      }
    }

    res.status = CORE_RPC_STATUS_OK;
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool core_rpc_server::on_banned(const COMMAND_RPC_BANNED::request& req, COMMAND_RPC_BANNED::response& res, epee::json_rpc::json_error& error_resp)
  {
    auto na_parsed = net::get_network_address(req.address, 0);
    if (!na_parsed)
    {
      error_resp.code = CORE_RPC_ERROR_CODE_WRONG_PARAM;
      error_resp.message = "Unsupported host type";
      return false;
    }

    const auto na = *na_parsed;

    time_t seconds;
    if (m_p2p.is_host_blocked(na, &seconds))
    {
      res.banned = true;
      res.seconds = seconds;
    }
    else
    {
      res.banned = false;
      res.seconds = 0;
    }

    res.status = CORE_RPC_STATUS_OK;
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool core_rpc_server::on_set_bans(const COMMAND_RPC_SETBANS::request& req, COMMAND_RPC_SETBANS::response& res, epee::json_rpc::json_error& error_resp)
  {
    for (auto i = req.bans.begin(); i != req.bans.end(); ++i)
    {
      epee::net_utils::network_address na;

      // then host
      if (!i->host.empty())
      {
        auto na_parsed = net::get_network_address(i->host, 0);
        if (!na_parsed)
        {
          error_resp.code = CORE_RPC_ERROR_CODE_WRONG_PARAM;
          error_resp.message = "Unsupported host type";
          return false;
        }
        na =*na_parsed;
      }

      if (i->ban)
        m_p2p.block_host(na, i->seconds);
      else
        m_p2p.unblock_host(na);
    }

    res.status = CORE_RPC_STATUS_OK;
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool core_rpc_server::on_flush_tx_pool(const COMMAND_RPC_FLUSH_TRANSACTION_POOL::request& req, COMMAND_RPC_FLUSH_TRANSACTION_POOL::response& res, epee::json_rpc::json_error& error_resp)
  {
    std::ranges::for_each
      (
       req.txids
       , std::bind_front
       (
        &blocktree_t::pool_take_tx
        , m_core.get_tree()
        )
       );

    res.status = CORE_RPC_STATUS_OK;
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool core_rpc_server::on_get_version(const COMMAND_RPC_GET_VERSION::request& req, COMMAND_RPC_GET_VERSION::response& res, epee::json_rpc::json_error& error_resp)
  {
    res.version = CORE_RPC_VERSION;
    res.status = CORE_RPC_STATUS_OK;
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool core_rpc_server::on_pop_blocks(const COMMAND_RPC_POP_BLOCKS::request& req, COMMAND_RPC_POP_BLOCKS::response& res)
  {
    for (size_t i = 0; i < req.nblocks; i++) {
      m_core.get_tree().pop_block();
    }

    res.top_block_index = m_core.get_tree().get_top_block_index();
    res.status = CORE_RPC_STATUS_OK;

    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool core_rpc_server::on_relay_tx(const COMMAND_RPC_RELAY_TX::request& req, COMMAND_RPC_RELAY_TX::response& res, epee::json_rpc::json_error& error_resp)
  {
    bool failed = false;
    res.status = "";
    for (const auto &x: req.txids)
    {
      if(x == crypto::null_hash)
      {
        if (!res.status.empty()) res.status += ", ";
        res.status += std::string("invalid transaction id");
        failed = true;
        continue;
      }

      const auto txid = x;

      const auto maybe_pool_tx = m_core.get_tree().pool_get_tx(txid);
      if (maybe_pool_tx)
      {
        NOTIFY_NEW_TRANSACTIONS::request r{};

        r.txs.push_back(review_ringct_to_blob(maybe_pool_tx->tx));
        m_core.get_protocol().relay_transactions
          (
           r
           , boost::uuids::nil_uuid()
           );
        //TODO: make sure that tx has reached other nodes here, probably wait to receive reflections from other nodes
      }
      else
      {
        if (!res.status.empty()) res.status += ", ";
        res.status +=
          std::string("transaction not found in pool: ") + txid.to_str();
        failed = true;
        continue;
      }
    }

    if (failed)
    {
      return false;
    }

    res.status = CORE_RPC_STATUS_OK;
    return true;
  }

  //------------------------------------------------------------------------------------------------------------------------------
  bool core_rpc_server::on_get_decoys
  (
   const COMMAND_RPC_GET_DECOYS::request& req
   , COMMAND_RPC_GET_DECOYS::response& res
   , epee::json_rpc::json_error& error_resp
   ) {
    try
    {
      const uint64_t usable_top_block_index =
        m_core.get_tree().get_top_block_index();
      LOG_TRACE
        (
         "usable top block index: "
         + std::to_string(usable_top_block_index)
         );

      if
        (
         usable_top_block_index
         < config::lol::ring_size + consensus::get_coinbase_unlock_time()
         )
        {
          error_resp.code = CORE_RPC_ERROR_CODE_INTERNAL_ERROR;
          error_resp.message = "Blockchain is too short to get decoys";
          return false;
        }


      LOG_DEBUG("on_get_decoys: " + req.output_public_key.to_str());

      const auto maybe_output_index =
        m_core.get_tree().get_output_index_by_public_key
        (req.output_public_key);

      if (!maybe_output_index) {
        error_resp.code = CORE_RPC_ERROR_CODE_INTERNAL_ERROR;
        error_resp.message = "Failed to get real output as decoy";
        return false;
      }

      const auto maybe_real_output =
        m_core.get_tree().get_decoy_by_index
        (*maybe_output_index);

      if (!maybe_real_output) {
        error_resp.code = CORE_RPC_ERROR_CODE_INTERNAL_ERROR;
        error_resp.message = "Failed to get real output as decoy";
        return false;
      }

      const auto real_output = *maybe_real_output;

      constexpr uint64_t max_acceptable_distance = 4;

      constexpr auto default_config =
        gamma_config_user;

      gamma_config config{};

      switch(req.gamma_config) {
      case 0:
        LOG_INFO("Gamma config: User");
        config = gamma_config_user;
        break;
      case 1:
        LOG_INFO("Gamma config: Exchange");
        config = gamma_config_hodler;
        break;
      case 2:
        LOG_INFO("Gamma config: Zen");
        config = gamma_config_zen;
        break;
      case 3:
        LOG_INFO("Gamma config: Hodler");
        config = gamma_config_zen;
        break;
      default:
        LOG_INFO("Gamma config: Default");
        config = default_config;
      }

      const auto decoy_indices = forge_gamma_ring
        (
         config
         , m_core.get_tree().get_outputs_size()
         , config::lol::ring_size
         , real_output.output_index
         , max_acceptable_distance
         );

      // std::vector<cryptonote::ring::decoy> decoys;
      // std::tran

      const auto decoys =
        decoy_indices
        | std::views::transform
        ([&](const auto& x) {
          const auto decoy =
            m_core.get_tree().get_decoy_by_index(x);

          if (!decoy) {
            throw std::runtime_error
              ("invalid decoy index: " + std::to_string(x));
          }

          return *decoy;
        });


      res.decoys = { decoys.begin(), decoys.end() };


      size_t i = 0;
      for (const auto& x: res.decoys) {
        if (x.public_key == real_output.public_key) {
          LOG_DEBUG
            (
             "Including real output at "
             + std::to_string(i)
             + ": "
             + x.public_key.to_str()
             );
        }
        i++;
      }

    }

    catch (const std::exception &e)
    {
      error_resp.code = CORE_RPC_ERROR_CODE_INTERNAL_ERROR;
      error_resp.message = "Failed to get decoys";
      return false;
    }

    res.status = CORE_RPC_STATUS_OK;
    return true;
  }
}  // namespace cryptonote
