// Copyright (c) 2014-2020, The Monero Project
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this list of
//    conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this list
//    of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may be
//    used to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
// THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
// THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Parts of this file are originally copyright (c) 2012-2013 The Cryptonote developers

// IP blocking adapted from Boolberry

#include "net_node.h"

#include "cryptonote/core/cryptonote_core.hpp"

#include "network/type/parse.h"

#include "math/crypto/controller/random.hpp"

#include <boost/uuid/uuid_io.hpp>





#define NET_MAKE_IP(b1,b2,b3,b4)  ((LPARAM)(((DWORD)(b1)<<24)+((DWORD)(b2)<<16)+((DWORD)(b3)<<8)+((DWORD)(b4))))

namespace nodetool
{

  node_server::~node_server()
  {
    // tcp server uses io_service in destructor, and every zone uses
    // io_service from public zone.
    for (auto current = m_network_zones.begin(); current != m_network_zones.end(); /* below */)
    {
      if (current->first != epee::net_utils::zone::public_)
        current = m_network_zones.erase(current);
      else
        ++current;
    }
  }
  //-----------------------------------------------------------------------------------
  bool append_net_address(std::vector<epee::net_utils::network_address> & seed_nodes, std::string const & addr, uint16_t default_port);
  //-----------------------------------------------------------------------------------

  bool node_server::init_config()
  {
    TRY_ENTRY();

    network_zone& public_zone = m_network_zones[epee::net_utils::zone::public_];
    public_zone.m_config.m_peer_id = crypto::rand<uint64_t>();

    m_first_connection_maker_call = true;

    CATCH_ENTRY_L0("node_server::init_config", false);
    return true;
  }
  //-----------------------------------------------------------------------------------

  void node_server::for_each_connection(std::function<bool(typename t_payload_net_handler::connection_context&, peerid_type, uint32_t)> f)
  {
    for(auto& zone : m_network_zones)
    {
      zone.second.m_net_server.get_config_object().foreach_connection([&](p2p_connection_context& cntx){
        return f(cntx, cntx.peer_id, cntx.support_flags);
      });
    }
  }
  //-----------------------------------------------------------------------------------

  bool node_server::for_connection(const boost::uuids::uuid &connection_id, std::function<bool(typename t_payload_net_handler::connection_context&, peerid_type, uint32_t)> f)
  {
    for(auto& zone : m_network_zones)
    {
      const bool result = zone.second.m_net_server.get_config_object().for_connection(connection_id, [&](p2p_connection_context& cntx){
        return f(cntx, cntx.peer_id, cntx.support_flags);
      });
      if (result)
        return true;
    }
    return false;
  }
  //-----------------------------------------------------------------------------------

  bool node_server::is_remote_host_allowed(const epee::net_utils::network_address &address, time_t *t)
  {
    LOCK_RECURSIVE_MUTEX(m_blocked_hosts_lock);

    const time_t now = time(nullptr);

    // look in the hosts list
    auto it = m_blocked_hosts.find(address.host_str());
    if (it != m_blocked_hosts.end())
    {
      if (now >= it->second)
      {
        m_blocked_hosts.erase(it);
        LOG_CATEGORY_COLOR
          (
           epee::LogLevel::Info
           , epee::GLOBAL_CATEGORY
           , epee::cyan
           , std::string()
           + "Host "
           + address.host_str()
           + " unblocked."
           );
        it = m_blocked_hosts.end();
      }
      else
      {
        if (t)
          *t = it->second - now;
        return false;
      }
    }

    // not found in hosts, allowed
    return true;
  }
  //-----------------------------------------------------------------------------------

  bool node_server::block_host(epee::net_utils::network_address addr, time_t seconds)
  {
    if(!addr.is_blockable())
      return false;

    const time_t now = time(nullptr);

    LOCK_RECURSIVE_MUTEX(m_blocked_hosts_lock);
    time_t limit;
    if (now > std::numeric_limits<time_t>::max() - seconds)
      limit = std::numeric_limits<time_t>::max();
    else
      limit = now + seconds;
    const std::string host_str = addr.host_str();
    m_blocked_hosts[host_str] = limit;

    // drop any connection to that address. This should only have to look into
    // the zone related to the connection, but really make sure everything is
    // swept ...
    std::vector<boost::uuids::uuid> conns;
    for(auto& zone : m_network_zones)
    {
      zone.second.m_net_server.get_config_object().foreach_connection([&](const p2p_connection_context& cntxt)
      {
        if (cntxt.m_remote_address.is_same_host(addr))
        {
          conns.push_back(cntxt.m_connection_id);
        }
        return true;
      });

      peerlist_entry pe{};
      pe.adr = addr;
      zone.second.m_peerlist.remove_from_peer_white(pe);
      zone.second.m_peerlist.remove_from_peer_gray(pe);
      zone.second.m_peerlist.remove_from_peer_anchor(addr);

      for (const auto &c: conns)
        zone.second.m_net_server.get_config_object().close(c);

      conns.clear();
    }

    LOG_CATEGORY_COLOR
      (
       epee::LogLevel::Info
       , epee::GLOBAL_CATEGORY
       , epee::cyan
       , "Host "
       + host_str
       + " blocked."
       );
    return true;
  }
  //-----------------------------------------------------------------------------------

  bool node_server::unblock_host(const epee::net_utils::network_address &address)
  {
    LOCK_RECURSIVE_MUTEX(m_blocked_hosts_lock);
    auto i = m_blocked_hosts.find(address.host_str());
    if (i == m_blocked_hosts.end())
      return false;
    m_blocked_hosts.erase(i);
    LOG_CATEGORY_COLOR
      (
       epee::LogLevel::Info
       , epee::GLOBAL_CATEGORY
       , epee::cyan
       , "Host "
       + address.host_str()
       + " unblocked."
       );
    return true;
  }

  //-----------------------------------------------------------------------------------

  bool node_server::add_host_fail(const epee::net_utils::network_address &address, unsigned int score)
  {
    if(!address.is_blockable())
      return false;

    {
      const std::scoped_lock<std::mutex> guard(m_host_fails_score_lock);
      const auto host = address.host_str();

      if (m_host_fails_score.contains(host))
        {
          m_host_fails_score[host] += score;
        }
      else
        {
          m_host_fails_score[host] = score;
        }

      const uint64_t current_score = m_host_fails_score[host];

      LOG_DEBUG
        (
         "Host "
         + host
         + " current score="
         + std::to_string(current_score)
         );

      if(current_score <= P2P_IP_FAILS_BEFORE_BLOCK) {
        return true;
      }

      m_host_fails_score[host] = P2P_IP_FAILS_BEFORE_BLOCK/2;
    }

    block_host(address);
    return true;
  }
  //-----------------------------------------------------------------------------------

  bool node_server::handle_command_line
  (
   const int64_t max_in_peers
   , const int64_t max_out_peers
   , const std::vector<std::string> anonymous_inbound
   , const std::vector<std::string> proxy_args
   , const std::vector<std::string> add_peers
   , const std::vector<std::string> add_exclusive_nodes
   , const std::vector<std::string> add_priority_nodes
   , const std::string p2p_bind_ip
   , const uint32_t p2p_port
   , const std::string p2p_bind_ip_ipv6
   , const uint32_t p2p_port_ipv6
   )
  {
    network_zone& public_zone = m_network_zones[epee::net_utils::zone::public_];
    public_zone.m_connect = &public_connect;
    public_zone.m_bind_ip = p2p_bind_ip;
    public_zone.m_bind_ipv6_address = p2p_bind_ip_ipv6;
    public_zone.m_port = std::to_string(p2p_port);
    public_zone.m_port_ipv6 = std::to_string(p2p_port_ipv6);

    public_zone.m_can_pingback = true;

    public_zone.m_notifier = cryptonote::levin::notify{
      public_zone.m_net_server.get_config_shared(), true
    };

    for(const std::string& pr_str: add_peers)
      {
        nodetool::peerlist_entry pe{};
        pe.id = crypto::rand<uint64_t>();
        const uint16_t default_port = cryptonote::get_config(m_nettype).P2P_DEFAULT_PORT;
        expect<epee::net_utils::network_address> adr = net::get_network_address(pr_str, default_port);
        if (adr)
          {
            add_zone(adr->get_zone());
            pe.adr = std::move(*adr);
            m_command_line_peers.push_back(std::move(pe));
            continue;
          }
        ASSERT_OR_LOG_ERROR_AND_RETURN
          (
           adr == net::error::unsupported_address
           , false
           , "Bad address (\""
           + pr_str
           + "\"): "
           + adr.error().message()
           );

        std::vector<epee::net_utils::network_address> resolved_addrs;
        bool r = append_net_address(resolved_addrs, pr_str, default_port);
        ASSERT_OR_LOG_ERROR_AND_RETURN
          (
           r
           , false
           , "Failed to parse or resolve address from string: "
           + pr_str
           );
        for (const epee::net_utils::network_address& addr : resolved_addrs)
          {
            pe.id = crypto::rand<uint64_t>();
            pe.adr = addr;
            m_command_line_peers.push_back(pe);
          }
      }

    if (!parse_peers_and_add_to_container
        (add_exclusive_nodes
         , m_exclusive_peers
         )
        )
      {
      return false;
    }

    if (!parse_peers_and_add_to_container
        (
         add_priority_nodes
         , m_priority_peers
         )
        ) {
      return false;
    }

    if ( !set_max_out_peers(public_zone, max_out_peers) )
      return false;
    else
      m_payload_handler.set_max_out_peers(public_zone.m_config.m_net_config.max_out_connection_count);


    if ( !set_max_in_peers(public_zone, max_in_peers ) )
      return false;

    auto proxies = get_proxies(proxy_args);
    if (!proxies)
      return false;

    for (auto& proxy : *proxies)
    {
      network_zone& zone = add_zone(proxy.zone);
      if (zone.m_connect != nullptr && zone.m_connect != &public_connect)
      {
        LOG_ERROR
          (
           "Listed --"
           + std::string(arg_proxy_name)
           + " twice with "
           + epee::net_utils::zone_to_string(proxy.zone)
           );
        return false;
      }
      zone.m_connect = &socks_connect;
      zone.m_proxy_address = std::move(proxy.address);

      if (!set_max_out_peers(zone, proxy.max_connections))
        return false;


      if (proxy.zone == epee::net_utils::zone::public_) {
        if (m_hide_my_port == false) {
          LOG_INFO("PUBLIC SOCKS USED: hide my port set to true");
          m_hide_my_port = true;
        }
      }

      zone.m_notifier = cryptonote::levin::notify{
        zone.m_net_server.get_config_shared(), false
      };
    }

    for (const auto& zone : m_network_zones)
    {
      if (zone.second.m_connect == nullptr)
      {
        LOG_ERROR
          (
           "Set outgoing peer for "
           + std::string(epee::net_utils::zone_to_string(zone.first))
           + " but did not set --"
           + std::string(arg_proxy_name)
           );
        return false;
      }
    }

    auto inbounds = get_anonymous_inbounds(anonymous_inbound);
    if (!inbounds)
      return false;

    const std::size_t tx_relay_zones = m_network_zones.size();
    for (auto& inbound : *inbounds)
    {
      network_zone& zone = add_zone(inbound.our_address.get_zone());

      if (!zone.m_bind_ip.empty())
      {
        LOG_ERROR
          (
           "Listed --"
           + std::string(arg_anonymous_inbound_name)
           + " twice with "
           + epee::net_utils::zone_to_string(inbound.our_address.get_zone())
           + " network"
           );
        return false;
      }

      if (zone.m_connect == nullptr && tx_relay_zones <= 1)
      {
        LOG_ERROR
          (
           "Listed --"
           + std::string(arg_anonymous_inbound_name)
           + " without listing any --"
           + std::string(arg_proxy_name)
           + ". The latter is necessary for sending local txes over anonymity networks"
           );
        return false;
      }

      zone.m_bind_ip = std::move(inbound.local_ip);
      zone.m_port = std::move(inbound.local_port);
      zone.m_net_server.set_default_remote(std::move(inbound.default_remote));
      zone.m_our_address = std::move(inbound.our_address);

      if (!set_max_in_peers(zone, inbound.max_connections))
        return false;
    }

    return true;
  }
  //-----------------------------------------------------------------------------------

  typename node_server::network_zone& node_server::add_zone(const epee::net_utils::zone zone)
  {
    const auto zone_ = m_network_zones.lower_bound(zone);
    if (zone_ != m_network_zones.end() && zone_->first == zone)
      return zone_->second;

    network_zone& public_zone = m_network_zones[epee::net_utils::zone::public_];
    return m_network_zones.emplace_hint(zone_, std::piecewise_construct, std::make_tuple(zone), std::tie(public_zone.m_net_server.get_io_service()))->second;
  }
  //-----------------------------------------------------------------------------------

  bool node_server::init
  (
   const int64_t max_in_peers
   , const int64_t max_out_peers
   , const std::vector<std::string> anonymous_inbounds
   , const std::vector<std::string> proxies
   , const std::vector<std::string> add_peers
   , const std::vector<std::string> add_exclusive_nodes
   , const std::vector<std::string> add_priority_nodes
   , const std::string p2p_bind_ip
   , const uint32_t p2p_port
   , const std::string p2p_bind_ip_ipv6
   , const uint32_t p2p_port_ipv6
   )
  {
    bool res = handle_command_line
      (
       max_in_peers
       , max_out_peers
       , anonymous_inbounds
       , proxies
       , add_peers
       , add_exclusive_nodes
       , add_priority_nodes
       , p2p_bind_ip
       , p2p_port
       , p2p_bind_ip_ipv6
       , p2p_port_ipv6
       );
    ASSERT_OR_LOG_ERROR_AND_RETURN(res, false, "Failed to handle command line");

    if (m_nettype == cryptonote::TESTNET)
    {
      memcpy(&m_network_id, &::cryptonote::testnet.NETWORK_ID, 16);
    }
    else
    {
      memcpy(&m_network_id, &::cryptonote::mainnet.NETWORK_ID, 16);
    }

    network_zone& public_zone = m_network_zones.at(epee::net_utils::zone::public_);

    if (
        (m_nettype == cryptonote::MAINNET && public_zone.m_port != std::to_string(::cryptonote::mainnet.P2P_DEFAULT_PORT))
        ||
        (m_nettype == cryptonote::TESTNET && public_zone.m_port != std::to_string(::cryptonote::testnet.P2P_DEFAULT_PORT))
        )
      {
        m_config_folder = m_config_folder + "/" + public_zone.m_port;
      }


    res = init_config();
    ASSERT_OR_LOG_ERROR_AND_RETURN(res, false, "Failed to init config.");

    for (auto& zone : m_network_zones)
    {
      res = zone.second.m_peerlist.init(m_peerlist_storage.take_zone(zone.first), m_allow_local_ip);
      ASSERT_OR_LOG_ERROR_AND_RETURN(res, false, "Failed to init peerlist.");
    }

    for(const auto& p: m_command_line_peers)
      m_network_zones.at(p.adr.get_zone()).m_peerlist.append_with_peer_white(p);

    //only in case if we really sure that we have external visible ip
    m_have_address = true;

    //configure self

    // from here onwards, it's online stuff
    if (m_offline)
      return res;

    //try to bind
    for (auto& zone : m_network_zones)
    {
      zone.second.m_net_server.get_config_object().set_handler(this);
      zone.second.m_net_server.get_config_object().m_invoke_timeout = P2P_DEFAULT_INVOKE_TIMEOUT;

      if (!zone.second.m_allow_inbound) {
        LOG_INFO
          (
           "Inbound disabled (IPv4) on "
           + zone.second.m_bind_ip
           + ":"
           + zone.second.m_port
           );
        continue;
      }

      if (!zone.second.m_bind_ip.empty())
      {
        std::string ipv6_addr = "";
        std::string ipv6_port = "";
        zone.second.m_net_server.set_connection_filter(this);
        LOG_GLOBAL
          (
           "Binding P2P (IPv4) on "
           + zone.second.m_bind_ip
           + ":"
           + zone.second.m_port
           );
        if (!zone.second.m_bind_ipv6_address.empty() && m_use_ipv6)
        {
          ipv6_addr = zone.second.m_bind_ipv6_address;
          ipv6_port = zone.second.m_port_ipv6;
          LOG_GLOBAL
            (
             "Binding P2P (IPv6) on "
             + zone.second.m_bind_ipv6_address
             + ":"
             + zone.second.m_port_ipv6
             );
        }
        res = zone.second.m_net_server.init_server(zone.second.m_port, zone.second.m_bind_ip, ipv6_port, ipv6_addr, m_use_ipv6, m_require_ipv4);
        ASSERT_OR_LOG_ERROR_AND_RETURN(res, false, "Failed to bind P2P server");
      }
    }

    if (public_zone.m_allow_inbound) {
    m_listening_port = public_zone.m_net_server.get_binded_port();
    LOG_COLOR
      (
       epee::green
       , epee::LogLevel::Info
       , std::string()
       + "Net service bound (IPv4) to "
       + public_zone.m_bind_ip
       + ":"
       + std::to_string(m_listening_port)
       );
    if (m_use_ipv6)
    {
      m_listening_port_ipv6 = public_zone.m_net_server.get_binded_port_ipv6();
      LOG_COLOR
        (
         epee::green
         , epee::LogLevel::Info
         , std::string()
         + "Net service bound (IPv6) to "
         + public_zone.m_bind_ipv6_address
         + ":"
         + std::to_string(m_listening_port_ipv6)
         );
    }
    }

    if(m_external_port) {
      LOG_DEBUG
        (
         "External port defined as "
         + std::to_string(m_external_port)
         );
    }

    return res;
  }
  //-----------------------------------------------------------------------------------

  typename node_server::payload_net_handler& node_server::get_payload_object()
  {
    return m_payload_handler;
  }
  //-----------------------------------------------------------------------------------

  bool node_server::run()
  {
    // creating thread to log number of connections
    mPeersLoggerThread = std::make_unique<std::thread>
    (
      [&]
      {
        _note("Thread monitor number of peers - start");
        const network_zone& public_zone = m_network_zones.at(epee::net_utils::zone::public_);
        while (!is_closing && !public_zone.m_net_server.is_stop_signal_sent())
        {
          // main loop of thread
          //number_of_peers = m_net_server.get_config_object().get_connections_count();
          for (auto& zone : m_network_zones)
          {
            unsigned int number_of_in_peers = 0;
            unsigned int number_of_out_peers = 0;
            zone.second.m_net_server.get_config_object().foreach_connection
              (
              [&](const p2p_connection_context& cntxt)
              {
                if (cntxt.m_is_income)
                  {
                    ++number_of_in_peers;
                  }
                else
                  {
                    ++number_of_out_peers;
                  }
                return true;
              }
              ); // lambda
            zone.second.m_current_number_of_in_peers = number_of_in_peers;
            zone.second.m_current_number_of_out_peers = number_of_out_peers;
          }
          std::this_thread::sleep_for(std::chrono::seconds(1));
        } // main loop of thread
       _note("Thread monitor number of peers - done");
      }
    );

    network_zone& public_zone = m_network_zones.at(epee::net_utils::zone::public_);
    public_zone.m_net_server.add_idle_handler(std::bind(&node_server::idle_worker, this), 1000);
    public_zone.m_net_server.add_idle_handler(std::bind(&t_payload_net_handler::on_idle, &m_payload_handler), 1000);

    //here you can set worker threads count
    int thrds_count = 10;
    //go to loop
    LOG_INFO("Run net_service loop( " + std::to_string(thrds_count) + " threads)...");
    if(!public_zone.m_net_server.run_server(thrds_count, true))
    {
      LOG_ERROR("Failed to run net tcp server!");
    }

    LOG_INFO("net_service loop stopped.");
    return true;
  }
  //-----------------------------------------------------------------------------------

  uint64_t node_server::get_public_connections_count()
  {
    auto public_zone = m_network_zones.find(epee::net_utils::zone::public_);
    if (public_zone == m_network_zones.end())
      return 0;
    return public_zone->second.m_net_server.get_config_object().get_connections_count();
  }
  //-----------------------------------------------------------------------------------

  bool node_server::deinit()
  {
    kill();

    if (!m_offline)
    {
      for(auto& zone : m_network_zones)
        zone.second.m_net_server.deinit_server();
    }
    return store_config();
  }
  //-----------------------------------------------------------------------------------

  bool node_server::store_config()
  {
    TRY_ENTRY();

    if (!tools::create_directories_if_necessary(m_config_folder))
    {
      LOG_WARNING
        (
         "Failed to create data directory \""
         + m_config_folder
         );
      return false;
    }

    CATCH_ENTRY_L0("node_server::store", false);
    return true;
  }
  //-----------------------------------------------------------------------------------

  bool node_server::send_stop_signal()
  {
    LOG_DEBUG("[node] sending stop signal");
    for (auto& zone : m_network_zones)
        zone.second.m_net_server.send_stop_signal();
    LOG_DEBUG("[node] Stop signal sent");

    for (auto& zone : m_network_zones)
    {
      std::list<boost::uuids::uuid> connection_ids;
      zone.second.m_net_server.get_config_object().foreach_connection([&](const p2p_connection_context& cntxt) {
        connection_ids.push_back(cntxt.m_connection_id);
        return true;
      });
      for (const auto &connection_id: connection_ids)
        zone.second.m_net_server.get_config_object().close(connection_id);
    }
    m_payload_handler.stop();
    return true;
  }
  //-----------------------------------------------------------------------------------

  bool node_server::do_handshake_with_peer(peerid_type& pi, p2p_connection_context& context_, bool just_take_peerlist)
  {
    network_zone& zone = m_network_zones.at(context_.m_remote_address.get_zone());

    typename COMMAND_HANDSHAKE::request arg;
    typename COMMAND_HANDSHAKE::response rsp;
    get_local_node_data(arg.node_data, zone);
    m_payload_handler.get_payload_sync_data(arg.payload_data);

    epee::simple_event ev;
    std::atomic<bool> hsh_result(false);
    bool timeout = false;

    bool r = epee::net_utils::async_invoke_remote_command2<typename COMMAND_HANDSHAKE::response>(context_, COMMAND_HANDSHAKE::ID, arg, zone.m_net_server.get_config_object(),
      [this, &pi, &ev, &hsh_result, &just_take_peerlist, &context_, &timeout](int code, const typename COMMAND_HANDSHAKE::response& rsp, p2p_connection_context& context)
    {
      const auto scope_exit_handler = epee::misc_utils::create_scope_leave_handler([&]{ev.raise();});

      if(code < 0)
      {
        LOG_WARNING_CC
          (
           context
           , "COMMAND_HANDSHAKE invoke failed. ("
           + std::to_string(code)
           + ", "
           + epee::levin::get_err_descr(code)
           + ")"
           );
        if (code == epee::levin::LEVIN_ERROR_CONNECTION_TIMEDOUT || code == epee::levin::LEVIN_ERROR_CONNECTION_DESTROYED)
          timeout = true;
        return;
      }

      if(rsp.node_data.network_id != m_network_id)
      {
        LOG_WARNING_CC
          (
           context
           , "COMMAND_HANDSHAKE Failed, wrong network!  ("
           + boost::uuids::to_string(rsp.node_data.network_id)
           + "), closing connection."
           );
        return;
      }

      if(!handle_remote_peerlist(rsp.local_peerlist_new, context))
      {
        LOG_WARNING_CC(context, "COMMAND_HANDSHAKE: failed to handle_remote_peerlist(...), closing connection.");
        add_host_fail(context.m_remote_address);
        return;
      }
      hsh_result = true;
      if(!just_take_peerlist)
      {
        if(!m_payload_handler.process_payload_sync_data(rsp.payload_data, context, true))
        {
          LOG_WARNING_CC(context, "COMMAND_HANDSHAKE invoked, but process_payload_sync_data returned false, dropping connection.");
          hsh_result = false;
          return;
        }

        pi = context.peer_id = rsp.node_data.peer_id;
        const auto azone = context.m_remote_address.get_zone();
        network_zone& zone = m_network_zones.at(azone);
        zone.m_peerlist.set_peer_just_seen(rsp.node_data.peer_id, context.m_remote_address);

        // move
        if(azone == epee::net_utils::zone::public_ && rsp.node_data.peer_id == zone.m_config.m_peer_id)
        {
          LOG_DEBUG_CC(context, "Connection to self detected, dropping connection");
          hsh_result = false;
          return;
        }
        LOG_INFO(context.to_str() + "New connection handshaked.");
        LOG_DEBUG_CC(context, " COMMAND_HANDSHAKE INVOKED OK");
      }else
      {
        LOG_DEBUG_CC(context, " COMMAND_HANDSHAKE(AND CLOSE) INVOKED OK");
      }
      context_ = context;
    }, P2P_DEFAULT_HANDSHAKE_INVOKE_TIMEOUT);

    if(r)
    {
      ev.wait();
    }

    if(!hsh_result)
    {
      LOG_WARNING_CC(context_, "COMMAND_HANDSHAKE Failed");
      if (!timeout)
        zone.m_net_server.get_config_object().close(context_.m_connection_id);
    }
    else if (!just_take_peerlist)
    {
      try_get_support_flags(context_, [](p2p_connection_context& flags_context, const uint32_t& support_flags)
      {
        flags_context.support_flags = support_flags;
      });
    }

    return hsh_result;
  }
  //-----------------------------------------------------------------------------------

  bool node_server::do_peer_timed_sync(const epee::net_utils::connection_context_base& context_, peerid_type peer_id)
  {
    COMMAND_TIMED_SYNC::request arg{};
    m_payload_handler.get_payload_sync_data(arg.payload_data);

    network_zone& zone = m_network_zones.at(context_.m_remote_address.get_zone());
    bool r = epee::net_utils::async_invoke_remote_command2<typename COMMAND_TIMED_SYNC::response>(context_, COMMAND_TIMED_SYNC::ID, arg, zone.m_net_server.get_config_object(),
      [this](int code, const typename COMMAND_TIMED_SYNC::response& rsp, p2p_connection_context& context)
    {
      context.m_in_timedsync = false;
      if(code < 0)
      {
        LOG_WARNING_CC
          (
           context
           , "COMMAND_TIMED_SYNC invoke failed. ("
           + std::to_string(code)
           + ", "
           + epee::levin::get_err_descr(code)
           + ")"
           );
        return;
      }

      if(!handle_remote_peerlist(rsp.local_peerlist_new, context))
      {
        LOG_WARNING_CC(context, "COMMAND_TIMED_SYNC: failed to handle_remote_peerlist(...), closing connection.");
        const auto remote_address = context.m_remote_address;
        m_network_zones.at(context.m_remote_address.get_zone()).m_net_server.get_config_object().close(context.m_connection_id );
        add_host_fail(remote_address);
      }
      if(!context.m_is_income)
        m_network_zones.at(context.m_remote_address.get_zone()).m_peerlist.set_peer_just_seen(context.peer_id, context.m_remote_address);
      if (!m_payload_handler.process_payload_sync_data(rsp.payload_data, context, false))
      {
        m_network_zones.at(context.m_remote_address.get_zone()).m_net_server.get_config_object().close(context.m_connection_id );
      }
    });

    if(!r)
    {
      LOG_WARNING_CC(context_, "COMMAND_TIMED_SYNC Failed");
      return false;
    }
    return true;
  }
  //-----------------------------------------------------------------------------------

  bool node_server::is_peer_used(const peerlist_entry& peer)
  {
    const auto zone = peer.adr.get_zone();
    const auto server = m_network_zones.find(zone);
    if (server == m_network_zones.end())
      return false;

    const bool is_public = (zone == epee::net_utils::zone::public_);
    if(is_public && server->second.m_config.m_peer_id == peer.id)
      return true;//dont make connections to ourself

    bool used = false;
    server->second.m_net_server.get_config_object().foreach_connection([&, is_public](const p2p_connection_context& cntxt)
    {
      if((is_public && cntxt.peer_id == peer.id) || (!cntxt.m_is_income && peer.adr == cntxt.m_remote_address))
      {
        used = true;
        return false;//stop enumerating
      }
      return true;
    });
    return used;
  }
  //-----------------------------------------------------------------------------------

  bool node_server::is_peer_used(const anchor_peerlist_entry& peer)
  {
    const auto zone = peer.adr.get_zone();
    const auto server = m_network_zones.find(zone);
    if (server == m_network_zones.end())
      return false;

    const bool is_public = (zone == epee::net_utils::zone::public_);
    if(is_public && server->second.m_config.m_peer_id == peer.id)
      return true;//dont make connections to ourself

    bool used = false;
    server->second.m_net_server.get_config_object().foreach_connection([&, is_public](const p2p_connection_context& cntxt)
    {
      if((is_public && cntxt.peer_id == peer.id) || (!cntxt.m_is_income && peer.adr == cntxt.m_remote_address))
      {
        used = true;
        return false;//stop enumerating
      }
      return true;
    });
    return used;
  }
  //-----------------------------------------------------------------------------------

  bool node_server::is_addr_connected(const epee::net_utils::network_address& peer)
  {
    const auto zone = m_network_zones.find(peer.get_zone());
    if (zone == m_network_zones.end())
      return false;

    bool connected = false;
    zone->second.m_net_server.get_config_object().foreach_connection([&](const p2p_connection_context& cntxt)
    {
      if(!cntxt.m_is_income && peer == cntxt.m_remote_address)
      {
        connected = true;
        return false;//stop enumerating
      }
      return true;
    });

    return connected;
  }

  void LOG_PRINT_CC_PRIORITY_NODE
  (
   const bool priority
   , const std::string msg
   )
  {
    if (priority) {
      LOG_VERBOSE("[priority]" + msg);
    } else {
      LOG_VERBOSE(msg);
    }
  }


  bool node_server::try_to_connect_and_handshake_with_new_peer(const epee::net_utils::network_address& na, bool just_take_peerlist, uint64_t last_seen_stamp, PeerType peer_type, uint64_t first_seen_stamp)
  {
    const auto i = m_network_zones.find(na.get_zone());
    if (i == m_network_zones.end())
    {
      LOG_ERROR("Tried connecting to address for disabled network");
      return false;
    }
    network_zone& zone = i->second;
    if (zone.m_connect == nullptr) // outgoing connections in zone not possible
      return false;

    if (zone.m_our_address == na)
      return false;

    if (zone.m_current_number_of_out_peers == zone.m_config.m_net_config.max_out_connection_count) // out peers limit
    {
      return false;
    }

    LOG_DEBUG
      (
       "Connecting to "
       + na.str()
       +  "..."
       );

    auto con = zone.m_connect(zone, na);
    if(!con)
    {
      bool is_priority = is_priority_node(na);
      LOG_PRINT_CC_PRIORITY_NODE
        (
         is_priority
         , "Connect failed to "
         + na.str()
         );
      record_addr_failed(na);
      return false;
    }

    con->m_anchor = peer_type == anchor;
    peerid_type pi{};
    bool res = do_handshake_with_peer(pi, *con, just_take_peerlist);

    if(!res)
    {
      bool is_priority = is_priority_node(na);
      LOG_PRINT_CC_PRIORITY_NODE
        (
         is_priority
         , con->to_str()
         + "Failed to HANDSHAKE with peer 1: "
         + na.str()
         );
      record_addr_failed(na);
      return false;
    }

    if(just_take_peerlist)
    {
      zone.m_net_server.get_config_object().close(con->m_connection_id);
      LOG_DEBUG(na.str() + "CONNECTION HANDSHAKED OK AND CLOSED.");
      return true;
    }

    peerlist_entry pe_local{};
    pe_local.adr = na;
    pe_local.id = pi;
    time_t last_seen;
    time(&last_seen);
    pe_local.last_seen = static_cast<int64_t>(last_seen);
    zone.m_peerlist.append_with_peer_white(pe_local);
    //update last seen and push it to peerlist manager

    anchor_peerlist_entry ape{};
    ape.adr = na;
    ape.id = pi;
    ape.first_seen = first_seen_stamp ? first_seen_stamp : time(nullptr);

    zone.m_peerlist.append_with_peer_anchor(ape);

    LOG_DEBUG(con->to_str() + "CONNECTION HANDSHAKED OK.");
    return true;
  }


  bool node_server::check_connection_and_handshake_with_peer(const epee::net_utils::network_address& na, uint64_t last_seen_stamp)
  {
    network_zone& zone = m_network_zones.at(na.get_zone());
    if (zone.m_connect == nullptr)
      return false;

    LOG_PRINT_L1
      (
       std::string("Connecting to ")
       + na.str()
       + "(last_seen: "
       + (last_seen_stamp ? std::to_string(time(NULL) - last_seen_stamp):"never")
       + ")..."
       );

    auto con = zone.m_connect(zone, na);
    if (!con) {
      bool is_priority = is_priority_node(na);

      LOG_PRINT_CC_PRIORITY_NODE
        (
         is_priority
         , p2p_connection_context{}.to_str()
         + "Connect failed to "
         + na.str()
         );
      record_addr_failed(na);

      return false;
    }

    con->m_anchor = false;
    peerid_type pi{};
    const bool res = do_handshake_with_peer(pi, *con, true);
    if (!res) {
      bool is_priority = is_priority_node(na);

      LOG_PRINT_CC_PRIORITY_NODE
        (
         is_priority
         , con->to_str()
         + "Failed to HANDSHAKE with peer 2: "
         + na.str()
         );
      record_addr_failed(na);
      return false;
    }

    zone.m_net_server.get_config_object().close(con->m_connection_id);

    LOG_DEBUG(na.str() + "CONNECTION HANDSHAKED OK AND CLOSED.");

    return true;
  }

  //-----------------------------------------------------------------------------------

  void node_server::record_addr_failed(const epee::net_utils::network_address& addr)
  {
    LOCK_RECURSIVE_MUTEX(m_conn_fails_cache_lock);
    m_conn_fails_cache[addr.host_str()] = time(NULL);
  }
  //-----------------------------------------------------------------------------------

  bool node_server::is_addr_recently_failed(const epee::net_utils::network_address& addr)
  {
    LOCK_RECURSIVE_MUTEX(m_conn_fails_cache_lock);
    auto it = m_conn_fails_cache.find(addr.host_str());
    if(it == m_conn_fails_cache.end())
      return false;

    if(time(NULL) - it->second > P2P_FAILED_ADDR_FORGET_SECONDS)
      return false;
    else
      return true;
  }
  //-----------------------------------------------------------------------------------

  bool node_server::make_new_connection_from_anchor_peerlist(const std::vector<anchor_peerlist_entry>& anchor_peerlist)
  {
    for (const auto& pe: anchor_peerlist) {
      _note
        (
         "Considering connecting (out) to anchor peer: "
         + peerid_to_string(pe.id)
         + " "
         + pe.adr.str()
         );

      if(is_peer_used(pe)) {
        _note("Peer is used");
        continue;
      }

      if(!is_remote_host_allowed(pe.adr)) {
        continue;
      }

      if(is_addr_recently_failed(pe.adr)) {
        continue;
      }

      LOG_DEBUG
        (
         "Selected peer: "
         + peerid_to_string(pe.id)
         + " "
         + pe.adr.str()
         );

      if(!try_to_connect_and_handshake_with_new_peer(pe.adr, false, 0, anchor, pe.first_seen)) {
        _note("Handshake failed");
        continue;
      }

      return true;
    }

    return false;
  }
  //-----------------------------------------------------------------------------------

  bool node_server::make_new_connection_from_peerlist(network_zone& zone, bool use_white_list)
  {
    size_t max_random_index = 0;

    std::set<size_t> tried_peers;

    size_t try_count = 0;
    size_t rand_count = 0;
    while(rand_count < (max_random_index+1)*3 &&  try_count < 10 && !zone.m_net_server.is_stop_signal_sent())
    {
      ++rand_count;
      size_t random_index;
      const uint32_t next_needed_pruning_stripe = 0;

      // build a set of all the /16 we're connected to, and prefer a peer that's not in that set
      std::set<uint32_t> classB;
      if (&zone == &m_network_zones.at(epee::net_utils::zone::public_)) // at returns reference, not copy
      {
        zone.m_net_server.get_config_object().foreach_connection([&](const p2p_connection_context& cntxt)
        {
          if (cntxt.m_remote_address.get_type_id() == epee::net_utils::ipv4_network_address::get_type_id())
          {

            const epee::net_utils::network_address na = cntxt.m_remote_address;
            const uint32_t actual_ip = na.as<const epee::net_utils::ipv4_network_address>().ip();
            classB.insert(actual_ip & 0x0000ffff);
          }
          else if (cntxt.m_remote_address.get_type_id() == epee::net_utils::ipv6_network_address::get_type_id())
          {
            const epee::net_utils::network_address na = cntxt.m_remote_address;
            const boost::asio::ip::address_v6 &actual_ip = na.as<const epee::net_utils::ipv6_network_address>().ip();
            if (actual_ip.is_v4_mapped())
            {
              boost::asio::ip::address_v4 v4ip = make_address_v4_from_v6(actual_ip);
              uint32_t actual_ipv4;
              memcpy(&actual_ipv4, v4ip.to_bytes().data(), sizeof(actual_ipv4));
              classB.insert(actual_ipv4 & ntohl(0xffff0000));
            }
          }
          return true;
        });
      }

      auto get_host_string = [](const epee::net_utils::network_address &address) {
        if (address.get_type_id() == epee::net_utils::ipv6_network_address::get_type_id())
        {
          boost::asio::ip::address_v6 actual_ip = address.as<const epee::net_utils::ipv6_network_address>().ip();
          if (actual_ip.is_v4_mapped())
          {
            boost::asio::ip::address_v4 v4ip = make_address_v4_from_v6(actual_ip);
            uint32_t actual_ipv4;
            memcpy(&actual_ipv4, v4ip.to_bytes().data(), sizeof(actual_ipv4));
            return epee::net_utils::ipv4_network_address(actual_ipv4, 0).host_str();
          }
        }
        return address.host_str();
      };
      std::set<std::string> hosts_added;
      std::vector<size_t> filtered;
      const size_t limit = use_white_list ? 20 : std::numeric_limits<size_t>::max();
      for (int step = 0; step < 2; ++step)
      {
        bool skip_duplicate_class_B = step == 0;
        size_t idx = 0, skipped = 0;
        zone.m_peerlist.foreach (use_white_list, [&classB, &filtered, &idx, &skipped, skip_duplicate_class_B, limit, &hosts_added, &get_host_string](const peerlist_entry &pe){
          if (filtered.size() >= limit)
            return false;
          bool skip = false;
          if (skip_duplicate_class_B && pe.adr.get_type_id() == epee::net_utils::ipv4_network_address::get_type_id())
          {
            const epee::net_utils::network_address na = pe.adr;
            uint32_t actual_ip = na.as<const epee::net_utils::ipv4_network_address>().ip();
            skip = classB.find(actual_ip & 0x0000ffff) != classB.end();
          }
          else if (skip_duplicate_class_B && pe.adr.get_type_id() == epee::net_utils::ipv6_network_address::get_type_id())
          {
            const epee::net_utils::network_address na = pe.adr;
            const boost::asio::ip::address_v6 &actual_ip = na.as<const epee::net_utils::ipv6_network_address>().ip();
            if (actual_ip.is_v4_mapped())
            {
              boost::asio::ip::address_v4 v4ip = make_address_v4_from_v6(actual_ip);
              uint32_t actual_ipv4;
              memcpy(&actual_ipv4, v4ip.to_bytes().data(), sizeof(actual_ipv4));
              skip = classB.find(actual_ipv4 & ntohl(0xffff0000)) != classB.end();
            }
          }

          // consider each host once, to avoid giving undue inflence to hosts running several nodes
          if (!skip)
          {
            const auto i = hosts_added.find(get_host_string(pe.adr));
            if (i != hosts_added.end())
              skip = true;
          }

          if (skip)
            ++skipped;
          filtered.push_back(idx);
          ++idx;
          hosts_added.insert(get_host_string(pe.adr));
          return true;
        });
        if (skipped == 0 || !filtered.empty())
          break;
        if (skipped)
          LOG_DEBUG
            (
             "Skipping "
             + std::to_string(skipped)
             + " possible peers as they share a class B with existing peers"
             );
      }
      if (filtered.empty())
      {
        LOG_DEBUG
          (
           std::string("No available peer in ")
           + (use_white_list ? "white" : "gray")
           + " list filtered by "
           + std::to_string(next_needed_pruning_stripe)
           );
        return false;
      }
      else
        random_index = crypto::rand_idx(filtered.size());

      ASSERT_OR_LOG_ERROR_AND_RETURN(random_index < filtered.size(), false, "random_index < filtered.size() failed!!");
      random_index = filtered[random_index];
      ASSERT_OR_LOG_ERROR_AND_RETURN(random_index < (use_white_list ? zone.m_peerlist.get_white_peers_count() : zone.m_peerlist.get_gray_peers_count()),
          false, "random_index < peers size failed!!");

      if(tried_peers.count(random_index))
        continue;

      tried_peers.insert(random_index);
      peerlist_entry pe{};
      bool r = use_white_list ? zone.m_peerlist.get_white_peer_by_index(pe, random_index):zone.m_peerlist.get_gray_peer_by_index(pe, random_index);
      ASSERT_OR_LOG_ERROR_AND_RETURN
        (
         r
         , false
         , "Failed to get random peer from peerlist(white:"
         + std::to_string(use_white_list)
         + ")"
         );

      ++try_count;

      _note
        (
         std::string("Considering connecting (out) to ")
         + (use_white_list ? "white" : "gray")
         + " list peer: "
         + peerid_to_string(pe.id)
         + " "
         + pe.adr.str()
         );

      if(zone.m_our_address == pe.adr)
        continue;

      if(is_peer_used(pe)) {
        _note("Peer is used");
        continue;
      }

      if(!is_remote_host_allowed(pe.adr))
        continue;

      if(is_addr_recently_failed(pe.adr))
        continue;

      LOG_DEBUG
        (
         "Selected peer: "
         + peerid_to_string(pe.id)
         + " "
         + pe.adr.str()
         );

      if(!try_to_connect_and_handshake_with_new_peer(pe.adr, false, pe.last_seen, use_white_list ? white : gray)) {
        _note("Handshake failed");
        continue;
      }

      return true;
    }
    return false;
  }
  //-----------------------------------------------------------------------------------

  bool node_server::connections_maker()
  {
    using zone_type = epee::net_utils::zone;

    if (m_offline) return true;

    if (!m_exclusive_peers.empty()) {
      if (!connect_to_peerlist(m_exclusive_peers)) {
        return false;
      }
      return true;
    }

    bool one_succeeded = false;

    for(auto& zone : m_network_zones)
      {
        if(!zone.second.m_peerlist.get_white_peers_count()) {
          LOG_DEBUG("No white peers");
          continue;
        }

        if (zone.first ==
            zone_type::public_ && !connect_to_peerlist(m_priority_peers))
          {
            LOG_DEBUG("can't connect to priority peers");
          }

        size_t base_expected_white_connections =
          (
           zone.second.m_config.m_net_config.max_out_connection_count
           *
           P2P_DEFAULT_WHITELIST_CONNECTIONS_PERCENT
           )/100
          ;

        // carefully avoid `continue` in nested loop

        while
          (
           get_outgoing_connections_count(zone.second)
           < zone.second.m_config.m_net_config.max_out_connection_count
           )
          {
            const size_t conn_count = get_outgoing_connections_count(zone.second);
            const size_t expected_white_connections = base_expected_white_connections;
            if(conn_count < expected_white_connections)
              {
                //start from anchor list
                if
                  (
                   get_outgoing_connections_count(zone.second)
                   < P2P_DEFAULT_ANCHOR_CONNECTIONS_COUNT
                   )
                  {
                    make_expected_connections_count
                      (
                       zone.second
                       , anchor
                       , P2P_DEFAULT_ANCHOR_CONNECTIONS_COUNT
                       );

                  }

                //then do white list
                if
                  (
                   get_outgoing_connections_count(zone.second)
                   < expected_white_connections
                   )
                  {
                    make_expected_connections_count
                      (
                       zone.second
                       , white
                       , expected_white_connections
                       );
                  }

                //then do grey list
                if
                  (
                   get_outgoing_connections_count(zone.second)
                   < zone.second.m_config.m_net_config.max_out_connection_count
                   )
                  {
                    make_expected_connections_count
                      (
                       zone.second
                       , gray
                       , zone.second.m_config.m_net_config.max_out_connection_count
                       );
                  }
              }
            else
              {
                //start from grey list
                if
                  (
                   get_outgoing_connections_count(zone.second)
                   < zone.second.m_config.m_net_config.max_out_connection_count
                   )
                  {
                    make_expected_connections_count
                      (
                       zone.second
                       , gray
                       , zone.second.m_config.m_net_config.max_out_connection_count
                       );
                  }

                //and then do white list
                if
                  (
                   get_outgoing_connections_count(zone.second)
                   < zone.second.m_config.m_net_config.max_out_connection_count
                   )
                  {
                    make_expected_connections_count
                      (
                       zone.second
                       , white
                       , zone.second.m_config.m_net_config.max_out_connection_count
                       );
                  }
              }

            if(zone.second.m_net_server.is_stop_signal_sent())
              return false;

            const size_t new_conn_count =
              get_outgoing_connections_count(zone.second);

            LOG_DEBUG
              (
               "new conn count: "
               + std::to_string(new_conn_count)
               + ", conn_count: "
               + std::to_string(conn_count)
               );

            if (new_conn_count <= conn_count)
              {
                // we did not make any connection, sleep a bit to avoid a busy loop in case we don't have

                // any peers to try, then break so we will try seeds to get more peers

                LOG_DEBUG("No new connections");
                std::this_thread::sleep_for(constant::p2p_sleep_duration);
                break;
              }
            else {
              one_succeeded = true;
            }
          }
      }

    return one_succeeded;
  }
  //-----------------------------------------------------------------------------------

  bool node_server::make_expected_connections_count(network_zone& zone, PeerType peer_type, size_t expected_connections)
  {
    if (m_offline)
      return false;

    std::vector<anchor_peerlist_entry> apl;

    if (peer_type == anchor) {
      zone.m_peerlist.get_and_empty_anchor_peerlist(apl);
    }

    size_t conn_count = get_outgoing_connections_count(zone);
    //add new connections from white peers
    if(conn_count < expected_connections)
    {
      if(zone.m_net_server.is_stop_signal_sent())
        return false;

      LOG_DEBUG
        (
         "Making expected connection, type "
         + std::to_string(peer_type)
         + ", "
         + std::to_string(conn_count)
         + "/"
         + std::to_string(expected_connections)
         + " connections"
         );

      if (peer_type == anchor && !make_new_connection_from_anchor_peerlist(apl)) {
        return false;
      }

      if (peer_type == white && !make_new_connection_from_peerlist(zone, true)) {
        return false;
      }

      if (peer_type == gray && !make_new_connection_from_peerlist(zone, false)) {
        return false;
      }
    }
    return true;
  }
  //-----------------------------------------------------------------------------------

  size_t node_server::get_public_outgoing_connections_count()
  {
    auto public_zone = m_network_zones.find(epee::net_utils::zone::public_);
    if (public_zone == m_network_zones.end())
      return 0;
    return get_outgoing_connections_count(public_zone->second);
  }
  //-----------------------------------------------------------------------------------

  size_t node_server::get_incoming_connections_count(network_zone& zone)
  {
    size_t count = 0;
    zone.m_net_server.get_config_object().foreach_connection([&](const p2p_connection_context& cntxt)
    {
      if(cntxt.m_is_income)
        ++count;
      return true;
    });
    return count;
  }
  //-----------------------------------------------------------------------------------

  size_t node_server::get_outgoing_connections_count(network_zone& zone)
  {
    size_t count = 0;
    zone.m_net_server.get_config_object().foreach_connection([&](const p2p_connection_context& cntxt)
    {
      if(!cntxt.m_is_income)
        ++count;
      return true;
    });
    return count;
  }
  //-----------------------------------------------------------------------------------

  size_t node_server::get_outgoing_connections_count()
  {
    size_t count = 0;
    for(auto& zone : m_network_zones)
      count += get_outgoing_connections_count(zone.second);
    return count;
  }
  //-----------------------------------------------------------------------------------

  size_t node_server::get_incoming_connections_count()
  {
    size_t count = 0;
    for (auto& zone : m_network_zones)
    {
      zone.second.m_net_server.get_config_object().foreach_connection([&](const p2p_connection_context& cntxt)
      {
        if(cntxt.m_is_income)
          ++count;
        return true;
      });
    }
    return count;
  }
  //-----------------------------------------------------------------------------------

  size_t node_server::get_public_white_peers_count()
  {
    auto public_zone = m_network_zones.find(epee::net_utils::zone::public_);
    if (public_zone == m_network_zones.end())
      return 0;
    return public_zone->second.m_peerlist.get_white_peers_count();
  }
  //-----------------------------------------------------------------------------------

  size_t node_server::get_public_gray_peers_count()
  {
    auto public_zone = m_network_zones.find(epee::net_utils::zone::public_);
    if (public_zone == m_network_zones.end())
      return 0;
    return public_zone->second.m_peerlist.get_gray_peers_count();
  }
  //-----------------------------------------------------------------------------------

  void node_server::get_peerlist(std::vector<peerlist_entry>& gray, std::vector<peerlist_entry>& white)
  {
    for (auto &zone: m_network_zones)
    {
      zone.second.m_peerlist.get_peerlist(gray, white); // appends
    }
  }
  //-----------------------------------------------------------------------------------

  bool node_server::idle_worker()
  {
    m_peer_handshake_idle_maker_interval.do_call(std::bind(&node_server::peer_sync_idle_maker, this));
    m_connections_maker_interval.do_call(std::bind(&node_server::connections_maker, this));
    m_gray_peerlist_housekeeping_interval.do_call(std::bind(&node_server::gray_peerlist_housekeeping, this));
    m_peerlist_store_interval.do_call(std::bind(&node_server::store_config, this));
    m_incoming_connections_interval.do_call(std::bind(&node_server::check_incoming_connections, this));
    return true;
  }
  //-----------------------------------------------------------------------------------

  bool node_server::check_incoming_connections()
  {
    if (m_offline)
      return true;

    const auto public_zone = m_network_zones.find(epee::net_utils::zone::public_);
    if (public_zone != m_network_zones.end() && get_incoming_connections_count(public_zone->second) == 0)
    {
      if (m_hide_my_port || public_zone->second.m_config.m_net_config.max_in_connection_count == 0)
      {
        // LOG_GLOBAL("Incoming connections disabled, enable them for full connectivity");
      }
      else
      {
        {
          const epee::LogLevel level = epee::LogLevel::Warning;
          LOG_CATEGORY_COLOR
            (
             level
             , "Debug"
             , epee::red
             , std::string()
             + "No incoming connections - check firewalls/routers allow port "
             + std::to_string(get_this_peer_port())
             );
        }
      }
    }
    return true;
  }
  //-----------------------------------------------------------------------------------

  bool node_server::peer_sync_idle_maker()
  {
    LOG_DEBUG("STARTED PEERLIST IDLE HANDSHAKE");
    typedef std::list<std::pair<epee::net_utils::connection_context_base, peerid_type> > local_connects_type;
    local_connects_type cncts;
    for(auto& zone : m_network_zones)
    {
      zone.second.m_net_server.get_config_object().foreach_connection([&](p2p_connection_context& cntxt)
      {
        if(cntxt.peer_id && !cntxt.m_in_timedsync)
        {
          cntxt.m_in_timedsync = true;
          cncts.push_back(local_connects_type::value_type(cntxt, cntxt.peer_id));//do idle sync only with handshaked connections
        }
        return true;
      });
    }

    std::for_each(cncts.begin(), cncts.end(), [&](const typename local_connects_type::value_type& vl){do_peer_timed_sync(vl.first, vl.second);});

    LOG_DEBUG("FINISHED PEERLIST IDLE HANDSHAKE");
    return true;
  }
  //-----------------------------------------------------------------------------------

  bool node_server::sanitize_peerlist(std::vector<peerlist_entry>& local_peerlist)
  {
    for (size_t i = 0; i < local_peerlist.size(); ++i)
    {
      bool ignore = false;
      peerlist_entry &be = local_peerlist[i];
      epee::net_utils::network_address &na = be.adr;
      if (na.is_loopback() || na.is_local())
      {
        ignore = true;
      }
      else if (be.adr.get_type_id() == epee::net_utils::ipv4_network_address::get_type_id())
      {
        const epee::net_utils::ipv4_network_address &ipv4 = na.as<const epee::net_utils::ipv4_network_address>();
        if (ipv4.ip() == 0)
          ignore = true;
      }
      if (ignore)
      {
        LOG_DEBUG("Ignoring " + be.adr.str());
        std::swap(local_peerlist[i], local_peerlist[local_peerlist.size() - 1]);
        local_peerlist.resize(local_peerlist.size() - 1);
        --i;
        continue;
      }
      local_peerlist[i].last_seen = 0;
    }
    return true;
  }
  //-----------------------------------------------------------------------------------

  bool node_server::handle_remote_peerlist(const std::vector<peerlist_entry>& peerlist, const epee::net_utils::connection_context_base& context)
  {
    if (peerlist.size() > P2P_MAX_PEERS_IN_HANDSHAKE)
    {
      LOG_WARNING
        (
         context.to_str()
         + "peer sent "
         + std::to_string(peerlist.size())
         + " peers, considered spamming"
         );
      return false;
    }
    std::vector<peerlist_entry> peerlist_ = peerlist;
    if(!sanitize_peerlist(peerlist_))
      return false;

    const epee::net_utils::zone zone = context.m_remote_address.get_zone();
    for(const auto& peer : peerlist_)
    {
      if(peer.adr.get_zone() != zone)
      {
        LOG_WARNING
          (
           context.to_str()
           + " sent peerlist from another zone, dropping"
           );
        return false;
      }
    }

    LOG_DEBUG
      (
       context.to_str()
       + "REMOTE PEERLIST: remote peerlist size="
       + std::to_string(peerlist_.size())
       );

    LOG_TRACE
      (
       context.to_str()
       + "REMOTE PEERLIST: " + print_peerlist_to_string(peerlist_)
       );

    LOCK_RECURSIVE_MUTEX(m_blocked_hosts_lock);
    return m_network_zones.at(context.m_remote_address.get_zone()).m_peerlist.merge_peerlist(peerlist_, [this](const peerlist_entry &pe) {
      return !is_addr_recently_failed(pe.adr) && is_remote_host_allowed(pe.adr);
    });
  }
  //-----------------------------------------------------------------------------------

  bool node_server::get_local_node_data(basic_node_data& node_data, const network_zone& zone)
  {
    node_data.peer_id = zone.m_config.m_peer_id;
    if(!m_hide_my_port && zone.m_can_pingback)
      node_data.my_port = m_external_port ? m_external_port : m_listening_port;
    else
      node_data.my_port = 0;
    node_data.network_id = m_network_id;
    return true;
  }
  //-----------------------------------------------------------------------------------

  int node_server::handle_get_support_flags
  (
   const int command
   , const COMMAND_SUPPORT_FLAGS::request& arg
   , COMMAND_SUPPORT_FLAGS::response& rsp
   , p2p_connection_context& context
   )
  {
    rsp.support_flags = m_network_zones.at(context.m_remote_address.get_zone()).m_config.m_support_flags;
    return 1;
  }
  //-----------------------------------------------------------------------------------

  void node_server::request_callback(const epee::net_utils::connection_context_base& context)
  {
    m_network_zones.at(context.m_remote_address.get_zone()).m_net_server.get_config_object().request_callback(context.m_connection_id);
  }
  //-----------------------------------------------------------------------------------

  bool node_server::relay_notify_to_list(int command, const std::span<const uint8_t> data_buff, std::vector<std::pair<epee::net_utils::zone, boost::uuids::uuid>> connections)
  {
    std::sort(connections.begin(), connections.end());
    auto zone = m_network_zones.begin();
    for(const auto& c_id: connections)
    {
      for (;;)
      {
        if (zone == m_network_zones.end())
        {
           LOG_WARNING
             (
              "Unable to relay all messages, "
              + std::string(epee::net_utils::zone_to_string(c_id.first))
              + " not available"
              );
           return false;
        }
        if (c_id.first <= zone->first)
          break;

        ++zone;
      }
      if (zone->first == c_id.first)
        zone->second.m_net_server.get_config_object().notify(command, data_buff, c_id.second);
    }
    return true;
  }
  //-----------------------------------------------------------------------------------

  bool node_server::send_txs
  (
   std::list<cryptonote::string_blob> txs
   , const boost::uuids::uuid& source_connection_id
   )
  {
    namespace enet = epee::net_utils;


    if (m_network_zones.empty())
      return false;

    bool sent = false;
    for (auto& network: m_network_zones)
    {
      if (network.second.m_connect) {
        if (network.second.m_notifier.send_txs(txs, source_connection_id)) {
          sent = true;
        }
      }
    }

    return sent;
  }
  //-----------------------------------------------------------------------------------

  void node_server::callback(p2p_connection_context& context)
  {
    m_payload_handler.on_callback(context);
  }
  //-----------------------------------------------------------------------------------

  bool node_server::invoke_notify_to_peer(int command, const std::span<const uint8_t> req_buff, const epee::net_utils::connection_context_base& context)
  {
    if(is_filtered_command(context.m_remote_address, command))
      return false;

    network_zone& zone = m_network_zones.at(context.m_remote_address.get_zone());
    int res = zone.m_net_server.get_config_object().notify(command, req_buff, context.m_connection_id);
    return res > 0;
  }
  //-----------------------------------------------------------------------------------

  bool node_server::drop_connection(const epee::net_utils::connection_context_base& context)
  {
    m_network_zones.at(context.m_remote_address.get_zone()).m_net_server.get_config_object().close(context.m_connection_id);
    return true;
  }
  bool node_server::try_get_support_flags(const p2p_connection_context& context, std::function<void(p2p_connection_context&, const uint32_t&)> f)
  {
    if(context.m_remote_address.get_zone() != epee::net_utils::zone::public_)
      return false;

    COMMAND_SUPPORT_FLAGS::request support_flags_request;
    bool r = epee::net_utils::async_invoke_remote_command2<typename COMMAND_SUPPORT_FLAGS::response>
    (
      context,
      COMMAND_SUPPORT_FLAGS::ID,
      support_flags_request,
      m_network_zones.at(epee::net_utils::zone::public_).m_net_server.get_config_object(),
      [=](int code, const typename COMMAND_SUPPORT_FLAGS::response& rsp, p2p_connection_context& context_)
      {
        if(code < 0)
        {
          LOG_WARNING_CC
            (
             context_
             , "COMMAND_SUPPORT_FLAGS invoke failed. ("
             + std::to_string(code)
             + ", "
             + epee::levin::get_err_descr(code)
             + ")"
             );
          return;
        }

        f(context_, rsp.support_flags);
      },
      P2P_DEFAULT_HANDSHAKE_INVOKE_TIMEOUT
    );

    return r;
  }
  //-----------------------------------------------------------------------------------

  int node_server::handle_timed_sync
  (
   const int command
   , const typename COMMAND_TIMED_SYNC::request& arg
   , typename COMMAND_TIMED_SYNC::response& rsp
   , p2p_connection_context& context
   )
  {
    if(!m_payload_handler.process_payload_sync_data(arg.payload_data, context, false))
    {
      LOG_WARNING_CC(context, "Failed to process_payload_sync_data(), dropping connection");
      drop_connection(context);
      return 1;
    }

    //fill response
    const epee::net_utils::zone zone_type = context.m_remote_address.get_zone();
    network_zone& zone = m_network_zones.at(zone_type);

    std::vector<peerlist_entry> local_peerlist_new;
    zone.m_peerlist.get_peerlist_head(local_peerlist_new, true, P2P_DEFAULT_PEERS_IN_HANDSHAKE);

    //only include out peers we did not already send
    for (const auto& x: local_peerlist_new)
    {
      if (context.sent_addresses.contains(x.adr)) continue;

      context.sent_addresses.insert(x.adr);
      rsp.local_peerlist_new.push_back(x);
    }
    m_payload_handler.get_payload_sync_data(rsp.payload_data);

    LOG_DEBUG_CC(context, "COMMAND_TIMED_SYNC");
    return 1;
  }
  //-----------------------------------------------------------------------------------

  int node_server::handle_handshake
  (
   const int command
   , const typename COMMAND_HANDSHAKE::request& arg
   , typename COMMAND_HANDSHAKE::response& rsp
   , p2p_connection_context& context
   )
  {
    // copy since dropping the connection will invalidate the context, and thus the address
    const auto remote_address = context.m_remote_address;

    if(arg.node_data.network_id != m_network_id)
    {
      LOG_INFO
        (
         context.to_str()
         + "WRONG NETWORK AGENT CONNECTED! id="
         + boost::uuids::to_string(arg.node_data.network_id)
         );
      drop_connection(context);
      add_host_fail(remote_address);
      return 1;
    }

    if(!context.m_is_income)
    {
      LOG_WARNING_CC(context, "COMMAND_HANDSHAKE came not from incoming connection");
      drop_connection(context);
      add_host_fail(remote_address);
      return 1;
    }

    if(context.peer_id)
    {
      LOG_WARNING_CC(context, "COMMAND_HANDSHAKE came, but seems that connection already have associated peer_id (double COMMAND_HANDSHAKE?)");
      drop_connection(context);
      return 1;
    }

    const auto azone = context.m_remote_address.get_zone();
    network_zone& zone = m_network_zones.at(azone);

    // test only the remote end's zone, otherwise an attacker could connect to you on clearnet
    // and pass in a tor connection's peer id, and deduce the two are the same if you reject it
    if(azone == epee::net_utils::zone::public_ && arg.node_data.peer_id == zone.m_config.m_peer_id)
    {
      LOG_DEBUG_CC(context, "Connection to self detected, dropping connection");
      drop_connection(context);
      return 1;
    }

    if (zone.m_current_number_of_in_peers >= zone.m_config.m_net_config.max_in_connection_count) // in peers limit
    {
      LOG_WARNING_CC(context, "COMMAND_HANDSHAKE came, but already have max incoming connections, so dropping this one.");
      drop_connection(context);
      return 1;
    }

    if(!m_payload_handler.process_payload_sync_data(arg.payload_data, context, true))
    {
      LOG_WARNING_CC
        (
         context
         , "COMMAND_HANDSHAKE came, but process_payload_sync_data returned false, dropping connection."
         );
      drop_connection(context);
      return 1;
    }

    if(has_too_many_connections(context.m_remote_address))
    {
      LOG_PRINT_CCONTEXT_L1
        (
         "CONNECTION FROM "
         + context.m_remote_address.host_str()
         + " REFUSED, too many connections from the same address"
         );
      drop_connection(context);
      return 1;
    }

    //associate peer_id with this connection
    context.peer_id = arg.node_data.peer_id;
    context.m_in_timedsync = false;

    if(arg.node_data.my_port && zone.m_can_pingback)
    {
      peerid_type peer_id_l = arg.node_data.peer_id;
      uint32_t port_l = arg.node_data.my_port;
      //try ping to be sure that we can add this peer to peer_list
      try_ping(arg.node_data, context, [peer_id_l, port_l, context, this]
      {
        ASSERT_OR_LOG_ERROR_AND_RETURN((context.m_remote_address.get_type_id() == epee::net_utils::ipv4_network_address::get_type_id() || context.m_remote_address.get_type_id() == epee::net_utils::ipv6_network_address::get_type_id()), void(),
            "Only IPv4 or IPv6 addresses are supported here");
        //called only(!) if success pinged, update local peerlist
        peerlist_entry pe;
        const epee::net_utils::network_address na = context.m_remote_address;
        if (context.m_remote_address.get_type_id() == epee::net_utils::ipv4_network_address::get_type_id())
        {
          pe.adr = epee::net_utils::ipv4_network_address(na.as<epee::net_utils::ipv4_network_address>().ip(), port_l);
        }
        else
        {
          pe.adr = epee::net_utils::ipv6_network_address(na.as<epee::net_utils::ipv6_network_address>().ip(), port_l);
        }
        time_t last_seen;
        time(&last_seen);
        pe.last_seen = static_cast<int64_t>(last_seen);
        pe.id = peer_id_l;
        this->m_network_zones.at(context.m_remote_address.get_zone()).m_peerlist.append_with_peer_white(pe);
        LOG_DEBUG_CC
          (
           context
           , "PING SUCCESS "
           + context.m_remote_address.host_str()
           + ":"
           + std::to_string(port_l)
           );
      });
    }

    try_get_support_flags(context, [](p2p_connection_context& flags_context, const uint32_t& support_flags)
    {
      flags_context.support_flags = support_flags;
    });

    //fill response
    zone.m_peerlist.get_peerlist_head(rsp.local_peerlist_new, true);
    for (const auto &e: rsp.local_peerlist_new)
      context.sent_addresses.insert(e.adr);
    get_local_node_data(rsp.node_data, zone);
    m_payload_handler.get_payload_sync_data(rsp.payload_data);
    LOG_DEBUG_CC(context, "COMMAND_HANDSHAKE");
    return 1;
  }
  //-----------------------------------------------------------------------------------

  int node_server::handle_ping
  (
   const int command
   , const COMMAND_PING::request& arg
   , COMMAND_PING::response& rsp
   , p2p_connection_context& context
   )
  {
    LOG_DEBUG_CC(context, "COMMAND_PING");
    rsp.status = PING_OK_RESPONSE_STATUS_TEXT;
    rsp.peer_id = m_network_zones.at(context.m_remote_address.get_zone()).m_config.m_peer_id;
    return 1;
  }
  //-----------------------------------------------------------------------------------

  bool node_server::log_connections()
  {
    LOG_INFO("Connections: \r\n" + print_connections_container() );
    return true;
  }
  //-----------------------------------------------------------------------------------

  std::string node_server::print_connections_container()
  {

    std::stringstream ss;
    for (auto& zone : m_network_zones)
    {
      zone.second.m_net_server.get_config_object().foreach_connection([&](const p2p_connection_context& cntxt)
      {
        ss << cntxt.m_remote_address.str()
          << " \t\tpeer_id " << peerid_to_string(cntxt.peer_id)
          << " \t\tconn_id " << cntxt.m_connection_id << (cntxt.m_is_income ? " INC":" OUT")
          << std::endl;
        return true;
      });
    }
    std::string s = ss.str();
    return s;
  }
  //-----------------------------------------------------------------------------------

  void node_server::on_connection_new(p2p_connection_context& context)
  {
    LOG_INFO("[" + epee::net_utils::print_connection_context(context) + "] NEW CONNECTION");
    m_payload_handler.on_connection_new(context);
  }
  //-----------------------------------------------------------------------------------

  void node_server::on_connection_close(p2p_connection_context& context)
  {
    network_zone& zone = m_network_zones.at(context.m_remote_address.get_zone());
    if (!zone.m_net_server.is_stop_signal_sent() && !context.m_is_income) {
      epee::net_utils::network_address na{};
      na = context.m_remote_address;

      zone.m_peerlist.remove_from_peer_anchor(na);
    }

    m_payload_handler.on_connection_close(context);

    LOG_INFO("[" + epee::net_utils::print_connection_context(context) + "] CLOSE CONNECTION");
  }


  bool node_server::is_priority_node(const epee::net_utils::network_address& na)
  {
    return (std::find(m_priority_peers.begin(), m_priority_peers.end(), na) != m_priority_peers.end()) || (std::find(m_exclusive_peers.begin(), m_exclusive_peers.end(), na) != m_exclusive_peers.end());
  }

  bool node_server::set_max_out_peers(network_zone& zone, int64_t max)
  {
    if(max == -1) {
      zone.m_config.m_net_config.max_out_connection_count = P2P_DEFAULT_CONNECTIONS_COUNT;
      return true;
    }
    zone.m_config.m_net_config.max_out_connection_count = max;
    return true;
  }


  bool node_server::set_max_in_peers(network_zone& zone, int64_t max)
  {
    zone.m_config.m_net_config.max_in_connection_count = max;
    return true;
  }


  uint32_t node_server::get_max_out_public_peers() const
  {
    const auto public_zone = m_network_zones.find(epee::net_utils::zone::public_);
    if (public_zone == m_network_zones.end())
      return 0;
    return public_zone->second.m_config.m_net_config.max_out_connection_count;
  }


  uint32_t node_server::get_max_in_public_peers() const
  {
    const auto public_zone = m_network_zones.find(epee::net_utils::zone::public_);
    if (public_zone == m_network_zones.end())
      return 0;
    return public_zone->second.m_config.m_net_config.max_in_connection_count;
  }


  bool node_server::has_too_many_connections(const epee::net_utils::network_address &address)
  {
    if (address.get_zone() != epee::net_utils::zone::public_)
      return false; // Unable to determine how many connections from host

    const size_t max_connections = ::config::lol::max_connections_per_address;
    size_t count = 0;

    if (address.is_loopback()) {
      return false;
    }


    m_network_zones.at(epee::net_utils::zone::public_).m_net_server.get_config_object().foreach_connection([&](const p2p_connection_context& cntxt)
    {
      if (cntxt.m_is_income && cntxt.m_remote_address.is_same_host(address)) {
        count++;

        if (count > max_connections) {
          return false;
        }
      }

      return true;
    });

    return count > max_connections;
  }


  bool node_server::gray_peerlist_housekeeping()
  {
    if (m_offline) return true;
    if (!m_exclusive_peers.empty()) return true;
    if (m_payload_handler.needs_new_sync_connections()) return true;

    for (auto& zone : m_network_zones)
    {
      if (zone.second.m_net_server.is_stop_signal_sent())
        return false;

      if (zone.second.m_connect == nullptr)
        continue;

      peerlist_entry pe{};
      if (!zone.second.m_peerlist.get_random_gray_peer(pe))
        continue;

      if (!check_connection_and_handshake_with_peer(pe.adr, pe.last_seen))
      {
        zone.second.m_peerlist.remove_from_peer_gray(pe);
        LOG_VERBOSE
          (
           "PEER EVICTED FROM GRAY PEER LIST: address: "
           + pe.adr.host_str()
           + " Peer ID: "
           + peerid_to_string(pe.id)
           );
      }
      else
      {
        zone.second.m_peerlist.set_peer_just_seen(pe.id, pe.adr);
        LOG_VERBOSE
          (
           "PEER PROMOTED TO WHITE PEER LIST IP address: "
           + pe.adr.host_str()
           + " Peer ID: "
           + peerid_to_string(pe.id)
           );
      }
    }
    return true;
  }

  std::optional<p2p_connection_context_t<typename t_payload_net_handler::connection_context>>
  node_server::socks_connect(network_zone& zone, const epee::net_utils::network_address& remote)
  {
    auto result = socks_connect_internal(zone.m_net_server.get_stop_signal(), zone.m_net_server.get_io_service(), zone.m_proxy_address, remote);
    if (result) // if no error
    {
      p2p_connection_context context{};
      if (zone.m_net_server.add_connection(context, std::move(*result), remote))
        return {std::move(context)};
    }
    return std::nullopt;
  }

  std::optional<p2p_connection_context_t<typename t_payload_net_handler::connection_context>>
  node_server::public_connect(network_zone& zone, epee::net_utils::network_address const& na)
  {
    bool is_ipv4 = na.get_type_id() == epee::net_utils::ipv4_network_address::get_type_id();
    bool is_ipv6 = na.get_type_id() == epee::net_utils::ipv6_network_address::get_type_id();
    ASSERT_OR_LOG_ERROR_AND_RETURN(is_ipv4 || is_ipv6, std::nullopt,
      "Only IPv4 or IPv6 addresses are supported here");

    std::string address;
    std::string port;

    if (is_ipv4)
    {
      const epee::net_utils::ipv4_network_address &ipv4 = na.as<const epee::net_utils::ipv4_network_address>();
      address = epee::string_tools::get_ip_string_from_int32(ipv4.ip());
      port = epee::string_tools::num_to_string_fast(ipv4.port());
    }
    else if (is_ipv6)
    {
      const epee::net_utils::ipv6_network_address &ipv6 = na.as<const epee::net_utils::ipv6_network_address>();
      address = ipv6.ip().to_string();
      port = epee::string_tools::num_to_string_fast(ipv6.port());
    }
    else
    {
      LOG_ERROR("Only IPv4 or IPv6 addresses are supported here");
      return std::nullopt;
    }

    typename net_server::t_connection_context con{};
    const bool res = zone.m_net_server.connect(address, port,
      zone.m_config.m_net_config.connection_timeout,
      con, "0.0.0.0");

    if (res)
      return {std::move(con)};
    return std::nullopt;
  }

  //-----------------------------------------------------------------------------------
  bool node_server::connect_to_peerlist(const Container& peers)
  {
    const network_zone& public_zone = m_network_zones.at(epee::net_utils::zone::public_);

    bool r = false;
    for(const epee::net_utils::network_address& na: peers)
    {
      if(public_zone.m_net_server.is_stop_signal_sent())
        return false;

      if(is_addr_connected(na))
        continue;

      if (try_to_connect_and_handshake_with_new_peer(na)) {
        r = true;
      }
    }

    return r;
  }

  //-----------------------------------------------------------------------------------
  bool node_server::parse_peers_and_add_to_container
  (
   const std::vector<std::string> peers_in
   , Container& container
   )
  {
    std::vector<std::string> peers_ = peers_in;

    for(const std::string& pr_str: peers_)
    {
      const uint16_t default_port = cryptonote::get_config(m_nettype).P2P_DEFAULT_PORT;
      expect<epee::net_utils::network_address> adr = net::get_network_address(pr_str, default_port);
      if (adr)
      {
        add_zone(adr->get_zone());
        container.push_back(std::move(*adr));
        continue;
      }
      std::vector<epee::net_utils::network_address> resolved_addrs;
      bool r = append_net_address(resolved_addrs, pr_str, default_port);
      ASSERT_OR_LOG_ERROR_AND_RETURN(r, false, "Failed to parse or resolve address from string: " + pr_str);
      for (const epee::net_utils::network_address& addr : resolved_addrs)
      {
        container.push_back(addr);
      }
    }

    return true;
  }

  //-----------------------------------------------------------------------------------
  bool node_server::try_ping
  (
   const basic_node_data& node_data
   , p2p_connection_context& context
   , const auto &cb
   )
  {
    if(!node_data.my_port)
      return false;

    bool address_ok = (context.m_remote_address.get_type_id() == epee::net_utils::ipv4_network_address::get_type_id() || context.m_remote_address.get_type_id() == epee::net_utils::ipv6_network_address::get_type_id());
    ASSERT_OR_LOG_ERROR_AND_RETURN(address_ok, false,
        "Only IPv4 or IPv6 addresses are supported here");

    const epee::net_utils::network_address na = context.m_remote_address;
    std::string ip;
    uint32_t ipv4_addr = 0;
    boost::asio::ip::address_v6 ipv6_addr;
    bool is_ipv4;
    if (na.get_type_id() == epee::net_utils::ipv4_network_address::get_type_id())
    {
      ipv4_addr = na.as<const epee::net_utils::ipv4_network_address>().ip();
      ip = epee::string_tools::get_ip_string_from_int32(ipv4_addr);
      is_ipv4 = true;
    }
    else
    {
      ipv6_addr = na.as<const epee::net_utils::ipv6_network_address>().ip();
      ip = ipv6_addr.to_string();
      is_ipv4 = false;
    }
    network_zone& zone = m_network_zones.at(na.get_zone());

    if(!zone.m_peerlist.is_host_allowed(context.m_remote_address))
      return false;

    std::string port = epee::string_tools::num_to_string_fast(node_data.my_port);

    epee::net_utils::network_address address;
    if (is_ipv4)
    {
      address = epee::net_utils::network_address{epee::net_utils::ipv4_network_address(ipv4_addr, node_data.my_port)};
    }
    else
    {
      address = epee::net_utils::network_address{epee::net_utils::ipv6_network_address(ipv6_addr, node_data.my_port)};
    }
    peerid_type pr = node_data.peer_id;
    bool r = zone.m_net_server.connect_async(ip, port, zone.m_config.m_net_config.ping_connection_timeout, [cb, /*context,*/ address, pr, this](
      const typename net_server::t_connection_context& ping_context,
      const boost::system::error_code& ec)->bool
    {
      if(ec)
      {
        LOG_WARNING_CC
          (
           ping_context
           , "back ping connect failed to "
           + address.str()
           );
        return false;
      }
      COMMAND_PING::request req{};
      COMMAND_PING::response rsp{};
      network_zone& zone = m_network_zones.at(address.get_zone());

      bool inv_call_res = epee::net_utils::async_invoke_remote_command2<COMMAND_PING::response>(ping_context, COMMAND_PING::ID, req, zone.m_net_server.get_config_object(),
        [=, this](int code, const COMMAND_PING::response& rsp, p2p_connection_context& context)
      {
        if(code <= 0)
        {
          LOG_WARNING_CC
            (
             ping_context
             , "Failed to invoke COMMAND_PING to "
             + address.str()
             + "("
             + std::to_string(code)
             + ", "
             + epee::levin::get_err_descr(code)
             + ")"
             );
          return;
        }

        network_zone& zone = m_network_zones.at(address.get_zone());
        if(rsp.status != PING_OK_RESPONSE_STATUS_TEXT || pr != rsp.peer_id)
        {
          LOG_WARNING_CC
            (
             ping_context
             , std::string()
             + "back ping invoke wrong response \""
             + rsp.status
             + "\" from"
             + address.str()
             + ", hsh_peer_id="
             + peerid_to_string(pr)
             + ", rsp.peer_id="
             + peerid_to_string(rsp.peer_id)
             );
          zone.m_net_server.get_config_object().close(ping_context.m_connection_id);
          return;
        }
        zone.m_net_server.get_config_object().close(ping_context.m_connection_id);
        cb();
      });

      if(!inv_call_res)
      {
        LOG_WARNING_CC
          (
           ping_context
           , "back ping invoke failed to "
           + address.str()
           );
        zone.m_net_server.get_config_object().close(ping_context.m_connection_id);
        return false;
      }
      return true;
    });
    if(!r)
    {
      LOG_WARNING_CC(context, "Failed to call connect_async, network error.");
    }
    return r;
  }
}
