// Copyright (c) 2014-2020, The Monero Project
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this list of
//    conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this list
//    of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may be
//    used to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
// THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
// THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Parts of this file are originally copyright (c) 2012-2013 The Cryptonote developers

#pragma once

#include "net_peerlist.h"
#include "net_node_common.h"

#include "cryptonote/protocol/cryptonote_protocol_handler.h"
#include "cryptonote/protocol/levin_notify.h"

#include "tools/epee/include/net/abstract_tcp_server2.h"
#include "tools/epee/include/net/levin_protocol_handler_async.h"
#include "tools/epee/include/net/levin_abstract_invoke2.h"
#include "tools/epee/include/time_helper.h"

#include "config/cryptonote.hpp"

#include <shared_mutex>


namespace nodetool
{
  constexpr std::string_view arg_anonymous_inbound_name =
    "anonymous-inbound";

  constexpr std::string_view arg_proxy_name =
    "proxy";

  boost::asio::ip::address_v4 make_address_v4_from_v6(const boost::asio::ip::address_v6& a);

  struct network_config
  {
    uint32_t max_out_connection_count;
    uint32_t max_in_connection_count;
    uint32_t connection_timeout;
    uint32_t ping_connection_timeout;
    uint32_t handshake_interval;
    uint32_t packet_max_size;
    uint32_t config_id;
    uint32_t send_peerlist_sz;
  };


  struct proxy
  {
    proxy()
      : max_connections(-1),
        address(),
        zone(epee::net_utils::zone::invalid),
        noise(true)
    {}

    std::int64_t max_connections;
    boost::asio::ip::tcp::endpoint address;
    epee::net_utils::zone zone;
    bool noise;
  };

  struct anonymous_inbound
  {
    anonymous_inbound()
      : max_connections(-1),
        local_ip(),
        local_port(),
        our_address(),
        default_remote()
    {}

    std::int64_t max_connections;
    std::string local_ip;
    std::string local_port;
    epee::net_utils::network_address our_address;
    epee::net_utils::network_address default_remote;
  };

  std::optional<std::vector<proxy>> get_proxies
  (const std::vector<std::string> xs);

  std::optional<std::vector<anonymous_inbound>> get_anonymous_inbounds
  (const std::vector<std::string> xs);



  //! \return True if `commnd` is filtered (ignored/dropped) for `address`
  bool is_filtered_command(epee::net_utils::network_address const& address, int command);

  // hides boost::future and chrono stuff from mondo template file
  std::optional<boost::asio::ip::tcp::socket>
  socks_connect_internal(const std::atomic<bool>& stop_signal, boost::asio::io_service& service, const boost::asio::ip::tcp::endpoint& proxy, const epee::net_utils::network_address& remote);

  bool append_net_address(std::vector<epee::net_utils::network_address> & seed_nodes, std::string const & addr, uint16_t default_port);


  typedef cryptonote::t_cryptonote_protocol_handler t_payload_net_handler;

  class node_server: public epee::levin::levin_commands_handler<p2p_connection_context_t<typename t_payload_net_handler::connection_context> >,
                     public i_p2p_endpoint<typename t_payload_net_handler::connection_context>,
                     public epee::net_utils::i_connection_filter
  {
    struct by_conn_id{};
    struct by_peer_id{};
    struct by_addr{};

    typedef p2p_connection_context_t<typename t_payload_net_handler::connection_context> p2p_connection_context;

    typedef COMMAND_HANDSHAKE_T<typename t_payload_net_handler::payload_type> COMMAND_HANDSHAKE;
    typedef COMMAND_TIMED_SYNC_T<typename t_payload_net_handler::payload_type> COMMAND_TIMED_SYNC;
    static_assert(p2p_connection_context::handshake_command() == COMMAND_HANDSHAKE::ID, "invalid handshake command id");

    typedef epee::net_utils::boosted_tcp_server<epee::levin::async_protocol_handler> net_server;

    struct network_zone;
    using connect_func = std::optional<p2p_connection_context>(network_zone&, epee::net_utils::network_address const&);

    struct config_t
    {
      network_config m_net_config{};
      uint64_t m_peer_id = 1;
      uint32_t m_support_flags = P2P_SUPPORT_FLAGS;
    };
    using config = config_t;

    struct network_zone
    {
      network_zone()
        : m_connect(nullptr),
          m_net_server(epee::net_utils::e_connection_type_P2P),
          m_bind_ip(),
          m_bind_ipv6_address(),
          m_port(),
          m_port_ipv6(),
          m_notifier(),
          m_our_address(),
          m_peerlist(),
          m_config{},
          m_proxy_address(),
          m_current_number_of_out_peers(0),
          m_current_number_of_in_peers(0),
          m_can_pingback(false),
          m_allow_inbound(true)
      {
        set_config_defaults();
      }

      network_zone(boost::asio::io_service& public_service)
        : m_connect(nullptr),
          m_net_server(public_service, epee::net_utils::e_connection_type_P2P),
          m_bind_ip(),
          m_bind_ipv6_address(),
          m_port(),
          m_port_ipv6(),
          m_notifier(),
          m_our_address(),
          m_peerlist(),
          m_config{},
          m_proxy_address(),
          m_current_number_of_out_peers(0),
          m_current_number_of_in_peers(0),
          m_can_pingback(false),
          m_allow_inbound(true)
      {
        set_config_defaults();
      }

      connect_func* m_connect;
      net_server m_net_server;
      std::string m_bind_ip;
      std::string m_bind_ipv6_address;
      std::string m_port;
      std::string m_port_ipv6;
      cryptonote::levin::notify m_notifier;
      epee::net_utils::network_address m_our_address; // in anonymity networks
      peerlist_manager m_peerlist;
      config m_config;
      boost::asio::ip::tcp::endpoint m_proxy_address;
      std::atomic<unsigned int> m_current_number_of_out_peers;
      std::atomic<unsigned int> m_current_number_of_in_peers;
      bool m_can_pingback;
      bool m_allow_inbound;

    private:
      void set_config_defaults() noexcept
      {
        // at this moment we have a hardcoded config
        m_config.m_net_config.handshake_interval = P2P_DEFAULT_HANDSHAKE_INTERVAL;
        m_config.m_net_config.packet_max_size = P2P_DEFAULT_PACKET_MAX_SIZE;
        m_config.m_net_config.config_id = 0;
        m_config.m_net_config.connection_timeout = P2P_DEFAULT_CONNECTION_TIMEOUT;
        m_config.m_net_config.ping_connection_timeout = P2P_DEFAULT_PING_CONNECTION_TIMEOUT;
        m_config.m_net_config.send_peerlist_sz = P2P_DEFAULT_PEERS_IN_HANDSHAKE;
      }
    };

  public:
    typedef t_payload_net_handler payload_net_handler;

    node_server
    (
     t_payload_net_handler& payload_handler
     , const cryptonote::network_type nettype
     , const bool offline
     , const std::string data_dir
     , const bool allow_local_ip
     , const bool hide_my_port
     , const uint32_t p2p_external_port
     , const bool p2p_use_ipv6
     , const bool p2p_ignore_ipv4
     )
      :
      m_nettype(nettype)
      , m_offline(offline)
      , m_config_folder(data_dir)
      , m_allow_local_ip(allow_local_ip)
      , m_hide_my_port(hide_my_port)
      , m_external_port(p2p_external_port)
      , m_use_ipv6(p2p_use_ipv6)
      , m_require_ipv4(!p2p_ignore_ipv4)

      , m_payload_handler(payload_handler)
      , m_network_id()
    {
    }

    virtual ~node_server();

    bool run();
    network_zone& add_zone(epee::net_utils::zone zone);

    bool init
    (
     const int64_t max_in_peers
     , const int64_t max_out_peers
     , const std::vector<std::string> anonymous_inbounds
     , const std::vector<std::string> proxies
     , const std::vector<std::string> add_peers
     , const std::vector<std::string> add_exclusive_nodes
     , const std::vector<std::string> add_priority_nodes
     , const std::string p2p_bind_ip
     , const uint32_t p2p_port
     , const std::string p2p_bind_ip_ipv6
     , const uint32_t p2p_port_ipv6

     );

    bool deinit();
    bool send_stop_signal();
    uint32_t get_this_peer_port(){return m_listening_port;}
    t_payload_net_handler& get_payload_object();

    // debug functions
    bool log_connections();

    // These functions only return information for the "public" zone
    virtual uint64_t get_public_connections_count();
    size_t get_public_outgoing_connections_count();
    size_t get_public_white_peers_count();
    size_t get_public_gray_peers_count();
    void get_peerlist(std::vector<peerlist_entry>& gray, std::vector<peerlist_entry>& white);

    uint32_t get_max_out_public_peers() const;
    uint32_t get_max_in_public_peers() const;
    virtual bool block_host(epee::net_utils::network_address address, time_t seconds = P2P_IP_BLOCKTIME);
    virtual bool unblock_host(const epee::net_utils::network_address &address);
    virtual bool is_host_blocked(const epee::net_utils::network_address &address, time_t *seconds) { LOCK_RECURSIVE_MUTEX(m_blocked_hosts_lock); return !is_remote_host_allowed(address, seconds); }
    virtual std::map<std::string, time_t> get_blocked_hosts() { LOCK_RECURSIVE_MUTEX(m_blocked_hosts_lock); return m_blocked_hosts; }

  private:

    CHAIN_LEVIN_INVOKE_MAP2(p2p_connection_context); //move levin_commands_handler interface invoke(...) callbacks into invoke map
    CHAIN_LEVIN_NOTIFY_MAP2(p2p_connection_context); //move levin_commands_handler interface notify(...) callbacks into nothing

    BEGIN_INVOKE_MAP2()
      if (is_filtered_command(context.m_remote_address, command))
        return epee::levin::LEVIN_ERROR_CONNECTION_HANDLER_NOT_DEFINED;

      HANDLE_INVOKE_T2(COMMAND_HANDSHAKE, &node_server::handle_handshake)
      HANDLE_INVOKE_T2(COMMAND_TIMED_SYNC, &node_server::handle_timed_sync)
      HANDLE_INVOKE_T2(COMMAND_PING, &node_server::handle_ping)
      HANDLE_INVOKE_T2(COMMAND_SUPPORT_FLAGS, &node_server::handle_get_support_flags)
      CHAIN_INVOKE_MAP_TO_OBJ_FORCE_CONTEXT(m_payload_handler, typename t_payload_net_handler::connection_context&)
    END_INVOKE_MAP2()

    enum PeerType { anchor = 0, white, gray };

    //----------------- commands handlers ----------------------------------------------
    int handle_handshake
    (
     const int command
     , const typename COMMAND_HANDSHAKE::request& arg
     , typename COMMAND_HANDSHAKE::response& rsp
     , p2p_connection_context& context
     );

    int handle_timed_sync
    (
     const int command
     , const typename COMMAND_TIMED_SYNC::request& arg
     , typename COMMAND_TIMED_SYNC::response& rsp
     , p2p_connection_context& context
     );

    int handle_ping
    (
     const int command
     , const COMMAND_PING::request& arg
     , COMMAND_PING::response& rsp
     , p2p_connection_context& context
     );

    int handle_get_support_flags
    (
     const int command
     , const COMMAND_SUPPORT_FLAGS::request& arg
     , COMMAND_SUPPORT_FLAGS::response& rsp
     , p2p_connection_context& context
     );


    bool init_config();
    bool make_default_peer_id();
    bool make_default_config();
    bool store_config();


    //----------------- levin_commands_handler -------------------------------------------------------------
    virtual void on_connection_new(p2p_connection_context& context);
    virtual void on_connection_close(p2p_connection_context& context);
    virtual void callback(p2p_connection_context& context);
    //----------------- i_p2p_endpoint -------------------------------------------------------------
    virtual bool relay_notify_to_list(int command, const std::span<const uint8_t> data_buff, std::vector<std::pair<epee::net_utils::zone, boost::uuids::uuid>> connections);

    virtual bool send_txs
    (
     std::list<cryptonote::string_blob> txs
     , const boost::uuids::uuid& source_connection_id
     );

    virtual bool invoke_notify_to_peer(int command, const std::span<const uint8_t> req_buff, const epee::net_utils::connection_context_base& context);
    virtual bool drop_connection(const epee::net_utils::connection_context_base& context);
    virtual void request_callback(const epee::net_utils::connection_context_base& context);
    virtual void for_each_connection(std::function<bool(typename t_payload_net_handler::connection_context&, peerid_type, uint32_t)> f);
    virtual bool for_connection(const boost::uuids::uuid&, std::function<bool(typename t_payload_net_handler::connection_context&, peerid_type, uint32_t)> f);
    virtual bool add_host_fail(const epee::net_utils::network_address &address, unsigned int score = 1);
    //----------------- i_connection_filter  --------------------------------------------------------
    virtual bool is_remote_host_allowed(const epee::net_utils::network_address &address, time_t *t = NULL);

    bool handle_command_line
    (
     const int64_t max_in_peers
     , const int64_t max_out_peers
     , const std::vector<std::string> anonymous_inbound
     , const std::vector<std::string> proxy_args
     , const std::vector<std::string> add_peers
     , const std::vector<std::string> add_exclusive_nodes
     , const std::vector<std::string> add_priority_nodes
     , const std::string p2p_bind_ip
     , const uint32_t p2p_port
     , const std::string p2p_bind_ip_ipv6
     , const uint32_t p2p_port_ipv6
     );

    bool idle_worker();
    bool handle_remote_peerlist(const std::vector<peerlist_entry>& peerlist, const epee::net_utils::connection_context_base& context);
    bool get_local_node_data(basic_node_data& node_data, const network_zone& zone);
    //bool get_local_handshake_data(handshake_data& hshd);

    bool sanitize_peerlist(std::vector<peerlist_entry>& local_peerlist);

    bool connections_maker();
    bool peer_sync_idle_maker();
    bool do_handshake_with_peer(peerid_type& pi, p2p_connection_context& context, bool just_take_peerlist = false);
    bool do_peer_timed_sync(const epee::net_utils::connection_context_base& context, peerid_type peer_id);

    bool make_new_connection_from_anchor_peerlist(const std::vector<anchor_peerlist_entry>& anchor_peerlist);
    bool make_new_connection_from_peerlist(network_zone& zone, bool use_white_list);
    bool try_to_connect_and_handshake_with_new_peer(const epee::net_utils::network_address& na, bool just_take_peerlist = false, uint64_t last_seen_stamp = 0, PeerType peer_type = white, uint64_t first_seen_stamp = 0);
    bool is_peer_used(const peerlist_entry& peer);
    bool is_peer_used(const anchor_peerlist_entry& peer);
    bool is_addr_connected(const epee::net_utils::network_address& peer);

    template<class t_callback>
    bool try_ping
    (
     const basic_node_data& node_data
     , p2p_connection_context& context
     , const t_callback &cb
     );

    bool try_get_support_flags(const p2p_connection_context& context, std::function<void(p2p_connection_context&, const uint32_t&)> f);
    bool make_expected_connections_count(network_zone& zone, PeerType peer_type, size_t expected_connections);
    void record_addr_failed(const epee::net_utils::network_address& addr);
    bool is_addr_recently_failed(const epee::net_utils::network_address& addr);
    bool is_priority_node(const epee::net_utils::network_address& na);

    typedef std::vector<epee::net_utils::network_address> Container;
    bool connect_to_peerlist(const Container& peers);
    bool parse_peers_and_add_to_container
    (
     const std::vector<std::string> peers_in
     , Container& container
     );

    bool set_max_out_peers(network_zone& zone, int64_t max);
    bool set_max_in_peers(network_zone& zone, int64_t max);

    bool has_too_many_connections(const epee::net_utils::network_address &address);
    size_t get_incoming_connections_count();
    size_t get_incoming_connections_count(network_zone&);
    size_t get_outgoing_connections_count();
    size_t get_outgoing_connections_count(network_zone&);

    bool check_connection_and_handshake_with_peer(const epee::net_utils::network_address& na, uint64_t last_seen_stamp);
    bool gray_peerlist_housekeeping();
    bool check_incoming_connections();

    void kill() { ///< will be called e.g. from deinit()
      _info("Killing the net_node");
      is_closing = true;
      if(mPeersLoggerThread != nullptr)
        mPeersLoggerThread->join(); // make sure the thread finishes
      _info("Joined extra background net_node threads");
    }

    //debug functions
    std::string print_connections_container();


  public:

  private:
    cryptonote::network_type m_nettype;
    bool m_offline = false;
    std::string m_config_folder;
    bool m_allow_local_ip = false;
    bool m_hide_my_port = false;
    uint32_t m_external_port = 0;
    bool m_use_ipv6 = false;
    bool m_require_ipv4 = true;

    bool m_have_address;
    bool m_first_connection_maker_call;
    uint32_t m_listening_port;
    uint32_t m_listening_port_ipv6;
    std::atomic<bool> is_closing = false;
    std::unique_ptr<std::thread> mPeersLoggerThread;
    //std::recursive_mutex m_connections_lock;
    //connections_indexed_container m_connections;

    t_payload_net_handler& m_payload_handler;
    peerlist_storage m_peerlist_storage;

    epee::time_helper::once_a_time_seconds<P2P_DEFAULT_HANDSHAKE_INTERVAL> m_peer_handshake_idle_maker_interval;
    epee::time_helper::once_a_time_seconds<10> m_connections_maker_interval;
    epee::time_helper::once_a_time_seconds<60*30, false> m_peerlist_store_interval;
    epee::time_helper::once_a_time_seconds<60> m_gray_peerlist_housekeeping_interval;
    epee::time_helper::once_a_time_seconds<3600, false> m_incoming_connections_interval;

    std::vector<epee::net_utils::network_address> m_priority_peers;
    std::vector<epee::net_utils::network_address> m_exclusive_peers;
    std::atomic_flag m_fallback_seed_nodes_added;
    std::vector<nodetool::peerlist_entry> m_command_line_peers;
    uint64_t m_peer_livetime;
    //keep connections to initiate some interactions


    static std::optional<p2p_connection_context> public_connect(network_zone&, epee::net_utils::network_address const&);
    static std::optional<p2p_connection_context> socks_connect(network_zone&, epee::net_utils::network_address const&);


    /* A `std::map` provides constant iterators and key/value pointers even with
    inserts/erases to _other_ elements. This makes the configuration step easier
    since references can safely be stored on the stack. Do not insert/erase
    after configuration and before destruction, lock safety would need to be
    added. `std::map::operator[]` WILL insert! */
    std::map<epee::net_utils::zone, network_zone> m_network_zones;


    std::map<std::string, time_t> m_conn_fails_cache;
    std::recursive_mutex m_conn_fails_cache_lock;

    std::recursive_mutex m_blocked_hosts_lock; // for both hosts
    std::map<std::string, time_t> m_blocked_hosts;

    std::mutex m_host_fails_score_lock;
    std::map<std::string, uint64_t> m_host_fails_score;

    boost::uuids::uuid m_network_id;
  };
}

