/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE


Parts of this file are originally 
copyright (c) 2012-2013 The Cryptonote developers

*/

#pragma once

#include "network/rpc/core_rpc_server.h"
#include "cryptonote/protocol/cryptonote_protocol_handler.h"

namespace daemonize {
  class t_daemon {
  public:
    t_daemon
    (
     const cryptonote::network_type nettype
     , const bool offline
     , const std::string data_dir
     , const std::string mining_address
     , const uint32_t opencl_mining_threads
     , const bool allow_local_ip
     , const std::string rpc_ip
     , const uint16_t rpc_port
     , const bool hide_my_port
     , const uint32_t p2p_external_port
     , const bool p2p_use_ipv6
     , const bool p2p_ignore_ipv4
     , const bool ignore_checkpoints
     , const std::optional<uint64_t> maybe_fix_difficulty
     );

    ~t_daemon();

    bool init
      (
       const int64_t max_in_peers
       , const int64_t max_out_peers
       , const std::vector<std::string> anonymous_inbounds
       , const std::vector<std::string> proxies
       , const std::vector<std::string> add_peers
       , const std::vector<std::string> add_exclusive_nodes
       , const std::vector<std::string> add_priority_nodes
       , const std::string p2p_bind_ip
       , const uint32_t p2p_port
       , const std::string p2p_bind_ip_ipv6
       , const uint32_t p2p_port_ipv6
       ) noexcept;

    bool run() noexcept;

  private:
    cryptonote::core core;
    nodetool::node_server p2p;
    cryptonote::core_rpc_server rpc;
    cryptonote::t_cryptonote_protocol_handler protocol;

    std::string rpc_ip;
    uint16_t rpc_port;
  };
}
