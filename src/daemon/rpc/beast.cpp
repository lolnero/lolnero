/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "beast.hpp"

namespace beast = boost::beast;
namespace http = beast::http;
using tcp = boost::asio::ip::tcp;

namespace cryptonote {

  void http_connection::start()
    {
      request_parser_.body_limit
        ((std::numeric_limits<std::uint64_t>::max)());

      read_request();
      check_deadline();
    }

  void http_connection::read_request()
    {
      auto self = shared_from_this();

      http::async_read
        (
         socket_,
         buffer_,
         request_parser_,
         [self](beast::error_code ec,
                std::size_t bytes_transferred)
         {
           boost::ignore_unused(bytes_transferred);
           if(!ec)
             self->process_request();
         });
    }

  void http_connection::process_request()
    {
      request_ = request_parser_.get();
      response_.version(request_.version());
      response_.keep_alive(false);

      switch(request_.method())
        {
        case http::verb::get:
        case http::verb::post:
          response_.result(http::status::ok);
          response_.set(http::field::server, "Beast");

          response_.set
            (
             http::field::content_type
             , "application/json; charset=utf-8"
             );

          json_response();
          break;

        default:
          // We return responses indicating an error if
          // we do not recognize the request method.
          response_.result(http::status::bad_request);
          response_.set(http::field::content_type, "text/plain");
          beast::ostream(response_.body())
            << "Invalid request-method '"
            << std::string(request_.method_string())
            << "'";
          break;
        }

      write_response();
    }
    // Asynchronously transmit the response message.
  void http_connection::write_response()
    {
      auto self = shared_from_this();

      response_.content_length(response_.body().size());

      http::async_write
        (
         socket_,
         response_,
         [self](beast::error_code ec, std::size_t)
         {
           self->socket_.shutdown(tcp::socket::shutdown_send, ec);
           self->deadline_.cancel();
         });
    }

    // Check whether we have spent enough time on this connection.
  void http_connection::check_deadline()
    {
      auto self = shared_from_this();

      deadline_.async_wait
        (
         [self](beast::error_code ec)
         {
           if(!ec)
             {
               // Close socket to cancel any outstanding operation.
               self->socket_.close(ec);
             }
         });
    }

} // cryptonote
