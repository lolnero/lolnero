/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "tools/epee/include/net/jsonrpc_structs.h"
#include "tools/epee/include/logging.hpp"

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio.hpp>

#include <optional>

namespace beast = boost::beast;
namespace http = beast::http;
using tcp = boost::asio::ip::tcp;

namespace cryptonote {

  template<class t_request, class t_response, typename T>
  std::optional<std::string> handle_json
  (
   const std::string x
   , const T f
   )
  {
    t_request req{};

    if (x.empty()) {
      LOG_TRACE("empty request body");
    }
    else {
      try {
        req = json::parse(x);
      }
      catch (...) {
        LOG_ERROR("Failed to parse json input: " + x);
      }
    }

    t_response res{};
    const bool r = f(req, res);

    if (!r) {
      return {};
    }

    const json res_json = res;

    return res_json.dump();
  }



  class http_connection :
    public std::enable_shared_from_this<http_connection>
  {
  public:
    http_connection(tcp::socket socket)
      : socket_(std::move(socket)) {}

    virtual ~http_connection() = default;

    void start();

  protected:
    // The socket for the currently connected client.
    tcp::socket socket_;

    // The buffer for performing reads.
    // beast::flat_buffer buffer_{2 << 16};
    beast::flat_buffer buffer_;

    // The request message.
    http::request_parser<http::string_body> request_parser_;
    http::request<http::string_body> request_;

    // The response message.
    http::response<http::dynamic_body> response_;

    // The timer for putting a deadline on connection processing.
    boost::asio::steady_timer deadline_{
      socket_.get_executor(), std::chrono::seconds(60)};

    // Asynchronously receive a complete request message.
    void read_request();

    // Determine what needs to be done with the request message.
    void process_request();

    template<class t_request, class t_response, typename T>
    void process_json(const T f)
    {
      const std::string body_ = request_.body();

      const std::optional<std::string> maybe_res = handle_json
        <
          t_request
        , t_response
        >
        (
         body_
         , [&](const auto x, auto& y) {
           return f(x, y);
         }
         );


      if(!maybe_res) {
        response_.result(http::status::bad_request);
        return;
      }

      beast::ostream(response_.body()) << *maybe_res;

      LOG_DEBUG(*maybe_res);
    }

    virtual void json_response() = 0;

    template<class t_request, class t_response, typename T>
    void process_json_rpc
    (
     const t_request req
     , const uint64_t req_id
     , const T f
     )
    {
      epee::json_rpc::response
        <
          t_response
        , epee::json_rpc::dummy_error
        > resp{};

      resp.jsonrpc = "2.0";
      resp.id = req_id;

      epee::json_rpc::error_response fail_resp{};
      fail_resp.jsonrpc = "2.0";
      fail_resp.id = req_id;

      // LOG_VERBOSE("Calling RPC method " + callback_name);
      bool res = false;

      try {
        res = f(req, resp.result, fail_resp.error);
      }
      catch (const std::exception &e) {
        LOG_ERROR
          (
           "Failed to "
           + std::string("on_sync_info")
           + "(): "
           + std::string(e.what())
           );
      }

      epee::json_rpc::ok_response
        <
          t_response
        > ok_resp{};

      ok_resp.result = resp.result;
      ok_resp.id = resp.id;
      ok_resp.jsonrpc = resp.jsonrpc;

      const json res_json = ok_resp;
      const std::string res_string = res_json.dump();

      if (!res)
        {
          beast::ostream(response_.body()) << res_string;
          return;
        }

      beast::ostream(response_.body()) << res_string;

      LOG_DEBUG(res_string);
    }


    // Asynchronously transmit the response message.
    void write_response();

    // Check whether we have spent enough time on this connection.
    void check_deadline();
  };

}
