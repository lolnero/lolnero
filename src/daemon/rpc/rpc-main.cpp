/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE


Parts of this file are originally 
copyright (c) 2012-2013 The Cryptonote developers

*/

#include "daemon/rpc/daemon.h"

#include "network/rpc/core_rpc_server.h"

#include "math/crypto/controller/init.hpp"

#include "config/version/version.hpp"

#include <boost/program_options.hpp>
#include <filesystem>

int main(int argc, char const * argv[])
{
  crypto::init();


  namespace po = boost::program_options;

  uint32_t log_level = 0;
  std::string data_dir;
  std::string mining_address;
  uint32_t opencl_mining_threads = 1;
  std::string rpc_bind_ip;
  uint16_t rpc_port;
  int64_t max_in_peers = -1;
  int64_t max_out_peers = -1;

  std::vector<std::string> anonymous_inbounds;
  std::vector<std::string> proxies;

  std::vector<std::string> add_peers;
  std::vector<std::string> add_exclusive_nodes;
  std::vector<std::string> add_priority_nodes;

  uint32_t p2p_external_port = 0;

  uint32_t p2p_port;
  std::string p2p_bind_ip;

  uint32_t p2p_port_ipv6;
  std::string p2p_bind_ip_ipv6;

  uint64_t fixed_difficulty = 0;


  // Declare the supported options.
  po::options_description command_options("Commands");
  command_options.add_options()
    ("help", "")
    ("version", "")
    ;

  po::options_description setting_options("Settings");
  setting_options.add_options()
    (
     "log-level"
     , po::value<uint32_t>(&log_level)->default_value(0)
     , "0 default, 1 info, 2 verbose, 3 debug, 4 trace"
     )
    (
     "data-dir"
     , po::value<std::string>(&data_dir)
     ->default_value("lolnerod-data-dir")
     , "Specify data directory"
     )
    ;

  po::options_description p2p_options("P2P");
  p2p_options.add_options()
    (
     "p2p-bind-ip"
     , po::value<std::string>(&p2p_bind_ip)
     ->default_value(std::string(config::lol::DAEMON_P2P_DEFAULT_IP))
     , "Interface for p2p (IPv4)"
     )

    (
     "p2p-bind-port"
     , po::value<uint32_t>(&p2p_port)
     ->default_value(cryptonote::mainnet.P2P_DEFAULT_PORT)
     , "Port for p2p (IPv4)"
     )

    (
     "p2p-bind-ip-v6"
     , po::value<std::string>(&p2p_bind_ip_ipv6)
     ->default_value
     (std::string(config::lol::DAEMON_P2P_DEFAULT_IP_V6))
     , "Interface for p2p (IPv6)"
     )
    (
     "p2p-bind-port-v6"
     , po::value<uint32_t>(&p2p_port_ipv6)
     ->default_value(cryptonote::mainnet.P2P_DEFAULT_PORT)
     , "Port for p2p (IPv6)"
     )
    ("p2p-use-ipv6", "Enable IPv6 for p2p")

    ("p2p-ignore-ipv4", "Ignore unsuccessful IPv4 bind for p2p")

    (
     "p2p-external-port"
     , po::value<uint32_t>(&p2p_external_port)
     ->default_value(0u)
     , "External port for p2p network protocol "
     "(if port forwarding used with NAT)"
     )


    (
     "add-peer"
     , po::value<std::vector<std::string>>(&add_peers)
     , "Manually add peer to local peerlist"
     )

    (
     "add-exclusive-node"
     , po::value<std::vector<std::string>>(&add_exclusive_nodes)
     , "Specify list of peers to connect to only."
     " If this option is given, --add-priority-node will be ignored"
     )
    (
     "add-priority-node"
     , po::value<std::vector<std::string>>(&add_priority_nodes)
     , "Specify list of peers to connect to and attempt to keep "
     "the connection open"
     )

    (
     "anonymous-inbound"
     , po::value<std::vector<std::string>>(&anonymous_inbounds)
     , "<hidden-service-address>,<[bind-ip:]port>"
     "[,max_connections]"
     " e.g. \"x.onion,127.0.0.1:4444,100\""
     )

    (
     "proxy"
     , po::value<std::vector<std::string>>(&proxies)
     , "<network-type>,<socks-ip:port>[,max_connections]"
     )

    (
     "in-peers"
     , po::value<int64_t>(&max_in_peers)
     ->default_value(-1ll)
     , "Set max number of in peers"
     )

    (
     "out-peers"
     , po::value<int64_t>(&max_out_peers)
     ->default_value(-1ll)
     , "Set max number of out peers"
     )

    (
     "hide-my-port"
     , "Do not announce as a peerlist candidate"
     )

    (
     "ignore-checkpoints"
     , "Do not skip validation before checkpoints"
     )
    ;

  po::options_description rpc_options("RPC");
  rpc_options.add_options()
    (
     "rpc-bind-ip"
     , po::value<std::string>(&rpc_bind_ip)
     ->default_value(std::string(config::lol::DAEMON_RPC_DEFAULT_IP))
     , "RPC bind IP"
     )
    (
     "rpc-bind-port"
     , po::value<uint16_t>(&rpc_port)
     ->default_value(cryptonote::mainnet.RPC_DEFAULT_PORT)
     , "RPC port"
     )

    ;


  po::options_description mining_options("Mining");
  mining_options.add_options()
    (
     "mining-address"
     , po::value<std::string>(&mining_address)
     , "Specify wallet mining address"
     )

#ifdef OpenCL
    (
     "opencl-mining-threads"
     , po::value<uint32_t>(&opencl_mining_threads)
     ->default_value(1ul)
     , "Specify opencl mining threads count"
     )
#endif
    ;


  po::options_description testing_options("Testing");
  testing_options.add_options()
    ("testnet", "")
    (
     "fix-difficulty"
     , po::value<uint64_t>(&fixed_difficulty)
     ->default_value(0ull)
     , "Fix difficulty for testing"
     )
    ("offline", "")
    (
     "allow-local-ip"
     , "Allow local IPs in peer list"
     )
    (
     "unsafe-skip-ringct"
     , "Do not validate ringct for testing"
     )
    ;



  po::options_description cmdline_options;
  cmdline_options
    .add(command_options)
    .add(setting_options)
    .add(p2p_options)
    .add(rpc_options)
    .add(mining_options)
    .add(testing_options)
    ;

  po::variables_map vm;

  try {
    po::store
      (po::parse_command_line(argc, argv, cmdline_options), vm);
    po::notify(vm);
  }
  catch (std::exception& e) {
    std::cout << e.what() << std::endl;
    return 1;
  }

  if (vm.count("help")) {
    std::cout << cmdline_options << "\n";
    return 0;
  }

  if (vm.count("version")) {
    std::cout << cryptonote::get_version_string() << std::endl;
    return 0;
  }

  if (vm.count("unsafe-skip-ringct")) {
    cryptonote::global_setting_verify_ringct = false;
  }

  const bool testnet = vm.count("testnet");

  epee::set_log_level(log_level);


  LOG_GLOBAL(cryptonote::get_version_string());

  const auto nettype =
    testnet ? cryptonote::TESTNET : cryptonote::MAINNET;


  tools::create_directories_if_necessary(data_dir);

  const std::optional<uint64_t>
    maybe_fixed_difficulty =
    fixed_difficulty == 0
    ? std::nullopt
    : std::make_optional<uint64_t>(fixed_difficulty)
    ;

  auto daemon = daemonize::t_daemon
    (
     nettype
     , vm.count("offline")
     , data_dir
     , mining_address
     , opencl_mining_threads
     , vm.count("allow-local-ip")
     , rpc_bind_ip
     , rpc_port
     , vm.count("hide-my-port")
     , p2p_external_port
     , vm.count("p2p-use-ipv6")
     , vm.count("p2p-ignore-ipv4")
     , vm.count("ignore-checkpoints")
     , maybe_fixed_difficulty
     );

  const auto init_result = daemon.init
    (
     max_in_peers
     , max_out_peers
     , anonymous_inbounds
     , proxies
     , add_peers
     , add_exclusive_nodes
     , add_priority_nodes
     , p2p_bind_ip
     , p2p_port
     , p2p_bind_ip_ipv6
     , p2p_port_ipv6
     );

  if (!init_result) return 1;

  return daemon.run() ? 0 : 1;
}
