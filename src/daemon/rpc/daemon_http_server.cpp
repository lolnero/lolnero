/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "daemon_http_server.hpp"

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio.hpp>
#include <chrono>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <string>
#include <functional>

namespace beast = boost::beast;
namespace http = beast::http;
using tcp = boost::asio::ip::tcp;

namespace cryptonote {

  void daemon_http_connection::json_response()
    {
      LOG_DEBUG(std::string(request_.target()));
      LOG_DEBUG(request_.body());
      try {

      if(request_.target() == "/json_rpc")
        {
          json_rpc_response();
        }

#define JSON(uri, callback, request_t)                          \
      else if(request_.target() == uri)                         \
        {                                                       \
          process_json<request_t::request, request_t::response> \
            ( std::bind_front                                   \
              ( &core_rpc_server::callback                      \
                , &rpc_ ));                                     \
        }                                                       \


      JSON("/get_blocks"
           , on_get_blocks
           , COMMAND_RPC_GET_BLOCKS_FAST)

      JSON("/is_output_key_image_spent"
           , on_is_output_key_image_spent
           , COMMAND_RPC_IS_KEY_IMAGE_SPENT)

      JSON("/send_raw_transaction"
           , on_send_raw_tx
           , COMMAND_RPC_SEND_RAW_TX)

      JSON("/start_mining"
           , on_start_mining
           , COMMAND_RPC_START_MINING)

      JSON("/stop_mining"
           , on_stop_mining
           , COMMAND_RPC_STOP_MINING)

      JSON("/mining_status"
           , on_mining_status
           , COMMAND_RPC_MINING_STATUS)

      JSON("/get_peer_list"
           , on_get_peer_list
           , COMMAND_RPC_GET_PEER_LIST)

      JSON("/get_transaction_pool"
           , on_get_transaction_pool
           , COMMAND_RPC_GET_TRANSACTION_POOL)

      JSON("/get_info"
           , on_get_info
           , COMMAND_RPC_GET_INFO)

      JSON("/get_tx_outputs"
           , on_get_tx_outputs
           , COMMAND_RPC_GET_OUTPUTS)

      JSON("/pop_blocks"
           , on_pop_blocks
           , COMMAND_RPC_POP_BLOCKS)

      JSON("/get_transaction"
           , on_get_transaction
           , COMMAND_RPC_GET_TRANSACTION)

      JSON("/get_ringct"
           , on_get_ringct
           , COMMAND_RPC_GET_RING_CT)

      else
        {
          response_.result(http::status::not_found);
        }

      }
      catch (std::exception& e) {
        LOG_WARNING(e.what());
        response_.result(http::status::bad_request);
      }
      catch (...) {
        response_.result(http::status::bad_request);
      }
    }


  void daemon_http_connection::json_rpc_response()
    {
      const std::string body_ = request_.body();
      const json json_body = json::parse(body_);

      LOG_DEBUG("json_body: " + json_body.dump());

      if (!json_body.contains("method")) {
        epee::json_rpc::error_response rsp{};
        rsp.jsonrpc = "2.0";
        rsp.error.code = -32600;
        rsp.error.message = "Invalid Request";

        const json json_error = rsp;

        beast::ostream(response_.body()) << json_error;

        return;
      }

      const std::string callback_name = json_body["method"];
      const uint64_t req_id = json_body.value("id", 0);

      if (false) {}

#define JSON_RPC(method, callback, request_t)                     \
      else if(callback_name == method) {                          \
        process_json_rpc<request_t::request, request_t::response> \
          ( json_body.value("params", request_t::request{})       \
            , req_id                                              \
            , std::bind_front                                     \
            ( &core_rpc_server::callback                          \
              , &rpc_ ));                                         \
      }

      JSON_RPC("get_block"
               , on_get_block
               , COMMAND_RPC_GET_BLOCK)

      JSON_RPC("get_connections"
               ,on_get_connections
               , COMMAND_RPC_GET_CONNECTIONS)

      JSON_RPC("get_info"
               , on_get_info_json
               , COMMAND_RPC_GET_INFO)

      JSON_RPC("set_bans"
               , on_set_bans
               , COMMAND_RPC_SETBANS)

      JSON_RPC("get_bans"
               , on_get_bans
               , COMMAND_RPC_GETBANS)

      JSON_RPC("is_banned"
               , on_banned
               , COMMAND_RPC_BANNED)

      JSON_RPC("flush_tx_pool"
               , on_flush_tx_pool
               , COMMAND_RPC_FLUSH_TRANSACTION_POOL)

      JSON_RPC("get_version"
               , on_get_version
               , COMMAND_RPC_GET_VERSION)

      JSON_RPC("relay_tx"
               , on_relay_tx
               , COMMAND_RPC_RELAY_TX)

      JSON_RPC("get_decoys"
               , on_get_decoys
               , COMMAND_RPC_GET_DECOYS)
    }


  // "Loop" forever accepting new connections.
  void daemon_http_server
  (
   tcp::acceptor& acceptor
   , tcp::socket& socket
   , core_rpc_server& rpc
   )
  {
    acceptor.async_accept
      (
       socket,
       [&](beast::error_code ec)
       {
         if(!ec)
           std::make_shared<daemon_http_connection>
             (
              std::move(socket)
              , rpc
              )
             ->start()
             ;
         daemon_http_server(acceptor, socket, rpc);
       });
  }

  void start_daemon_http_server
  (
   const std::string_view ip
   , const uint16_t port
   , boost::asio::io_context& ioc
   , core_rpc_server& rpc
   )
  {
    const auto address = boost::asio::ip::make_address(ip);

    tcp::acceptor acceptor{ioc, {address, port}};
    tcp::socket socket{ioc};
    daemon_http_server(acceptor, socket, rpc);

    ioc.run();
  }

} // cryptonote
