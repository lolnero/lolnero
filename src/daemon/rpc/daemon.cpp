/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE


Parts of this file are originally 
copyright (c) 2012-2013 The Cryptonote developers

*/

#include "daemon.h"

#include "daemon_http_server.hpp"

#include "network/p2p/net_node.h"
#include "network/rpc/core_rpc_server.h"

#include "cryptonote/core/cryptonote_core.hpp"
#include "cryptonote/protocol/cryptonote_protocol_handler.h"

#include "tools/epee/include/logging.hpp"
#include "tools/common/signal.hpp"

namespace daemonize {
  constexpr std::string_view protocol_str = "protocol";
  constexpr std::string_view core_str = "core";
  constexpr std::string_view p2p_str = "P2P";

  const std::string rpc_description =
    "Lolnero daemon RPC server";

  const std::string beast_rpc_description =
    "Lolnero Beast daemon RPC server";

  const std::string p2p_description =
    "Lolnero daemon P2P server";

  void deinit_msg(const std::string_view x) {
    LOG_INFO("Deinitializing " + std::string(x) + " ...");
  }

  void deinit_done_msg(const std::string_view x) {
    LOG_INFO(std::string(x) + " deinitialized");
  }

  void deinit_error_msg(const std::string_view x) {
    LOG_ERROR("Failed to deinitialize " + std::string(x) + " ...");
  }

  t_daemon::~t_daemon()
  {
    deinit_msg(protocol_str);
    try {
      protocol.deinit();
      deinit_done_msg(protocol_str);
    } catch (...) {
      deinit_error_msg(protocol_str);
    }

    deinit_msg(p2p_str);
    try {
      p2p.deinit();
      deinit_done_msg(p2p_str);
    } catch (...) {
      deinit_error_msg(p2p_str);
    }

    deinit_msg(core_str);
    try {
      core.deinit();
      deinit_done_msg(core_str);
    } catch (...) {
      deinit_error_msg(core_str);
    }

  }

  void init_msg(const std::string_view x) {
    LOG_INFO("Initializing " + std::string(x) + " ...");
  }

  void init_done_msg(const std::string_view x) {
    LOG_INFO(std::string(x) + " initialized");
  }

  void init_report(const bool r, const std::string_view x) {
    LOG_ERROR_AND_THROW_UNLESS
      (
       r
       , "Failed to initialize "
       + std::string(x)
       );

    init_done_msg(x);
  }

  t_daemon::t_daemon
  (
   const cryptonote::network_type nettype
   , const bool offline
   , const std::string data_dir
   , const std::string mining_address
   , const uint32_t opencl_mining_threads
   , const bool allow_local_ip
   , const std::string rpc_ip
   , const uint16_t rpc_port
   , const bool hide_my_port
   , const uint32_t p2p_external_port
   , const bool p2p_use_ipv6
   , const bool p2p_ignore_ipv4
   , const bool ignore_checkpoints
   , const std::optional<uint64_t> maybe_fix_difficulty
   )
    : core
      (
       protocol
       , nettype
       , offline
       , data_dir
       , mining_address
       , opencl_mining_threads
       , ignore_checkpoints
       , maybe_fix_difficulty
       )
    , p2p
      (
       protocol
       , nettype
       , offline
       , data_dir
       , allow_local_ip
       , hide_my_port
       , p2p_external_port
       , p2p_use_ipv6
       , p2p_ignore_ipv4
       )
    , rpc
      (
       core
       , p2p
       )
    , protocol
      (
       core
       , p2p
       , offline
       )
    , rpc_ip(rpc_ip)
    , rpc_port(rpc_port)
  {
  }

  bool t_daemon::init
    (
     const int64_t max_in_peers
     , const int64_t max_out_peers
     , const std::vector<std::string> anonymous_inbounds
     , const std::vector<std::string> proxies
     , const std::vector<std::string> add_peers
     , const std::vector<std::string> add_exclusive_nodes
     , const std::vector<std::string> add_priority_nodes
     , const std::string p2p_bind_ip
     , const uint32_t p2p_port
     , const std::string p2p_bind_ip_ipv6
     , const uint32_t p2p_port_ipv6
     ) noexcept
  {
    try
      {
        init_msg(p2p_str);
        init_report
          (
           p2p.init
           (
            max_in_peers
            , max_out_peers
            , anonymous_inbounds
            , proxies
            , add_peers
            , add_exclusive_nodes
            , add_priority_nodes
            , p2p_bind_ip
            , p2p_port
            , p2p_bind_ip_ipv6
            , p2p_port_ipv6
            )
           , p2p_str
           );

        init_msg(protocol_str);
        init_report(protocol.init(), protocol_str);

        init_msg(core_str);
        init_report(core.init(), core_str);

        return true;
      }
    catch (std::exception const & ex)
      {
        LOG_FATAL("daemon init error: " + std::string(ex.what()));
        return false;
      }
    catch (...)
      {
        LOG_FATAL("daemon init uncaught error");
        return false;
      }
  }


  bool t_daemon::run() noexcept
  {
    try
      {
        tools::signal_handler_install([this](int type) {
          LOG_INFO
            (
             "Daemon interrupted with signal: "
             + std::to_string(type)
             );

          LOG_INFO
            ("Stopping " + p2p_description + " ...");
          p2p.send_stop_signal();
        });


        boost::asio::io_context ioc{1};

        boost::asio::signal_set signals(ioc, SIGINT, SIGTERM);
        signals.async_wait(tools::signal_handler);

        LOG_GLOBAL
          (
           "Starting "
           + beast_rpc_description
           + " on "
           + rpc_ip
           + ":"
           + std::to_string(rpc_port)
           + " ..."
           );

        std::thread beast_thread([&] {
          cryptonote::start_daemon_http_server
            (
             rpc_ip
             , rpc_port
             , ioc
             , rpc
             );
        });
        LOG_INFO(beast_rpc_description + " started");

        // blocks until p2p goes down
        LOG_GLOBAL("Starting " + p2p_description + " ...");
        p2p.run();
        LOG_GLOBAL(p2p_description + " stopped");

        // stop everything

        LOG_INFO("Stopping " + beast_rpc_description + " ...");
        ioc.stop();
        beast_thread.join();
        LOG_GLOBAL(beast_rpc_description + " stopped");

        protocol.stop();
        core.stop();

        return true;
      }
    catch (std::exception const & ex)
      {
        LOG_FATAL("daemon error: " + std::string(ex.what()));
        return false;
      }
    catch (...)
      {
        LOG_FATAL("daemon uncaught error");
        return false;
      }
  }

} // namespace daemonize
