/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/

#pragma once

#include "subaddress_index.hpp"

#include "math/crypto/functional/key.hpp"


namespace cryptonote {

  struct spend_view_secret_keys
  {
    crypto::secret_key   m_spend_secret_key;
    crypto::secret_key   m_view_secret_key;
  };

  struct spend_view_public_keys_unsafe
  {
    crypto::ec_point_unsafe m_spend_public_key_unsafe;
    crypto::ec_point_unsafe m_view_public_key_unsafe;
  };

  struct spend_view_public_keys
  {
    crypto::public_key m_spend_public_key;
    crypto::public_key m_view_public_key;

    bool operator==(const spend_view_public_keys& rhs) const =
      default;

    auto operator <=> (const spend_view_public_keys& x) const = default;
  };

  crypto::secret_key get_subaddress_spend_secret_key
  (
   const cryptonote::spend_view_secret_keys keys
   , const cryptonote::subaddress_index index
   );

  crypto::secret_key get_subaddress_view_secret_key_base_G
  (
   const cryptonote::spend_view_secret_keys keys
   , const cryptonote::subaddress_index index
   );

  crypto::public_key get_subaddress_spend_public_key
  (
   const cryptonote::spend_view_secret_keys keys
   , const cryptonote::subaddress_index index
   );

  crypto::public_key get_subaddress_view_public_key
  (
   const cryptonote::spend_view_secret_keys keys
   , const cryptonote::subaddress_index index
   );

  std::vector<crypto::public_key> get_subaddress_spend_public_keys
  (
   const cryptonote::spend_view_secret_keys keys
   , const uint32_t account
   , const uint32_t begin
   , const uint32_t end
   );

  cryptonote::spend_view_public_keys get_subaddress
  (
   const cryptonote::spend_view_secret_keys keys
   , const cryptonote::subaddress_index index
   );

  crypto::ec_scalar hash_secret_key_with_subaddress_index
  (
   const crypto::secret_key sec
   , const cryptonote::subaddress_index index
   );

  std::optional<spend_view_public_keys>
  maybe_safe_spend_view_public_keys
  (const spend_view_public_keys_unsafe x);

}

BLOB_SERIALIZER(cryptonote::spend_view_public_keys);
BLOB_SERIALIZER(cryptonote::spend_view_public_keys_unsafe);
