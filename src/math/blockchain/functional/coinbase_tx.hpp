/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#pragma once

#include "tx_common.hpp"

namespace cryptonote {

  struct coinbase_output
  {
    uint64_t amount;
    crypto::public_key public_key;

    auto operator <=> (const coinbase_output& x) const = default;
  };

  struct coinbase_tx: tx_common
  {
    uint64_t index;
    crypto::public_key ecdh_tx_public_key;
    std::vector<coinbase_output> outputs;

    crypto::hash hash() const;

    auto operator <=> (const coinbase_tx& x) const = default;
  };

  std::optional<coinbase_tx>
  preview_coinbase_tx(const transaction& tx);

  transaction review_coinbase_tx(const coinbase_tx& tx);

  string_blob review_coinbase_tx_to_blob(const coinbase_tx& x);
}
