/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Parts of this file are originally
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE


Parts of this file are originally
copyright (c) 2012-2013 The Cryptonote developers

*/

#include "tx_check.hpp"

#include "cryptonote/basic/functional/format_utils.hpp"

#include "tools/epee/functional/algorithm.hpp"

#include "math/consensus/consensus.hpp"
#include "math/ringct/functional/ringCT.hpp"

namespace cryptonote
{
  bool check_coinbase_fee
  (
   const coinbase_tx tx
   , const uint64_t fee
   )
  {
    std::vector<rct::amount_t> output_amount;
    std::transform
      (
       tx.outputs.begin()
       , tx.outputs.end()
       , std::back_inserter(output_amount)
       , [](const auto& x) { return x.amount; }
       );

    if(!consensus::
       rule_19_coinbase_tx_should_be_balanced(output_amount, fee))
      {
        LOG_ERROR("coinbase transaction is not balanced.");
        return false;
      }

    return true;
  }

  bool check_fee(const size_t tx_size, const uint64_t fee)
  {
    const auto min_fee =
      consensus::
      rule_26_minimum_fee_should_be_tx_size_times_fee_per_byte
      (static_cast<uint64_t>(tx_size));

    if(fee < min_fee){
      LOG_ERROR
        (
         "transaction doesn't have enough fee: "
         + print_money(fee)
         + ", minimum fee: "
         + print_money(min_fee)
         );

      return false;
    }

    return true;
  }

} // cryptonote
