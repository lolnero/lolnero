/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/

#include "subaddress.hpp"


#include "tools/epee/functional/int-util.hpp"
#include "tools/epee/functional/span.hpp"
#include "tools/epee/include/string_tools.h"

#include "math/crypto/controller/keyGen.hpp"

#include "config/lol.hpp"


namespace cryptonote {

  crypto::secret_key get_subaddress_spend_secret_key
  (
   const cryptonote::spend_view_secret_keys keys
   , const cryptonote::subaddress_index index
   )
  {
    if (index.is_zero())
      return keys.m_spend_secret_key;

    const auto offset =
      hash_secret_key_with_subaddress_index
      (
       keys.m_view_secret_key
       , index
       );

    return crypto::s2sk(keys.m_spend_secret_key + offset);
  }

  crypto::secret_key get_subaddress_view_secret_key_base_G
  (
   const cryptonote::spend_view_secret_keys keys
   , const cryptonote::subaddress_index index
   )
  {
    return index.is_zero()
      ? keys.m_view_secret_key
      : s2sk
      (
       keys.m_view_secret_key
       * get_subaddress_spend_secret_key(keys, index)
       );
  }

  crypto::public_key get_subaddress_spend_public_key
  (
   const cryptonote::spend_view_secret_keys keys
   , const cryptonote::subaddress_index index
   )
  {
    return
      crypto::to_pk(get_subaddress_spend_secret_key(keys, index));
  }

  crypto::public_key get_subaddress_view_public_key
  (
   const cryptonote::spend_view_secret_keys keys
   , const cryptonote::subaddress_index index
   )
  {
    return crypto::to_pk
      (get_subaddress_view_secret_key_base_G(keys, index));
  }


  std::vector<crypto::public_key> get_subaddress_spend_public_keys
  (
    const cryptonote::spend_view_secret_keys keys
    , const uint32_t account
    , const uint32_t begin
    , const uint32_t end
    )
  {
    LOG_ERROR_AND_THROW_UNLESS(begin <= end, "begin > end");

    std::vector<crypto::public_key> pkeys;

    for (uint32_t idx = begin; idx < end; ++idx)
    {
      pkeys.push_back
        (get_subaddress_spend_public_key(keys, {account, idx}));
    }
    return pkeys;
  }


  cryptonote::spend_view_public_keys get_subaddress
  (
   const cryptonote::spend_view_secret_keys keys
   , const cryptonote::subaddress_index index
   )
  {
    return
      {
        cryptonote::get_subaddress_spend_public_key(keys, index)
        , cryptonote::get_subaddress_view_public_key(keys, index)
      };
  }

  crypto::ec_scalar hash_secret_key_with_subaddress_index
  (
   const crypto::secret_key sec
   , const cryptonote::subaddress_index index
   )
  {
    const uint32_t major_i = SWAP32LE(index.major);
    const uint32_t minor_i = SWAP32LE(index.minor);
    const auto major = epee::pod_to_span(major_i);
    const auto minor = epee::pod_to_span(minor_i);

    // here trailing 0 is part of the HASH_KEY ..
    const epee::blob::data hashData =
      epee::string_tools::string_to_blob(config::HASH_KEY_SUBADDRESS)
      + epee::blob::data({0})
      + epee::blob::data(sec.data.begin(), sec.data.size())
      + epee::blob::data(major.begin(), major.end())
      + epee::blob::data(minor.begin(), minor.end())
      ;

    return crypto::hash_to_scalar(hashData);
  }

  std::optional<spend_view_public_keys>
  maybe_safe_spend_view_public_keys
  (const spend_view_public_keys_unsafe x)
  {
    const auto spend_pk =
      preview_safe_point(x.m_spend_public_key_unsafe);

    const auto view_pk =
      preview_safe_point(x.m_view_public_key_unsafe);

    if (spend_pk && view_pk) {
      return {{{*spend_pk}, {*view_pk}}};
    }
    else {
      return {};
    }
  }


}
