/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#pragma once

#include "ringct.hpp"
#include "coinbase_tx.hpp"

namespace cryptonote {

  struct block_t
  {
    uint8_t major_version;
    uint8_t minor_version;
    uint64_t timestamp;

    crypto::hash prev_hash;
    uint64_t nonce;

    coinbase_tx miner_tx;
    std::vector<ringct> txs;

    crypto::hash hash() const;

    std::vector<crypto::public_key> get_outputs() const;
    std::vector<crypto::key_image> get_key_images() const;
    uint64_t get_index() const;

    auto operator <=> (const block_t& x) const = default;
  };

  std::optional<block_t> preview_block
  (
   const raw_block& x
   , const std::span<const ringct> txs
   );

  raw_block review_block
  (
   const block_t& x
   );

  block_and_txs_data_t review_block_to_block_and_txs_data_t
  (const block_t& b);

  std::optional<block_t> preview_block_from_block_and_txs_data_t
  (const block_and_txs_data_t& b);
}
