/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#pragma once

#include "tx_common.hpp"

#include "math/ringct/functional/rctTypes.hpp"
#include "cryptonote/basic/type/string_blob_type.hpp"

namespace cryptonote {

  extern bool global_setting_verify_ringct;

  struct ringct_input
  {
    crypto::key_image key_image;
    std::vector<uint64_t> output_relative_offsets;
    crypto::ec_point pseudo_input_commit;
    rct::clsag signature;

    auto operator <=> (const ringct_input& x) const = default;
  };

  struct ringct_output
  {
    rct::output_public_data output;
    uint64_t ecdh_encrypted_amount;
    crypto::public_key ecdh_output_public_key;
    uint64_t ecdh_encrypted_payment_id = 0;

    auto operator <=> (const ringct_output& x) const = default;
  };

  struct ringct: tx_common
  {
    std::vector<ringct_input> inputs;
    std::vector<ringct_output> outputs;

    uint64_t fee;

    rct::Bulletproof bulletproof;

    crypto::hash hash() const;

    std::vector<crypto::key_image> get_key_images() const;
    std::vector<crypto::public_key> get_output_pks() const;
    std::vector<crypto::ec_point> get_output_commits() const;
    std::vector<crypto::ec_point> get_pseudo_input_commits() const; 
    std::vector<uint64_t> get_ecdh_encrypted_amounts() const;
    std::vector<rct::clsag> get_signatures() const;

    crypto::hash get_tx_prefix_hash() const;

    auto operator <=> (const ringct& x) const = default;
  };

  std::optional<ringct> preview_ringct(const transaction& tx);

  transaction review_ringct(const ringct& x);

  std::optional<ringct> preview_ringct_from_blob
  (const string_blob_view tx_blob);

  string_blob review_ringct_to_blob(const ringct& x);
}
