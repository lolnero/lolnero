/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE


Parts of this file are originally 
copyright (c) 2012-2013 The Cryptonote developers

*/

#pragma once

#include "cryptonote/basic/type/string_blob_type.hpp"
#include "cryptonote/basic/functional/base.hpp"

#include "math/blockchain/functional/coinbase_tx.hpp"
#include "math/blockchain/functional/ringct.hpp"

#include "math/consensus/consensus.hpp"

#include <set>

namespace cryptonote
{
  bool check_coinbase_fee
  (
   const coinbase_tx tx
   , const uint64_t fee
   );

  bool check_fee(const size_t tx_size, const uint64_t fee);

  consteval uint64_t get_max_tx_size()
  {
    return consensus::get_minimum_block_size_bound() / 2;
  }
}
