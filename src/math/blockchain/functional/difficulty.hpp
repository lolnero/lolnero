/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#pragma once

#include "math/hash/functional/hash.hpp"

#include "config/lol.hpp"

// strangely needed by ubuntu 21.04 which has gcc 10.3.0
#include <numeric>

#include <boost/multiprecision/cpp_int.hpp>


namespace cryptonote
{

  struct diff_pair_t
  {
    uint64_t high;
    uint64_t low;
  };

  using diff_t = boost::multiprecision::uint128_t;

  consteval diff_t initial_diff() {
    constexpr diff_t _b = 1;
    return _b << 38;
  }

  constexpr boost::multiprecision::uint512_t max256bit
  (std::numeric_limits<boost::multiprecision::uint256_t>::max());

  constexpr boost::multiprecision::uint512_t
  max_int_for_diff(const diff_t difficulty)
  {
    return max256bit / difficulty;
  }

  constexpr bool check_hash_int
  (
   const boost::multiprecision::uint512_t hashInt
   , const diff_t difficulty
   )
  {
    return hashInt * difficulty <= max256bit;
  }

  // LWMA-1 difficulty algorithm
  // https://github.com/zawy12/difficulty-algorithms/issues/3
  // rewritten in purely functional style by fuwa
  constexpr diff_t next_difficulty_pure
  (
   const std::span<const uint64_t> timestamps
   , const std::span<const diff_t> cumulative_difficulties
   , const uint64_t next_block_index
   )
  {
    constexpr uint64_t T = constant::DIFFICULTY_TARGET_IN_SECONDS;
    constexpr uint64_t N = constant::DIFFICULTY_WINDOW_IN_BLOCKS;

    struct L_collector {
      uint64_t linear_index;
      uint64_t sum;
      uint64_t last;
    };

    constexpr auto accumulate_linearly_weighted_timestamp_diff =
      [](const L_collector x, const uint64_t t) -> L_collector
      {
        constexpr uint64_t maximum_allowed_time_diff = 6 * T;
        constexpr uint64_t dt = 1;

        const bool is_past_solve_time = t <= x.last;
        const uint64_t accepted_time_diff =
          is_past_solve_time
          ? dt
          : std::min<uint64_t>
          ( t - x.last, maximum_allowed_time_diff )
          ;

        const uint64_t weight = x.linear_index * accepted_time_diff;

        const uint64_t accepted_timestamp =
          is_past_solve_time
          ? x.last + dt
          : t
          ;

        return L_collector
          {
            x.linear_index + 1
            , x.sum + weight
            , accepted_timestamp
          };
      };


    // potential bug here, timestamps[0] should already be T seconds
    // away from timestamps[1] but not worth fixing, since it's a
    // surplus at the least significant weight index, should affect
    // less than 1 second of target (28ms?), so not really observable.

    const L_collector l_init{ 1, 0, timestamps.front() - T };

    const L_collector l_collector = std::accumulate
     (
       std::next(timestamps.begin())
       , timestamps.end()
       , l_init
       , accumulate_linearly_weighted_timestamp_diff
       );

    constexpr uint64_t min_weight = N * N * T / 20;
    const uint64_t L =
      std::max<uint64_t>(l_collector.sum, min_weight);


    using namespace boost::multiprecision;

    const uint256_t avg_D =
      uint256_t
      (
       cumulative_difficulties.back()
       - cumulative_difficulties.front()
       ) / uint256_t(N);

    constexpr uint256_t n_n_plus_1_t_99 = N * (N + 1) * T * 99;
    const uint256_t l_200 = 200 * L;
    const uint256_t up = avg_D * n_n_plus_1_t_99;
    constexpr uint64_t overflow_until_height = 279;

    const uint256_t next_D =
      next_block_index < overflow_until_height ?
      // overflow bug fix
      uint256_t(uint64_t(up)) / l_200
      : up / l_200;

    return uint128_t(next_D);
  }


  boost::multiprecision::uint512_t
  hash_to_int(const crypto::hash &hash);

  bool check_hash(const crypto::hash &hash, const diff_t difficulty);

  crypto::hash int_to_hash(const boost::multiprecision::uint512_t x);

  diff_t next_difficulty
  (
   const std::vector<std::uint64_t> timestamps
   , const std::vector<diff_t> cumulative_difficulties
   , const uint64_t block_index
   );

  std::string diff_to_hex(const diff_t v);
  std::string diff_to_hex_no_prefix(const diff_t v);

  diff_pair_t diff_to_pair(const diff_t x);
  diff_t pair_to_diff(const diff_pair_t x);
}
