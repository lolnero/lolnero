/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include "block.hpp"

#include "tools/epee/include/logging.hpp"

#include "cryptonote/basic/functional/object_hash.hpp"
#include "cryptonote/basic/functional/format_utils.hpp"
#include "cryptonote/tx/functional/tx_utils.hpp"

#include "math/consensus/consensus.hpp"

#include <ranges>
#include <execution>

namespace cryptonote {

  uint64_t block_t::get_index() const {
    return this->miner_tx.index - 1;
  }

  crypto::hash block_t::hash() const {
    return get_block_hash(review_block(*this));
  }

  std::vector<crypto::public_key> block_t::get_outputs() const {

    const auto coinbase_outputs = 
      miner_tx.outputs
      | std::views::transform
      ([](const auto& x) { return x.public_key; })
      ;

    const auto xs =
      txs
      | std::views::transform
      ([](const auto& x) { return x.get_output_pks(); })
      ;

    const std::vector<std::vector<crypto::public_key>> ys
      = { xs.begin(), xs.end() };

    const auto zs = std::ranges::join_view(ys);

    using outputV = std::basic_string<crypto::public_key>;

    const auto outputs =
      outputV{ coinbase_outputs.begin(), coinbase_outputs.end() }
    + outputV {zs.begin(), zs.end() }
    ;
    

    return { outputs.begin(), outputs.end() };
  }

  std::vector<crypto::key_image> block_t::get_key_images() const {
    const auto xs =
      txs
      | std::views::transform
      (std::bind_front(&ringct::get_key_images))
      ;

    const std::vector<std::vector<crypto::key_image>> ys
      = { xs.begin(), xs.end() };

    const auto zs = std::ranges::join_view(ys);

    return { zs.begin(), zs.end() };
  }
    

  std::optional<block_t> preview_block
  (
   const raw_block& x
   , const std::span<const ringct> txs
   )
  {
    const auto maybe_coinbase = preview_coinbase_tx(x.miner_tx);

    if (!maybe_coinbase) {
      LOG_DEBUG("block has invalid miner tx");
      return {};
    }

    const auto miner_tx = *maybe_coinbase;

    const block_t block =
      {
        x.major_version
        , x.minor_version
        , x.timestamp
        , x.prev_hash
        , x.nonce
        , miner_tx
        , {txs.begin(), txs.end()}
      };

    if
      (
       !consensus::rule_8_input_key_images_should_be_unique
       (block.get_key_images())
       )
      {
        LOG_WARNING("duplicated key images in block");
        return {};
      }

    if
      (
       !consensus::rule_32_output_public_keys_should_be_unique
       (block.get_outputs())
       )
      {
        LOG_WARNING("duplicated output public keys in block");
        return {};
      }


    return block;
  }

  raw_block review_block
  (
   const block_t& x
   )
  {
    const auto unsafe_txs =
      x.txs
      | std::views::transform
      (
       [](const auto& x) {
         return get_transaction_hash(review_ringct(x));
       }
       );

    const block_header h =
      {
        x.major_version
        , x.minor_version
        , x.timestamp
        , x.prev_hash
        , x.nonce
      };

    return
      {
        { h }
        , review_coinbase_tx(x.miner_tx)
        , { unsafe_txs.begin(), unsafe_txs.end() }
      };
  }

  block_and_txs_data_t review_block_to_block_and_txs_data_t
  (const block_t& b)
  {
    const auto txs =
      b.txs
      | std::views::transform(review_ringct_to_blob)
      ;

    return
      {
        block_to_blob(review_block(b))
        , { txs.begin(), txs.end() }
      };
  }

  std::optional<block_t> preview_block_from_block_and_txs_data_t
  (const block_and_txs_data_t& b) {

    return preview_block_from_blob(b.block)
      .and_then
      (
       [&b](const auto& block) -> std::optional<block_t> {
         std::vector<std::optional<ringct>> maybe_txs(b.txs.size());

         std::transform
           (
            std::execution::par_unseq
            , b.txs.begin()
            , b.txs.end()
            , maybe_txs.begin()
            , preview_ringct_from_blob
            );

         std::list<ringct> txs;

         for (const auto& maybe_tx: maybe_txs) {
           if (!maybe_tx) return {};

           txs.push_back(*maybe_tx);
         }

         return preview_block
           (block, std::vector<ringct>{txs.begin(), txs.end()});
       }
       )
      ;
  }

}
