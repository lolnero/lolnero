/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#pragma once

#include "cryptonote/basic/functional/base.hpp"

namespace cryptonote {

  struct tx_extra
  {
    std::vector<uint8_t> data;

    auto operator <=> (const tx_extra& x) const = default;
  };

  struct tx_common
  {
    uint64_t unlock_block_index;
    tx_extra extra;

    auto operator <=> (const tx_common& x) const = default;
  };

  std::optional<std::vector<crypto::public_key>>
  are_tx_output_pks_valid(const transaction& tx);

  bool check_ringct_points(const transaction& tx);
}
