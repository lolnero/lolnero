/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "difficulty.hpp"

#include "tools/epee/include/logging.hpp"

namespace cryptonote {

  boost::multiprecision::uint512_t
  hash_to_int(const crypto::hash &hash)
  {
    boost::multiprecision::uint512_t v;

    boost::multiprecision::import_bits
      (
       v
       , std::begin(hash.data)
       , std::end(hash.data)
       , 0
       , false
       );

    return v;
  }

  crypto::hash int_to_hash(const boost::multiprecision::uint512_t x)
  {
    boost::multiprecision::uint512_t v = x;
    crypto::hash h;
    for (size_t i = 0; i < HASH_SIZE; i++) {
      h.data[i] = (uint8_t)v;
      v = v >> 8;
    }
    return h;
  }

  bool check_hash(const crypto::hash &hash, const diff_t difficulty)
  {
    return check_hash_int(hash_to_int(hash), difficulty);
  }

  diff_t next_difficulty
  (
   const std::vector<std::uint64_t> timestamps
   , const std::vector<diff_t> cumulative_difficulties
   , const uint64_t next_block_index
   )
  {
    constexpr uint64_t N = constant::DIFFICULTY_WINDOW_IN_BLOCKS;

    LOG_ERROR_AND_THROW_UNLESS
      (
       timestamps.size() == cumulative_difficulties.size()
       , "timestamp size mismatch"
       );

    // constant initial diff for CPU farms which never came
    if (next_block_index < N + 2) { return initial_diff(); }

    LOG_ERROR_AND_THROW_UNLESS
      (
       timestamps.size() == constant::DIFFICULTY_BLOCKS_COUNT
       , "timestamp size is invalid"
       );

    std::array<std::uint64_t, constant::DIFFICULTY_BLOCKS_COUNT>
      timestamps_array;

    std::ranges::copy
      (
       timestamps
       , timestamps_array.begin()
       );

    std::array<diff_t, constant::DIFFICULTY_BLOCKS_COUNT>
      cumulative_difficulties_array;

    std::ranges::copy
      (
       cumulative_difficulties
       , cumulative_difficulties_array.begin()
       );

    const diff_t next = next_difficulty_pure
      (
       timestamps_array
       , cumulative_difficulties_array
       , next_block_index
       );

    return next;
  }

  std::string diff_to_hex_no_prefix(const diff_t _v)
  {
    std::stringstream stream;
    stream << std::hex << _v;
    return stream.str();
  }

  std::string diff_to_hex(const diff_t _v) {
    return "0x" + diff_to_hex_no_prefix(_v);
  }


  diff_pair_t diff_to_pair(const diff_t x) {
    const uint64_t high = static_cast<uint64_t>(x >> 64);
    const uint64_t low = static_cast<uint64_t>(x);
    return {high, low};
  }
   
  diff_t pair_to_diff(const diff_pair_t x) {
    const auto high = static_cast<diff_t>(x.high) << 64;
    const auto low = static_cast<diff_t>(x.low);
    return high + low;
  }
}
