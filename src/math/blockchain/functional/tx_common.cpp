/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include "tx_common.hpp"

#include "tools/epee/include/logging.hpp"
#include "math/consensus/consensus.hpp"

#include <ranges>

namespace cryptonote {

  std::optional<std::vector<crypto::public_key>>
  are_tx_output_pks_valid(const transaction& tx) {

    const auto output_targets =
      tx.vout
      | std::views::transform
      (
       [](const auto& x) {
         return x.target;
       }
       );

    return consensus::are_tx_output_targets_valid
      (
       std::vector<txout_target_v>
       { output_targets.begin(), output_targets.end() }
       )
      .and_then
      (
       [output_targets](const auto& targets) {
         const auto output_public_keys =
           targets
           | std::views::transform
           (
            [](const auto& x) {
              return x.output_public_key;
            }
            );

         return consensus::
           rule_11_tx_output_public_keys_should_be_safe_points
           (
            std::vector<crypto::ec_point_unsafe>
            { output_public_keys.begin(), output_public_keys.end() }
            );
       }
       );
  }

  bool check_ringct_points(const transaction& tx) {
    // double check points in ringct
    const auto output_commits =
      tx.ringct.output_commits
      | std::views::transform
      (
       [](const auto& x) {
         return x.commit;
       }
       );

    const bool valid_output_commits =
      consensus::
      rule_13_tx_output_commits_should_be_safe_points
      (
       std::vector<crypto::ec_point_unsafe>
       { output_commits.begin(), output_commits.end() }
       ).has_value();

    const auto pseudo_input_commits =
      tx.ringct.p.pseudo_input_commits
      | std::views::transform
      (
       [](const auto& x) {
         return x;
       }
       );

    const bool valid_pseudo_input_commits =
      consensus::
      rule_14_tx_pseudo_input_commits_should_be_safe_points
      (
       std::vector<crypto::ec_point_unsafe>
       { pseudo_input_commits.begin(), pseudo_input_commits.end() }
       ).has_value();


    return valid_output_commits && valid_pseudo_input_commits;
  }

}
