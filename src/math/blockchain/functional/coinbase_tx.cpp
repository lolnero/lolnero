/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include "coinbase_tx.hpp"

#include "tools/epee/include/logging.hpp"
#include "math/consensus/consensus.hpp"
#include "cryptonote/basic/functional/object_hash.hpp"
#include "cryptonote/basic/functional/tx_extra.hpp"

#include <ranges>

namespace cryptonote {

  crypto::hash coinbase_tx::hash() const {
    return get_transaction_hash(review_coinbase_tx(*this));
  }

  std::optional<coinbase_tx> preview_coinbase_tx(const transaction& tx)
  {
    ASSERT_OR_LOG_TRACE_AND_RETURN
      (
       tx.version == config::lol::tx_version
       , {}
       , "Invalid coinbase transaction version: "
       + std::to_string(tx.version)
       + ", expected: "
       + std::to_string(config::lol::tx_version)
       );

    ASSERT_OR_LOG_TRACE_AND_RETURN
      (
       consensus::
       rule_28_coinbase_tx_should_have_rct_null_type_in_rct_data
       (tx.ringct.rct_type)
      , {}
      , "Invalid ringct type for coinbase"
      );
      

    const auto maybe_vin =
      consensus::
      rule_22_coinbase_tx_should_have_only_one_input(tx.vin);

    ASSERT_OR_LOG_TRACE_AND_RETURN
      (
       maybe_vin
        , {}
        , "Wrong number of inputs"
        );

    const auto maybe_input =
      consensus::
      rule_23_coinbase_tx_input_type_should_be_gen(*maybe_vin);

    ASSERT_OR_LOG_TRACE_AND_RETURN
      (
       maybe_input
       , {}
       , "input has the wrong type"
       );

    const auto input = *maybe_input;

    const auto maybe_outputs = are_tx_output_pks_valid(tx);
    ASSERT_OR_LOG_TRACE_AND_RETURN
      (
       maybe_outputs
       , {}
       , "invalid outputs"
       );

    const std::vector<crypto::public_key> output_public_keys =
      *maybe_outputs;


    const auto amounts =
      tx.vout
      | std::views::transform
      (
       [](const auto& x) {
         return x.amount;
       }
       );

    ASSERT_OR_LOG_TRACE_AND_RETURN
      (
       consensus::
       rule_24_coinbase_output_amount_sum_should_not_overflow_amount_t
       (std::vector<uint64_t>{ amounts.begin(), amounts.end() })
       , {}
       , "coinbase transaction has money overflow in block"
       );

    if (consensus::after_unlock_ignore_height(input.block_index)) {
      ASSERT_OR_LOG_TRACE_AND_RETURN
        (
         consensus::rule_20_coinbase_outputs_are_unlocked
         (tx.unlock_block_index)
         , {}
         , "coinbase transaction has the wrong unlock time="
         + std::to_string(tx.unlock_block_index)
         + ", expected 0"
        );
    }


    const tx_common common = {
      tx.unlock_block_index
      , { tx.extra }
    };


    const auto maybe_ecdh_tx_public_key = get_tx_ecdh_public_key_from_extra(tx.extra);
    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       maybe_ecdh_tx_public_key
       , {}
       , "no tx ecdh public key in tx_extra"
      );

    const auto ecdh_tx_public_key = *maybe_ecdh_tx_public_key;

    std::vector<coinbase_output> outputs;
    std::transform
      (
       amounts.begin()
       , amounts.end()
       , output_public_keys.begin()
       , std::back_inserter(outputs)
       , [](const auto&x, const auto& y) -> coinbase_output {
         return {x, y};
       }
       );

    const coinbase_tx x = {
      { common }
      , input.block_index
      , ecdh_tx_public_key
      , outputs
    };

    return x;
  }

  transaction review_coinbase_tx(const coinbase_tx& tx) {

    const txin_v input = txin_gen { tx.index };

    const auto outputs =
      tx.outputs
      | std::views::transform
      (
       [](const auto& x) -> tx_out {
         const txout_target_v target =
           txout_to_key { x.public_key };

         return
           {
             x.amount
             , target
           };
       }
       )
      ;

    const transaction_prefix tx_prefix =
      {
        config::lol::tx_version
        , tx.unlock_block_index
        , { input }
        , { outputs.begin(), outputs.end() }
        , tx.extra.data
      };

    const transaction x = {
      { tx_prefix }
      , {}
    };

    return x;
  }

  string_blob review_coinbase_tx_to_blob(const coinbase_tx& x) {
    return t_serializable_object_to_blob(review_coinbase_tx(x));
  }

}
