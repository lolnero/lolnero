/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include "short_chain_history.hpp"

#include <functional>
#include <ranges>

namespace cryptonote {

  constexpr std::pair<size_t, size_t>
  ceiling_log2_review(const size_t x)
  {
    size_t y = 1;
    size_t _log = 0;

    while (y < x) {
      _log++;
      y = y << 1;
    }

    return {y, _log};
  }


  std::list<size_t> build_short_chain_history(const size_t size)
  {
    if (size == 0) return {};
    if (size == 1) return {0};

    const auto last_index = size - 1;

    const auto log_2_index =
      ceiling_log2_review(last_index).second;

    const auto powers_of_2 =
      std::ranges::iota_view(0uz, log_2_index)
      | std::views::transform([](const auto x) { return 1 << x; })
      ;

    auto xs =
      std::list<size_t>{ powers_of_2.begin(), powers_of_2.end() };

    xs.push_front(0);

    if (xs.back() >= last_index) {
      xs.back() = last_index;
    } else {
      xs.push_back(last_index);
    }

    const auto chain_indices =
      xs
      | std::views::transform
      (std::bind_front(std::minus(), last_index))
      ;

    return { chain_indices.begin(), chain_indices.end() };
  }
}
