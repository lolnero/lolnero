/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include "ringct.hpp"

#include "tools/epee/include/logging.hpp"
#include "math/consensus/consensus.hpp"
#include "cryptonote/basic/functional/object_hash.hpp"
#include "cryptonote/basic/functional/tx_extra.hpp"

#include "math/ringct/functional/ringCT.hpp"

#include <ranges>

namespace cryptonote {

  bool global_setting_verify_ringct = true;

  crypto::hash ringct::hash() const {
    return get_transaction_hash(review_ringct(*this));
  }

  std::vector<crypto::key_image> ringct::get_key_images() const {
    const auto xs_view =
      inputs
      | std::views::transform
      ([](const auto& x) { return x.key_image; })
      ;

    return { xs_view.begin(), xs_view.end() };
  }

  std::vector<rct::clsag> ringct::get_signatures() const {
    const auto xs_view =
      inputs
      | std::views::transform
      ([](const auto& x) { return x.signature; })
      ;

    return { xs_view.begin(), xs_view.end() };
  }

  std::vector<crypto::public_key> ringct::get_output_pks() const {
    const auto xs_view =
      outputs
      | std::views::transform
      ([](const auto& x) { return x.output.public_key; })
      ;

    return { xs_view.begin(), xs_view.end() };
  }

  std::vector<crypto::ec_point> ringct::get_output_commits() const {
    const auto xs_view =
      outputs
      | std::views::transform
      ([](const auto& x) { return x.output.commit; })
      ;

    return { xs_view.begin(), xs_view.end() };
  }

  std::vector<crypto::ec_point>
  ringct::get_pseudo_input_commits() const
  {
    const auto xs_view =
      inputs
      | std::views::transform
      ([](const auto& x) { return x.pseudo_input_commit; })
      ;

    return { xs_view.begin(), xs_view.end() };
  }

  std::vector<uint64_t> ringct::get_ecdh_encrypted_amounts() const {
    const auto xs_view =
      outputs
      | std::views::transform
      ([](const auto& x) { return x.ecdh_encrypted_amount; })
      ;

    return { xs_view.begin(), xs_view.end() };
  }

  crypto::hash ringct::get_tx_prefix_hash() const {
    return get_transaction_prefix_hash(review_ringct(*this));
  }

  std::optional<ringct> preview_ringct(const transaction& tx)
  {
    ASSERT_OR_LOG_TRACE_AND_RETURN
      (
       tx.version == config::lol::tx_version
       , {}
       , "Invalid coinbase transaction version: "
       + std::to_string(tx.version)
       + ", expected: "
       + std::to_string(config::lol::tx_version)
       );

    ASSERT_OR_LOG_TRACE_AND_RETURN
      (
       consensus::
       rule_29_ringct_should_have_rct_type_clsag_in_rct_data
       (tx.ringct.rct_type)
       , {}
       , "Invalid ringct type for coinbase"
       );

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       consensus::
       rule_25_ringct_should_have_at_least_one_input(tx.vin)
       , {}
       , "tx with empty inputs"
       );

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       consensus::
       rule_30_ringct_should_have_0_amount_outputs(tx.vout)
       , {}
       , "invalid output amount"
       );

    const auto maybe_outputs = are_tx_output_pks_valid(tx);
    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       maybe_outputs
       , {}
       , "invalid outputs"
       );

    const std::vector<crypto::public_key> output_public_keys =
      *maybe_outputs;

    const auto maybe_size_checked_rct_data =
      rct::preview_rctData(tx.ringct);

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       maybe_size_checked_rct_data
       , {}
       , "rctData_unsafe size check failed"
       );

    const auto maybe_inputs =
      consensus::are_ringct_input_types_valid(tx.vin);

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       maybe_inputs
       , {}
       , "invalid inputs"
       );

    const auto inputs = *maybe_inputs;

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       consensus::
       rule_31_ringct_should_have_0_amount_inputs(inputs)
       , {}
       , "invalid input amount in ringct"
       );
      

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       std::ranges::all_of
       (
        inputs
        , [](const auto& x) {
          return x.output_relative_offsets.size()
            == config::lol::ring_size;
        }
        )
       , {}
       , "invalid ring size"
       );

    const bool offsets_different =
      std::ranges::all_of
      (
       inputs
       , [](const auto& x) {
         return consensus::
           rule_7_ringct_input_decoys_offsets_should_not_be_zero_except_the_first_one
           (
            x.output_relative_offsets
            );
       }
       );

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       offsets_different
       , {}
       , "referenced output indices should be different"
       );

    const auto output_key_images =
      inputs
      | std::views::transform
      (
       [](const auto& x) {
         return x.output_key_image;
       }
       );

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       consensus::
       rule_8_input_key_images_should_be_unique
       (
        std::vector<crypto::key_image>
        {
          output_key_images.begin()
          , output_key_images.end()
        }
        )
       , {}
       , "output key images are not unique"
       );


    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       consensus::
       rule_18_ringct_input_key_images_should_be_sorted
       (
        std::vector<crypto::key_image>
        {
          output_key_images.begin()
          , output_key_images.end()
        }
        )
       , {}
       , "output key images aren't sorted"
       );

    const auto
      [
       output_commits
       , pseudo_input_commits
       , fee
       , ecdh_encrypted_amounts
       , signatures
       , bulletproof
       ]
      = *maybe_size_checked_rct_data;

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       signatures.size() == inputs.size()
       , {}
       , "Bad CLSAGs size"
       );

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       output_commits.size() == output_public_keys.size()
       , {}
       , "output commits and output public keys don't match"
       );

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       consensus::
       rule_14_tx_pseudo_input_commits_should_be_safe_points
       (
        std::vector<crypto::ec_point_unsafe>
        {
          pseudo_input_commits.begin()
          , pseudo_input_commits.end()
        }
        ).has_value()
       , {}
       , "invalid pseudo input commits"
       );

    output_public_dataV outputs;

    std::transform
      (
       output_public_keys.begin()
       , output_public_keys.end()
       , output_commits.begin()
       , std::back_inserter(outputs)
       , [](const auto& x, const auto& y) -> output_public_data
       { return {x, y}; }
       );

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       consensus::
       rule_21_ringct_should_have_at_least_1_outputs(outputs.size())
       , {}
       , std::string()
       + "Tx "
       + " has fewer than two outputs"
       );


    if (global_setting_verify_ringct) {
      ASSERT_OR_LOG_ERROR_AND_RETURN
        (
         verify_range_proof
         (output_commits, bulletproof)
         , {}
         , "Transaction range proof verification failed: "
         + get_transaction_hash(tx).to_str()
         );
    } else {
      // LOG_ERROR("###### Skipping ringct");
    }

    const auto ringct_inputs_1 =
      inputs
      | std::views::transform
      (
       [](const auto& x) {
         return
           ringct_input {
           x.output_key_image
           , x.output_relative_offsets
         };
       }
       );

    std::list<ringct_input> ringct_inputs_2;

    std::transform
      (
       ringct_inputs_1.begin()
       , ringct_inputs_1.end()
       , pseudo_input_commits.begin()
       , std::back_inserter(ringct_inputs_2)
       , [](const auto& x, const auto& y) {
         return ringct_input {
           x.key_image
           , x.output_relative_offsets
           , y
         };
       }
       );


    auto signatures_mut = signatures;

    for (size_t i = 0; i < output_key_images.size(); i++)
      {
        signatures_mut[i].signer_key_image = output_key_images[i];
      }

    std::list<ringct_input> ringct_inputs;

    std::transform
      (
       ringct_inputs_2.begin()
       , ringct_inputs_2.end()
       , signatures_mut.begin()
       , std::back_inserter(ringct_inputs)
       , [](const auto& x, const auto& y) {
         return ringct_input {
           x.key_image
           , x.output_relative_offsets
           , x.pseudo_input_commit
           , y
         };
       }
       );


    const tx_common common = {
      tx.unlock_block_index
      , { tx.extra }
    };

    const auto maybe_ecdh_output_public_keys = 
      get_all_output_ecdh_public_keys_from_extra
      (tx.extra, outputs.size());

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       maybe_ecdh_output_public_keys
       , {}
       , "invalid output ecdh public keys in tx_extra"
       );

    const auto ecdh_output_public_keys =
      *maybe_ecdh_output_public_keys;

    std::vector<std::pair<uint64_t, crypto::ec_point>> ecdh_data;
    std::transform
      (
       ecdh_encrypted_amounts.begin()
       , ecdh_encrypted_amounts.end()
       , ecdh_output_public_keys.begin()
       , std::back_inserter(ecdh_data)
       , [](const auto& x, const auto& y)
       -> std::pair<uint64_t, crypto::ec_point> {
         return {x, y};
       }
       );

    std::vector<ringct_output> ringct_outputs;
    std::transform
      (
       outputs.begin()
       , outputs.end()
       , ecdh_data.begin()
       , std::back_inserter(ringct_outputs)
       , [](const auto& x, const auto& y) {
         return ringct_output {x, y.first, y.second, 0};
       }
       );

    const ringct x = {
      { common }
      , std::vector<ringct_input>
      {
        ringct_inputs.begin()
        , ringct_inputs.end()
      }
      , ringct_outputs
      , fee
      , bulletproof
    };

    return x;
  }

  transaction review_ringct(const ringct& tx) {

    const auto rctData_unsafe = review_rctData
      (
       tx.get_output_commits()
       , tx.get_pseudo_input_commits()
       , tx.fee
       , tx.get_ecdh_encrypted_amounts()
       , tx.get_signatures()
       , tx.bulletproof
       );

    const auto inputs =
      tx.inputs
      | std::views::transform
      (
       [](const auto& x) -> txin_v {
         const txin_v in =
           txin_from_key
           {
             0
             , x.output_relative_offsets
             , x.key_image
           };

         return in;
       }
       )
      ;

    const auto outputs =
      tx.get_output_pks()
      | std::views::transform
      (
       [](const auto& x) -> tx_out {
         const txout_target_v target =
           txout_to_key { x };

         return
           {
             0
             , target
           };
       }
       )
      ;

    const transaction_prefix tx_prefix =
      {
        config::lol::tx_version
        , tx.unlock_block_index
        , { inputs.begin(), inputs.end() }
        , { outputs.begin(), outputs.end() }
        , tx.extra.data
      };

    const transaction x = {
      { tx_prefix }
      , rctData_unsafe
    };

    return x;
  }

  std::optional<ringct>
  preview_ringct_from_blob
  (const string_blob_view tx_blob)
  {
    const auto maybeTx =
      preview_object_from_blob<transaction>(tx_blob);

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       maybeTx
       , {}
       , "Failed to parse transaction from blob"
       );

    const auto tx = *maybeTx;

    if (is_coinbase(tx)) {
      return {};
    }

    const auto maybe_ringct = preview_ringct(tx);

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       maybe_ringct
       , {}
       , "Failed to expand transaction data"
       );

    return {*maybe_ringct};
  }

  string_blob review_ringct_to_blob(const ringct& x) {
    return t_serializable_object_to_blob(review_ringct(x));
  }


}
