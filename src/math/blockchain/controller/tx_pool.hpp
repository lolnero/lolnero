/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE


Parts of this file are originally 
copyright (c) 2012-2013 The Cryptonote developers

*/


#pragma once

#include "cryptonote/basic/type/string_blob_type.hpp"

#include "cryptonote/basic/type/verification_context.hpp"
#include "math/blockchain/functional/ringct.hpp"

namespace cryptonote
{
  struct pool_tx_t
  {
    ringct tx;
    size_t size;
    time_t receive_time;
  };

  class tx_memory_pool
  {
  private:
    std::map<crypto::hash, pool_tx_t> m_txs;

  public:

    // mutable
    tx_verification_context add_tx
    (
     const ringct ringct
     );

    std::optional<pool_tx_t> take_tx(const crypto::hash id);

    // const

    std::optional<pool_tx_t> get_tx(const crypto::hash id) const;

    std::list<std::optional<pool_tx_t>> get_txs
    (const std::span<const crypto::hash> hashes) const;

    bool contains_tx
    (
     const crypto::hash id
     ) const;

    std::tuple
    <
      std::list<crypto::hash>
      , size_t
      , uint64_t
      >
    pick_txs_for_block_template
    (
     const uint64_t block_index 
     ) const;

    std::map<crypto::hash, pool_tx_t> get_all_txs() const;

    std::list<pool_tx_t>
    get_complement
    (
     const std::span<const crypto::hash> hashes
     ) const;


    std::set<crypto::key_image> get_all_key_images() const;

    bool tx_key_images_seen(const ringct tx) const;

    std::set<crypto::public_key> get_all_outputs() const;

    bool tx_outputs_seen(const ringct tx) const;

  };

}

