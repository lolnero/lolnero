/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#pragma once

#include "blockchain.hpp"

#include "math/blockchain/controller/tx_pool.hpp"

#include "tools/serialization/json.hpp"
#include "math/json/all.hpp"

#include <deque>

namespace cryptonote
{
  constexpr uint64_t blocktree_json_version = 13;

  struct blocktree_t
  {
    blockchain_t m_chain;
    blockchain_t m_alt_chain;

    tx_memory_pool m_pool;

    uint64_t version = blocktree_json_version;

    static std::optional<blocktree_t>
    load(const std::string file_path);


    // mutable

    void set_fixed_difficulty(const std::optional<diff_t> x);
    void set_ignore_checkpoints(const bool x);


    // pool
    bool pool_add_tx(const ringct& tx);
    std::optional<pool_tx_t> pool_take_tx(const crypto::hash id);

    // blockchain
    block_add_t push_block(const block_t& x);
    block_add_t push_blocks(const std::span<const block_t> x);
    std::optional<block_t> pop_block();


    // const

    bool store(const std::string file_path) const;


    // pool
    std::tuple
    <
      std::list<crypto::hash>
      , size_t
      , uint64_t
      >
    pick_txs_for_block_template
    (
     const uint64_t block_index
     ) const;

    std::optional<pool_tx_t> pool_get_tx(const crypto::hash id) const;

    std::list<std::optional<pool_tx_t>> pool_get_txs
    (const std::span<const crypto::hash> hashes) const;

    bool pool_contains_tx
    (
     const crypto::hash id
     ) const;

    std::map<crypto::hash, pool_tx_t> pool_get_all_txs() const;

    std::list<pool_tx_t>
    pool_get_complement
    (
     const std::span<const crypto::hash> hashes
     ) const;



    // blockchain
    block_t get_top_block() const;

    diff_t get_top_diff() const;
    diff_t get_top_cumulative_diff() const;

    std::optional<block_t>
    get_block_by_index(const size_t x) const;

    std::optional<block_t>
    get_block_by_hash(const crypto::hash x) const;

    size_t get_size() const;

    size_t get_p2p_index() const;

    size_t get_top_block_index() const;

    diff_t get_next_diff() const;

    std::list<crypto::hash> get_short_chain_history() const;

    std::vector<std::optional<block_t>>
    get_blocks(const std::span<const crypto::hash> xs) const;

    std::list<block_t>
    get_blocks_by_short_chain_history
    (const std::list<crypto::hash> xs) const;

    size_t get_txs_size() const;
    size_t get_outputs_size() const;

    std::optional<uint64_t>
    get_output_index_by_public_key(const crypto::public_key x) const;

    std::optional<ring::decoy>
    get_decoy_by_index(const size_t x) const;

    std::optional<coinbase_tx>
    get_coinbase_tx_by_hash(const crypto::hash x) const;

    std::optional<ringct>
    get_tx_by_hash(const crypto::hash x) const;

    bool check_block_timestamp(const uint64_t x) const;

    bool block_known(const crypto::hash x) const;

    std::pair<std::list<crypto::hash>,uint64_t>
    get_hashes_by_short_chain_history
    (
     const std::list<crypto::hash> xs
     , const size_t max_size = constant::BLOCKS_IDS_SYNCHRONIZING_DEFAULT_COUNT
     ) const;

    bool is_output_key_image_spent
    (const crypto::key_image& x) const;

    std::optional<diff_t>
    get_cumulative_diff_by_index (const size_t x) const;

    std::optional<diff_t>
    get_diff_by_index (const size_t x) const;


  private:
    bool pool_add_tx_no_lock(const ringct& tx);
    block_add_t push_block_no_lock(const block_t& x);
    std::optional<block_t> pop_block_no_lock();
  };

  JSON_LAYOUT
  (
   blocktree_t
   , version
   , m_chain
   );

}
