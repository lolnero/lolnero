/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/

#pragma once


#include "math/crypto/functional/key.hpp"
#include "math/crypto/controller/random.hpp"

#include <random>

namespace cryptonote
{
  namespace ring
  {
    struct decoy
    {
      crypto::public_key public_key;
      crypto::ec_point commit;
      uint64_t block_index;
      uint64_t output_index;

      bool operator<(const decoy &x) const noexcept {
        return public_key < x.public_key;
      }
    };
  }

  // gamma reach is from observation
  // reach needs to be conservatively small to make
  // picking possible without time out
  // https://homepage.divms.uiowa.edu/~mbognar/applets/gamma.html
  struct gamma_config
  {
    double shape;
    double scale;
    double clamp;
    double reach;
    bool reverse = false;
  };

  // bell
  constexpr gamma_config gamma_config_user =
    {
      2
      , 1
      , 0.5
      , 4.5
    };

  // exponential
  constexpr gamma_config gamma_config_exchange =
    {
      1
      , 1
      , 0
      , 4
    };

  // pseudo linear
  constexpr gamma_config gamma_config_zen =
    {
      1
      , 1
      , 1
      , 2
    };

  // reverse pseudo linear
  constexpr gamma_config gamma_config_hodler =
    {
      1
      , 1
      , 1
      , 2
      , true
    };
  
  std::optional<uint64_t> gamma_generate_distance
  (
   const gamma_config config
   , const uint64_t exclusive_bound
   , std::default_random_engine& gen
   );

  std::set<uint64_t> get_gamma_decoy_pool
  (
   const gamma_config config
   , const uint64_t num_outputs
   , const size_t ring_size
   );

  uint64_t get_sample_distance
  (
   const std::set<uint64_t> decoys
   , const uint64_t real_output
   );

  std::vector<uint64_t> forge_gamma_ring
  (
   const gamma_config config
   , const uint64_t num_outputs
   , const size_t ring_size
   , const uint64_t real_output
   , const uint64_t max_acceptable_distance
   );

} // cryptonote
