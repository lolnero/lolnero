/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/


#include "gamma_ring.hpp"

#include "tools/epee/include/logging.hpp"

#include "config/lol.hpp"

#include <random>

namespace cryptonote {

  std::optional<uint64_t> gamma_generate_distance
  (
   const gamma_config config
   , const uint64_t exclusive_bound
   , std::default_random_engine& gen
   ) {
    if (exclusive_bound == 0) return {};

    auto gamma = std::gamma_distribution<double>(config.shape, config.scale);

    const double x = gamma(gen);

    if (x <= config.clamp) return {};

    const double gamma_step = exclusive_bound
      / (config.reach - config.clamp);

    const uint64_t distance =
      (x - config.clamp) * gamma_step;

    LOG_TRACE
      (
       "gamma picked distance: "
       + std::to_string(distance)
       );

    if (distance >= exclusive_bound) return {};

    return distance;
  }

  std::set<uint64_t> get_gamma_decoy_pool
  (
   const gamma_config config
   , const uint64_t num_outputs
   , const size_t ring_size
   )
  {
    std::random_device rd;
    std::default_random_engine gen(rd());

    std::set<uint64_t> decoy_pool;
    while (decoy_pool.size() < ring_size)
      {
        const auto maybe_picked_output_distance =
          gamma_generate_distance
          (
           config
           , num_outputs
           , gen
           );

        if (!maybe_picked_output_distance) {
          LOG_DEBUG("Try gamma generate distance again");
          continue;
        }

        const auto picked_output_distance
          = *maybe_picked_output_distance;

        const auto picked_output_index =
          config.reverse
          ? picked_output_distance
          : num_outputs - picked_output_distance - 1
          ;

        decoy_pool.insert(picked_output_index);

        LOG_DEBUG
          (
           "picked index "
           + std::to_string(picked_output_index)
           + ", "
           + std::to_string(decoy_pool.size())
           + " outputs picked"
           );

      }

    return decoy_pool;
  }

  uint64_t get_sample_distance
  (
   const std::set<uint64_t> decoys
   , const uint64_t real_output
   ) {
    return std::accumulate
      (
       decoys.begin()
       , decoys.end()
       , std::numeric_limits<uint64_t>::max()
       , [real_output](const auto& x, const auto& y) {
         const uint64_t distance =
           std::abs
           (
            static_cast<int64_t>(real_output)
            - static_cast<int64_t>(y)
            );

         return std::min(x, distance);
       }
       );
  }

  std::vector<uint64_t> forge_gamma_ring
  (
   const gamma_config config
   , const uint64_t num_outputs
   , const size_t ring_size
   , const uint64_t real_output
   , const uint64_t max_acceptable_distance
   )
  {
    size_t iteration = 0;
    // const size_t max_iteration = 1e6;
    while (true) {
      const std::set<uint64_t> decoy_pool
        = get_gamma_decoy_pool
        (
         config
         , num_outputs
         , ring_size
         );

      // really should re run the fake sample here
      const uint64_t sample_distance =
        get_sample_distance(decoy_pool, real_output);

      if(sample_distance > max_acceptable_distance) {
        LOG_DEBUG
          (
           "Skipping decoy pool: sample distance: "
           + std::to_string(sample_distance)
           + ", max acceptable distance: "
           + std::to_string(max_acceptable_distance)
           );


        iteration++;
        continue;
      }

      LOG_DEBUG("Found decoy pool.");


      std::vector<uint64_t> ring
        = {decoy_pool.begin(), decoy_pool.end()};

      std::sort
        (
         ring.begin()
         , ring.end()
         );

      const auto lower_bound =
        std::lower_bound
        (
         ring.begin()
         , ring.end()
         , real_output
         , [](const auto& x, const auto& y) {
           return x < y;
         }
         );

      auto selected_decoy = lower_bound;

      if (lower_bound == ring.end()) {
        LOG_DEBUG("lower_bound at the end");
        selected_decoy = std::prev(ring.end());
      }
      else if (lower_bound == ring.begin()) {
        LOG_DEBUG("lower_bound at the begining");
      }
      else {
        LOG_DEBUG
          (
           "lower_bound at: "
           + std::to_string(std::distance(ring.begin(), lower_bound))
           );

        const uint64_t this_distance =
          *lower_bound - real_output;

        LOG_DEBUG
          (
           "this distance: " + std::to_string(this_distance)
           );

        const uint64_t previous_distance =
          real_output
          - *std::prev(lower_bound);


        LOG_DEBUG
          (
           "prev distance: " + std::to_string(previous_distance)
           );

        if (previous_distance < this_distance) {
          selected_decoy = std::prev(lower_bound);
        }
      }

      LOG_INFO
        (
         "Decoy "
         + std::to_string(*selected_decoy)
         + " -> "
         + std::to_string(real_output)
         + ", loop: "
         + std::to_string(iteration)
         + ", gamma("
         + std::to_string(config.shape)
         + ", "
         + std::to_string(config.scale)
         + "), "
         + "clamp: "
         + std::to_string(config.clamp)
         + ", reach: "
         + std::to_string(config.reach)
         + ", output count: "
         + std::to_string(num_outputs)
         );


      *selected_decoy = real_output;

      return ring;
    }
  }

} // cryptonote
