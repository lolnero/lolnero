/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include "blockchain.hpp"

#include "cryptonote/basic/functional/format_utils.hpp"
#include "math/blockchain/functional/tx_check.hpp"
#include "math/consensus/consensus.hpp"

#include "math/ringct/functional/ringCT.hpp"
#include "math/ringct/functional/rctOps.hpp"
#include "math/ringct/functional/ringCT.hpp"

#include "math/blockchain/functional/ringct.hpp"
#include "math/blockchain/functional/short_chain_history.hpp"

#include "tools/epee/functional/algorithm.hpp"

#include <ranges>
#include <execution>

#include "config/lol.hpp"

namespace cryptonote {

  blockchain_t::blockchain_t() {
    push_block(lol_genesis_block());
  }

  // mutable

  bool blockchain_t::push_block(const block_t& x)
  {
    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       consensus::
       is_block_valid_against_checkpoints(x.get_index(), x.hash())
       , false
       , "failed to check with internal checkpoints"
       );

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       check_block_timestamp(x.timestamp)
       , false
       , "invalid solve time"
       );

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       !m_block_index.contains(x.hash())
       , false
       , "adding duplicated block"
       );

    if (!m_blocks.empty()) {
      ASSERT_OR_LOG_ERROR_AND_RETURN
        (
         get_top_block_index() + 1 == x.get_index()
         , false
         , "invalid block height"
         );

      ASSERT_OR_LOG_ERROR_AND_RETURN
        (
         m_blocks.back().hash() == x.prev_hash
         , false
         , "prev hash doesn't match tail"
         );
    }

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       !m_coinbase_index.contains(x.miner_tx.hash())
       , false
       , "adding duplicated coinbase"
       );


    // poor miner
    // duplicated miner outputs at block index 28648 and 28649
    if (x.get_index() > 30000) {
      ASSERT_OR_LOG_ERROR_AND_RETURN
        (
         !std::ranges::any_of
         (
          x.get_outputs()
          , [&](const auto& output) {
            return m_output_index.contains(output);
          }
          )
         , false
         , "double sending at block " + x.hash().to_str()
         );
    }

    const bool txs_valid =
      std::transform_reduce
      (
       std::execution::par_unseq
       , x.txs.begin()
       , x.txs.end()
       , true
       , std::logical_and()
       , std::bind_front(&blockchain_t::verify_ringct, this)
       );

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       txs_valid
       , false
       , "failed to verify ringct"
       );

    const auto block_diff = get_next_diff();
    const auto check_diff = m_fixed_difficulty.value_or(block_diff);

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       check_hash(get_mining_hash(review_block(x)), check_diff)
       , false
       , "not enough pow"
       );


    m_cumulative_diffs.push_back
      (get_top_cumulative_diff() + block_diff);

    if (m_blocks.size() == 0) {
      m_cumulative_output_start_indices.push_back(0);
    } else {
      const auto output_index_start =
        m_cumulative_output_start_indices.back()
        + m_blocks.back().get_outputs().size();

      m_cumulative_output_start_indices.push_back
        (output_index_start);
    }

    m_blocks.push_back(x);

    const size_t block_index = get_top_block_index();


    m_block_index[x.hash()] = block_index;
    m_coinbase_index[x.miner_tx.hash()] = block_index;

    std::ranges::for_each
      (
       std::views::iota(0)
       | std::views::take(x.miner_tx.outputs.size())
       , [&, x, block_index](const size_t i) {
         const auto coinbase_output = x.miner_tx.outputs[i];
         const auto output = coinbase_output.public_key;

         const output_index_t idx =
           {true, block_index, 0, i};

         LOG_TRACE("adding coinbase output");

         m_output_index[output] = idx;
         m_outputs.push_back(idx);
       }
       );

    std::ranges::for_each
      (
       std::views::iota(0)
       | std::views::take(x.txs.size())
       , [&, x, block_index](const size_t i) {
         const auto& tx = x.txs[i];

         m_tx_index[tx.hash()] = {block_index, i};

         size_t output_idx = 0;
         for (const auto& output: tx.get_output_pks()) {
           const output_index_t idx =
             {false, block_index, i, output_idx};


           LOG_TRACE("adding ringct output");

           m_output_index[output] = idx;
           m_outputs.push_back(idx);

           output_idx++;
         }

         for (const auto& x: tx.inputs) {
           m_key_images.insert(x.key_image);
         }
       }
       );

    return true;
  }

  std::optional<block_t> blockchain_t::pop_block() {
    if (m_blocks.empty()) {
      LOG_ERROR("Poping empty chain.");

      return {};
    }
    if (m_blocks.size() == 1) {
      LOG_ERROR("Can't pop genesis block");
      return {};
    }

    const block_t b = m_blocks.back();

    LOG_VERBOSE("Popping: " + b.hash().to_str());

    m_blocks.pop_back();
    m_cumulative_diffs.pop_back();
    m_cumulative_output_start_indices.pop_back();
    m_block_index.erase(b.hash());

    m_coinbase_index.erase(b.miner_tx.hash());

    for (const auto& output: b.miner_tx.outputs) {
        m_output_index.erase(output.public_key);
        m_outputs.pop_back();
    }

    for(const auto& tx: b.txs) {
      m_tx_index.erase(tx.hash());

      for(const auto& x: tx.outputs) {
        m_output_index.erase(x.output.public_key);
        m_outputs.pop_back();
      }

      for(const auto& x: tx.inputs) {
        m_key_images.erase(x.key_image);
      }
    }


    return b;
  }


  // const

  size_t blockchain_t::get_size() const {
    return m_blocks.size();
  }

  size_t blockchain_t::get_outputs_size() const {
    return m_outputs.size();
  }

  size_t blockchain_t::get_txs_size() const {
    return m_tx_index.size();
  }


  size_t blockchain_t::get_top_block_index() const {
    return m_blocks.size() - 1;
  }

  std::optional<rct::output_public_data>
  blockchain_t::get_output_by_index(const size_t x) const {
    if (m_outputs.empty()) return {};
    if (!(m_outputs.size() > x)) return {};

    const auto idx = m_outputs.at(x);

    if (idx.coinbase) {
      const auto coinbase = m_blocks.at(idx.block_idx).miner_tx;

      const auto output = coinbase.outputs[idx.output_idx];

      return output_public_data {
        output.public_key
        , dummyCommit(output.amount)
      };

    }

    const ringct& output_tx =
      m_blocks.at(idx.block_idx)
      .txs.at(idx.tx_idx);

    return output_tx.outputs.at(idx.output_idx).output;
  }

  uint64_t blockchain_t::output_idx_to_global_index
  (const output_index_t x) const
  {
    // json j = x;
    // LOG_GLOBAL("output index: " + j.dump(4));

    const auto block_index = x.block_idx;

    const auto block_start_output_index =
      m_cumulative_output_start_indices.at(block_index);

    LOG_TRACE
      ("block start output index: "
       + std::to_string(block_start_output_index)
       );

    if (x.coinbase) {
      return block_start_output_index + x.output_idx;
    }

    const auto block = m_blocks.at(x.block_idx);

    const size_t early_output_count_init =
      block_start_output_index + block.miner_tx.outputs.size();

    const size_t early_output_count =
      std::transform_reduce
      (
       block.txs.begin()
       , std::next(block.txs.begin(), x.tx_idx)
       , early_output_count_init
       , std::plus()
       , [](const auto& x) { return x.outputs.size(); }
       );

    const auto output_index = early_output_count + x.output_idx;

    LOG_TRACE
      (
       "output global index: "
       + std::to_string(output_index)
       );

    return output_index;
  }

  std::optional<uint64_t>
  blockchain_t::get_output_index_by_public_key
  (const crypto::public_key x) const {
    if (!m_output_index.contains(x)) {
      return {};
    }

    const auto idx = m_output_index.at(x);

    return output_idx_to_global_index(idx);
  }


  std::optional<ring::decoy>
  blockchain_t::get_decoy_by_index(const size_t x) const {
    LOG_TRACE("getting decoy for: " + std::to_string(x));

    const auto maybe_output = get_output_by_index(x);

    if (!maybe_output) {
      LOG_DEBUG
        (
         "decoy not found for index: "
         + std::to_string(x)
         );

      std::terminate();
      return {};
    }

    const auto output = *maybe_output;

    if (!m_output_index.contains(output.public_key)) {
      LOG_FATAL
        (
         "Bad data integrity: m_output_index does not contain"
         + output.public_key.to_str()
         );

      return {};
    }

    const auto output_idx = m_output_index.at(output.public_key);

    // const json output_idx_json = output_idx;
    // LOG_DEBUG
    //   (
    //    "found output at idx: " + output_idx_json.dump(4)
    //    );

    return
      {
        {
          output.public_key
          , output.commit
          , output_idx.block_idx
          , output_idx_to_global_index(output_idx)
        }
      };

  }

  std::optional<block_t>
  blockchain_t::get_block_by_index(const size_t x) const {
    if (m_blocks.empty()) return {};

    if (x >= m_blocks.size()) return {};

    return m_blocks.at(x);
  }

  std::optional<diff_t>
  blockchain_t::get_cumulative_diff_by_index(const size_t x) const {
    if (m_cumulative_diffs.empty()) return {};

    if (x >= m_cumulative_diffs.size()) return {};

    return m_cumulative_diffs.at(x);
  }

  std::optional<diff_t>
  blockchain_t::get_diff_by_index(const size_t x) const {
    if (x == 0) return get_cumulative_diff_by_index(x);

    const auto maybe_current_diff = get_cumulative_diff_by_index(x);
    const auto maybe_prev_diff = get_cumulative_diff_by_index(x - 1);

    return maybe_current_diff.and_then
      (
       [maybe_prev_diff](const auto& current_diff)
       -> std::optional<diff_t> {
         return maybe_prev_diff.transform
           (
            [current_diff](const auto& prev_diff) {
              return current_diff - prev_diff;
            }
            );
       }
       );
  }

  std::optional<coinbase_tx>
  blockchain_t::get_coinbase_tx_by_hash(const crypto::hash x) const {
    if (!m_coinbase_index.contains(x)) return {};

    const auto idx = m_coinbase_index.at(x);

    return m_blocks.at(idx).miner_tx;
  }

  std::optional<ringct>
  blockchain_t::get_ringct_by_hash(const crypto::hash x) const {
    if (!m_tx_index.contains(x)) return {};

    const auto idx = m_tx_index.at(x);

    return m_blocks.at(idx.first).txs.at(idx.second);
  }

  std::optional<block_t>
  blockchain_t::get_block_by_hash(const crypto::hash x) const
  {
    if (!m_block_index.contains(x)) return {};

    return m_blocks.at(m_block_index.at(x));
  }

  std::vector<std::optional<block_t>>
  blockchain_t::get_blocks
  (const std::span<const crypto::hash> xs) const
  {
    LOG_TRACE("get blocks: " + std::to_string(xs.size()));

    const auto blocks_maybe =
      xs
      | std::views::transform
      (std::bind_front
       (&blockchain_t::get_block_by_hash, this))
      ;

    return { blocks_maybe.begin(), blocks_maybe.end() };
  }


  block_t blockchain_t::get_top_block() const {
    return m_blocks.back();
  }

  bool blockchain_t::verify_ringct(const ringct& tx) const {

    if (!global_setting_verify_ringct) {
      LOG_TRACE
        (
         "skipping ringct verification"
         );

      return true;
    }

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       !m_tx_index.contains(tx.hash())
       , false
       , "adding duplicated ringct"
       );

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       !std::ranges::any_of
       (
        tx.inputs
        , [&](const auto& input) {
          return m_key_images.contains(input.key_image);
        }
        )
       , false
       , "double spending"
       );

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       std::ranges::all_of
       (
        tx.inputs
        , [&](const auto& x) {
          const auto absolute_offsets =
            relative_output_offsets_to_absolute
            (x.output_relative_offsets);

          const uint64_t output_size =
            static_cast<uint64_t>(m_outputs.size());

          return std::ranges::all_of
            (
             absolute_offsets
             , std::bind_front(std::greater<uint64_t>(), output_size)
             );
        }
        )
       , false
       , "invalid ouptut indices"
       );

    const auto decoysV_view =
      tx.inputs
      | std::views::transform
      (
       [&](const auto& input) -> output_public_dataV {
         const auto absolute_offsets =
           relative_output_offsets_to_absolute
           (input.output_relative_offsets);

         const auto decoys =
           absolute_offsets
           | std::views::transform
           ( std::bind_front
             (&blockchain_t::get_output_by_index, this) )
           | std::views::transform
           ( [](const auto& x) -> rct::output_public_data
           { return *x; } )
           ;

         return { decoys.begin(), decoys.end() };
       }
       )
      ;


    // return true if we don't want to validate clsag
    if (m_nettype == MAINNET) {
      const bool before_latest_checkpoint =
        get_top_block_index() <
        consensus::latest_check_point_index();

      if (before_latest_checkpoint) {

        // ignoring checkpoints means validate everything
        // not ignoring means do not validate, i.e. skip
        if (!m_ignore_checkpoints) {
          LOG_DEBUG
            (
             "Skipping clsag on block index: "
             + std::to_string(get_top_block_index())
             + " for "
             + tx.hash().to_str()
             );

          return true;
        }
      }
    }

    const std::vector<output_public_dataV> decoysV =
      { decoysV_view.begin(), decoysV_view.end() };

          
    const auto clsag_message =
      rct::get_ringct_basic_and_bp_data_hash_for_clsag
      (
       tx.get_output_commits()
       , decoysV
       , tx.get_pseudo_input_commits()
       , tx.fee
       , tx.get_ecdh_encrypted_amounts()
       , tx.get_tx_prefix_hash()
       , tx.get_signatures()
       , tx.bulletproof
       );


    if (!rct::verify_clsag_signatures
        (
         tx.get_pseudo_input_commits()
         , decoysV
         , clsag_message
         , tx.get_signatures()
         ))
      {
        LOG_ERROR("clsag verification failed");
        return false;
      }

    return true;
  }

  diff_t blockchain_t::get_next_diff() const {
    if (m_blocks.empty()) {
      return initial_diff();
    }

    const auto timestamps =
      m_blocks
      | std::views::reverse
      | std::views::take(constant::DIFFICULTY_BLOCKS_COUNT)
      | std::views::reverse
      | std::views::transform
      (
       [](const auto& x) {
         return x.timestamp;
       }
       );


    const auto diffs =
      m_cumulative_diffs
      | std::views::reverse
      | std::views::take(constant::DIFFICULTY_BLOCKS_COUNT)
      | std::views::reverse
      ;


    return consensus::
      rule_16_next_difficult_target_over_average_difficulty_should_be_the_inverse_of_the_lwma_of_block_time_over_target_time
      (
       { timestamps.begin(), timestamps.end() }
       , { diffs.begin(), diffs.end() }
       , get_top_block_index() + 1
       );

  }

  diff_t blockchain_t::get_top_cumulative_diff() const {
    if (m_cumulative_diffs.empty()) return 0;
    return m_cumulative_diffs.back();
  }

  diff_t blockchain_t::get_top_diff() const {
    if (m_cumulative_diffs.empty()) return 0;
    if (m_cumulative_diffs.size() == 1) {
      return get_top_cumulative_diff();
    }

    return m_cumulative_diffs.back() -
      *std::prev(m_cumulative_diffs.end(), 2);
  }


  bool blockchain_t::is_output_key_image_spent
  (const crypto::key_image& x) const
  {
    return m_key_images.contains(x);
  }

  std::optional<ringct>
  blockchain_t::get_tx_by_hash(const crypto::hash x) const
  {
    if (m_tx_index.contains(x)) {
      const auto tx_idx = m_tx_index.at(x);
      return m_blocks.at(tx_idx.first).txs.at(tx_idx.second);
    }
    else {
      return {};
    }
  }

  bool blockchain_t::check_block_timestamp
  (
   const uint64_t x
   ) const
  {
    if (m_blocks.size() < constant::DIFFICULTY_BLOCKS_COUNT) {
      return true;
    }

    const size_t blockchain_timestamp_check_window =
      constant::BLOCKCHAIN_TIMESTAMP_CHECK_WINDOW_V2;

    auto timestamps_view =
      m_blocks
      | std::views::reverse
      | std::views::take(blockchain_timestamp_check_window)
      | std::views::reverse
      | std::views::transform
      ([](const auto& x) { return x.timestamp; })
      ;

    const std::vector<uint64_t> timestamps =
      { timestamps_view.begin(), timestamps_view.end() };

    const auto maybe_median_ts =
      epee::algorithm::median(timestamps);

    if (!maybe_median_ts) {
      LOG_ERROR("Empty timestamps to check");
      return false;
    }

    const auto median_ts = *maybe_median_ts;

    if(x < median_ts)
      {
        LOG_DEBUG
          (
           "Timestamp "
           + std::to_string(x)
           +  " less than median of last "
           + std::to_string(blockchain_timestamp_check_window)
           + " blocks: "
           + std::to_string(median_ts)
           );

        return false;
      }

    return true;
  }

  std::pair<std::list<crypto::hash>, uint64_t>
  blockchain_t::get_hashes_by_short_chain_history
  (
   const std::list<crypto::hash> xs
   , const size_t max_size
   ) const
  {
    const auto common_hash =
      std::ranges::find_if
      (
       xs
       , [&](const auto& x) {
         return m_block_index.contains(x);
       }
       )
      ;

    if (common_hash == xs.end()) {
      return {};
    }

    const auto common_index = m_block_index.at(*common_hash);

    const auto block_hashes_view =
      m_blocks
      | std::views::drop(common_index)
      | std::views::take(max_size)
      | std::views::transform
      (
       [](const auto& x) { return x.hash(); }
       )
      ;

    return
      {
        { block_hashes_view.begin(), block_hashes_view.end() }
        , common_index
      };
  }


  std::list<block_t>
  blockchain_t::get_blocks_by_short_chain_history
  (const std::list<crypto::hash> xs) const
  {
    const auto common_hash =
      std::ranges::find_if
      (
       xs
       , [&](const auto& x) {
         return m_block_index.contains(x);
       }
       )
      ;

    if (common_hash == xs.end()) {
      return {};
    }

    const auto common_index = m_block_index.at(*common_hash);

    const auto blocks_view =
      m_blocks
      | std::views::drop(common_index)
      | std::views::take
      (constant::BLOCKS_IDS_SYNCHRONIZING_DEFAULT_COUNT)
      ;

    return { blocks_view.begin(), blocks_view.end() };
  }

  bool blockchain_t::in_range
  (const std::span<const block_t> xs) const {
    return std::ranges::all_of
      (
       xs
       | std::views::transform
       ([](const auto& x) { return x.hash(); })
       | std::views::transform
       (std::bind_front
        (&blockchain_t::get_block_by_hash, this))
       , [](const auto& x) { return x.has_value(); }
       );
  }

  std::list<crypto::hash>
  blockchain_t::get_short_chain_history() const
  {
    LOG_TRACE
      (
       "GET short chain: "
       + std::to_string(m_blocks.size())
       );

    std::list<crypto::hash> ids;

    const auto xs = build_short_chain_history(m_blocks.size());

    std::transform
      (
       xs.begin()
       , xs.end()
       , std::back_inserter(ids)
       , [&](const auto& x) {
         return m_blocks.at(x).hash();
       }
       );

    return ids;
  }


}
