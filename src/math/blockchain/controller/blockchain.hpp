/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#pragma once

#include "math/blockchain/functional/block.hpp"
#include "math/blockchain/functional/difficulty.hpp"

#include "math/blockchain/controller/gamma_ring.hpp"

#include "tools/serialization/json.hpp"
#include "math/json/all.hpp"

#include <deque>

namespace cryptonote
{
  struct output_index_t
  {
    bool coinbase = true;
    size_t block_idx;
    size_t tx_idx;
    size_t output_idx;
  };

  enum class block_add_t
    {
      added
      , saved
      , rejected
      , ignored
    };

  struct blockchain_t
  {
    blockchain_t();

    using tx_index_t = std::pair<size_t, size_t>;

    // 1. data
    std::deque<block_t> m_blocks;

    // 2. index
    std::map<crypto::hash, size_t> m_block_index;
    std::map<crypto::hash, tx_index_t> m_tx_index;
    std::map<crypto::hash, size_t> m_coinbase_index;
    std::map<crypto::public_key, output_index_t> m_output_index;

    // 3. meta
    std::deque<diff_t> m_cumulative_diffs;
    std::deque<uint64_t> m_cumulative_output_start_indices;
    network_type m_nettype = MAINNET;
    bool m_ignore_checkpoints = true;

    // 4. consensus
    std::set<crypto::key_image> m_key_images;
    std::deque<output_index_t> m_outputs;

    // 5. testing
    std::optional<diff_t> m_fixed_difficulty;


    // mutable

    bool push_block(const block_t& x);
    std::optional<block_t> pop_block();


    // const

    std::optional<block_t>
    get_block_by_index(const size_t x) const;

    std::optional<diff_t>
    get_cumulative_diff_by_index (const size_t x) const;

    std::optional<diff_t>
    get_diff_by_index (const size_t x) const;

    std::optional<coinbase_tx>
    get_coinbase_tx_by_hash(const crypto::hash x) const;

    std::optional<ringct>
    get_ringct_by_hash(const crypto::hash x) const;

    std::optional<block_t>
    get_block_by_hash(const crypto::hash x) const;

    std::optional<ringct>
    get_tx_by_hash(const crypto::hash x) const;

    std::vector<std::optional<block_t>>
    get_blocks(const std::span<const crypto::hash> xs) const;

    block_t get_top_block() const;
    diff_t get_top_diff() const;
    diff_t get_top_cumulative_diff() const;

    size_t get_size() const;
    size_t get_top_block_index() const;

    size_t get_outputs_size() const;
    size_t get_txs_size() const;

    std::optional<rct::output_public_data>
    get_output_by_index(const size_t x) const;

    std::optional<uint64_t>
    get_output_index_by_public_key(const crypto::public_key x) const;

    std::optional<ring::decoy>
    get_decoy_by_index(const size_t x) const;

    diff_t get_next_diff() const;

    bool is_output_key_image_spent
    (const crypto::key_image& x) const;

    std::pair<std::list<crypto::hash>,uint64_t>
    get_hashes_by_short_chain_history
    (
     const std::list<crypto::hash> xs
     , const size_t max_size = constant::BLOCKS_IDS_SYNCHRONIZING_DEFAULT_COUNT
     ) const;

    std::list<block_t>
    get_blocks_by_short_chain_history
    (const std::list<crypto::hash> xs) const;

    std::list<crypto::hash> get_short_chain_history() const;

    bool in_range(const std::span<const block_t>) const;

    bool check_block_timestamp(const uint64_t x) const;

    bool verify_ringct
    (
     const ringct& tx
     ) const;


  private:

    uint64_t output_idx_to_global_index
    (const output_index_t output_idx) const;

  };

  JSON_LAYOUT
  (
   output_index_t
   , coinbase
   , block_idx
   , tx_idx
   , output_idx
   );

  JSON_LAYOUT
  (
   blockchain_t
   , m_blocks
   , m_block_index
   , m_tx_index
   , m_coinbase_index
   , m_key_images
   , m_output_index
   , m_outputs
   , m_cumulative_diffs
   , m_cumulative_output_start_indices
   );

}
