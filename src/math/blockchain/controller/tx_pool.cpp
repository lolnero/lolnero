/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE


Parts of this file are originally 
copyright (c) 2012-2013 The Cryptonote developers

*/

#include "tx_pool.hpp"

#include "cryptonote/basic/functional/format_utils.hpp"
#include "math/blockchain/functional/tx_check.hpp"

#include "math/consensus/consensus.hpp"
#include "math/blockchain/functional/ringct.hpp"

#include <ranges>


namespace cryptonote
{
  // mutable
  
  tx_verification_context tx_memory_pool::add_tx
  (
   const ringct ringct
   )
  {
    const auto unsafe_tx = review_ringct(ringct);

    const crypto::hash tx_hash = get_transaction_hash(unsafe_tx);
    tx_verification_context tvc{};

    if (contains_tx(tx_hash)) {
      LOG_WARNING("tx already in pool");
      return tvc;
    }

    tvc.m_verification_failed = true;

    const cryptonote::string_blob tx_blob =
      t_serializable_object_to_blob(unsafe_tx);

    const size_t tx_weight = tx_blob.size();

    if (tx_weight > get_max_tx_size())
    {
      LOG_WARNING
        (
         "transaction is too heavy: "
         + std::to_string(tx_weight)
         + " bytes, maximum weight: "
         + std::to_string(get_max_tx_size())
         );
      tvc.m_too_big = true;
      return tvc;
    }

    const uint64_t fee = ringct.fee;

    if (!check_fee(tx_weight, fee))
      {
        LOG_WARNING("Not enough fee");
        tvc.m_fee_too_low = true;
        return tvc;
      }

    if
      (
       !consensus::rule_8_input_key_images_should_be_unique
       (ringct.get_key_images())
       )
      {
        LOG_WARNING("duplicated key images in tx");
        tvc.m_double_spend = true;
        return tvc;
      }

    if
      (
       !consensus::rule_32_output_public_keys_should_be_unique
       (ringct.get_output_pks())
       )
      {
        LOG_WARNING("duplicated output public keys in block");
        tvc.m_double_send = true;
        return tvc;
      }
    

    if (tx_key_images_seen(ringct)) {
      LOG_WARNING("Double spend");
      tvc.m_double_spend = true;
      return tvc;
    }

    if (tx_outputs_seen(ringct)) {
      LOG_WARNING("Double send");
      tvc.m_double_send = true;
      return tvc;
    }

    tvc.m_added_to_pool = true;

    tvc.m_verification_failed = false;

    LOG_VERBOSE
      (
       "Transaction added to pool: txid "
       + tx_hash.to_str()
       + " weight: "
       + std::to_string(tx_weight)
       + " fee/byte: "
       + std::to_string(fee / (double)(tx_weight ? tx_weight : 1))
      );

    const auto current_time = time(nullptr);

    const auto pool_tx =
      pool_tx_t
      {
        ringct
        , tx_weight
        , current_time
      };

    m_txs.emplace(tx_hash, pool_tx);

    return tvc;
  }

  std::optional<pool_tx_t> tx_memory_pool::take_tx
  (
   const crypto::hash id
   )
  {
    if (m_txs.contains(id)) {
      const auto x = m_txs.at(id);
      m_txs.erase(id);
      return x;
    } else {
      return {};
    }
  }


  // const

  std::optional<pool_tx_t> tx_memory_pool::get_tx
  (
   const crypto::hash id
   ) const
  {
    if (m_txs.contains(id)) {
      const auto x = m_txs.at(id);
      return x;
    } else {
      return {};
    }
  }

  std::list<std::optional<pool_tx_t>> tx_memory_pool::get_txs
  (const std::span<const crypto::hash> hashes) const
  {
    const auto txs =
      hashes
      | std::views::transform
      (
       [&](const auto& id) -> std::optional<pool_tx_t> {
         if (m_txs.contains(id)) {
           const auto x = m_txs.at(id);
           return x;
         } else {
           return {};
         }
       }
       );

    return { txs.begin(), txs.end() };
  }

  std::list<pool_tx_t>
  tx_memory_pool::get_complement
  (
   const std::span<const crypto::hash> hashes
   ) const
  {
    const std::set<crypto::hash> inputs(hashes.begin(), hashes.end());

    auto xs =
      m_txs
      | std::views::values
      | std::views::filter
      (
       [&inputs](const auto& x) { return !inputs.contains(x.tx.hash()); }
       )
      ;

    return { xs.begin(), xs.end() };
  }

  std::map<crypto::hash, pool_tx_t>
  tx_memory_pool::get_all_txs() const
  {
    return m_txs;
  }

  bool tx_memory_pool::contains_tx(const crypto::hash id) const
  {
    return m_txs.contains(id);
  }

  std::tuple
  <
    std::list<crypto::hash>
    , size_t
    , uint64_t
    >
  tx_memory_pool::pick_txs_for_block_template
  (
   const uint64_t block_index
   ) const
  {
    const uint64_t max_total_weight =
      consensus::get_block_size_bound(block_index)
      - constant::CRYPTONOTE_COINBASE_BLOB_RESERVED_SIZE;

    const auto& txs = m_txs;

    LOG_VERBOSE
      (
       "Filling block template, max weight "
       + std::to_string(max_total_weight)
       + ", "
       + std::to_string(txs.size())
       + " txes in the pool"
       );


    std::list<crypto::hash> picked_txs;
    size_t total_weight = 0;
    uint64_t total_fee = 0;

    for (const auto& [h, x]: txs)
      {
        LOG_VERBOSE
          (
           "Considering "
           + h.to_str()
           + ", size"
           + std::to_string(x.size)
           + ", current block weight "
           + std::to_string(total_weight)
           + "/"
           + std::to_string(max_total_weight)
           );

        if (total_weight + x.size > max_total_weight)
          {
            LOG_PRINT_L2
              (
               h.to_str()
               + "  would exceed maximum block weight"
               );

            continue;
          }

        if(!consensus::
           rule_6_block_size_should_be_bounded_by_block_index
           (block_index, total_weight + x.size))
          {
            LOG_PRINT_L2
              (
               h.to_str()
               + "  would exceed maximum block weight"
               );

            continue;
          }

        picked_txs.push_back(h);

        total_weight += x.size;
        total_fee += x.tx.fee;

        LOG_VERBOSE
          (
           h.to_str()
           + "  added, new block weight "
           + std::to_string(total_weight)
           + "/"
           + std::to_string(max_total_weight)
           + ", fee"
           + print_money(x.tx.fee)
           );
      }

    LOG_VERBOSE
      (
       "Block template filled with "
       + std::to_string(picked_txs.size())
       + " txes, weight "
       + std::to_string(total_weight)
       + "/"
       + std::to_string(max_total_weight)
       + ", fee "
       + print_money(total_fee)
       );


    return
      {
        picked_txs
        , total_weight
        , total_fee
      };
  }

  std::set<crypto::key_image>
  tx_memory_pool::get_all_key_images() const
  {
    std::set<crypto::key_image> xs;

    for (const auto& [h, x]: m_txs) {
      for (const auto& y: x.tx.inputs) {
        xs.insert(y.key_image);
      }
    }

    return xs;
  }

  bool tx_memory_pool::tx_key_images_seen
  (
   const ringct tx
   ) const
  {
    const auto ys = get_all_key_images();

    return std::ranges::any_of
      (
       tx.inputs
       , [&ys](const auto& x) {
         return ys.contains(x.key_image);
       }
       )
      ;
  }

  std::set<crypto::public_key>
  tx_memory_pool::get_all_outputs() const
  {
    std::set<crypto::public_key> xs;

    for (const auto& [h, x]: m_txs) {
      for (const auto& y: x.tx.get_output_pks()) {
        xs.insert(y);
      }
    }

    return xs;
  }

  bool tx_memory_pool::tx_outputs_seen
  (
   const ringct tx
   ) const
  {
    const auto ys = get_all_outputs();

    return std::ranges::any_of
      (
       tx.get_output_pks()
       , [&ys](const auto& x) {
         return ys.contains(x);
       }
       )
      ;
  }
}
