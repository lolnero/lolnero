/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include "blockchain_mem.hpp"

#include "cryptonote/basic/functional/format_utils.hpp"
#include "math/blockchain/functional/tx_check.hpp"
#include "math/consensus/consensus.hpp"

#include "math/ringct/functional/ringCT.hpp"
#include "math/ringct/functional/rctOps.hpp"
#include "math/blockchain/functional/ringct.hpp"
#include "math/blockchain/functional/short_chain_history.hpp"

#include "tools/epee/functional/algorithm.hpp"

#include <iostream>
#include <fstream>

#include <ranges>
#include <filesystem>

#include "config/lol.hpp"

namespace cryptonote {

  std::mutex blockchain_lock;

  // mutable

  void blocktree_t::set_fixed_difficulty
  (const std::optional<diff_t> x)
  {
    std::scoped_lock lock(blockchain_lock);

    m_chain.m_fixed_difficulty = x;
    m_alt_chain.m_fixed_difficulty = x;
  }
  
  void blocktree_t::set_ignore_checkpoints(const bool x) {
    std::scoped_lock lock(blockchain_lock);

    m_chain.m_ignore_checkpoints = x;
    m_alt_chain.m_ignore_checkpoints = x;
  }

  // pool
  bool blocktree_t::pool_add_tx(const ringct& tx) {
    std::scoped_lock lock(blockchain_lock);

    return pool_add_tx_no_lock(tx);
  }

  bool blocktree_t::pool_add_tx_no_lock(const ringct& tx) {
    if (m_chain.m_tx_index.contains(tx.hash())) {
      // prevent confusing error message
      return true;
    }

    if (!m_chain.verify_ringct(tx)) {
      LOG_WARNING("chain verify ringct failed");
      return false;
    }

    const auto tvc = m_pool.add_tx(tx);

    return !tvc.m_verification_failed;
  }

  std::optional<pool_tx_t>
  blocktree_t::pool_take_tx(const crypto::hash id) {
    std::scoped_lock lock(blockchain_lock);
    return m_pool.take_tx(id);
  }

  // blockchain
  block_add_t blocktree_t::push_block(const block_t& x) {
    std::scoped_lock lock(blockchain_lock);
    return push_block_no_lock(x);
  }

  block_add_t blocktree_t::push_block_no_lock(const block_t& x) {
    // overlapping
    if (m_chain.get_block_by_hash(x.hash())) {
      LOG_DEBUG("skipping overlapping block on main chain");
      return block_add_t::ignored;
    }

    if (m_alt_chain.get_block_by_hash(x.hash())) {
      LOG_DEBUG("skipping overlapping block on alt chain");
      return block_add_t::ignored;
    }

    // json j = x;
    // LOG_DEBUG("adding block: ");
    // LOG_DEBUG(j.dump(4));

    /*

      When adding a new block to a blockchain, three things can
      happen.

      A. It's added to the top of the main chain.

      B. It's added to the top of the alt chain

      B.1. After adding to the alt chain, the alt chain has _less_
      cumulative proof of work (pow) than the main chain.

      B.2. After adding to the alt chain, the alt chain has _more_
      cumulative pow than the main chain.

      C. It's added to a new branch from the main chain, essentially
      creating a chain split.

      All cases are labeled in the following code. Case A and C are
      easy to deal with. Case B.1. is also easy, since we are just
      adding a new block to a dead branch.

      The most important case, or the essence of crypto currency is
      case B.2.

      In our implementation, which is really a heuristic of modeling
      infinite branches, since peers can potentially be creating
      multiple branches simultaneously, with only a main chain and an
      alt chain, the way we deal with B.2. is to do a simple
      swap. Since by convention, note _not_ consensus, the chain with
      the most cumulative pow should be the one to follow.

      This swap should be atomic, and it ensures that there's no
      double spend, which is really what blockchain is all about.

     */


    // Case A
    if (m_chain.m_blocks.back().hash() == x.prev_hash) {
      LOG_DEBUG("adding to main chain");
      const auto added_to_main = m_chain.push_block(x);

      if (!added_to_main) return block_add_t::rejected;

      LOG_DEBUG("block added to main chain: " + x.hash().to_str());

      if (x.txs.empty()) {
        LOG_TRACE("block txs is empty");
      }

      for (const auto& tx: x.txs) {
        LOG_TRACE
          (
           "block txs: "
           + tx.hash().to_str()
           );
      }


      const auto pool_txs = m_pool.get_all_txs();

      for (const auto& [_, pool_tx]: pool_txs) {
        LOG_TRACE
          (
           "pool txs before: "
           + pool_tx.tx.hash().to_str()
           );
      }

      m_pool = tx_memory_pool{};

      std::ranges::for_each
        (
         pool_txs
         | std::views::values
         | std::views::transform
         (
          [](const auto& x) { return x.tx; }
          )
         , std::bind_front(&blocktree_t::pool_add_tx_no_lock, this)
         );

      const auto pool_txs_after = m_pool.get_all_txs();


      if (pool_txs_after.empty()) {
        LOG_TRACE("pool txs after is empty");
      }

      for (const auto& [_, pool_tx]: pool_txs_after) {
        LOG_TRACE
          (
           "pool txs after: "
           + pool_tx.tx.hash().to_str()
           );
      }

      return block_add_t::added;
    }

    // Case B
    if (m_alt_chain.m_blocks.back().hash() == x.prev_hash) {
      LOG_TRACE("adding to alt chain");

      const auto added_to_alt =
        m_alt_chain.push_block(x);

      if (!added_to_alt) return block_add_t::rejected;


      LOG_TRACE("block added to alt chain: " + x.hash().to_str());

      // Case B.1
      if (m_alt_chain.m_cumulative_diffs.back() <= m_chain.m_cumulative_diffs.back()) {
        return block_add_t::saved;
      }


      // Case B.2
      LOG_DEBUG("reorg");

      while (!m_alt_chain.m_block_index
             .contains(m_chain.m_blocks.back().hash())) {
        LOG_TRACE("Popping main chain block txs to pool");
        pop_block_no_lock();
      }

      LOG_DEBUG("All txs from reorged blocks are back to pool");

      const auto pool_txs = m_pool.get_all_txs();

      LOG_DEBUG("switching to altchain");
      std::swap(m_chain, m_alt_chain);

      LOG_DEBUG("reorg completed");

      m_alt_chain = {};

      LOG_TRACE("altchain cleared");

      m_pool = {};
      LOG_TRACE("pool cleared");

      LOG_DEBUG("Add popped txs back to main chain");

      std::ranges::for_each
        (
         pool_txs
         | std::views::values
         | std::views::transform
         (
          [](const auto& x) { return x.tx; }
          )
         , std::bind_front(&blocktree_t::pool_add_tx_no_lock, this)
         );

      LOG_INFO("Blockchain reorganized");

      return block_add_t::added;
    }

    LOG_DEBUG("maybe chain split");
    // split a new alt chain
    if (m_chain.m_block_index.contains(x.prev_hash)) {

      // Case 3
      LOG_DEBUG("yep, chain split");

      blockchain_t new_alt_chain = m_chain;

      while (new_alt_chain.m_blocks.back().hash() != x.prev_hash) {
        LOG_TRACE("popping new altchain to accept the new block");
        new_alt_chain.pop_block();
      }

      LOG_DEBUG("new altchain initialized");

      if (new_alt_chain.push_block(x)) {
        LOG_TRACE("block added to new alt chain");

        std::swap(m_alt_chain, new_alt_chain);
        return block_add_t::saved;
      } else {
        LOG_TRACE("block rejected by new alt chain");
        return block_add_t::rejected;
      }

    }

    return block_add_t::rejected;
  }

  block_add_t blocktree_t::push_blocks
  (const std::span<const block_t> xs)
  {
    std::scoped_lock lock(blockchain_lock);

    block_add_t r = block_add_t::ignored;

    for (const auto& x: xs) {
      r = push_block_no_lock(x);

      switch (r)
        {
        case block_add_t::rejected: return r;
        default: break;
        }
    }

    return r;
  }

  std::optional<block_t> blocktree_t::pop_block() {
    std::scoped_lock lock(blockchain_lock);
    return pop_block_no_lock();
  }

  std::optional<block_t> blocktree_t::pop_block_no_lock() {
    const auto maybe_block = m_chain.pop_block();

    if (!maybe_block) {
      LOG_DEBUG("Nothing to pop for blocktree");
      return {};
    }

    LOG_TRACE("Saving current txs in pool");
    const auto pool_txs = m_pool.get_all_txs();
    m_pool = {};

    const auto popped_block = *maybe_block;

    LOG_TRACE("Adding block txs back to pool");
    std::ranges::for_each
      (
       popped_block.txs
       , std::bind_front(&blocktree_t::pool_add_tx_no_lock, this)
       );

    LOG_TRACE("Adding pool txs back to pool");
    std::ranges::for_each
      (
       pool_txs
       | std::views::values
       | std::views::transform
       (
        [](const auto& x) { return x.tx; }
        )
       , std::bind_front(&blocktree_t::pool_add_tx_no_lock, this)
       );

    LOG_TRACE("pop block done for blocktree");
    return popped_block;
  }


  // const

  bool blocktree_t::store(const std::string file_path) const
  {
    json j;

    LOG_GLOBAL("saving mem db ...");

    {
      std::scoped_lock lock(blockchain_lock);
      j = *this;
    }

    try
    {
      const std::string tmp_file_path = file_path + ".tmp";

      std::ofstream json_file(tmp_file_path);
      json_file << j;

      std::filesystem::rename
        (
         std::filesystem::path(tmp_file_path)
         , std::filesystem::path(file_path)
         );

      LOG_GLOBAL("saving mem db done.");
    }
    catch (const std::exception &e) {
      LOG_GLOBAL
        (
         std::string()
         + "ERROR in saving mem db: "
         + e.what()
         );
      return false;
    }
    catch (...) {
      LOG_GLOBAL
        (
         std::string()
         + "ERROR in saving mem db"
         );
      return false;
    }

    return true;
  }

  std::optional<blocktree_t>
  blocktree_t::load(const std::string file_path)
  {
    try {
      {
        std::ifstream json_file(file_path);

        if (json_file.is_open()) {
          LOG_GLOBAL("loading mem db ...");

          json j;
          json_file >> j;

          const uint64_t loaded_version = j.value("version", 0);

          if (loaded_version != blocktree_json_version) {
            LOG_GLOBAL
              (
               "db version mismatch: "
               + std::to_string(loaded_version)
               + ", expected: "
               + std::to_string(blocktree_json_version)
               );
            return {};
          }

          const blocktree_t x = j;


          LOG_GLOBAL("loading mem db done.");

          return x;
        }
      }
    }
    catch (const std::exception &e) {
      LOG_GLOBAL
        (
         std::string()
         + "ERROR in loading mem db: "
         + e.what()
         );
    }
    catch (...) {
      LOG_GLOBAL
        (
         std::string()
         + "ERROR in loading mem db"
         );
    }

    return {};
  }


  std::tuple
  <
    std::list<crypto::hash>
    , size_t
    , uint64_t
    >
  blocktree_t::pick_txs_for_block_template
  (
   const uint64_t block_index
   ) const {
    std::scoped_lock lock(blockchain_lock);

    return m_pool.pick_txs_for_block_template(block_index);
  }

  std::optional<pool_tx_t> blocktree_t::pool_get_tx
  (const crypto::hash id) const {
    std::scoped_lock lock(blockchain_lock);

    return m_pool.get_tx(id);
  }

  std::list<std::optional<pool_tx_t>> blocktree_t::pool_get_txs
  (const std::span<const crypto::hash> hashes) const {
    std::scoped_lock lock(blockchain_lock);

    return m_pool.get_txs(hashes);
  }

  bool blocktree_t::pool_contains_tx
  (
   const crypto::hash id
   ) const {
    std::scoped_lock lock(blockchain_lock);

    return m_pool.contains_tx(id);

  }

  std::map<crypto::hash, pool_tx_t>
  blocktree_t::pool_get_all_txs() const {
    std::scoped_lock lock(blockchain_lock);

    return m_pool.get_all_txs();
  }

  std::list<pool_tx_t>
  blocktree_t::pool_get_complement
  (
   const std::span<const crypto::hash> hashes
   ) const {
    std::scoped_lock lock(blockchain_lock);

    return m_pool.get_complement(hashes);
  }

  std::optional<block_t>
  blocktree_t::get_block_by_index(const size_t x) const {
    std::scoped_lock lock(blockchain_lock);

    return m_chain.get_block_by_index(x);
  };

  std::optional<block_t>
  blocktree_t::get_block_by_hash(const crypto::hash x) const {
    std::scoped_lock lock(blockchain_lock);
    return m_chain.get_block_by_hash(x);
  }

  size_t blocktree_t::get_size() const {
    std::scoped_lock lock(blockchain_lock);
    return m_chain.get_size();
  }

  size_t blocktree_t::get_p2p_index() const {
    std::scoped_lock lock(blockchain_lock);
    return m_chain.get_size() + 1;
  }

  size_t blocktree_t::get_top_block_index() const {
    std::scoped_lock lock(blockchain_lock);
    return m_chain.get_top_block_index();
  }

  diff_t blocktree_t::get_next_diff() const {
    std::scoped_lock lock(blockchain_lock);
    return m_chain.get_next_diff();
  }


  block_t blocktree_t::get_top_block() const {
    std::scoped_lock lock(blockchain_lock);
    return m_chain.get_top_block();
  }


  diff_t blocktree_t::get_top_diff() const {
    std::scoped_lock lock(blockchain_lock);
    return m_chain.get_top_diff();
  }

  diff_t blocktree_t::get_top_cumulative_diff() const {
    std::scoped_lock lock(blockchain_lock);
    return m_chain.get_top_cumulative_diff();
  }


  std::list<crypto::hash>
  blocktree_t::get_short_chain_history() const
  {
    std::scoped_lock lock(blockchain_lock);
    return m_chain.get_short_chain_history();
  }

  std::vector<std::optional<block_t>>
  blocktree_t::get_blocks
  (const std::span<const crypto::hash> xs) const
  {
    std::scoped_lock lock(blockchain_lock);
    return m_chain.get_blocks(xs);
  }


  std::list<block_t>
  blocktree_t::get_blocks_by_short_chain_history
  (const std::list<crypto::hash> xs) const {
    std::scoped_lock lock(blockchain_lock);
    return m_chain.get_blocks_by_short_chain_history(xs);
  }

  size_t blocktree_t::get_txs_size() const {
    std::scoped_lock lock(blockchain_lock);
    return m_chain.get_txs_size();
  }

  size_t blocktree_t::get_outputs_size() const {
    std::scoped_lock lock(blockchain_lock);
    return m_chain.get_outputs_size();
  }


  std::optional<uint64_t>
  blocktree_t::get_output_index_by_public_key
  (const crypto::public_key x) const {
    std::scoped_lock lock(blockchain_lock);
    return m_chain.get_output_index_by_public_key(x);
  }

  std::optional<ring::decoy>
  blocktree_t::get_decoy_by_index(const size_t x) const {
    std::scoped_lock lock(blockchain_lock);
    return m_chain.get_decoy_by_index(x);
  }

  std::optional<coinbase_tx>
  blocktree_t::get_coinbase_tx_by_hash(const crypto::hash x) const
  {
    std::scoped_lock lock(blockchain_lock);
    return m_chain.get_coinbase_tx_by_hash(x);
  }

  std::optional<ringct>
  blocktree_t::get_tx_by_hash(const crypto::hash x) const
  {
    std::scoped_lock lock(blockchain_lock);
    return m_chain.get_tx_by_hash(x);
  }

  bool blocktree_t::check_block_timestamp
  (
   const uint64_t x
   ) const
  {
    std::scoped_lock lock(blockchain_lock);
    return m_chain.check_block_timestamp(x);
  }

  bool blocktree_t::block_known(const crypto::hash x) const {
    std::scoped_lock lock(blockchain_lock);

    return 
      m_chain.get_block_by_hash(x).has_value()
      ||
      m_alt_chain.get_block_by_hash(x).has_value()
      ;
  }

  std::pair<std::list<crypto::hash>,uint64_t>
  blocktree_t::get_hashes_by_short_chain_history
  (
   const std::list<crypto::hash> xs
   , const size_t max_size
   ) const
  {
    std::scoped_lock lock(blockchain_lock);
    return m_chain.get_hashes_by_short_chain_history(xs, max_size);
  }

  bool blocktree_t::is_output_key_image_spent
  (const crypto::key_image& x) const {
    std::scoped_lock lock(blockchain_lock);

    return m_chain.is_output_key_image_spent(x);
  }


  std::optional<diff_t>
  blocktree_t::get_cumulative_diff_by_index(const size_t x) const {
    std::scoped_lock lock(blockchain_lock);

    return m_chain.get_cumulative_diff_by_index(x);
  }

  std::optional<diff_t>
  blocktree_t::get_diff_by_index(const size_t x) const {
    std::scoped_lock lock(blockchain_lock);

    return m_chain.get_diff_by_index(x);
  }

}
