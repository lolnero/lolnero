/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "consensus.hpp"

#include "math/ringct/functional/bulletproofs_verify.hpp"
#include "math/ringct/functional/clsag_verify.hpp"
#include "math/ringct/functional/rctOps.hpp"
#include "math/ringct/functional/vectorOps.hpp"
#include "math/group/functional/group.hpp"
#include "tools/epee/functional/base64.hpp"

#include <boost/multiprecision/cpp_int.hpp>

#include <ranges>

namespace consensus {

  bool rule_2_ringct_output_amounts_should_not_overflow_amount_type
  (
   const pointS outputs
   , const Bulletproof proof
   ) {
    return bulletproof_VERIFY(outputs, proof);
  }

  bool rule_3_ringct_should_be_balanced
  (
   const pointS inputs
   , const pointS outputs
   , const amount_t fee
   ) {
    return
      sum(inputs) == sum(outputs) + H_(crypto::int_to_scalar(fee));
  }

  bool rule_4_ringct_input_should_be_from_a_key
  (
   const crypto::hash message
   , const clsag sig
   , const output_public_dataS decoys
   , const crypto::ec_point pseudo_input_commit
   ) {
    return verify_clsag_signature
      (message, sig, decoys, pseudo_input_commit);
  }

  bool rule_8_input_key_images_should_be_unique
  (const std::span<const crypto::key_image> xs)
  {
    const std::set<crypto::key_image> s(xs.begin(), xs.end());
    return s.size() == xs.size();
  }


  std::optional<Bulletproof>
  rule_9_range_proof_should_not_contain_invalid_data
  (
   const Bulletproof_unsafe x
   )
  {
    return preview_safe_bulletproof(x);
  }

  std::optional<clsag>
  rule_10_ring_signature_should_not_contain_invalid_data
  (
   const clsag_unsafe x
   )
  {
    return preview_safe_clsag(x);
  }

  std::optional<std::vector<crypto::ec_point>> are_points_safe
  (
   const std::span<const crypto::ec_point_unsafe> xs
   )
  {
    const auto maybe_safe_points =
      xs
      | std::views::transform
      (
       crypto::preview_safe_point
       );

    const bool all_safe_points =
      std::ranges::all_of
      (
       maybe_safe_points
       , [](const auto& x) -> bool {
         return x.has_value();
       }
       );

    if (!all_safe_points) return {};

    const auto safe_points =
      maybe_safe_points
      | std::views::transform
      (
       [](const auto&x) { return *x; }
       );

    return {{ safe_points.begin(), safe_points.end() }};
  }

  std::optional<std::vector<crypto::public_key>>
  rule_11_tx_output_public_keys_should_be_safe_points
  (
   const std::span<const crypto::ec_point_unsafe> xs
   ) {
    const auto maybe_safe_points = are_points_safe(xs);
    if (!maybe_safe_points) {
      return {};
    } else {
      const auto safe_points = *maybe_safe_points;

      return {{ safe_points.begin(), safe_points.end() }};
    }
  }


  std::optional<cryptonote::txout_to_key>
  rule_12_tx_output_target_should_be_output_public_key
  (const cryptonote::txout_target_v x)
  {
    if (x.type() == typeid(cryptonote::txout_to_key)) {
      return boost::get<cryptonote::txout_to_key>(x);
    } else {
      return {};
    }
  }

  std::optional<std::vector<cryptonote::txout_to_key>>
  are_tx_output_targets_valid
  (const std::span<const cryptonote::txout_target_v> xs)
  {
    const auto maybe_valid =
      xs
      | std::views::transform
      (
       rule_12_tx_output_target_should_be_output_public_key
       );

    const bool all_valid =
      std::ranges::all_of
      (
       maybe_valid
       , [](const auto& x) -> bool {
         return x.has_value();
       }
       )
      ;

    if (!all_valid) return {};

    const auto valid_targets =
      maybe_valid
      | std::views::transform
      (
       [](const auto& x) { return *x; }
       );

    return {{ valid_targets.begin(), valid_targets.end() }};
  }

  std::optional<cryptonote::txin_from_key>
  rule_15_ringct_input_type_should_be_from_key
  (const cryptonote::txin_v x)
  {
    if (x.type() == typeid(cryptonote::txin_from_key)) {
      return boost::get<cryptonote::txin_from_key>(x);
    } else {
      return {};
    }
  }

  std::optional<std::vector<cryptonote::txin_from_key>>
  are_ringct_input_types_valid
  (const std::span<const cryptonote::txin_v> xs)
  {
    const auto maybe_from_keys =
      xs
      | std::views::transform
      (
       rule_15_ringct_input_type_should_be_from_key
       );

    const bool all_from_keys =
      std::ranges::all_of
      (
       maybe_from_keys
       ,
       [](const auto& x) -> bool {
         return x.has_value();
       }
       );

    if (!all_from_keys) return {};

    const auto from_keys =
      maybe_from_keys
      | std::views::transform
      (
       [](const auto& x) { return *x; }
       );

    return {{ from_keys.begin(), from_keys.end() }};
  }

  std::optional<rct::Bulletproof_unsafe>
  rule_17_ringct_should_contain_only_one_range_proof
  (
   const std::span<const rct::Bulletproof_unsafe> proofs
   )
  {
    if (proofs.size() == 1) {
      return proofs.front();
    } else {
      return {};
    }
  }

  bool rule_21_ringct_should_have_at_least_1_outputs
  (
   const size_t x
   )
  {
    return x >= 1;
  }

  std::optional<cryptonote::txin_v>
  rule_22_coinbase_tx_should_have_only_one_input
  (
   const std::span<const cryptonote::txin_v> xs
   )
  {
    if (xs.size() == 1) {
      return xs.front();
    } else {
      return {};
    };
  }

  std::optional<cryptonote::txin_gen>
  rule_23_coinbase_tx_input_type_should_be_gen
  (
   const cryptonote::txin_v x
   )
  {
    if (x.type() == typeid(cryptonote::txin_gen)) {
      return boost::get<cryptonote::txin_gen>(x);
    } else {
      return {};
    };
  }

  std::optional<uint64_t>
  rule_24_coinbase_output_amount_sum_should_not_overflow_amount_t
  (
   const std::span<const uint64_t> xs
   )
  {
    using namespace boost::multiprecision;
    constexpr uint128_t max64bit =
      std::numeric_limits<uint64_t>::max();

    const uint128_t sum = std::transform_reduce
      (
       xs.begin()
       , xs.end()
       , uint128_t(0)
       , std::plus<uint128_t>() 
       , [](const uint64_t x) -> uint128_t { return uint128_t(x); }
       );

    if (sum <= max64bit) {
      return uint64_t(sum);
    } else {
      return {};
    };
  }

  std::optional<cryptonote::txin_v>
  rule_25_ringct_should_have_at_least_one_input
  (
   const std::span<const cryptonote::txin_v> xs
   )
  {
    if (!xs.empty()) {
      return xs.front();
    } else {
      return {};
    };
  }

  const std::map<const uint64_t, const std::string> checkpoints =
    {
      {
        1e4
        , "RWJcJhv2LgokuzC+ixgWu4k9azNSXc5UB3uGX7h9V6k="
      }

      , {
        1e5
        , "DIPNi5NUuScChXfZsAEfHF8Wid2UoMjW5GTSEqLWkos="
      }

      , {
        1.7e5
        , "cLjpFNQCqy0t36FNQQAfCir67B55baCEHE3IVhU9w9s="
      }
    };

  const std::map<const uint64_t, const cryptonote::diff_t>
  diff_checks =
    {
      { 300 - 1, 41159068846824 }
      , { 1e4 - 1, 114352829863877 }
      , { 1e5 - 1, 2273557716019995 }
      , { 1.3e5 - 1, 13901381391867985 } // rpc + 1
      , { 1.6e5 - 1, 28087902709368131 } // rpc - 1
    };


  uint64_t latest_check_point_index() {
    return std::ranges::max(checkpoints | std::views::keys);
  }

  bool is_block_valid_against_checkpoints
  (
   const uint64_t block_index
   , const crypto::hash hash
   )
  {
    if (!checkpoints.contains(block_index)) return true;

    const auto checkpoint_hash = checkpoints.at(block_index);

    const auto block_hash =
      epee::base64::encode_to_base64(hash.data);

    return checkpoint_hash == block_hash;
  }

  bool is_block_valid_against_diff_checks
  (
   const uint64_t block_index
   , const cryptonote::diff_t diff
   )
  {
    if (!diff_checks.contains(block_index)) return true;

    const auto x = diff_checks.at(block_index);

    return x == diff;
  }

  bool rule_32_output_public_keys_should_be_unique
  (const std::span<const crypto::public_key> xs)
  {
    const std::set<crypto::public_key> s(xs.begin(), xs.end());
    return s.size() == xs.size();
  }

}
