/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#pragma once

#include "constant.hpp"

#include "math/ringct/functional/rctTypes.hpp"
#include "math/crypto/functional/key.hpp"
#include "math/blockchain/functional/difficulty.hpp"

#include "cryptonote/basic/functional/base.hpp"

#include <numeric>
#include <set>
#include <ranges>

using namespace rct;

namespace consensus {
  constexpr bool rule_1_you_don_t_talk_about_lolnero() {
    return true;
  }

  bool rule_2_ringct_output_amounts_should_not_overflow_amount_type
  (
   const pointS outputs
   , const Bulletproof proof
   );

  bool rule_3_ringct_should_be_balanced
  (
   const pointS inputs
   , const pointS outputs
   , const amount_t fee
   );

  bool rule_4_ringct_input_should_be_from_a_key
  (
   const crypto::hash message
   , const clsag sig
   , const output_public_dataS decoys
   , const crypto::ec_point pseudo_input_commit
   );


  consteval uint64_t get_block_reward() {
    return get_coin_amount() * 300ull;
  }

  consteval bool rule_5_block_reward_is_constant() {
    return get_block_reward() == get_coin_amount() * 300ull;
  }

  static_assert(rule_5_block_reward_is_constant());

  consteval uint64_t get_minimum_block_size_bound() {
    constexpr uint64_t min_block_size = 128ull * 1024ull; // 128 kB
    return min_block_size;
  }

  constexpr uint64_t get_block_size_bound(const uint64_t block_index)
  {
    return std::max<uint64_t>
      (get_minimum_block_size_bound(), block_index);
  }

  constexpr bool rule_6_block_size_should_be_bounded_by_block_index
  (
   const uint64_t block_index
   , const size_t block_size
   )
  {
    return block_size <= consensus::get_block_size_bound(block_index);
  }

  constexpr bool rule_7_ringct_input_decoys_offsets_should_not_be_zero_except_the_first_one
  (
   const std::span<const uint64_t> offsets
   )
  {
    if (offsets.empty()) return true;

    // Key offsets are relative increments of output indices in a
    // blockchain sorted by block index.  The first value can be
    // 0. When it's 0, it references _the_ output of the coinbase tx
    // of the first block after the genesis block, i.e. block at index
    // 1.
    //
    // We can verify this by playing with the `lolnerod-output`
    // script:
    //
    // lolnerod-output 0

    return std::ranges::all_of
      (
       offsets
       | std::views::drop(1)
       , [](const auto& x) {
         return x != 0;
       }
       );
  }

  bool rule_8_input_key_images_should_be_unique
  (const std::span<const crypto::key_image> xs);

  std::optional<Bulletproof>
  rule_9_range_proof_should_not_contain_invalid_data
  (
   const Bulletproof_unsafe x
   );

  std::optional<clsag>
  rule_10_ring_signature_should_not_contain_invalid_data
  (
   const clsag_unsafe x
   );

  std::optional<std::vector<crypto::ec_point>> are_points_safe
  (
   const std::span<const crypto::ec_point_unsafe> xs
   );

  std::optional<std::vector<crypto::public_key>>
  rule_11_tx_output_public_keys_should_be_safe_points
  (
   const std::span<const crypto::ec_point_unsafe> xs
   );

  std::optional<cryptonote::txout_to_key>
  rule_12_tx_output_target_should_be_output_public_key
  (const cryptonote::txout_target_v x);

  std::optional<std::vector<cryptonote::txout_to_key>>
  are_tx_output_targets_valid
  (const std::span<const cryptonote::txout_target_v> xs);

  constexpr auto
  rule_13_tx_output_commits_should_be_safe_points = are_points_safe;

  constexpr auto
  rule_14_tx_pseudo_input_commits_should_be_safe_points =
    are_points_safe;

  std::optional<cryptonote::txin_from_key>
  rule_15_ringct_input_type_should_be_from_key
  (const cryptonote::txin_v x);

  std::optional<std::vector<cryptonote::txin_from_key>>
  are_ringct_input_types_valid
  (const std::span<const cryptonote::txin_v> xs);

  constexpr auto
  rule_16_next_difficult_target_over_average_difficulty_should_be_the_inverse_of_the_lwma_of_block_time_over_target_time
  = cryptonote::next_difficulty;

  std::optional<rct::Bulletproof_unsafe>
  rule_17_ringct_should_contain_only_one_range_proof
  (
   const std::span<const rct::Bulletproof_unsafe> proofs
   );

  constexpr bool rule_18_ringct_input_key_images_should_be_sorted
  (
   const std::span<const crypto::key_image> xs
   )
  {
    return std::is_sorted
      (
       xs.begin()
       , xs.end()
       , std::greater_equal<crypto::key_image>()
       );
  }

  constexpr bool rule_19_coinbase_tx_should_be_balanced
  (
   const std::span<const amount_t> xs
   , const amount_t fee
   )
  {
    return
      get_block_reward() + fee == std::reduce(xs.begin(), xs.end());
  }

  constexpr bool rule_20_coinbase_outputs_are_unlocked
  ( const size_t x ) { return x == 0; }

  bool rule_21_ringct_should_have_at_least_1_outputs
  (
   const size_t x
   );

  std::optional<cryptonote::txin_v>
  rule_22_coinbase_tx_should_have_only_one_input
  (
   const std::span<const cryptonote::txin_v> xs
   );

  std::optional<cryptonote::txin_gen>
  rule_23_coinbase_tx_input_type_should_be_gen
  (
   const cryptonote::txin_v x
   );

  std::optional<uint64_t>
  rule_24_coinbase_output_amount_sum_should_not_overflow_amount_t
  (
   const std::span<const uint64_t> xs
   );

  std::optional<cryptonote::txin_v>
  rule_25_ringct_should_have_at_least_one_input
  (
   const std::span<const cryptonote::txin_v> xs
   );

  constexpr uint64_t
  rule_26_minimum_fee_should_be_tx_size_times_fee_per_byte
  (
    const uint64_t tx_size
   ) {
    return tx_size * constant::FEE_PER_BYTE;
  }


  constexpr bool after_unlock_ignore_height
  (const uint64_t block_index) {
    return block_index > 173750;
  }

  constexpr uint64_t
  rule_27_unlock_block_index_should_be_0_and_ignore() {
    return 0;
  }

  bool is_block_valid_against_checkpoints
  (
   const uint64_t block_index
   , const crypto::hash hash
   );

  constexpr bool
  rule_28_coinbase_tx_should_have_rct_null_type_in_rct_data
  (
   const uint8_t x
   ) {
    return x == rct::RCTTypeNull;
  }

  constexpr bool
  rule_29_ringct_should_have_rct_type_clsag_in_rct_data
  (
   const uint8_t x
   ) {
    return x == rct::RCTTypeCLSAG;
  }

  constexpr bool
  rule_30_ringct_should_have_0_amount_outputs
  (
   const std::span<const cryptonote::tx_out> xs
   ) {
    return std::ranges::all_of
      (
       xs
       , [](const auto& x) {
         return x.amount == 0;
       }
       );
  }

  uint64_t latest_check_point_index();


  constexpr bool
  rule_31_ringct_should_have_0_amount_inputs
  (
   const std::span<const cryptonote::txin_from_key> xs
   ) {
    return std::ranges::all_of
      (
       xs
       , [](const auto& x) {
         return x.amount == 0;
       }
       );
  }

  bool is_block_valid_against_diff_checks
  (
   const uint64_t block_index
   , const cryptonote::diff_t diff
   );

  bool rule_32_output_public_keys_should_be_unique
  (const std::span<const crypto::public_key> xs);

  constexpr uint64_t rule_33_miner_index_is_block_index_plus_1
  (
   uint64_t x
   ) {
    return x + 1;
  }

}
