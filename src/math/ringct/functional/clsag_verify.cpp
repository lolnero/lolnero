/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/

#include "clsag_verify.hpp"

#include "tools/epee/include/logging.hpp"

#include "math/ringct/functional/curveConstants.hpp"
#include "math/ringct/functional/rctOps.hpp"
#include "math/ringct/functional/vectorOps.hpp"

#include "config/cryptonote.hpp"

#include "tools/epee/include/logging_macro.hpp"

#include <ranges>


namespace rct {

  crypto::crypto_data crypto_data_from_hash_key
  (
   const std::string_view hash_key
   )
  {
    crypto::crypto_data hash_key_data{};
    std::ranges::copy
      (
       hash_key
       , hash_key_data.data.begin()
       );

    return hash_key_data;
  }

  crypto::dataBS get_hash_data_from_decoys_with_key
  (
   const std::string_view hash_key
   , const output_public_dataS decoys
   )
  {
    const auto pks =
      decoys
      | std::views::transform([](const auto& x) { return x.public_key; });

    const auto commits =
      decoys
      | std::views::transform([](const auto& x) { return x.commit; });

    return 
      crypto::dataBS{crypto_data_from_hash_key(hash_key)}
    + crypto::dataBS{pks.begin(), pks.end()}
    + crypto::dataBS{commits.begin(), commits.end()}
    ;
  }

  crypto::ec_scalar hash_clsag_data_with_key
  (
   const std::string_view hash_key
   , const output_public_dataS decoys
   , const crypto::ec_point signer_key_image
   , const crypto::ec_point blinding_factor_surplus_ki
   , const crypto::ec_point pseudo_input_commit
   )
  {
    // Aggregation hashes
    return hash_dataV_to_scalar
      (
       get_hash_data_from_decoys_with_key
       (
        hash_key
        , decoys
        )
       + crypto::dataBS
       {
         signer_key_image
         , to_inv8(blinding_factor_surplus_ki)
         , pseudo_input_commit
       }
       );
  }

  bool verify_clsag_signature
  (
   const crypto::hash message
   , const clsag sig
   , const output_public_dataS decoys
   , const crypto::ec_point pseudo_input_commit
   )
  {
    // Check data
    const auto signer_key_image = sig.signer_key_image;
    const auto blinding_factor_surplus_ki =
      sig.blinding_factor_surplus_ki;

    // Aggregation hashes
    const crypto::ec_scalar mu_P = hash_clsag_data_with_key
      (
       config::HASH_KEY_CLSAG_AGG_0
       , decoys
       , signer_key_image
       , blinding_factor_surplus_ki
       , pseudo_input_commit
       );

    const crypto::ec_scalar mu_C = hash_clsag_data_with_key
      (
       config::HASH_KEY_CLSAG_AGG_1
       , decoys
       , signer_key_image
       , blinding_factor_surplus_ki
       , pseudo_input_commit
       );

    // Set up round hash
    // domain, P, C, pseudo_input_commit, message, L, R
    const auto c_hash_data =
      get_hash_data_from_decoys_with_key
      (
       config::HASH_KEY_CLSAG_ROUND
       , decoys
       )
      + crypto::dataBS
      {
        pseudo_input_commit
        , crypto::h2d(message)
      };

    using decoy_with_sig =
      std::pair<output_public_data, crypto::ec_scalar>;

    std::vector<decoy_with_sig> decoy_with_sigV;
    std::transform
      (
       decoys.begin()
       , decoys.end()
       , sig.s.begin()
       , std::back_inserter(decoy_with_sigV)
       , [](const auto& x, const auto& y) -> decoy_with_sig {
         return {x, y};
       }
       );

    const auto maybe_c = std::accumulate
      (
       decoy_with_sigV.begin()
       , decoy_with_sigV.end()
       , std::optional<crypto::ec_scalar>{sig.c1}
       , [
          mu_P
          , mu_C
          , pseudo_input_commit
          , c_hash_data
          , signer_key_image
          , blinding_factor_surplus_ki
          ]
       (const auto& maybe_c, const auto& decoy_with_sig)
       -> std::optional<crypto::ec_scalar> {
         if (!maybe_c) {
           return {};
         }

         const auto c = *maybe_c;
         const auto decoy = decoy_with_sig.first;
         const auto decoy_sig = decoy_with_sig.second;

         const crypto::ec_scalar c_p = mu_P * c;
         const crypto::ec_scalar c_c = mu_C * c;

         const crypto::ec_point decoy_commit = decoy.commit;

         const crypto::ec_point decoy_commit_surplus =
           decoy_commit - pseudo_input_commit;

         // Compute L
         const crypto::ec_point L =
           G_(decoy_sig)
           +
           vector_commit
           (
            std::array
            {
              c_p
              , c_c
            }
            , std::array
            {
              crypto::pk2p(decoy.public_key)
              , decoy_commit_surplus
            }
            );

         // Compute R
         const crypto::ec_point k =
           crypto::hash_to_point_via_field(decoy.public_key);

         const crypto::ec_point R = vector_commit
           (
            std::array
            {
              decoy_sig
              , c_p
              , c_c
            }
            ,
            std::array
            {
              k
              , signer_key_image
              , blinding_factor_surplus_ki
            }
            );

         const auto c_next =
           hash_dataV_to_scalar(c_hash_data + crypto::dataBS{L, R});

         ASSERT_OR_LOG_ERROR_AND_RETURN
           (
            c_next != rct::s_zero
            , {}
            , "Bad signature hash"
            );

         return c_next;
       }
       );

    if (!maybe_c) {
      return false;
    }

    return sig.c1 == *maybe_c;
  }

}
