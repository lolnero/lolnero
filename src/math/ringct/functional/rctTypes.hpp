/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/

#pragma once

#include "math/crypto/functional/key.hpp"

#include <span>


namespace rct {

  using pointV = std::vector<crypto::ec_point>;
  using pointS = std::span<const crypto::ec_point>;

  using scalarV = std::vector<crypto::ec_scalar>;
  using scalarS = std::span<const crypto::ec_scalar>;

  using inv8 = crypto::ec_point_unsafe;
  using inv8V = std::vector<inv8>;

  using reconstructed_point = crypto::ec_point;

  using amount_t = uint64_t;


  struct output_public_data {
    crypto::public_key public_key;
    crypto::ec_point commit;

    auto operator <=> (const output_public_data& x) const = default;
  };

  using output_public_dataV = std::vector<output_public_data>;
  using output_public_dataS = std::span<const output_public_data>;
  using decoysV = std::vector<output_public_dataV>;


  struct LR
  {
    crypto::ec_point L;
    crypto::ec_point R;

    auto operator <=> (const LR& x) const = default;
  };

  using LR_V = std::vector<LR>;
  using LR_BS = std::basic_string<LR>;

  const rct::inv8V to_inv8V(const pointS xs);

  LR_V zipLR(const pointS L, const pointS R);

  std::pair<pointV, pointV> splitLR
  (
   const std::span<const LR> xs
   );
  
}

