/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/

#include "clsag_gen.hpp"

#include "tools/epee/include/logging.hpp"

#include "math/ringct/functional/curveConstants.hpp"
#include "math/ringct/functional/rctOps.hpp"
#include "math/ringct/functional/vectorOps.hpp"
#include "math/ringct/functional/clsag_verify.hpp"

#include "config/cryptonote.hpp"

#include <ranges>

namespace rct {
  // Generate a CLSAG signature
  // See paper by Goodell et al. (https://eprint.iacr.org/2019/654)
  //
  // The keys are set as follows:
  //   P[l] == p*G
  //   C[l] == z*G
  //   C[i] == C_nonzero[i] - C_offset (for hashing purposes) for all i
  clsag generate_clsag_signature
  (
   const crypto::hash message
   , const crypto::ec_scalar signer_sk
   , const crypto::ec_scalar signer_blinding_factor
   , const size_t index_in_decoys
   , const crypto::ec_scalar pseudo_input_blinding_factor
   , const crypto::ec_point pseudo_input_commit
   , const output_public_dataS decoys
   , const crypto::ec_scalar random_a
   , const scalarS random_s_V
   )
  {
    const size_t n = decoys.size(); // ring size

    LOG_ERROR_AND_THROW_UNLESS
      (
       n == random_s_V.size()
       , "Decoy and random scalar vector sizes must match!"
       );

    LOG_ERROR_AND_THROW_UNLESS
      (
       index_in_decoys < n
       , "Signing index out of range!"
       );

    const auto decoy_commit_surplus = decoys
      | std::views::transform
      (
       [pseudo_input_commit](const auto& x) {
         return x.commit - pseudo_input_commit;
       }
       );

    const crypto::ec_scalar signer_blinding_factor_surplus =
      signer_blinding_factor - pseudo_input_blinding_factor;


    // mages images
    const crypto::ec_point signer_pk_hash =
      crypto::hash_to_point_via_field
      (decoys[index_in_decoys].public_key);

    const crypto::ec_point sig_signer_key_image =
      signer_pk_hash ^ signer_sk;

    const crypto::ec_point D =
      signer_pk_hash ^ signer_blinding_factor_surplus;

    // Offset key image
    const crypto::ec_point sig_blinding_factor_surplus_ki = D;

    const crypto::ec_scalar mu_P = hash_clsag_data_with_key
      (
       config::HASH_KEY_CLSAG_AGG_0
       , decoys
       , sig_signer_key_image
       , sig_blinding_factor_surplus_ki
       , pseudo_input_commit
       );

    const crypto::ec_scalar mu_C = hash_clsag_data_with_key
      (
       config::HASH_KEY_CLSAG_AGG_1
       , decoys
       , sig_signer_key_image
       , sig_blinding_factor_surplus_ki
       , pseudo_input_commit
       );

    // Initial commitment
    const crypto::dataBS c_hash_data =
      get_hash_data_from_decoys_with_key
      (
       config::HASH_KEY_CLSAG_ROUND
       , decoys
       )
      + crypto::dataBS
      {
        pseudo_input_commit
        , crypto::h2d(message)
      };

    const crypto::ec_scalar a = random_a;

    crypto::ec_scalar c =
      rct::hash_dataV_to_scalar
      (
       c_hash_data
       + crypto::dataBS
       {
         G_(a)
         , signer_pk_hash ^ a
       }
       );

    size_t i = (index_in_decoys + 1) % n;
    crypto::ec_scalar sig_c1;
    if (i == 0) {
      sig_c1 = c;
    }

    // Decoy indices
    scalarV s(n);

    while (i != index_in_decoys) {
      // carried from last round
      const crypto::ec_scalar c_p = mu_P * c;
      const crypto::ec_scalar c_c = mu_C * c;

      const auto sk = random_s_V[i];

      // Compute L
      const crypto::ec_point L = sum
        (
         std::array
         {
           G_(sk)
           , decoys[i].public_key ^ c_p
           , decoy_commit_surplus[i] ^ c_c
         }
         );

      // Compute R
      const crypto::ec_point A =
        crypto::hash_to_point_via_field(decoys[i].public_key);

      const crypto::ec_point R = sum
        (
         std::array
         {
           A ^ sk
           , sig_signer_key_image ^ c_p
           , D ^ c_c
         }
         );

      s[i] = sk;

      // need to be remembered
      c = rct::hash_dataV_to_scalar(c_hash_data + crypto::dataBS{L, R});

      i = (i + 1) % n;
      if (i == 0) {
        sig_c1 = c;
      }
    }

    // Compute final scalar
    s[index_in_decoys] =
      a - c *
      (mu_C * signer_blinding_factor_surplus + mu_P * signer_sk);

    return {
      s
      , sig_c1
      , sig_signer_key_image
      , sig_blinding_factor_surplus_ki
    };
  }

}
