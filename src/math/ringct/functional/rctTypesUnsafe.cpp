// Copyright (c) 2016, Monero Research Labs
//
// Author: Shen Noether <shen.noether@gmx.com>
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this list of
//    conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this list
//    of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may be
//    used to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
// THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
// THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "rctTypesUnsafe.hpp"
#include "rctOps.hpp"

#include "tools/epee/include/logging.hpp"

#include "config/cryptonote.hpp"

#include "math/consensus/consensus.hpp"

#include <ranges>


namespace rct {

  size_t n_bulletproof_max_commit_size(const Bulletproof_unsafe &x)
  {
    // log(2, 64) = 6
    return 1 << (x.L.size() - 6);
  }

  std::optional<Bulletproof> preview_safe_bulletproof
  (const Bulletproof_unsafe proof)
  {
    const auto maybe_proof_A = maybe_from_inv8(proof.A);
    ASSERT_OR_LOG_ERROR_AND_RETURN(maybe_proof_A, {}, "Bad proof.A");
    const crypto::ec_point proof_A = *maybe_proof_A;

    const auto maybe_proof_S = maybe_from_inv8(proof.S);
    ASSERT_OR_LOG_ERROR_AND_RETURN(maybe_proof_S, {}, "Bad proof.S");
    const crypto::ec_point proof_S = *maybe_proof_S;

    const auto maybe_proof_T1 = maybe_from_inv8(proof.T1);
    ASSERT_OR_LOG_ERROR_AND_RETURN(maybe_proof_T1, {}, "Bad proof.T1");
    const crypto::ec_point proof_T1 = *maybe_proof_T1;

    const auto maybe_proof_T2 = maybe_from_inv8(proof.T2);
    ASSERT_OR_LOG_ERROR_AND_RETURN(maybe_proof_T2, {}, "Bad proof.T2");
    const crypto::ec_point proof_T2 = *maybe_proof_T2;

    std::vector<crypto::ec_point> proof_L;
    for (const auto& x: proof.L) {
      const auto y = maybe_from_inv8(x);
      ASSERT_OR_LOG_ERROR_AND_RETURN(y, {}, "Bad proof.L");
      proof_L.push_back(*y);
    }

    std::vector<crypto::ec_point> proof_R;
    for (const auto& x: proof.R) {
      const auto y = maybe_from_inv8(x);
      ASSERT_OR_LOG_ERROR_AND_RETURN(y, {}, "Bad proof.R");
      proof_R.push_back(*y);
    }

    // check crypto::ec_scalar range
    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       is_reduced(proof.tau)
       , {}
       , "Input crypto::ec_scalar not in range"
       );

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       is_reduced(proof.mu)
       , {}
       , "Input crypto::ec_scalar not in range"
       );

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       is_reduced(proof.a)
       , {}
       , "Input crypto::ec_scalar not in range"
       );

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       is_reduced(proof.b)
       , {}
       , "Input crypto::ec_scalar not in range"
       );

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       is_reduced(proof.t)
       , {}
       , "Input crypto::ec_scalar not in range"
       );

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       proof_L.size() == proof_R.size()
       , {}
       , "failed to construct LR from L and R"
       );

    return Bulletproof {
      proof_A
      , proof_S
      , proof_T1
      , proof_T2
      , crypto::reduce(proof.tau)
      , crypto::reduce(proof.mu)
      , zipLR(proof_L, proof_R)
      , crypto::reduce(proof.a)
      , crypto::reduce(proof.b)
      , crypto::reduce(proof.t)
    };
  }

  Bulletproof_unsafe toUnsafeBulletproof(const Bulletproof proof) {
    const auto& [L, R] = splitLR(proof.LR);
    return Bulletproof_unsafe {
      to_inv8(proof.A)
      , to_inv8(proof.S)
      , to_inv8(proof.T1)
      , to_inv8(proof.T2)
      , proof.tau
      , proof.mu
      , to_inv8V(L)
      , to_inv8V(R)
      , proof.a
      , proof.b
      , proof.t
    };
  }

  std::optional<clsag> preview_safe_clsag(const clsag_unsafe clsag) {

    scalarV clsag_s;
    for (const auto& x: clsag.s) {
      ASSERT_OR_LOG_ERROR_AND_RETURN
        (crypto::is_reduced(x), {}, "Bad clsag.s");

      clsag_s.push_back(crypto::reduce(x));
    }

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (crypto::is_reduced(clsag.c1), {}, "Bad clsag.c1");

    const auto maybe_clsag_blinding_factor_surplus_ki =
      maybe_from_inv8(clsag.blinding_factor_surplus_ki);

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       maybe_clsag_blinding_factor_surplus_ki
       , {}
       , "Bad clsag.blinding_factor_surplus_ki"
       );

    const crypto::ec_point clsag_blinding_factor_surplus_ki =
      *maybe_clsag_blinding_factor_surplus_ki;

    return {{
        clsag_s
        , crypto::reduce(clsag.c1)
        , clsag.signer_key_image
        , clsag_blinding_factor_surplus_ki
      }};
  }

  clsag_unsafe toUnsafeCLSAG(const clsag clsag) {
    return {
      {clsag.s.begin(), clsag.s.end()}
      , clsag.c1
      , clsag.signer_key_image
      , to_inv8(clsag.blinding_factor_surplus_ki)
    };
  }

  std::optional
  <
    std::tuple
    <
      pointV
      , pointV
      , amount_t
      , std::vector<amount_t>
      , std::vector<clsag>
      , Bulletproof
      >
    > preview_rctData
  (const rctData_unsafe& x)
  {
    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       consensus::
       rule_29_ringct_should_have_rct_type_clsag_in_rct_data
       (x.rct_type)
       , {}
       , "Checking rct data on non ringct"
       );

    const std::optional<Bulletproof_unsafe> maybe_proof =
      consensus::rule_17_ringct_should_contain_only_one_range_proof
      (x.p.bulletproofs);

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       maybe_proof
       , {}
       , "More than one proofs"
       );

    const auto proof = *maybe_proof;

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       x.p.pseudo_input_commits.size() == x.p.CLSAGs.size()
       , {}
       , "Mismatched sizes of pseudo_input_commits and CLSAGs"
       );

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       x.output_commits.size() == x.ecdh_encrypted_data.size()
       , {}
       , "Mismatched sizes of output_commits and ecdh_encrypted_data"
       );


    std::vector<clsag> sigs;

    for (const auto& s: x.p.CLSAGs) {
      const auto maybe_safe_clsag = preview_safe_clsag(s);

      ASSERT_OR_LOG_ERROR_AND_RETURN
        (
         maybe_safe_clsag
         , {}
         , "Unsafe clsag"
         );

      sigs.push_back(*maybe_safe_clsag);
    }

    const auto maybe_safe_bp =
      preview_safe_bulletproof(proof);

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       maybe_safe_bp
       , {}
       , "Unsafe bulletproof"
       );

    const auto ecdh_encrypted_amounts =
      x.ecdh_encrypted_data
      | std::views::transform
      ([](const auto& x) { return x.masked_amount; })
      ;

    const auto output_commits_view =
      x.output_commits
      | std::views::transform
      ([](const auto& x) { return x.commit; })
      ;

    const pointV output_commits =
      { output_commits_view.begin(), output_commits_view.end() };

    ASSERT_OR_LOG_TRACE_AND_RETURN
      (
       consensus::
       rule_13_tx_output_commits_should_be_safe_points
       (
        std::vector<crypto::ec_point_unsafe>
        {
          output_commits.begin()
          , output_commits.end()
        }
        ).has_value()
       , {}
       , "invalid output commits"
       );

    return
      {
        {
          output_commits
          , x.p.pseudo_input_commits
          , x.fee
          , {
            ecdh_encrypted_amounts.begin()
            , ecdh_encrypted_amounts.end()
          }
          , sigs
          , *maybe_safe_bp
        }
      };
  }

  rctData_unsafe review_rctData
  (
   const pointS output_commits
   , const pointS pseudo_input_commits
   , const amount_t fee
   , const std::span<const amount_t> ecdh_encrypted_amounts
   , const std::span<const clsag> signatures
   , const Bulletproof& bulletproof
   )
  {
    const auto sigs =
      signatures
      | std::views::transform
      (
       [](const auto& x) {
         return toUnsafeCLSAG(x);
       }
       );

    const auto ecdh_encrypted_data =
      ecdh_encrypted_amounts
      | std::views::transform
      ([](const auto& x) { return ecdh_encrypted_data_t{x}; })
      ;

    const auto output_commits_for_raw =
      output_commits
      | std::views::transform
      ([](const auto& x) { return output_commit{x}; })
      ;

    const rctDataBasic rctBasic = 
      {
        RCTTypeCLSAG
        , {
          ecdh_encrypted_data.begin()
          , ecdh_encrypted_data.end()
        }
        , {
          output_commits_for_raw.begin()
          , output_commits_for_raw.end()
        }
        , fee
      };

    return {
      rctBasic
      , {
        { toUnsafeBulletproof(bulletproof) }
        , { sigs.begin(), sigs.end() }
        , {pseudo_input_commits.begin(), pseudo_input_commits.end() }
      }
    };
  }

}
