/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/

#include "rctOps.hpp"

#include "cryptonote/basic/functional/format_utils.hpp"
#include "math/group/functional/curve25519_cryptonote_extension.hpp"

#include "tools/epee/include/logging.hpp"
#include "tools/epee/include/string_tools.h"

#include "config/lol.hpp"

#include <boost/lexical_cast.hpp>

#include <sodium.h>

#include <numeric>

namespace rct {

  crypto::ec_point G_(const crypto::ec_scalar a) {
    return crypto::multBase(a);
  }

  crypto::ec_point H_(const crypto::ec_scalar a) {
    return H ^ a;
  }

  inv8 to_inv8(const crypto::ec_point x) {
    return x ^ s_inv_eight;
  }

  std::optional<crypto::ec_point> maybe_from_inv8(const inv8 x) {
    return preview_safe_point(x).transform(crypto::mult8_fast);
  }

  // ct
  crypto::ec_point commit
  (
   const amount_t amount
   , const crypto::ec_scalar mask
   )
  {
    return G_(mask) + H_(crypto::int_to_scalar(amount));
  }

  crypto::ec_point dummyCommit(const amount_t amount) {
    return commit(amount, s_one);
  }


  // hash
  crypto::hash hash_data(const crypto::crypto_data in) {
    return crypto::sha3(in.data);
  }

  crypto::ec_scalar hash_to_scalar(const crypto::crypto_data in) {
    return reduce(d2s(h2d(hash_data(in))));
  }

  crypto::hash hash_dataV
  (const std::span<const crypto::crypto_data> keys)
  {
    if (keys.empty()) {
      LOG_ERROR("hashing empty vector");
      return {};
    }

    const epee::blob::span hash_data =
      {
        (const uint8_t*)keys.data()
        , keys.size_bytes()
      };

    return crypto::sha3(hash_data);
  }

  crypto::ec_scalar hash_dataV_to_scalar
  (const std::span<const crypto::crypto_data> keys)
  {
    return reduce(d2s(h2d(hash_dataV(keys))));
  }

  std::optional<crypto::ec_scalar> maybe_hash_V_to_non_zero_scalar
  (const std::span<const crypto::crypto_data> keys)
  {
    const auto h = reduce(d2s(h2d(hash_dataV(keys))));
    if (h == crypto::s_0) {
      LOG_ERROR("Unluckily hashed to scalar 0");
      return {};
    } else {
      return h;
    }
  }


  // ecdh
  uint64_t hash_and_xor_int
  (
   const uint64_t x
   , const crypto::ec_scalar y
   )
  {
    const epee::blob::data hashData =
      epee::string_tools::string_to_blob(config::ecdhHashPrefix)
      + y.blob();

    const crypto::hash h = crypto::sha3(hashData);

    crypto::ec_scalar r = crypto::int_to_scalar(x);

    for (size_t i = 0; i < sizeof(uint64_t); ++i)
      r.data[i] ^= h.data[i];

    return crypto::scalar_to_int(r);
  }

  crypto::ec_scalar get_blinding_factor_from_hashed_shared_secret
  (const crypto::ec_scalar x)
  {
    const epee::blob::data hashData =
      epee::string_tools::string_to_blob(config::commitmentMaskPrefix)
      + x.blob();

    return crypto::hash_to_scalar(hashData);
  }
}
