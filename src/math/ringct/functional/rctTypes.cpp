/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/

#include "rctTypes.hpp"
#include "rctOps.hpp"

#include <ranges>

namespace rct {

  const rct::inv8V to_inv8V(const pointS xs) {
    const auto ys =
      xs
      | std::views::transform(to_inv8);

    return {ys.begin(), ys.end()};
  }


  LR_V zipLR(const pointS L, const pointS R) {
    LR_V lr;

    std::transform
      (
       L.begin()
       , L.end()
       , R.begin()
       , std::back_inserter(lr)
       , [](const auto& x, const auto& y) -> LR {
         return {x, y};
       }
       );

    return lr;
  }

  std::pair<pointV, pointV> splitLR
  (
   const std::span<const LR> xs
   )
  {
    const auto L =
      xs
      | std::views::transform
      (
       [](const auto& x) { return x.L; }
       )
      ;

    const auto R =
      xs
      | std::views::transform
      (
       [](const auto& x) { return x.R; }
       )
      ;

    return {{L.begin(), L.end()}, {R.begin(), R.end()}};
  }

}
