/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/

#pragma once

#include "math/ringct/functional/rctTypes.hpp"

namespace rct {

  struct clsag
  {
    scalarV s;
    crypto::ec_scalar c1;
    crypto::ec_point signer_key_image;
    crypto::ec_point blinding_factor_surplus_ki;

    auto operator <=> (const clsag& x) const = default;
  };


  clsag generate_clsag_signature
  (
   const crypto::hash message
   , const crypto::ec_scalar signer_sk
   , const crypto::ec_scalar signer_blinding_factor_surplus
   , const size_t index_in_decoys
   , const crypto::ec_scalar pseudo_input_blinding_factor
   , const crypto::ec_point pseudo_input_commit
   , const output_public_dataS decoys
   , const crypto::ec_scalar random_a
   , const scalarS random_s_V
   );
}
