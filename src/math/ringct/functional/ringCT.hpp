/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/

#pragma once

#include "math/ringct/functional/bulletproofs_gen.hpp"
#include "math/ringct/functional/clsag_gen.hpp"

namespace rct {

  bool verify_range_proof
  (
   const pointS output_commits
   , const Bulletproof bp
   );

  bool verify_ringct_balance
  (
   const pointS pseudo_input_commits
   , const pointS output_commits
   , const amount_t fee
   );

  bool verify_clsag_signatures
  (
   const pointS pseudo_input_commits
   , const decoysV decoys
   , const crypto::hash clsag_message
   , const std::span<const clsag> clsags
   );

  bool verify_ringct
  (
   const pointS output_commits
   , const decoysV decoys
   , const pointS pseudo_input_commits
   , const amount_t fee
   , const std::span<const amount_t> ecdh_encrypted_amounts
   , const crypto::hash tx_prefix_hash
   , const std::span<const clsag> signatures
   , const Bulletproof bulletproof
   );

  crypto::hash get_ringct_basic_and_bp_data_hash_for_clsag
  (
   const pointS output_commits
   , const decoysV decoys
   , const pointS pseudo_input_commits
   , const amount_t fee
   , const std::span<const amount_t> ecdh_encrypted_amounts
   , const crypto::hash tx_prefix_hash
   , const std::span<const clsag> signatures
   , const Bulletproof bulletproof
   );

  std::optional<std::pair<amount_t, crypto::ec_scalar>>
  decode_ringct_commitment
  (
   const pointS output_commits
   , const std::span<const amount_t> ecdh_encrypted_amounts
   , const crypto::ec_scalar ecdh_shared_secret_hashed_by_index
   , const size_t output_index
   );
}

