/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/

#include "ringCT.hpp"

#include "tools/epee/include/logging.hpp"

#include "math/ringct/functional/curveConstants.hpp"
#include "math/ringct/functional/rctOps.hpp"
#include "math/ringct/functional/clsag_verify.hpp"
#include "math/ringct/functional/ringCT.hpp"
#include "math/ringct/functional/bulletproofs_verify.hpp"
#include "math/consensus/consensus.hpp"

#include "cryptonote/basic/functional/format_utils.hpp"

#include "config/cryptonote.hpp"

#include <execution>
#include <ranges>

namespace rct {

  bool verify_ringct_balance
  (
   const pointS pseudo_input_commits
   , const pointS output_commits
   , const amount_t fee
   )
  {
    return consensus::rule_3_ringct_should_be_balanced
      (
       pseudo_input_commits
       , output_commits
       , fee
       );
  }

  bool verify_range_proof
  (
   const pointS output_commits
   , const Bulletproof bp
   )
  {
    return consensus::
      rule_2_ringct_output_amounts_should_not_overflow_amount_type
      (
       output_commits
       , bp
       );
  }

  bool verify_clsag_signatures
  (
   const pointS pseudo_input_commits
   , const decoysV decoys
   , const crypto::hash clsag_message
   , const std::span<const clsag> clsags
   )
  {
    std::vector<std::pair<output_public_dataV, crypto::ec_point>>
      sigs;

    std::transform
      (
       decoys.begin()
       , decoys.end()
       , pseudo_input_commits.begin()
       , std::back_inserter(sigs)
       , [](const auto& x, const auto& y) {
         return std::make_pair(x, y);
       }
       );

    return std::transform_reduce
      (
       std::execution::par_unseq
       , clsags.begin()
       , clsags.end()
       , sigs.begin()
       , true
       , std::logical_and()
       , [clsag_message](const auto& x, const auto& y) {
         return verify_clsag_signature
           (clsag_message, x, y.first, y.second);
       }
       );
  }

  bool verify_ringct
  (
   const pointS output_commits
   , const decoysV decoys
   , const pointS pseudo_input_commits
   , const amount_t fee
   , const std::span<const amount_t> ecdh_encrypted_amounts
   , const crypto::hash tx_prefix_hash
   , const std::span<const clsag> signatures
   , const Bulletproof bulletproof
   )
  {

    const auto clsag_message =
      get_ringct_basic_and_bp_data_hash_for_clsag
      (
       output_commits
       , decoys
       , pseudo_input_commits
       , fee
       , ecdh_encrypted_amounts
       , tx_prefix_hash
       , signatures
       , bulletproof
       );

    return
      verify_range_proof
      (
       output_commits
       , bulletproof
       )

      &&

      verify_ringct_balance
      (
       pseudo_input_commits
       , output_commits
       , fee
       )

      &&

      verify_clsag_signatures
      (
       pseudo_input_commits
       , decoys
       , clsag_message
       , signatures
       )
      ;
  }

  crypto::hash
  get_ringct_basic_and_bp_data_hash_for_clsag
  (
   const pointS output_commits
   , const decoysV decoys
   , const pointS pseudo_input_commits
   , const amount_t fee
   , const std::span<const amount_t> ecdh_encrypted_amounts
   , const crypto::hash tx_prefix_hash
   , const std::span<const clsag> signatures
   , const Bulletproof bulletproof
   )
  {
    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       !decoys.empty()
       , {}
       , "Empty decoys"
       );

    std::stringstream ss;
    binary_archive<true> ba(ss);

    const size_t inputs = pseudo_input_commits.size();
    const size_t outputs = output_commits.size();

    auto rv_unsafe = review_rctData
      (
       output_commits
       , pseudo_input_commits
       , fee
       , ecdh_encrypted_amounts
       , signatures
       , bulletproof
       );

    rv_unsafe.serialize_rctsig_base(ba, inputs, outputs);

    const crypto::hash h = cryptonote::get_blob_hash(ss.str());

    const auto& p = toUnsafeBulletproof(bulletproof);

    const crypto::dataBS kv_0 = crypto::dataBS
      {
        p.A
        , p.S
        , p.T1
        , p.T2
        , p.tau
        , p.mu
      };

    const crypto::dataBS kv =
      kv_0
      + crypto::dataBS(p.L.begin(), p.L.end())
      + crypto::dataBS(p.R.begin(), p.R.end())
      + crypto::dataBS
      {
        p.a
        , p.b
        , p.t
      };

    return hash_dataV
      (
       crypto::dataV
       {
         crypto::h2d(tx_prefix_hash)
         , h2d(h)
         , h2d(hash_dataV(kv))
       }
       );
  }

  std::optional<std::pair<amount_t, crypto::ec_scalar>>
  decode_ringct_commitment
  (
   const pointS output_commits
   , const std::span<const amount_t> ecdh_encrypted_amounts
   , const crypto::ec_scalar ecdh_shared_secret_hashed_by_index
   , const size_t output_index
   )
  {
    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       output_index < ecdh_encrypted_amounts.size()
       , {}
       , "Bad index"
       );

    const crypto::ec_scalar blinding_factor =
      rct::get_blinding_factor_from_hashed_shared_secret
      (ecdh_shared_secret_hashed_by_index);

    const uint64_t amount =
      rct::decode_amount_by_hashed_ecdh_shared_secret
      (
       ecdh_encrypted_amounts[output_index]
       , ecdh_shared_secret_hashed_by_index
       );

    const crypto::ec_point C = output_commits[output_index];

    if (C != commit(amount, blinding_factor)) {
      LOG_ERROR
        (
         "warning, amount decoded incorrectly"
         ", will be unable to spend"
         );
      return {};
    }

    return {{amount, blinding_factor}};
  }

}
