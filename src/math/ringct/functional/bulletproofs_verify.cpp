/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Adapted from C++ code from The Monero Project
Adapted from Java code by Sarang Noether
Paper references are to https://eprint.iacr.org/2017/1066
(revision 1 July 2018)

*/

#include "bulletproofs_verify.hpp"

#include "math/ringct/functional/vectorOps.hpp"
#include "math/ringct/functional/rctOps.hpp"
#include "math/ringct/functional/innerProductArgument_verify.hpp"

#include "tools/epee/include/logging.hpp"
#include "tools/epee/include/logging_macro.hpp"

namespace rct
{

  std::optional<HashChallenge> get_hash_challenges
  (const pointS commits, const Bulletproof proof)
  {
    return
      hash_V_A_S(commits, proof.A, proof.S)
      .and_then
      (
       [proof](const auto& hash_V_A_S) {
         const auto [challenge_y, challenge_z] = hash_V_A_S;

         return maybe_hash_V_to_non_zero_scalar
           (
            crypto::dataV
            {
              challenge_z
              , challenge_z
              , to_inv8(proof.T1)
              , to_inv8(proof.T2)
            }
            )
           .and_then
           (
            [proof, challenge_y, challenge_z](const auto& challenge_x)
            {
              return maybe_hash_V_to_non_zero_scalar
                (
                 crypto::dataV
                 {
                   challenge_x
                   , challenge_x
                   , proof.tau
                   , proof.mu
                   , proof.t
                 })
                .transform
                (
                 [challenge_x, challenge_y, challenge_z]
                 (const auto& inner_product_challenge) {
                   return HashChallenge
                     {
                       challenge_y
                       , challenge_z
                       , challenge_x
                       , inner_product_challenge
                     };
                 }
                 );
            }
            );
       }
       );
  }


  std::map<uint64_t, std::pair<pointV, pointV>>
  G_V_and_H_V_cache;
  std::mutex G_V_and_H_V_cache_lock;

  bool bulletproof_VERIFY
  (
   const pointS commits
   , const Bulletproof proof
   )
  {
    constexpr size_t log_bit_width =
      ceiling_log2_review(bit_width).second;

    const auto
      [
       padded_number_of_inputs
       , log_padded_number_of_inputs
       ] =
      ceiling_log2_review(commits.size());

    const size_t rounds = log_padded_number_of_inputs + log_bit_width;

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       proof.LR.size() == rounds
       , false, "Proof is not the expected size"
       );


    const auto maybe_challenges = get_hash_challenges(commits, proof);

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (maybe_challenges, false, "invalid hash challenges");

    const auto challenges = *maybe_challenges;


    // 1. check blinding terms

    const crypto::ec_point blinding_terms_commit_L =
      H_(proof.t) + G_(proof.tau);

    const size_t total_bit_width =
      padded_number_of_inputs * bit_width;

    pointV G_V, H_V;
    {
      std::scoped_lock lock(G_V_and_H_V_cache_lock);

      if (G_V_and_H_V_cache.contains(total_bit_width)) {
        std::tie(G_V, H_V) = G_V_and_H_V_cache[total_bit_width];
      }
      else {
        G_V = get_bp_generator_G_V(total_bit_width);
        H_V = get_bp_generator_H_V(total_bit_width);
        G_V_and_H_V_cache[total_bit_width] = {G_V, H_V};
      }
    }

    const auto z = challenges.V_A_S_rehash;
    const auto y = challenges.V_A_S;

    const auto delta_1 = (z - (z * z)) *
      sum_of_scalar_powers(y, total_bit_width);

    const scalarV z_powers = scalar_powers
      (z, padded_number_of_inputs + 3);

    const crypto::ec_scalar z_sum_skip_3 = sum
      (std::span(z_powers).subspan(3, padded_number_of_inputs));

    const crypto::ec_scalar delta_2 =
      z_sum_skip_3 * sum_of_scalar_powers(crypto::s_2, bit_width);

    const auto delta = delta_1 - delta_2;

    const auto coefficient_T_0 = H_(delta)
      + vector_commit
      (
       std::span(z_powers).subspan(2, commits.size())
       , commits
       );

    const auto challenge_x = challenges.V_A_S_T1_T2;

    const crypto::ec_point blinding_terms_commit_R =
      evaluate_polynomial
      (
       pointV
       {
         coefficient_T_0
         , proof.T1
         , proof.T2
       }
       , challenge_x
       );

       
    if (blinding_terms_commit_L != blinding_terms_commit_R) {
      LOG_ERROR("Invalid commits of blinding terms");
      return false;
    }


    // 2. check inner product argument
    
    const auto challenge_z = challenges.V_A_S_rehash;
    const auto challenge_y = challenges.V_A_S;

    const auto y_powers =
      scalar_powers(challenge_y, total_bit_width);

    const crypto::ec_scalar y_inv =
      crypto::multiplicative_inverse(challenge_y);

    const auto y_inv_powers =
      scalar_powers(y_inv, total_bit_width);

    const scalarV two_powers =
      scalar_powers(rct::s_two, bit_width);

    const std::vector<scalarV> z_exp_mult_two_exp_monadic =
      vector_mult_V_monadic
      (
       std::span(z_powers).subspan(2, padded_number_of_inputs)
       , two_powers
       );

    const scalarV z_exp_mult_two_exp_flatten =
      vector_concat(z_exp_mult_two_exp_monadic);

    const auto P_G =
      vector_commit
      (
       scalar_repeat(s_zero - challenge_z, total_bit_width)
       , G_V
       );

    // I = H'
    const auto I_V = vector_multP_V(y_inv_powers, H_V);

    const auto P_I_scalars =
      vector_add_V
      (
       z_exp_mult_two_exp_flatten
       , vector_mult
       ( y_powers
         , challenge_z
         )
       );

    const auto P_I = vector_commit(P_I_scalars, I_V);

    const auto u = H_(challenges.inner_product_challenge);

    const auto P =
      sum
      (
       pointV
       {
         proof.A
         , (proof.S ^ challenge_x)
         , P_G
         , P_I
         , (u ^ proof.t)
         , G_(s_zero - proof.mu)
       }
       );

    const RecursiveInnerProductArgument ipa =
      {
        P
        , G_V
        , I_V
        , proof.LR
        , proof.a
        , proof.b
        , u
      };

    const auto is_valid_ipa =
      verify_recursive_inner_product_argument
      (
       ipa
       , challenges.inner_product_challenge
       );

    if (is_valid_ipa)
      {
        return true;
      }
    else
      {
        LOG_ERROR("Verification failure");
        return false;
      }
  }

}
