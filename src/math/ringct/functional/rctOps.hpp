/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/

#pragma once

#include "rctTypes.hpp"
#include "curveConstants.hpp"

namespace rct {

  // Can't use consteval here or android will panic

  constexpr crypto::ec_scalar s_zero = crypto::s_0;
  constexpr crypto::ec_scalar s_one = crypto::s_1;
  constexpr crypto::ec_scalar s_two = crypto::s_2;
  constexpr crypto::ec_scalar s_minus_one = MINUS_ONE;
  constexpr crypto::ec_scalar s_eight = crypto::s_8;
  // inv is multiplicative inverse
  constexpr crypto::ec_scalar s_inv_eight = INV_EIGHT;
  constexpr crypto::ec_scalar s_minus_inv_eight = MINUS_INV_EIGHT;

  crypto::ec_point G_(const crypto::ec_scalar a);
  crypto::ec_point H_(const crypto::ec_scalar a);

  crypto::ec_point commit
  (
   const amount_t amount
   , const crypto::ec_scalar mask
   );

  crypto::ec_point dummyCommit(const amount_t amount);

  inv8 to_inv8(const crypto::ec_point x);
  std::optional<crypto::ec_point> maybe_from_inv8(const inv8 x);


  // hash
  crypto::hash hash_data(const crypto::crypto_data in);
  crypto::ec_scalar hash_to_scalar(const crypto::crypto_data in);

  crypto::hash hash_dataV
  (const std::span<const crypto::crypto_data> keys);

  crypto::ec_scalar hash_dataV_to_scalar
  (const std::span<const crypto::crypto_data> keys);

  std::optional<crypto::ec_scalar> maybe_hash_V_to_non_zero_scalar
  (const std::span<const crypto::crypto_data> keys);


  // ecdh
  crypto::ec_scalar get_blinding_factor_from_hashed_shared_secret
  (const crypto::ec_scalar x);

  uint64_t hash_and_xor_int
  (const uint64_t, const crypto::ec_scalar y);

  inline const auto encode_amount_by_hashed_ecdh_shared_secret =
    hash_and_xor_int;

  inline const auto decode_amount_by_hashed_ecdh_shared_secret =
    hash_and_xor_int;
}
