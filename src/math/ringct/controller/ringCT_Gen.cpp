/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/

#include "ringCT_Gen.hpp"

#include "clsag_gen.hpp"
#include "bulletproofs_gen.hpp"

#include "tools/epee/include/logging.hpp"

#include "math/ringct/functional/curveConstants.hpp"
#include "math/ringct/functional/rctOps.hpp"
#include "math/ringct/functional/vectorOps.hpp"
#include "math/ringct/functional/ringCT.hpp"
#include "math/ringct/functional/rctTypes.hpp"

#include "math/ringct/functional/bulletproofs_verify.hpp"
#include "math/ringct/functional/ringCT.hpp"

#include "math/crypto/controller/keyGen.hpp"

#include "config/cryptonote.hpp"

#include <ranges>


namespace rct {
  std::tuple<crypto::ec_scalar, pointV, Bulletproof>
  generate_range_proof
  (
   const std::span<const rctOutputData> outputs
   )
  {
    using bp_data_t =
      std::pair<const uint64_t, const crypto::ec_scalar>;

    const auto xs =
      outputs
      | std::views::transform
      (
       [](const auto& x) -> bp_data_t {
         return {
           x.amount
           , get_blinding_factor_from_hashed_shared_secret
           (x.ecdh_shared_secret_hashed_by_index)
         };
       }
       )
      ;

    const Bulletproof proof = bulletproof_MAKE
      (
       std::vector<bp_data_t>{xs.begin(), xs.end()}
       );

    const crypto::ec_scalar output_blinding_factors_sum =
      std::transform_reduce
      (
       xs.begin()
       , xs.end()
       , s_zero
       , std::plus()
       , [](const auto& x) { return x.second; }
       );

    const auto output_commits =
      xs
      | std::views::transform
      ( [](const auto& x) { return apply(commit, x); } )
      ;

    return
      {
        output_blinding_factors_sum
        , std::vector<crypto::ec_point>
        {output_commits.begin(), output_commits.end()}
        , proof
      };
  }

  std::vector<std::pair<crypto::ec_scalar, crypto::ec_point>>
  generate_matching_input_commits
  (const crypto::ec_scalar match, const std::span<const amount_t> xs)
  {
    if (xs.empty()) return {};

    scalarV bs;
    std::generate_n
      (
       std::back_inserter(bs)
       , xs.size() - 1
       , crypto::randomScalar
       );

    const crypto::ec_scalar last = match - sum(bs);
    bs.push_back(last);

    std::vector<std::pair<crypto::ec_scalar, crypto::ec_point>> r;
    std::transform
      (
       bs.begin()
       , bs.end()
       , xs.begin()
       , std::back_inserter(r)
       , [](const auto& b, const auto& x) ->
       std::pair<crypto::ec_scalar, crypto::ec_point> {
         return {b, commit(x, b)};
       }
       );

    return r;
  }

  std::tuple
  <
    pointV
    , decoysV
    , pointV
    , amount_t
    , std::vector<amount_t>
    , std::vector<clsag>
    , Bulletproof
    >
  generate_ringct
  (
   const crypto::hash tx_prefix_hash
   , const std::vector<rctInputData> inputs
   , const std::vector<rctOutputData> outputs
   , const amount_t fee
   )
  {
    for (size_t n = 0; n < inputs.size(); ++n) {
      LOG_ERROR_AND_THROW_UNLESS
        (
         inputs[n].index_in_decoys < inputs[n].decoys.size()
         , "Bad index into decoys"
         );
    }


    // 1. basic

    const auto [output_blinding_factors_sum, output_commits, proof] =
      generate_range_proof(outputs);

    const auto decoys_view =
      inputs
      | std::views::transform
      ( [](const auto& x) { return x.decoys; } )
      ;


    const decoysV decoys = { decoys_view.begin(), decoys_view.end() };

    const auto ecdh =
      outputs
      | std::views::transform
      (
       [](const auto& x) -> amount_t {
         return {
           encode_amount_by_hashed_ecdh_shared_secret
           (x.amount, x.ecdh_shared_secret_hashed_by_index)
         };
       }
       )
      ;

    // 2. prunable

    const auto input_amounts =
      inputs
      | std::views::transform
      ( [](const auto& x) { return x.amount; } )
      ;

    const std::vector<std::pair<crypto::ec_scalar, crypto::ec_point>>
      pseudo_inputs =
      generate_matching_input_commits
      (
       output_blinding_factors_sum
       , std::vector<uint64_t>
       {input_amounts.begin(), input_amounts.end()}
       );


    const auto pseudo_input_commits_view =
      std::views::elements<1>(pseudo_inputs)
      ;

    const pointV pseudo_input_commits =
      { pseudo_input_commits_view.begin()
        , pseudo_input_commits_view.end()
      };

    const std::vector<amount_t> ecdh_encrypted_amounts =
      { ecdh.begin(), ecdh.end() };

    const auto clsag_message =
      get_ringct_basic_and_bp_data_hash_for_clsag
      (
       output_commits
       , decoys
       , pseudo_input_commits
       , fee
       , ecdh_encrypted_amounts
       , tx_prefix_hash
       , {}
       , proof
       );

    std::vector<clsag> clsags;
    std::transform
      (
       inputs.begin()
       , inputs.end()
       , pseudo_inputs.begin()
       , std::back_inserter(clsags)
       , [ clsag_message ] (const auto& i, const auto& p) {
         return generate_clsag_signature
           (
            clsag_message
            , i.signer_sk
            , i.signer_blinding_factor
            , i.index_in_decoys
            , p.first
            , p.second
            , i.decoys
            );
       }
       );

    return
      {
        output_commits
        , decoys
        , pseudo_input_commits
        , fee
        , ecdh_encrypted_amounts
        , clsags
        , proof
      };
  }

}
