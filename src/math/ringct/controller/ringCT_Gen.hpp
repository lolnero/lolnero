/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/

#pragma once

#include "math/ringct/functional/bulletproofs_gen.hpp"
#include "math/ringct/functional/clsag_gen.hpp"

namespace rct {

  struct rctInputData
  {
    const amount_t amount;
    const crypto::ec_scalar signer_sk;
    const crypto::ec_scalar signer_blinding_factor;
    const size_t index_in_decoys;
    const output_public_dataV decoys;
  };

  struct rctOutputData
  {
    const amount_t amount;
    const crypto::ec_scalar ecdh_shared_secret_hashed_by_index;
  };

  std::tuple
  <
    pointV
    , decoysV
    , pointV
    , amount_t
    , std::vector<amount_t>
    , std::vector<clsag>
    , Bulletproof
    >
  generate_ringct
  (
   const crypto::hash tx_prefix_hash
   , const std::vector<rctInputData> inputs
   , const std::vector<rctOutputData> outputs
   , const amount_t fee
   );
}

