/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/


#include "keyGen.hpp"

#include "random.hpp"

#include "tools/common/varint.h"
#include "tools/epee/include/string_tools.h"
#include "tools/epee/include/logging.hpp"
#include "tools/epee/functional/int-util.hpp"

#include "config/cryptonote.hpp"

#include <sodium.h>

#include <cassert>
#include <mutex>
#include <memory>


extern "C" {
#include "crypto-ops.h"
}

namespace crypto {
  ec_scalar randomScalar() {
    ec_scalar s{};
    crypto_core_ed25519_scalar_random(s.data.data());
    return s;
  }

  std::vector<ec_scalar> randomScalars(size_t n) {
    std::vector<ec_scalar> xs;
    std::generate_n
      (
       std::back_inserter(xs)
       , n
       , randomScalar
       );
    return xs;
  }

  ec_point randomPoint() {
    ec_point x{};
    crypto_core_ed25519_random(x.data.data());
    return x;
  }

  uint64_t randomAmount() {
    return crypto::rand<uint64_t>();
  }

  crypto_data randomCryptoData() {
    crypto_data x{};
    generate_random_bytes(x.data.data(), x.data.size());
    return x;
  }
}

