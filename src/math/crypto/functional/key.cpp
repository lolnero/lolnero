/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/

#include "key.hpp"

#include "math/group/functional/curve25519_cryptonote_extension.hpp"

#include "tools/common/varint.h"
#include "tools/epee/include/string_tools.h"
#include "tools/epee/include/logging.hpp"
#include "tools/epee/functional/int-util.hpp"

#include "config/cryptonote.hpp"

#include <sodium.h>

#include <cassert>
#include <mutex>
#include <memory>

namespace crypto {

  public_key to_pk(const secret_key& sk) noexcept {
    return p2pk(multBase(sk));
  }


  ec_scalar hash_to_scalar(const std::span<const uint8_t> x) noexcept
  {
    const auto h = sha3(x);
    return reduce(h2s(h));
  }

  ec_point hash_to_point_via_field(const crypto_data k) {
    return viaFieldMult8(h2p(sha3(k.data)));
  }

  key_image derive_key_image(const secret_key sec) noexcept {
    return p2ki(hash_to_point_via_field(to_pk(sec)) ^ sec);
  }

  ecdh_shared_secret derive_tx_output_ecdh_shared_secret
  (
   const public_key pk
   , const secret_key sk
   ) noexcept
  {
    return p2ecdh_shared_secret(mult8_fast(pk) ^ sk);
  }

  ec_scalar hash_tx_output_shared_secret_to_scalar
  (
   const ecdh_shared_secret &tx_output_shared_secret
   , const size_t index
   ) noexcept
  {
    const epee::blob::data hashData =
      tx_output_shared_secret.blob()
      + epee::string_tools::string_to_blob
      (tools::get_varint_data(index));

    return hash_to_scalar(hashData);
  }

  secret_key compute_output_secret_key_from_subaddress_spend_sk
  (
   const ecdh_shared_secret &tx_output_shared_secret
   , const size_t output_index
   , const secret_key &spend_sk
   ) noexcept
  {
    const ec_scalar shared_secret_hash =
      hash_tx_output_shared_secret_to_scalar
      (
       tx_output_shared_secret
       , output_index
       );

    return s2sk(spend_sk + shared_secret_hash);
  }

  std::optional<public_key>
  compute_output_public_key_from_subaddress_spend_pk
  (
   const ecdh_shared_secret &tx_output_shared_secret
   , const size_t output_index
   , const ec_point_unsafe &unsafe_spend_public_key
   ) noexcept
  {
    const auto spend_public_key =
      preview_safe_point(unsafe_spend_public_key);

    if (!spend_public_key) return {};

    const ec_scalar shared_secret_hash =
      hash_tx_output_shared_secret_to_scalar
      (
       tx_output_shared_secret
       , output_index
       );

    return p2pk(multBase(shared_secret_hash) + *spend_public_key);
  }

  std::optional<public_key>
  compute_subaddress_spend_pk_from_output_public_key
  (
     const ecdh_shared_secret tx_output_shared_secret
   , const std::size_t output_index
   , const ec_point output_public_key
   ) noexcept
  {
    const ec_scalar shared_secret_hash =
      hash_tx_output_shared_secret_to_scalar
      (
       tx_output_shared_secret
       , output_index
       );

    if (shared_secret_hash == s_0) return {};

    return p2pk(output_public_key - multBase(shared_secret_hash));
  }

  std::optional<public_key> maybeNotNull(const public_key x)
  {
    if (x == null_pkey) {
      return {};
    } else {
      return x;
    }
  }

  bool verify_keys
  (
   const ec_scalar_unnormalized secret_key
   , const public_key public_key
   )
  {
    if (is_not_reduced(secret_key)) return false;
    return public_key == to_pk(s2sk(reduce(secret_key)));
  }

}

