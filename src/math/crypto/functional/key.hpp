/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/

#pragma once

#include "math/hash/functional/hash.hpp"
#include "math/group/functional/group.hpp"
#include "math/group/functional/group_constant.hpp"

#include <sodium.h>

#include <boost/functional/hash.hpp>

namespace crypto {
  struct secret_key: ec_scalar{
  };

  struct public_key: ec_point {
  };

  struct ecdh_shared_secret: ec_point {};

  // public key with another base P,
  // where P is hash_to_point(public key)
  struct key_image: ec_point {
    auto operator <=> (const key_image &x) const noexcept {
      return data <=> x.data;
    }
  };

  constexpr public_key null_pkey = {};
  constexpr secret_key null_skey = {};



  // hash and cryptodata
  inline const crypto_data &h2d(const hash &x) noexcept
  { return (const crypto_data&)x; }

  inline const ec_point_unsafe &h2p(const hash &x) noexcept
  { return (const ec_point&)x; }

  inline const ec_scalar_unnormalized &h2s(const hash &x) noexcept
  { return (const ec_scalar&)x; }

  inline const hash &d2h(const crypto_data &x) noexcept
  { return (const hash&)x; }


  // keys
  inline const ecdh_shared_secret &p2ecdh_shared_secret
  (const ec_point &x) noexcept
  { return (const ecdh_shared_secret&)x; }

  inline const key_image &p2ki(const ec_point &x) noexcept
  { return (const key_image&)x; }

  inline const public_key &p2pk(const ec_point &x) noexcept
  { return (const public_key&)x; }

  inline const ec_point &pk2p(const public_key &x) noexcept
  { return (const ec_point&)x; }

  inline const secret_key &s2sk(const ec_scalar &x) noexcept
  { return (const secret_key&)x; }


  public_key to_pk(const secret_key& sk) noexcept;


  ec_point hash_to_point_via_field(const crypto_data k);
  key_image derive_key_image(const secret_key) noexcept;

  ec_scalar hash_to_scalar(const std::span<const uint8_t>x) noexcept;


  ecdh_shared_secret derive_tx_output_ecdh_shared_secret
  (
   const public_key pk
   , const secret_key sk
   ) noexcept;

  ec_scalar hash_tx_output_shared_secret_to_scalar
  (
   const ecdh_shared_secret &tx_output_shared_secret
   , const size_t index
   ) noexcept;

  secret_key compute_output_secret_key_from_subaddress_spend_sk
  (
   const ecdh_shared_secret &tx_output_shared_secret
   , const size_t output_index
   , const secret_key &base
   ) noexcept;

  std::optional<public_key>
  compute_output_public_key_from_subaddress_spend_pk
  (
   const ecdh_shared_secret &tx_output_shared_secret
   , const size_t output_index
   , const ec_point_unsafe &unsafe_spend_public_key
   ) noexcept;

  std::optional<public_key>
  compute_subaddress_spend_pk_from_output_public_key
  (
   const ecdh_shared_secret tx_output_shared_secret
   , const std::size_t output_index
   , const ec_point output_public_key
   ) noexcept;

  std::optional<public_key> maybeNotNull(const public_key);

  bool verify_keys
  (
   const ec_scalar_unnormalized secret_key
   , const public_key public_key
   );
}

namespace std
{
  template<> struct hash<crypto::public_key>
  {
    std::size_t operator()(crypto::public_key const& x) const noexcept
    {
      boost::hash<std::array<uint8_t,32>> array_hash;
      return array_hash(x.data);
    }
  };

  template<> struct hash<crypto::key_image>
  {
    std::size_t operator()(crypto::key_image const& x) const noexcept
    {
      boost::hash<std::array<uint8_t,32>> array_hash;
      return array_hash(x.data);
    }
  };

}

