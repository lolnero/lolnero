/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#pragma once

#include "tools/epee/functional/hex.hpp"
#include "tools/epee/functional/base64.hpp"
#include "tools/epee/functional/blob.hpp"

#include <sodium.h>

#include <boost/functional/hash.hpp>

#include <array>
#include <vector>

namespace crypto {
  struct crypto_data {
    std::array<uint8_t, 32> data;

    inline epee::blob::data blob() const {
      return epee::blob::data(data.begin(), data.end());
    }

    std::string to_str() const;

    std::string to_base64() const;

    inline std::string to_hex() const {
      return epee::hex::encode_to_hex(data);
    }
  };

  // not really functional but needed in other part of the code
  inline std::ostream &operator <<
  (std::ostream &o, const crypto::crypto_data &v)
  {
    o << v.to_str();
    return o;
  }

  std::optional<crypto_data> preview_crypto_data_from_hex
  (const std::string_view src);

  std::optional<crypto_data> preview_crypto_data_from_base64
  (const std::string_view src);

  using dataV = std::vector<crypto_data>;
  using dataS = std::span<const crypto_data>;
  using dataBS = std::basic_string<crypto_data>;

  struct ec_point_unsafe : crypto_data {
    bool operator==(const ec_point_unsafe &x) const noexcept {
      return data == x.data;
    }

    bool operator==(const crypto_data &x) const noexcept {
      return 0 == crypto_verify_32(data.data(), x.data.data());
    }
  };

  struct ec_point_unsafe_small_order : ec_point_unsafe {};

  struct ec_scalar; // for ^
  struct ec_point : ec_point_unsafe {
    ec_point operator+(const ec_point& x) const noexcept;
    ec_point operator-(const ec_point& x) const noexcept;
    ec_point operator^(const ec_scalar& x) const noexcept;
    ec_point operator^(const uint64_t x) const noexcept;

    // for set ordering
    auto operator <=> (const ec_point& x) const noexcept {
      return data <=> x.data;
    }
  };

  struct ec_scalar_unnormalized : crypto_data {
    bool operator==(const ec_scalar_unnormalized &x) const noexcept {
      return 0 == crypto_verify_32(data.data(), x.data.data());
    }
  };

  struct ec_scalar : ec_scalar_unnormalized {
    ec_scalar operator+(const ec_scalar& x) const noexcept;
    ec_scalar operator-(const ec_scalar& x) const noexcept;
    ec_scalar operator*(const ec_scalar& x) const noexcept;
  };

  ec_point mult(const ec_scalar a, const ec_point X) noexcept;

  inline const ec_point &unsafe_p2p
  (const ec_point_unsafe &x) noexcept {
    return (const ec_point&)x;
  }


  bool is_safe_point(const ec_point_unsafe x) noexcept;
  bool is_valid_group_element(const ec_point_unsafe x) noexcept;

  std::optional<ec_point> preview_safe_point
  (const ec_point_unsafe x) noexcept;

  std::optional<crypto::ec_point> preview_point_from_hex
  (const std::string_view x);

  std::optional<crypto::ec_point> preview_point_from_base64
  (const std::string_view x);

  std::optional<ec_scalar> preview_safe_scalar
  (const ec_scalar_unnormalized x) noexcept;

  std::optional<crypto::ec_scalar> preview_scalar_from_base64
  (const std::string_view x);


  // ec_point mult8(const ec_point X) noexcept;
  ec_point multBase(const ec_scalar) noexcept;

  ec_scalar multiplicative_inverse(const ec_scalar x) noexcept;

  ec_scalar reduce(const ec_scalar_unnormalized x) noexcept;
  bool is_reduced(const ec_scalar_unnormalized x) noexcept;
  bool is_not_reduced(const ec_scalar_unnormalized x) noexcept;

  uint64_t scalar_to_int(const ec_scalar &in) noexcept;
  ec_scalar int_to_scalar(const uint64_t in) noexcept;



  inline const ec_point_unsafe &d2p(const crypto_data &x) noexcept
  { return (const ec_point_unsafe&)x; }

  inline const ec_scalar_unnormalized &d2s
  (const crypto_data &x) noexcept
  { return (const ec_scalar_unnormalized&)x; }


}


namespace std
{
  template<> struct hash<crypto::ec_point>
  {
    std::size_t operator()(const crypto::ec_point& x) const noexcept
    {
      boost::hash<std::array<uint8_t,32>> array_hash;
      return array_hash(x.data);
    }
  };
}
