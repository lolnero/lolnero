/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#pragma once

#include "group.hpp"

namespace crypto {

  inline constexpr ec_scalar s_0 = {};
  inline constexpr ec_scalar s_1 =
    {{
        1, 0, 0, 0, 0, 0, 0, 0
        , 0, 0, 0, 0, 0, 0, 0, 0
        , 0, 0, 0, 0, 0, 0, 0, 0
        , 0, 0, 0, 0, 0, 0, 0, 0
      }};

  inline constexpr ec_scalar s_2 =
    {{
        2, 0, 0, 0, 0, 0, 0, 0
        , 0, 0, 0, 0, 0, 0, 0, 0
        , 0, 0, 0, 0, 0, 0, 0, 0
        , 0, 0, 0, 0, 0, 0, 0, 0
      }};

  inline constexpr ec_scalar s_8 =
    {{
        8, 0, 0, 0, 0, 0, 0, 0
        , 0, 0, 0, 0, 0, 0, 0, 0
        , 0, 0, 0, 0, 0, 0, 0, 0
        , 0, 0, 0, 0, 0, 0, 0, 0
      }};

  inline const ec_scalar order_minus_1 = s_0 - s_1;

  inline constexpr ec_point_unsafe_small_order zero_small_order = {};

  inline constexpr ec_point_unsafe_small_order one_small_order =
    {{
        1, 0, 0, 0, 0, 0, 0, 0
        , 0, 0, 0, 0, 0, 0, 0, 0
        , 0, 0, 0, 0, 0, 0, 0, 0
        , 0, 0, 0, 0, 0, 0, 0, 0
      }};

  // pretty arbitrarily defined in libsodium
  inline constexpr ec_point identity =
    {{
        1, 0, 0, 0, 0, 0, 0, 0
        , 0, 0, 0, 0, 0, 0, 0, 0
        , 0, 0, 0, 0, 0, 0, 0, 0
        , 0, 0, 0, 0, 0, 0, 0, 0
      }};

  inline constexpr ec_point generator =
    {{
        0x58, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66
        , 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66
        , 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66
        , 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66
      }};
}
