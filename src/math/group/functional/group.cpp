/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "group.hpp"
#include "group_constant.hpp"

#include "tools/epee/include/logging.hpp"
#include "tools/epee/functional/hex.hpp"

#include <sodium.h>

extern "C" {
#include "crypto-ops.h"
}

namespace crypto {

  std::string crypto_data::to_str() const {
    return "<" + this->to_base64() + ">";
  }

  std::string crypto_data::to_base64() const {
    return epee::base64::encode_to_base64(data);
  }

  ec_point add(const ec_point X, const ec_point Y) noexcept {
    ec_point p{};
    const int r = crypto_core_ed25519_add
      (
       p.data.data()
       , X.data.data()
       , Y.data.data()
       );

    if (r != 0) {
      LOG_FATAL
        (
         "add keys not in main group: "
         + X.to_str()
         + "\t"
         + Y.to_str());
    }

    return p;
  }

  ec_point ec_point::operator+(const ec_point& x) const noexcept {
    return add(*this, x);
  }

  ec_point sub(const ec_point X, const ec_point Y) noexcept {
    ec_point p{};
    const int r = crypto_core_ed25519_sub
      (
       p.data.data()
       , X.data.data()
       , Y.data.data()
       );

    if (r != 0) {
      LOG_FATAL
        (
         "sub keys not in main group: "
         + X.to_str()
         + "\t"
         + Y.to_str()
         );

    }

    return p;
  }

  ec_point ec_point::operator-(const ec_point& x) const noexcept {
    return sub(*this, x);
  }


  ec_point mult(const ec_scalar a, const ec_point X) noexcept {
    if (a == s_0) {
      return identity;
    }

    ec_point x{};
    const int r = crypto_scalarmult_ed25519_noclamp
      (
       x.data.data()
       , a.data.data()
       , X.data.data()
       );

    if (r != 0) {
      LOG_FATAL
        (
         "mult point is not on curve: \npoint: "
         + X.to_str()
         // << "\nscalar" << a
         + "\t result: "
         + x.to_str()
         );
    }

    return x;
  }

  ec_point ec_point::operator^(const ec_scalar& x) const noexcept {
    return mult(x, *this);
  }

  ec_point ec_point::operator^(const uint64_t x) const noexcept {
    return mult(int_to_scalar(x), *this);
  }

  ec_scalar ec_scalar::operator+(const ec_scalar& x) const noexcept {
    ec_scalar s{};
    crypto_core_ed25519_scalar_add
      (
       s.data.data()
       , this->data.data()
       , x.data.data()
       );

    return s;
  }

  ec_scalar ec_scalar::operator-(const ec_scalar& x) const noexcept {
    ec_scalar s{};
    crypto_core_ed25519_scalar_sub
      (
       s.data.data()
       , this->data.data()
       , x.data.data()
       );

    return s;
  }

  ec_scalar ec_scalar::operator*(const ec_scalar& x) const noexcept {
    ec_scalar s{};
    crypto_core_ed25519_scalar_mul
      (
       s.data.data()
       , this->data.data()
       , x.data.data()
       );

    return s;
  }

  bool is_safe_point(const ec_point_unsafe x) noexcept {
    return crypto_core_ed25519_is_valid_point(x.data.data());
  }

  bool is_valid_group_element(const ec_point_unsafe x) noexcept {
    return x == identity || is_safe_point(x);
  }


  ec_point multBase(const ec_scalar x) noexcept {
    ec_point p{};
    const int r = crypto_scalarmult_ed25519_base_noclamp
      (
       p.data.data()
       , x.data.data()
       );

    if (r != 0) {
      LOG_FATAL("scalar mult base failed");
    }
    return p;
  }

  ec_point mult8(const ec_point X) noexcept {
    return mult(s_8, X);
  }

  ec_scalar multiplicative_inverse(const ec_scalar x) noexcept
  {
    ec_scalar r{};
    crypto_core_ed25519_scalar_invert
      (
       r.data.data()
       , x.data.data()
       );

    return r;
  }

  ec_scalar reduce(const ec_scalar_unnormalized x) noexcept {
    std::array<uint8_t, 64> t{};
    std::ranges::copy(x.data, t.begin());

    ec_scalar s{};
    crypto_core_ed25519_scalar_reduce(s.data.data(), t.data());
    return s;
  }

  bool is_reduced(const ec_scalar_unnormalized x) noexcept {
    return reduce(x) == x;
  }

  bool is_not_reduced(const ec_scalar_unnormalized x) noexcept {
    return !(is_reduced(x));
  }

  std::optional<ec_point> preview_safe_point
  (const ec_point_unsafe x) noexcept
  {
    if (is_safe_point(x)) {
      return unsafe_p2p(x);
    } else {
      return {};
    }
  }

  std::optional<ec_scalar> preview_safe_scalar
  (const ec_scalar_unnormalized x) noexcept
  {
    if (is_reduced(x)) {
      return reduce(x);
    } else {
      return {};
    }
  }

  ec_scalar int_to_scalar(const uint64_t in) noexcept {
    ec_scalar x = {};
    std::memcpy(x.data.data(), &in, sizeof(uint64_t));
    return x;
  }

  uint64_t scalar_to_int(const ec_scalar & in) noexcept {
    uint64_t out = 0;
    std::memcpy(&out, in.data.data(), sizeof(uint64_t));
    return out;
  }

  std::optional<crypto_data> preview_crypto_data_from_hex
  (const std::string_view src) {
    const auto maybeBlob = epee::hex::decode_from_hex_to_blob(src);

    if (!maybeBlob) return {};

    const auto blobData = *maybeBlob;
    crypto_data out{};

    if (blobData.size() != out.data.size()) return {};

    std::ranges::copy(blobData, out.data.begin());

    return out;
  }

  std::optional<crypto_data> preview_crypto_data_from_base64
  (const std::string_view src) {
    const auto maybeBlob = epee::base64::decode_from_base64_to_blob(src);

    if (!maybeBlob) return {};

    const auto blobData = *maybeBlob;
    crypto_data out{};

    if (blobData.size() != out.data.size()) return {};

    std::ranges::copy(blobData, out.data.begin());

    return out;
  }

  std::optional<crypto::ec_point> preview_point_from_hex
  (const std::string_view x)
  {
    return crypto::preview_crypto_data_from_hex(x)
      .transform([](const auto&x ) { return crypto::d2p(x); })
      .and_then(crypto::preview_safe_point);
  }

  std::optional<crypto::ec_point> preview_point_from_base64
  (const std::string_view x)
  {
    return crypto::preview_crypto_data_from_base64(x)
      .transform([](const auto&x ) { return crypto::d2p(x); })
      .and_then(crypto::preview_safe_point);
  }

  std::optional<crypto::ec_scalar> preview_scalar_from_base64
  (const std::string_view x)
  {
    return crypto::preview_crypto_data_from_base64(x)
      .transform([](const auto&x ) { return crypto::d2s(x); })
      .and_then(crypto::preview_safe_scalar);
  }


}
