/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#pragma once

#include "tools/serialization/json.hpp"

#include "math/hash/functional/hash.hpp"
#include "math/group/functional/group.hpp"
#include "math/ringct/functional/innerProductArgument_gen.hpp"
#include "math/blockchain/functional/coinbase_tx.hpp"
#include "math/blockchain/functional/ringct.hpp"
#include "math/blockchain/functional/block.hpp"
#include "math/blockchain/functional/difficulty.hpp"

namespace crypto {
  void to_json(json& j, const hash& x);
  void from_json(const json& j, hash& x);

  void to_json(json& j, const crypto_data& x);
  void from_json(const json& j, crypto_data& x);

}

namespace rct {
  JSON_LAYOUT
  (
   ecdh_encrypted_data_t
   , masked_amount
   );

  JSON_LAYOUT
  (
   clsag
   , s
   , c1
   , signer_key_image
   , blinding_factor_surplus_ki
   );

  struct clsag_view
  {
    scalarV s;
    crypto::ec_scalar c1;
    crypto::ec_point key_image;
    crypto::ec_point blinding_ki;
    crypto::ec_point pseudo_commit;
  };

  JSON_LAYOUT
  (
   clsag_view
   // , s
   // , c1
   , key_image
   , blinding_ki
   , pseudo_commit
   );

  // JSON_LAYOUT
  // (
  //  LR
  //  , L
  //  , R
  //  );

  void to_json(json& j, const LR& x);
  void from_json(const json& j, LR& x);

  JSON_LAYOUT
  (
   Bulletproof
   , A, S
   , T1, T2
   , tau, mu
   , LR
   , a, b, t
   );

  JSON_LAYOUT
  (
   InnerProductArgument
   , P
   , G
   , H
   , L
   , R
   , u
   );

  JSON_LAYOUT
  (
   RecursiveInnerProductArgument
   , P
   , G
   , H
   , LR
   , a
   , b
   , u
   );


  JSON_LAYOUT
  (
   output_commit
   , commit
   );
  // void to_json(json& j, const output_commit& x);
  // void from_json(const json& j, output_commit& x);


  JSON_LAYOUT
  (
   output_public_data
   , public_key
   , commit
   );
   
}

namespace cryptonote {

  JSON_LAYOUT
  (
   coinbase_output
   , amount
   , public_key
   );

  // JSON_LAYOUT
  // (
  //  tx_extra
  //  , data
  //  );


  void to_json(json& j, const tx_extra& x);
  void from_json(const json& j, tx_extra& x);

  void review_tx_extra_to_json(json& j, const tx_extra& x);
  // inline void from_json(const json& j, tx_extra& x){};

  void review_coinbase_tx_to_json(json& j, const coinbase_tx& x);

  // struct tx_extra
  // {
  //   std::vector<uint8_t> data;
  // };

  // struct tx_common
  // {
  //   uint64_t unlock_block_index;
  //   tx_extra extra;
  // };

  void to_json(json& j, const transaction& x);
  inline void from_json(const json& j, transaction& x) {};

  JSON_LAYOUT
  (
   coinbase_tx
   , unlock_block_index
   , extra
   , index
   , outputs
   , ecdh_tx_public_key
   );

  // struct coinbase_output
  // {
  //   uint64_t amount;
  //   crypto::public_key public_key;
  // };

  // struct coinbase_tx: tx_common
  // {
  //   uint64_t index;
  //   std::vector<coinbase_output> outputs;

  //   crypto::hash hash() const;
  // };

  JSON_LAYOUT
  (
   ringct_input
   , key_image
   , output_relative_offsets
   , pseudo_input_commit
   , signature
   );

  JSON_LAYOUT
  (
   ringct_output
   , output
   , ecdh_encrypted_amount
   , ecdh_output_public_key
   , ecdh_encrypted_payment_id
   );

  JSON_LAYOUT
  (
   ringct
   , unlock_block_index
   , extra
   , inputs
   , outputs
   , fee
   , bulletproof
   );

  void review_ringct_to_json(json& j, const ringct& x);

  JSON_LAYOUT
  (
   block_t
   , major_version
   , minor_version
   , timestamp
   , prev_hash
   , nonce
   , miner_tx
   , txs
   );

  JSON_LAYOUT
  (
   diff_pair_t
   , high
   , low
   );

}

namespace boost {
  namespace multiprecision {
    void to_json(json& j, const uint128_t& x);
    void from_json(const json& j, uint128_t& x);
  }
}
