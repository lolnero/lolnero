/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "all.hpp"

#include "tools/epee/include/string_tools.h"
#include "tools/epee/functional/base64.hpp"
#include "cryptonote/basic/functional/tx_extra.hpp"
#include "cryptonote/basic/functional/base_json.hpp"
#include "math/hash/functional/hash.hpp"

#include "boost/beast/core/detail/base64.hpp"

namespace crypto {

  void to_json(json& j, const crypto::hash & x) {
    j = epee::base64::encode_to_base64(x.data);
  }

  void from_json(const json& j, hash & x) {
    const std::string base64_str = j;

    const auto maybe_hash = preview_hash_from_base64(j);
    if (maybe_hash) {
      x = *maybe_hash;
    }
  }

  void to_json(json& j, const crypto_data& x) {
    j = epee::base64::encode_to_base64(x.data);
  }

  void from_json(const json& j, crypto_data& x) {
    const std::string base64_str = j;

    const auto maybe_crypto_data = preview_crypto_data_from_base64(j);
    if (maybe_crypto_data) {
      x = *maybe_crypto_data;
    }
  }
}

namespace rct {
  void to_json(json& j, const LR& x) {
    j = std::array<crypto::ec_point, 2>{x.L, x.R};
  }

  void from_json(const json& j, LR& x) {
    const std::array<crypto::ec_point, 2> lr = j;
    x.L = lr[0];
    x.R = lr[1];
  }
}

namespace cryptonote {

  void to_json(json& j, const tx_extra& x) {
    j = epee::base64::encode_to_base64(x.data);
  }

  void from_json(const json& j, tx_extra& x) {
    const std::string data_str = j;

    const auto maybe_data =
      epee::base64::decode_from_base64_to_blob(data_str);

    if (maybe_data) {
      x.data = {maybe_data->begin(), maybe_data->end()};
    }
  }

  void review_tx_extra_to_json(json& j, const tx_extra& x) {
    const auto maybe_output_ecdh_pks =
      get_output_ecdh_public_keys_from_extra(x.data);

    if (maybe_output_ecdh_pks) {
      j["output_ecdh_pks"] = *maybe_output_ecdh_pks;
    } else {
      const auto maybe_tx_ecdh_pk =
        get_tx_ecdh_public_key_from_extra(x.data);

      if (maybe_tx_ecdh_pk) {
        j["tx_ecdh_pk"] = *maybe_tx_ecdh_pk;
      } else {
        j["raw"] = epee::hex::encode_to_hex(x.data);
      }
    }
  }

  void review_coinbase_tx_to_json(json& j, const coinbase_tx& x) {
    j["_type"] = "Coinbase";
    j["outputs"] = x.outputs;
    // j["unlock_block_index"] = x.unlock_block_index;
    // json json_extra;
    // review_tx_extra_to_json(json_extra, x.extra);
    j["ecdh_tx_public_key"] = x.ecdh_tx_public_key;
    // j["extra"] = json_extra;
  }

  void review_ringct_to_json(json& j, const ringct& x) {
    j["_type"] = "RingCT";

    const auto signatures = x.get_signatures();

    std::vector<rct::clsag_view> sigs;
    std::transform
      (
       signatures.begin()
       , signatures.end()
       , x.inputs.begin()
       , std::back_inserter(sigs)
       , [](const auto& x, const auto& y) -> rct::clsag_view {
         return
           {
             x.s
             , x.c1
             , x.signer_key_image
             , x.blinding_factor_surplus_ki
             , y.pseudo_input_commit
           };
       }
       );

    j["inputs"] = sigs;

    j["outputs"] = x.outputs;
    j["fee"] = x.fee;
    // j["range_proof"] = x.bulletproof;
    // json json_extra;
    // review_tx_extra_to_json(json_extra, x.extra);
    // j["extra"] = json_extra;
  }

  void to_json(json& j, const transaction& x) {
    const auto maybe_coinbase =
      preview_coinbase_tx(x);

    if (maybe_coinbase) {
      review_coinbase_tx_to_json(j, *maybe_coinbase);
      return;
    }

    const auto maybe_ringct =
      preview_ringct(x);

    if (maybe_ringct) {
      review_ringct_to_json(j, *maybe_ringct);
      return;
    }
  }

}

namespace boost {
  namespace multiprecision {
    void to_json(json& j, const uint128_t& x) {
      j = cryptonote::diff_to_pair(x);
  }
   
  void from_json(const json& j, uint128_t& x) {
    const cryptonote::diff_pair_t diff = j;
    x = cryptonote::pair_to_diff(diff);
  }
}
}
