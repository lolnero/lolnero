/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#pragma once

#include "math/hash/pseudo_functional/sha3.hpp"

#include "tools/epee/functional/hex.hpp"
#include "tools/epee/functional/base64.hpp"

#include <boost/functional/hash.hpp>

constexpr size_t HASH_SIZE = 32;

namespace crypto {

  struct hash {
    std::array<uint8_t, HASH_SIZE> data;
    bool operator==(const hash&) const = default;

    inline epee::blob::data blob() const {
      return epee::blob::data(data.begin(), data.end());
    }

    std::string to_base64() const;
    std::string to_hex() const;
    std::string to_str() const;

    auto operator <=> (const hash& x) const noexcept {
      return data <=> x.data;
    }
  };

  inline std::ostream &operator <<
  (std::ostream &o, const crypto::hash &v)
  {
    o << v.to_str();
    return o;
  }

  struct hash8 {
    std::array<uint8_t, 8> data;
  };

  inline std::ostream &operator <<
  (std::ostream &o, const crypto::hash8 &v)
  {
    o << epee::hex::encode_to_hex_formatted(v.data);
    return o;
  }

  constexpr crypto::hash null_hash = {};
  constexpr crypto::hash8 null_hash8 = {};

  hash sha3(const epee::blob::span) noexcept;

  std::optional<crypto::hash> preview_hash_from_hex
  (const std::string_view x);

  std::optional<crypto::hash> preview_hash_from_base64
  (const std::string_view x);
}

namespace std
{
  template<> struct hash<crypto::hash>
  {
    std::size_t operator()(crypto::hash const& x) const noexcept
    {
      boost::hash<std::array<uint8_t, HASH_SIZE>> array_hash;
      return array_hash(x.data);
    }
  };
}
