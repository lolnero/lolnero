/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "hash.hpp"
#include "tools/epee/include/logging.hpp"

namespace crypto {

  std::string hash::to_str() const {
    return "<" + this->to_base64() + ">";
  }

  std::string hash::to_hex() const {
    return epee::hex::encode_to_hex(data);
  }

  std::string hash::to_base64() const {
    return epee::base64::encode_to_base64(data);
  }

  hash sha3(const epee::blob::span x) noexcept {
    hash h;
    sha3_raw(x.data(), x.size(), h.data.data());
    return h;
  }


  std::optional<crypto::hash> preview_hash_from_hex
  (const std::string_view x)
  {
    return
      epee::hex::decode_from_hex_to_blob(x)
      .and_then
      (
       [x](const auto& buf) -> std::optional<crypto::hash> {
         crypto::hash out;

         if (buf.size() != out.data.size())
           {
             LOG_TRACE("invalid hash format: " + std::string(x));
             return {};
           }

         std::ranges::copy(buf, out.data.begin());
         return out;
       }
       );
  }

  std::optional<crypto::hash> preview_hash_from_base64
  (const std::string_view x)
  {
    return
      epee::base64::decode_from_base64_to_blob(x)
      .and_then
      (
       [x](const auto& buf) -> std::optional<crypto::hash> {
         crypto::hash out;

         if (buf.size() != out.data.size())
           {
             LOG_TRACE("invalid hash format: " + std::string(x));
             return {};
           }

         std::ranges::copy(buf, out.data.begin());
         return out;
       }
       );
  }
}


