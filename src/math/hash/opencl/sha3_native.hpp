
/*

Copyright (c) 2020-2021, The Lolnero Project

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#pragma once

#include <cstdint>
#include <array>
#include <optional>

#include "math/hash/functional/hash.hpp"

#include "cl_version.hpp"

namespace opencl
{
  struct sha3_ctx_t {
    union {           // state:
      uint8_t b[200]; // 8-bit bytes
      uint64_t q[25]; // 64-bit words
    } st;
    size_t pt;
  };

  constexpr uint8_t KECCAKF_ROUNDS = 24;

  constexpr size_t c_mdlen = 32;
  constexpr size_t c_rsiz = 136; // (200 - 2 * c_mdlen)

// constants
  constexpr uint64_t keccakf_rndc[24] = {
    0x0000000000000001, 0x0000000000008082, 0x800000000000808a,
    0x8000000080008000, 0x000000000000808b, 0x0000000080000001,
    0x8000000080008081, 0x8000000000008009, 0x000000000000008a,
    0x0000000000000088, 0x0000000080008009, 0x000000008000000a,
    0x000000008000808b, 0x800000000000008b, 0x8000000000008089,
    0x8000000000008003, 0x8000000000008002, 0x8000000000000080,
    0x000000000000800a, 0x800000008000000a, 0x8000000080008081,
    0x8000000000008080, 0x0000000080000001, 0x8000000080008008};

  constexpr uint8_t keccakf_rotc[24] =
    {
      1,  3,  6,  10, 15, 21, 28, 36, 45, 55, 2,  14,
      27, 41, 56, 8,  25, 43, 62, 18, 39, 61, 20, 44
    };

  constexpr uint8_t keccakf_piln[24] =
    {
      10, 7,  11, 17, 18, 3, 5,  16, 8,  21, 24, 4,
      15, 23, 19, 13, 12, 2, 20, 14, 22, 9,  6,  1
    };

  void sha3_keccakf(uint64_t st[25]);

  void sha3_init(sha3_ctx_t *c);
  void sha3_update(sha3_ctx_t* c, const uint8_t* data, size_t len);
}

