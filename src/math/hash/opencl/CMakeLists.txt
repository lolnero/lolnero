add_library(opencl-sha3
  error.cpp
  sha3.cpp
  sha3_native.cpp
  )

target_link_libraries(opencl-sha3
  OpenCL::OpenCL
  math_hash
  )

install(TARGETS opencl-sha3)

# configure_file(opencl/kernel/sha3.cl kernel COPYONLY)


