// Copyright (c) 2021, The Lolnero Project
// Copyright (c) 2014-2020, The Monero Project
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this list of
//    conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this list
//    of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may be
//    used to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
// THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
// THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Parts of this file are originally copyright (c) 2012-2013 The Cryptonote developers

#pragma once

#include "wallet/logic/type/transfer.hpp" // tranfser_details
#include "wallet/logic/type/typedef.hpp"
#include "wallet/logic/type/tx.hpp"
#include "wallet/logic/type/wallet.hpp"

# include "network/rpc/core_rpc_server_commands_defs.hpp" // COMMAND_RPC_SEND_RAW_TX, backlog_entry

#include "cryptonote/basic/functional/account.hpp"
#include "math/blockchain/functional/tx_check.hpp"


using namespace wallet::logic::type::transfer;

namespace wallet {
namespace logic {
namespace functional {
namespace wallet {

  size_t get_num_outputs
  (
   const std::vector<cryptonote::tx_destination_entry> &dsts
   , const type::wallet::received_tx_details_container_span transfers
   , const std::vector<size_t> &selected_txs
   );

  std::string get_text_reason(const cryptonote::COMMAND_RPC_SEND_RAW_TX::response &res);

  std::string get_weight_string(const size_t weight);

  std::string get_weight_string(const cryptonote::transaction &tx, const size_t blob_size);

  constexpr uint32_t get_subaddress_clamped_sum(const uint32_t idx, const uint32_t extra)
  {
    constexpr uint32_t uint32_max = std::numeric_limits<uint32_t>::max();
    if (idx > uint32_max - extra)
      return uint32_max;
    return idx + extra;
  }

  float get_output_relatedness(const received_tx_details& td0, const received_tx_details& td1);

  type::tx::tx_scan_info_t scan_output_for_subaddresses
  (
   const cryptonote::tx_out o
   , const crypto::ecdh_shared_secret tx_output_shared_secret
   , const size_t i
   , const std::map<crypto::public_key, cryptonote::subaddress_index>& m_subaddresses
   );

  type::tx::tx_scan_info_t scan_output
  (
   const cryptonote::transaction &tx
   , const bool miner_tx
   , const size_t i
   , const type::tx::tx_scan_info_t tx_scan_info_in
   , const std::span<size_t> &outs
   , const cryptonote::account_keys keys
   );

  std::vector<size_t> pick_preferred_rct_inputs
  (
   const uint64_t needed_money
   , const uint32_t subaddr_account
   , const std::set<uint32_t> &subaddr_indices
   , const uint64_t current_height
   , const type::wallet::received_tx_details_container_span m_received_txs
   );

  std::pair<type::tx::pending_sent_tx, cryptonote::transaction> prepare_selected_txs
  (
   const std::vector<cryptonote::tx_destination_entry> dsts
   , const std::vector<size_t> selected_txs
   , const size_t decoy_size
   , const std::span<const std::vector<type::get_tx_outputs_entry>> outs
   , const uint64_t fee
   , const std::vector<uint8_t> extra
   , const type::wallet::received_tx_details_container_span m_received_txs
   , const cryptonote::account_keys account_keys
   , const std::map<crypto::public_key, cryptonote::subaddress_index>& m_subaddresses
   , const cryptonote::network_type m_nettype
   );

  std::map<uint32_t, uint64_t> balance_per_subaddress
  (
   const uint32_t subaddr_index_major
   , const type::wallet::received_tx_details_container_span m_received_txs
   , const std::map<crypto::hash, type::transfer::un_mined_sent_tx_details> m_pending_sent_txs
   );

  std::map<uint32_t, std::pair<uint64_t, std::pair<uint64_t, uint64_t>>> unlocked_balance_per_subaddress
  (
   const uint32_t subaddr_index_major
   , const type::wallet::received_tx_details_container_span m_received_txs
   , const uint64_t blockchain_height
   );

  un_mined_sent_tx_details get_un_mined_sent_tx_details
  (
   const cryptonote::transaction& tx
   , const uint64_t amount_in
   , const std::vector<cryptonote::tx_destination_entry> &dests
   , const uint64_t change_amount
   , const uint32_t subaddr_account
   , const std::set<uint32_t>& subaddr_indices
   );

} // wallet
} // functional
} // logic
} // wallet
