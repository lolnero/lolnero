// Copyright (c) 2021, The Lolnero Project
// Copyright (c) 2014-2020, The Monero Project
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this list of
//    conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this list
//    of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may be
//    used to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
// THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
// THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Parts of this file are originally copyright (c) 2012-2013 The Cryptonote developers

#pragma once


#include "cryptonote/tx/pseudo_functional/tx_utils.hpp" // keypair

namespace wallet {
namespace logic {
namespace type {
namespace transfer {

  struct received_tx_details
  {
    uint64_t m_block_index;
    cryptonote::transaction m_tx;
    crypto::hash m_txid;
    size_t m_internal_output_index;
    crypto::public_key m_output_public_key;
    std::optional<uint64_t> m_spent_block_index;
    crypto::key_image m_output_key_image; //TODO: output_key_image stored twice :(
    crypto::ec_scalar m_mask;
    uint64_t m_amount;
    size_t m_pk_index;
    cryptonote::subaddress_index m_subaddr_index;

    uint64_t amount() const { return m_amount; }

    const crypto::public_key &get_public_key() const
    {
      return boost::get<const cryptonote::txout_to_key>
        (m_tx.vout[m_internal_output_index].target).output_public_key;
    }

    const bool is_spent() const { return m_spent_block_index.has_value(); };
  };

  struct un_mined_sent_tx_details
  {
    cryptonote::transaction m_tx;
    uint64_t m_amount_in;
    uint64_t m_amount_out;
    uint64_t m_change;
    time_t m_sent_time;
    std::vector<cryptonote::tx_destination_entry> m_dests;
    crypto::hash d_payment_id;
    enum { pending, failed } m_state;
    uint64_t m_timestamp;
    uint32_t m_subaddr_account;   // subaddress account of your wallet to be used in this transfer
    std::set<uint32_t> m_subaddr_indices;  // set of address indices used as inputs in this transfer
    std::vector<std::pair<crypto::key_image, std::vector<uint64_t>>> m_rings; // relative
  };

  struct mined_sent_tx_details
  {
    uint64_t m_amount_in;
    uint64_t m_amount_out;
    uint64_t m_change;
    uint64_t m_block_index;
    std::vector<cryptonote::tx_destination_entry> m_dests;
    crypto::hash d_payment_id;
    uint64_t m_timestamp;
    uint32_t m_subaddr_account;   // subaddress account of your wallet to be used in this transfer
    std::set<uint32_t> m_subaddr_indices;  // set of address indices used as inputs in this transfer
    std::vector<std::pair<crypto::key_image, std::vector<uint64_t>>> m_rings; // relative

    mined_sent_tx_details(): m_amount_in(0), m_amount_out(0), m_change((uint64_t)-1), m_block_index(0), d_payment_id(crypto::null_hash), m_timestamp(0), m_subaddr_account((uint32_t)-1) {}
    mined_sent_tx_details(const un_mined_sent_tx_details &utd, uint64_t block_index):
      m_amount_in(utd.m_amount_in), m_amount_out(utd.m_amount_out), m_change(utd.m_change), m_block_index(block_index), m_dests(utd.m_dests), d_payment_id(crypto::null_hash), m_timestamp(utd.m_timestamp), m_subaddr_account(utd.m_subaddr_account), m_subaddr_indices(utd.m_subaddr_indices), m_rings(utd.m_rings) {}
  };

} // transfer
} // type
} // logic
} // wallet
