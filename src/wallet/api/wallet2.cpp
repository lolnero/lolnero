/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/

#include "wallet2.h"

#include "wallet/logic/functional/fee.hpp"
#include "wallet/logic/functional/wallet.hpp"
#include "wallet/logic/controller/wallet.hpp"

#include "math/blockchain/functional/subaddress.hpp"

#include "network/rpc/core_rpc_server_error_codes.h"

#include "math/ringct/functional/ringCT.hpp"
#include "math/crypto/controller/random.hpp"
#include "math/blockchain/functional/short_chain_history.hpp"

#include "math/blockchain/functional/tx_check.hpp"
#include "cryptonote/basic/functional/tx_extra.hpp"


#include "tools/common/apply_permutation.h"
#include "tools/common/util.h"


#include <boost/format.hpp>
#include <boost/exception/to_string.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string.hpp>

#include <fstream>
#include <filesystem>
#include <execution>


using namespace cryptonote;
using namespace wallet::logic::functional::fee;

namespace tools
{

wallet2::wallet2
(
 const network_type nettype
 , const std::string daemon_host
 , const uint16_t daemon_port
 , const crypto::secret_key recovery_key
 , const uint64_t scan_from_block_index
 , const uint32_t max_account
 , const uint32_t max_address
 ):
  m_nettype(nettype)
  , m_account(recovery_key)
  , m_rpc_client{daemon_host, daemon_port}
{
  LOG_INFO
    (
     std::string()
     + "Using daemon "
     + daemon_host
     + ":"
     + std::to_string(daemon_port)
     );

  m_spend_view_public_keys =
    m_account.get_keys().get_spend_view_public_keys();

  m_scan_from_block_index = scan_from_block_index;

  m_subaddress_lookahead_major = max_account;
  m_subaddress_lookahead_minor = max_address;

  m_blockchain_hashes.push_back
    (
     lol_genesis_block().hash()
     );
  add_subaddress_account("");
}

//-----------------------------------------------------------
cryptonote::spend_view_public_keys wallet2::get_subaddress
(const cryptonote::subaddress_index& index) const
{
  return cryptonote::get_subaddress
    (m_account.get_keys().get_spend_view_secret_keys(), index);
}

//-----------------------------------------------------------
std::optional<cryptonote::subaddress_index>
wallet2::get_subaddress_index
(const cryptonote::spend_view_public_keys& address) const
{
  auto index = m_subaddresses.find(address.m_spend_public_key);
  if (index == m_subaddresses.end())
    return std::nullopt;
  return index->second;
}

//-----------------------------------------------------------
crypto::public_key wallet2::get_subaddress_spend_public_key
(const cryptonote::subaddress_index& index) const
{
  return cryptonote::get_subaddress_spend_public_key
    (m_account.get_keys().get_spend_view_secret_keys(), index);
}

//-----------------------------------------------------------
std::string wallet2::get_subaddress_as_str
(const cryptonote::subaddress_index& index) const
{
  cryptonote::spend_view_public_keys address = get_subaddress(index);
  return cryptonote::get_account_address_as_str
    (m_nettype, !index.is_zero(), address);
}

//-----------------------------------------------------------
void wallet2::add_subaddress_account(const std::string& label)
{
  uint32_t index_major = (uint32_t)get_num_subaddress_accounts();
  expand_subaddresses({index_major, 0});
  m_subaddress_labels[index_major][0] = label;
}

//-----------------------------------------------------------
void wallet2::add_subaddress
(uint32_t index_major, const std::string& label)
{
  THROW_WALLET_EXCEPTION_IF
    (
     index_major >= m_subaddress_labels.size()
     , error::account_index_outofbound
     );

  uint32_t index_minor = (uint32_t)get_num_subaddresses(index_major);
  expand_subaddresses({index_major, index_minor});
  m_subaddress_labels[index_major][index_minor] = label;
}

//-----------------------------------------------------------
bool wallet2::should_expand
(const cryptonote::subaddress_index &index) const
{
  const uint32_t last_major =
    m_subaddress_labels.size() - 1
    > (
       std::numeric_limits<uint32_t>::max()
       - m_subaddress_lookahead_major
       )
    ? std::numeric_limits<uint32_t>::max()
    : (m_subaddress_labels.size() + m_subaddress_lookahead_major - 1)
    ;

  if (index.major > last_major)
    return false;

  const size_t nsub =
    index.major < m_subaddress_labels.size()
    ? m_subaddress_labels[index.major].size()
    : 0
    ;

  const uint32_t last_minor =
    nsub - 1 >
    (
     std::numeric_limits<uint32_t>::max()
     - m_subaddress_lookahead_minor
     )
    ? std::numeric_limits<uint32_t>::max()
    : (nsub + m_subaddress_lookahead_minor - 1)
    ;

  if (index.minor > last_minor)
    return false;
  return true;
}

//-----------------------------------------------------------
void wallet2::expand_subaddresses
(const cryptonote::subaddress_index& index)
{
  if (m_subaddress_labels.size() <= index.major)
  {
    // add new accounts
    cryptonote::subaddress_index index2;
    const uint32_t major_end =
      wallet::logic::functional::wallet::get_subaddress_clamped_sum
      (index.major, m_subaddress_lookahead_major);

    for
      (
       index2.major = m_subaddress_labels.size()
         ; index2.major < major_end
         ; ++index2.major
       )
    {
      const uint32_t end =
        wallet::logic::functional::wallet::get_subaddress_clamped_sum
        (
         (index2.major == index.major ? index.minor : 0)
         , m_subaddress_lookahead_minor
         );

      const std::vector<crypto::public_key> pkeys =
        cryptonote::get_subaddress_spend_public_keys
        (
         m_account.get_keys().get_spend_view_secret_keys()
         , index2.major
         , 0
         , end
         );

      for (index2.minor = 0; index2.minor < end; ++index2.minor)
      {
         const crypto::public_key &D = pkeys[index2.minor];
         m_subaddresses[D] = index2;
      }
    }
    m_subaddress_labels.resize(index.major + 1);
    m_subaddress_labels[index.major].resize(index.minor + 1);
  }
  else if (m_subaddress_labels[index.major].size() <= index.minor)
  {
    // add new subaddresses
    const uint32_t end =
      wallet::logic::functional::wallet::get_subaddress_clamped_sum
      (index.minor, m_subaddress_lookahead_minor);

    const uint32_t begin = m_subaddress_labels[index.major].size();
    cryptonote::subaddress_index index2 = {index.major, begin};

    const std::vector<crypto::public_key> pkeys =
      cryptonote::get_subaddress_spend_public_keys
      (
       m_account.get_keys().get_spend_view_secret_keys()
       , index2.major
       , index2.minor
       , end
       );

    for (; index2.minor < end; ++index2.minor)
    {
       const crypto::public_key &D = pkeys[index2.minor - begin];
       m_subaddresses[D] = index2;
    }
    m_subaddress_labels[index.major].resize(index.minor + 1);
  }
}

//-----------------------------------------------------------
void wallet2::create_one_off_subaddress
(const cryptonote::subaddress_index& index)
{
  const crypto::public_key pkey =
    get_subaddress_spend_public_key(index);

  m_subaddresses[pkey] = index;
}

//-----------------------------------------------------------
void wallet2::set_subaddress_lookahead(size_t major, size_t minor)
{
  THROW_WALLET_EXCEPTION_IF
    (
     major == 0
     , error::wallet_internal_error
     , "Subaddress major lookahead may not be zero"
     );

  THROW_WALLET_EXCEPTION_IF
    (
     major > 0xffffffff
     , error::wallet_internal_error
     , "Subaddress major lookahead is too large"
     );

  THROW_WALLET_EXCEPTION_IF
    (
     minor == 0
     , error::wallet_internal_error
     , "Subaddress minor lookahead may not be zero"
     );

  THROW_WALLET_EXCEPTION_IF
    (
     minor > 0xffffffff
     , error::wallet_internal_error
     , "Subaddress minor lookahead is too large"
     );

  m_subaddress_lookahead_major = major;
  m_subaddress_lookahead_minor = minor;
}

//-----------------------------------------------------------
void wallet2::process_new_transaction
(
 const crypto::hash &txid
 , const cryptonote::transaction& tx
 , const uint64_t block_index
 , const uint8_t block_version
 , const uint64_t ts
 , const bool miner_tx
 )
{

  // In this function, tx (probably) only contains the base
  // information (that is, the prunable stuff may or may not be
  // included)
  if (!miner_tx)
    finish_pending_sent_txs(txid, tx, block_index);

  // per receiving subaddress index
  std::map<cryptonote::subaddress_index, uint64_t>
    tx_money_got_in_outs;

  std::map<cryptonote::subaddress_index, amounts_container>
    tx_amounts_individual_outs;

  // Don't try to extract tx public key if tx has no ouputs
  uint64_t total_received_1 = 0;

  const cryptonote::account_keys& keys = m_account.get_keys();

  if (!tx.vout.empty())
    {
      std::vector<size_t> outs;

      std::map<size_t, crypto::ecdh_shared_secret>
        tx_output_shared_secrets;

      const auto output_ecdh_public_keys
        = get_all_output_ecdh_public_keys_from_extra
        (tx.extra, tx.vout.size());

      if (output_ecdh_public_keys)
        {
          for (size_t i = 0; i < output_ecdh_public_keys->size(); ++i)
            {
              tx_output_shared_secrets[i] =
                crypto::derive_tx_output_ecdh_shared_secret
                (
                 output_ecdh_public_keys->at(i)
                 , keys.m_view_secret_key
                 );
            }
        }

      int num_vouts_received = 0;
      std::vector<tx_scan_info_t> tx_scan_info;
      for (size_t i = 0; i < tx.vout.size(); ++i)
        {
          const auto secret
            = tx_output_shared_secrets.contains(i)
            ? tx_output_shared_secrets.at(i)
            : std::optional<crypto::ecdh_shared_secret>();

          if (!secret) continue;

          const tx_scan_info_t check_info =
            wallet::logic::functional::wallet
            ::scan_output_for_subaddresses
            (tx.vout[i], *secret, i, m_subaddresses);

          THROW_WALLET_EXCEPTION_IF
            (
             check_info.error
             , error::acc_outs_lookup_error
             , tx
             , crypto::null_pkey
             , m_account.get_keys()
             );

          if (check_info.received)
            {
              const tx_scan_info_t scan_info =
                wallet::logic::functional::wallet::scan_output
                (
                 tx
                 , miner_tx
                 , i
                 , check_info
                 , outs
                 , m_account.get_keys()
                 );

              if (!scan_info.error)
                {
                  num_vouts_received++;
                  outs.push_back(i);

                  THROW_WALLET_EXCEPTION_IF
                    (
                     tx_money_got_in_outs[scan_info.received->index]
                     >= std::numeric_limits<uint64_t>::max()
                     - scan_info.money_transfered
                     , error::wallet_internal_error
                     , "Overflow in received amounts"
                     );

                  tx_money_got_in_outs[scan_info.received->index]
                    += scan_info.money_transfered;

                  tx_amounts_individual_outs
                    [scan_info.received->index]
                    .push_back(scan_info.money_transfered);

                  tx_scan_info.push_back(scan_info);
                }
            }
          else {
            tx_scan_info.push_back(check_info);
          }
        }

      if(!outs.empty() && num_vouts_received > 0)
        {
          //good news - got money! take care about it
          //usually we have only one transfer for user in transaction
          for(size_t o: outs)
            {
              THROW_WALLET_EXCEPTION_IF
                (
                 tx.vout.size() <= o
                 , error::wallet_internal_error
                 , "wrong out in transaction: internal index="
                 + std::to_string(o)
                 + ", total_outs="
                 + std::to_string(tx.vout.size())
                 );

              const auto kit =
                m_received_txs_map_by_output_pk.find
                (tx_scan_info[o].output_key_pair.pub);

              if (kit != m_received_txs_map_by_output_pk.end()) {
                // already processed
                continue;
              }

              const uint64_t amount =
                tx.vout[o].amount
                ? tx.vout[o].amount
                : tx_scan_info[o].amount;

              received_tx_details td;
              td.m_block_index = block_index;
              td.m_internal_output_index = o;
              td.m_output_public_key =
                tx_scan_info[o].output_key_pair.pub;

              td.m_tx = tx;
              td.m_txid = txid;
              td.m_output_key_image = tx_scan_info[o].ki;
              td.m_amount = amount;
              td.m_pk_index = 0;
              td.m_subaddr_index = tx_scan_info[o].received->index;

              if (should_expand(tx_scan_info[o].received->index))
                expand_subaddresses(tx_scan_info[o].received->index);

              if (tx.vout[o].amount == 0)
                {
                  td.m_mask = tx_scan_info[o].mask;
                }
              else if (miner_tx)
                {
                  td.m_mask = rct::s_one;
                }
              else
                {
                  THROW_WALLET_EXCEPTION
                    (
                     error::wallet_internal_error
                     , "non rct tx isn't allowed"
                     );
                }

              m_received_txs.push_back(td);

              m_received_txs_map_by_output_key_image
                [td.m_output_key_image] =
                m_received_txs.size()-1;

              m_received_txs_map_by_output_pk
                [tx_scan_info[o].output_key_pair.pub] =
                m_received_txs.size()-1;

              LOG_CATEGORY_COLOR
                (
                 epee::LogLevel::Info
                 // , epee::GLOBAL_CATEGORY
                 , DEFAULT_CAT
                 , epee::green
                 , "Received money: "
                 + print_money(td.amount())
                 + ", with tx: "
                 + txid.to_str()
                 );

              if (0 != m_callback)
                m_callback->on_money_received
                  (
                   block_index
                   , txid
                   , tx
                   , td.m_amount
                   , td.m_subaddr_index
                   );

              total_received_1 += amount;
            }
        }
    }

  THROW_WALLET_EXCEPTION_IF
    (
     tx_money_got_in_outs.size()
     != tx_amounts_individual_outs.size()
     , error::wallet_internal_error
     , "Inconsistent size of output arrays"
     );

  uint64_t tx_money_spent_in_ins = 0;
  std::optional<uint32_t> subaddr_account;
  std::set<uint32_t> subaddr_indices;
  // check all outputs for spending (compare key images)
  for(auto& in: tx.vin)
    {
      if(in.type() != typeid(cryptonote::txin_from_key))
        continue;

      const cryptonote::txin_from_key in_to_key =
        boost::get<cryptonote::txin_from_key>(in);

      const auto it = m_received_txs_map_by_output_key_image.find
        (in_to_key.output_key_image);

      if(it != m_received_txs_map_by_output_key_image.end())
        {
          received_tx_details& td = m_received_txs[it->second];

          const auto amount = td.amount();
          tx_money_spent_in_ins += amount;

          if
            (
             subaddr_account
             && *subaddr_account != td.m_subaddr_index.major
             ) {
            LOG_ERROR
              (
               "spent funds are from different subaddress accounts;"
               "count of incoming/outgoing payments will be incorrect"
               );
          }

          subaddr_account = td.m_subaddr_index.major;
          subaddr_indices.insert(td.m_subaddr_index.minor);

          LOG_CATEGORY_COLOR
            (
             epee::LogLevel::Info
             , DEFAULT_CAT
             , epee::magenta
             , "Spent money: "
             + print_money(amount)
             + ", with tx: "
             + txid.to_str()
             );

          m_received_txs[it->second].m_spent_block_index = block_index;

          if (0 != m_callback) {
            m_callback->on_money_spent
              (block_index, txid, tx, amount, tx, td.m_subaddr_index);
          }
        }
    }

  const uint64_t fee = miner_tx ? 0 : tx.ringct.fee;

  if (tx_money_spent_in_ins > 0)
    {

      uint64_t self_received =
        std::accumulate
        <decltype(tx_money_got_in_outs.begin()), uint64_t>
        (
         tx_money_got_in_outs.begin()
         , tx_money_got_in_outs.end()
         , 0
         , [&subaddr_account]
         (
          uint64_t acc
          , const auto& p
          )
         {
           return acc
             + (p.first.major == *subaddr_account ? p.second : 0);
         }
         );

      process_outgoing
        (
         txid
         , tx
         , block_index
         , ts
         , tx_money_spent_in_ins
         , self_received
         , *subaddr_account
         , subaddr_indices
         );

      // if sending to yourself at the same subaddress account, set
      // the outgoing payment amount to 0 so that it's less
      // confusing
      if (tx_money_spent_in_ins == self_received + fee)
        {
          auto i = m_sent_txs.find(txid);
          THROW_WALLET_EXCEPTION_IF
            (
             i == m_sent_txs.end()
             , error::wallet_internal_error
             , "mined tx wasn't found: "
             + epee::string_tools::pod_to_hex(txid)
             );

          i->second.m_change = self_received;
        }
    }

  // remove change sent to the spending subaddress account from the
  // list of received funds
  uint64_t sub_change = 0;
  for
    (
     auto i = tx_money_got_in_outs.begin()
       ; i != tx_money_got_in_outs.end()
       ;
     )
    {
      if (subaddr_account && i->first.major == *subaddr_account)
        {
          sub_change += i->second;
          tx_amounts_individual_outs.erase(i->first);
          i = tx_money_got_in_outs.erase(i);
        }
      else
        ++i;
    }

  // create received_tx_details_2  for each incoming transfer to a
  // subaddress index
  if (tx_money_got_in_outs.size() > 0)
    {
      uint64_t total_received_2 = sub_change;
      for (const auto& i : tx_money_got_in_outs)
        total_received_2 += i.second;

      if (total_received_1 != total_received_2)
        {
          const epee::LogLevel level = epee::LogLevel::Warning;
          LOG_CATEGORY_COLOR
            (
             level
             // , epee::GLOBAL_CATEGORY
             , DEFAULT_CAT
             , epee::red
             , std::string()
             + "**************************************************"
             + "Consistency failure in amounts received"
             + "Check transaction "
             + txid.to_str()
             + "**************************************************"
             );
          exit(1);
          return;
        }

      for (const auto& i : tx_money_got_in_outs)
        {
          received_tx_details_2  payment;
          payment.m_tx_hash      = txid;
          payment.m_fee          = fee;
          payment.m_amount       = i.second;
          payment.m_amounts      =
            tx_amounts_individual_outs[i.first];
          payment.m_block_index = block_index;
          payment.m_timestamp    = ts;
          payment.m_coinbase     = miner_tx;
          payment.m_subaddr_index = i.first;
          m_received_txs_format_2.push_back(payment);
          LOG_VERBOSE
            (
             std::string("Payment found in ")
             + "block"
             + " / "
             + payment.m_tx_hash.to_str()
             + " / "
             + std::to_string(payment.m_amount)
             );
        }
    }
}

//-----------------------------------------------------------
void wallet2::finish_pending_sent_txs
(
 const crypto::hash txid
 , const cryptonote::transaction tx
 , const uint64_t block_index
 )
{
  if (m_pending_sent_txs.empty())
    return;

  auto unconf_it = m_pending_sent_txs.find(txid);
  if(unconf_it != m_pending_sent_txs.end()) {
    try {
      m_sent_txs.insert
        (
         std::make_pair
         (
          txid
          , mined_sent_tx_details(unconf_it->second, block_index)
          )
         );
    }
    catch (...) {
      // can fail if the tx has unexpected input types
      LOG_PRINT_L0
        ("Failed to add outgoing transaction "
         "to mined transaction map");
    }
    m_pending_sent_txs.erase(unconf_it);
  }
}

//-----------------------------------------------------------
void wallet2::process_outgoing
(
 const crypto::hash txid
 , const cryptonote::transaction tx
 , const uint64_t block_index
 , const uint64_t ts
 , const uint64_t spent
 , const uint64_t received
 , const uint32_t subaddr_account
 , const std::set<uint32_t> subaddr_indices
 )
{
  const auto entry =
    m_sent_txs.insert(std::make_pair(txid, mined_sent_tx_details()));

  // fill with the info we know, some info might already be there
  if (entry.second)
  {
    // this case will happen if the tx is from our outputs, but was
    // sent by another wallet (eg, we're a cold wallet and the hot
    // wallet sent it). For RCT transactions, we only see 0 input
    // amounts, so have to deduce amount out from other parameters.
    entry.first->second.m_amount_in = spent;
    entry.first->second.m_amount_out = spent - tx.ringct.fee;
    entry.first->second.m_change = received;

    entry.first->second.m_subaddr_account = subaddr_account;
    entry.first->second.m_subaddr_indices = subaddr_indices;
  }

  entry.first->second.m_rings.clear();
  for (const auto &in: tx.vin)
  {
    if (in.type() != typeid(cryptonote::txin_from_key))
      continue;
    const auto &txin = boost::get<cryptonote::txin_from_key>(in);
    entry.first->second.m_rings.push_back
      (
       std::make_pair
       (txin.output_key_image, txin.output_relative_offsets)
       );
  }
  entry.first->second.m_block_index = block_index;
  entry.first->second.m_timestamp = ts;
}


//-----------------------------------------------------------
void wallet2::process_new_blockchain_entry
(
 const cryptonote::raw_block& b
 , const cryptonote::block_t& block
 , const parsed_block &parsed_block
 , const crypto::hash& bl_id
 , const uint64_t block_index
 )
{
  //handle transactions from new block

  //optimization: seeking only for blocks that are not older then the
  //wallet creation time plus 1 day. 1 day is for possible user
  //incorrect time setup

  if (block_index >= m_scan_from_block_index)
  {
    process_new_transaction
      (
       get_transaction_hash(b.miner_tx)
       , b.miner_tx
       , block_index
       , b.major_version
       , b.timestamp
       , true
       );

    for (size_t idx = 0; idx < b.tx_hashes.size(); ++idx)
      {
        process_new_transaction
          (
           b.tx_hashes[idx]
           , review_ringct(parsed_block.block.txs[idx])
           , block_index
           , b.major_version
           , b.timestamp
           , false
           );
      }

    if (!(block_index % (1 << 10))) {

      LOG_COLOR
        (
         epee::yellow
         , epee::LogLevel::Info
         , "Scanned "
         + std::to_string(block_index)
         );
    }
  }
  else
  {
    if (!(block_index % (1 << 10))) {

      LOG_COLOR
        (
         epee::cyan
         , epee::LogLevel::Info
         , "Skipped ⛓ "
         + std::to_string(block_index)
         );
    }
  }

  m_blockchain_hashes.push_back(bl_id);

  if (0 != m_callback)
    m_callback->on_new_block(block_index, b);
}

//-----------------------------------------------------------
std::list<crypto::hash> wallet2::get_short_chain_history() const
{
  THROW_WALLET_EXCEPTION_IF
    (
     m_blockchain_hashes.empty()
     , error::wallet_internal_error
     , "m_blockchain_hashes is empty"
     );

  std::list<crypto::hash> ids;

  const auto xs = build_short_chain_history(m_blockchain_hashes.size());

  std::transform
    (
     xs.begin()
     , xs.end()
     , std::back_inserter(ids)
     , [&](const auto& x) {
       return m_blockchain_hashes[x];
     }
     );

  return ids;
}

//-----------------------------------------------------------
std::optional<uint64_t>
wallet2::pull_blocks()
{

  cryptonote::COMMAND_RPC_GET_BLOCKS_FAST::request req{};
  cryptonote::COMMAND_RPC_GET_BLOCKS_FAST::response res{};

  req.block_ids = get_short_chain_history();

  {
    bool r = m_rpc_client.invoke_http_json("/get_blocks", req, res);

    THROW_ON_RPC_RESPONSE_ERROR
      (
       r
       , {}
       , res
       , "get_blocks"
       , error::get_blocks_error, (res.status)
       );
  }



  LOG_DEBUG
    (
     "Pulled blocks: blocks_start_height "
     + std::to_string(res.start_block_index)
     + ", count "
     + std::to_string(res.blocks.size())
     + ", height "
     + std::to_string(res.start_block_index + res.blocks.size())
     + ", node height "
     + std::to_string(res.blockchain_size)
     );

  if (res.blocks.empty()) return {};

  std::vector<parsed_block> parsed_blocks(res.blocks.size());
  std::transform
    (
     std::execution::par
     , res.blocks.begin()
     , res.blocks.end()
     , parsed_blocks.begin()
     , [](const auto& x) -> parsed_block {

       parsed_block b;
       b.block = x;
       return b;
     }
     );

  const auto block_start_index = res.start_block_index;
  const auto blocks = res.blocks;

  THROW_WALLET_EXCEPTION_IF
    (
     blocks.size() != parsed_blocks.size()
     , error::wallet_internal_error
     , "size mismatch"
     );

  uint64_t blocks_added = 0;
  size_t current_index = block_start_index;

  for (size_t i = 0; i < blocks.size(); ++i)
  {
    const crypto::hash &bl_id = parsed_blocks[i].block.hash();
    const cryptonote::raw_block &bl =
      review_block(parsed_blocks[i].block);

    if(current_index >= m_blockchain_hashes.size())
    {
      process_new_blockchain_entry
        (bl, blocks[i], parsed_blocks[i], bl_id, current_index);

      ++blocks_added;
    }
    else if(bl_id != m_blockchain_hashes[current_index])
    {
      //split detected here !!!
      THROW_WALLET_EXCEPTION_IF
        (
         current_index == block_start_index
         , error::wallet_internal_error
         , "wrong daemon response: "
         "split starts from the first block in response "
         + epee::string_tools::pod_to_hex(bl_id)
         + " (height "
         + std::to_string(block_start_index)
         + "), local block id at this index: "
         + epee::string_tools::pod_to_hex
         (m_blockchain_hashes[current_index])
         );

      delete_blocks_after_and_including_block_index(current_index);
      process_new_blockchain_entry
        (bl, blocks[i], parsed_blocks[i], bl_id, current_index);
    }
    else
    {
      LOG_DEBUG("Block is already in blockchain: " + bl_id.to_str());
    }
    ++current_index;
  }

  return blocks_added;

}

//-----------------------------------------------------------
std::pair<uint64_t, bool> wallet2::refresh()
{
  if (m_offline)
  {
    return {0, false};
  }

  std::optional<crypto::hash> last_tx_hash_id =
    m_received_txs.size()
    ? std::optional<crypto::hash>(m_received_txs.back().m_txid)
    : std::nullopt;

  uint64_t blocks_fetched = 0;
  bool received_money = false;

  try {
    while(true)
      {
        const auto maybe_pulled_blocks = pull_blocks();

        if (!maybe_pulled_blocks) {
          break;
        }

        blocks_fetched += *maybe_pulled_blocks;
      }
  }

  catch (const std::exception& e) {
    LOG_ERROR(std::string() + "refresh error: " + e.what());
  }

  if (!m_received_txs.empty()) {
    received_money = last_tx_hash_id != m_received_txs.back().m_txid;
  }

  LOG_INFO
    (
     "Refresh done, blocks received: "
     + std::to_string(blocks_fetched)
     + ", balance (all accounts): "
     + print_money(balance_all())
     + ", unlocked: "
     + print_money(unlocked_balance_all())
     );

  return {
    blocks_fetched
    , received_money
  };
}

//-----------------------------------------------------------
void wallet2::delete_blocks_after_and_including_block_index
(const uint64_t block_index)
{
  LOG_PRINT_L0
    ("Detaching blockchain after and including index: "
     + std::to_string(block_index));

  // size  1 2 3 4 5 6 7 8 9
  // block 0 1 2 3 4 5 6 7 8
  //               C

  size_t transfers_detached = 0;

  for (auto& td: m_received_txs)
  {
    if (td.m_spent_block_index) {
      if (*td.m_spent_block_index >= block_index)
        {
          LOG_PRINT_L1
            (
             "Resetting spent/frozen status for output "
             + std::to_string(*td.m_spent_block_index)
             + ": "
             + td.m_output_key_image.to_str()
             );
          td.m_spent_block_index = {};
        }
    }
  }

  const auto it = std::find_if
    (
     m_received_txs.begin()
     , m_received_txs.end()
     , [&](const received_tx_details& td){
       return td.m_block_index >= block_index;
     }
     );

  const size_t i_start = it - m_received_txs.begin();

  for(size_t i = i_start; i!= m_received_txs.size();i++)
  {
    auto it_ki = m_received_txs_map_by_output_key_image.find
      (m_received_txs[i].m_output_key_image);

    THROW_WALLET_EXCEPTION_IF
      (
       it_ki == m_received_txs_map_by_output_key_image.end()
       , error::wallet_internal_error
       , "key image not found: index "
       + std::to_string(i)
       + ", ki "
       + epee::string_tools::pod_to_hex
       (m_received_txs[i].m_output_key_image)
       + ", "
       + std::to_string
       (m_received_txs_map_by_output_key_image.size())
       + " key images known"
       );

    m_received_txs_map_by_output_key_image.erase(it_ki);
  }

  for(size_t i = i_start; i!= m_received_txs.size();i++)
  {
    const auto it_pk =
      m_received_txs_map_by_output_pk.find
      (m_received_txs[i].get_public_key());

    THROW_WALLET_EXCEPTION_IF
      (
       it_pk == m_received_txs_map_by_output_pk.end()
       , error::wallet_internal_error
       , "public key not found"
       );

    m_received_txs_map_by_output_pk.erase(it_pk);
  }

  transfers_detached = std::distance(it, m_received_txs.end());
  m_received_txs.erase(it, m_received_txs.end());

  for
    (
     auto it = m_received_txs_format_2.begin()
       ; it != m_received_txs_format_2.end()
       ;
     )
  {
    if(it->m_block_index >= block_index)
      it = m_received_txs_format_2.erase(it);
    else
      ++it;
  }

  for (auto it = m_sent_txs.begin(); it != m_sent_txs.end(); )
  {
    if(it->second.m_block_index >= block_index)
      it = m_sent_txs.erase(it);
    else
      ++it;
  }

  const size_t blocks_detached =
    m_blockchain_hashes.size() - block_index;

  LOG_PRINT_L0
    (
     "Detached blockchain after and including block_index "
     + std::to_string(block_index)
     + ", transfers detached "
     + std::to_string(transfers_detached)
     + ", blocks detached "
     + std::to_string(blocks_detached)
     );
}

//-----------------------------------------------------------
uint64_t wallet2::balance(uint32_t index_major) const
{
  uint64_t amount = 0;
  for (const auto& i : balance_per_subaddress(index_major))
    amount += i.second;
  return amount;
}

//-----------------------------------------------------------
uint64_t wallet2::unlocked_balance(uint32_t index_major) const
{
  uint64_t amount = 0;
  for (const auto& i : unlocked_balance_per_subaddress(index_major))
  {
    amount += i.second.first;
  }
  return amount;
}

//-----------------------------------------------------------
std::map<uint32_t, uint64_t>
wallet2::balance_per_subaddress(uint32_t index_major) const
{
  return wallet::logic::functional::wallet
    ::balance_per_subaddress
    (index_major, m_received_txs, m_pending_sent_txs);
}

//-----------------------------------------------------------
std::map
<
  uint32_t
  , std::pair<uint64_t, std::pair<uint64_t, uint64_t>>
  >
wallet2::unlocked_balance_per_subaddress(uint32_t index_major) const
{
  return wallet::logic::functional::wallet
    ::unlocked_balance_per_subaddress
    (index_major, m_received_txs, get_blockchain_size());
}

//-----------------------------------------------------------
uint64_t wallet2::balance_all() const
{
  uint64_t r = 0;
  for
    (
     uint32_t index_major = 0
       ; index_major < get_num_subaddress_accounts()
       ; ++index_major
     ) {
    r += balance(index_major);
  }
  return r;
}

//-----------------------------------------------------------
uint64_t wallet2::unlocked_balance_all()
{
  uint64_t r = 0;
  for
    (
     uint32_t index_major = 0
       ; index_major < get_num_subaddress_accounts()
       ; ++index_major
     )
  {
    r += unlocked_balance(index_major);
  }
  return r;
}

//-----------------------------------------------------------
wallet::logic::type::wallet::received_tx_details_container
wallet2::get_txs() const
{
  return m_received_txs;
}

//-----------------------------------------------------------
std::list
<
  wallet::logic::type::payment::received_tx_details_2 
  >
wallet2::get_received_txs
(
 const uint64_t min_height
 , const uint64_t max_height
 , const std::optional<uint32_t> subaddr_account
 , const std::set<uint32_t> subaddr_indices
 ) const
{

  std::list<
    wallet::logic::type::payment::received_tx_details_2 
    >
    xs;

  std::for_each
    (
     m_received_txs_format_2.begin()
     , m_received_txs_format_2.end()
     , [
        &xs
        , &min_height
        , &max_height
        , &subaddr_account
        , &subaddr_indices
        ](const auto& x) {
       if
         (
          min_height < x.m_block_index
          && max_height >= x.m_block_index
          &&
          (
           !subaddr_account
           || *subaddr_account == x.m_subaddr_index.major
           )
          &&
          (
           subaddr_indices.empty()
           || subaddr_indices.count(x.m_subaddr_index.minor)
           == 1
           )
          )
         {
           xs.push_back(x);
         }
     }
     );

  return xs;
}

//-----------------------------------------------------------
std::list
<
  std::pair
  <
    crypto::hash
    , wallet::logic::type::transfer::mined_sent_tx_details
    >>
wallet2::get_sent_txs
(
 const uint64_t min_height
 , const uint64_t max_height
 , const std::optional<uint32_t> subaddr_account
 , const std::set<uint32_t> subaddr_indices
 ) const
{
  std::list
    <
      std::pair
    <
      crypto::hash
        , wallet::logic::type::transfer::mined_sent_tx_details
        >> xs;

  for (const auto& x: m_sent_txs) {
    if
      (
       x.second.m_block_index <= min_height
       || x.second.m_block_index > max_height
       ) {
      continue;
    }

    if
      (
       subaddr_account &&
       *subaddr_account != x.second.m_subaddr_account
       ) {
      continue;
    }

    if(!subaddr_indices.empty()) {

      const auto in_subaddr = std::find_if
        (
         x.second.m_subaddr_indices.begin()
         , x.second.m_subaddr_indices.end()
         , [&subaddr_indices](uint32_t index) {
           return subaddr_indices.contains(index);
         }
         );

      if (in_subaddr == x.second.m_subaddr_indices.end()) {
        continue;
      }
    }

    xs.push_back(x);
  }

  return xs;
}

//-----------------------------------------------------------
std::list
<
  std::pair
  <
    crypto::hash
    , wallet::logic::type::transfer::un_mined_sent_tx_details
    >>
wallet2::get_pending_sent_txs
(
 const std::optional<uint32_t> subaddr_account
 , const std::set<uint32_t> subaddr_indices
 ) const
{
  
  std::list
    <
      std::pair
    <
      crypto::hash
        , wallet::logic::type::transfer::un_mined_sent_tx_details
        >> xs;

  for (const auto& x: m_pending_sent_txs) {
    if
      (
       subaddr_account
       && *subaddr_account != x.second.m_subaddr_account
       ) {
      continue;
    }

    if (!subaddr_indices.empty()) {
      const auto in_subaddr = std::find_if
        (
         x.second.m_subaddr_indices.begin()
         , x.second.m_subaddr_indices.end()
         , [&subaddr_indices](uint32_t index) {
           return subaddr_indices.contains(index);
         }
         );

      if (in_subaddr == x.second.m_subaddr_indices.end()) {
        continue;
      }
    }

    xs.push_back(x);
  }

  return xs;
}

//-----------------------------------------------------------
// take a pending tx and actually send it to the daemon
void wallet2::commit_tx(const pending_sent_tx& ptx)
{
  using namespace cryptonote;

  // Normal submit
  COMMAND_RPC_SEND_RAW_TX::request req{};

  const auto maybe_tx = preview_ringct(ptx.tx);

  if (!maybe_tx) {
    LOG_FATAL("Failed to review ringct from prepared tx");
  }

  req.tx = *maybe_tx;
    
  COMMAND_RPC_SEND_RAW_TX::response daemon_send_resp{};

  bool r = m_rpc_client.invoke_http_json
    ("/send_raw_transaction", req, daemon_send_resp);

  THROW_ON_RPC_RESPONSE_ERROR
    (
     r
     , {}
     , daemon_send_resp
     , "sendrawtransaction"
     , error::tx_rejected
     , ptx.tx
     , (daemon_send_resp.status)
     , wallet::logic::functional::wallet::get_text_reason
     (daemon_send_resp)
     );

  // sanity checks
  for (size_t idx: ptx.selected_txs)
    {
      THROW_WALLET_EXCEPTION_IF
        (
         idx >= m_received_txs.size()
         , error::wallet_internal_error
         , "Bad output index in selected transfers: "
         + boost::lexical_cast<std::string>(idx)
         );
    }

  crypto::hash txid;

  txid = get_transaction_hash(ptx.tx);
  std::vector<cryptonote::tx_destination_entry> dests;
  uint64_t amount_in = 0;

  dests = ptx.dests;
  for(size_t idx: ptx.selected_txs)
    amount_in += m_received_txs[idx].amount();

  const auto utd =
    wallet::logic::functional::wallet::get_un_mined_sent_tx_details
    (
     ptx.tx
     , amount_in
     , dests
     , ptx.change_dts.amount
     , ptx.construction_data.subaddr_account
     , ptx.construction_data.subaddr_indices
     );

  m_pending_sent_txs[cryptonote::get_transaction_hash(ptx.tx)] = utd;

  LOG_VERBOSE
    (
     "transaction "
     + txid.to_str()
     + " generated ok and sent to daemon, output_key_images: ["
     + ptx.output_key_images
     + "]"
     );

  for(size_t idx: ptx.selected_txs)
  {
    m_received_txs[idx].m_spent_block_index = 0;
  }

  //fee includes dust if dust policy specified it.
  LOG_INFO
    (
     "Transaction successfully sent. "
     + txid.to_str()
     );

  LOG_INFO
    (
     "Commission: "
     + print_money(ptx.fee)
     + " (dust sent to dust addr: "
     + print_money((ptx.dust_added_to_fee ? 0 : ptx.dust))
     + ")"
     );

  LOG_INFO
    (
     "Balance: "
     + print_money(balance(ptx.construction_data.subaddr_account))
     );

  LOG_INFO
    (
     "Unlocked: "
     + print_money
     (unlocked_balance(ptx.construction_data.subaddr_account))
     );

  LOG_INFO
    ("Please wait for confirmation for your balance to be unlocked.");
}

std::vector<wallet::logic::type::tx::pending_sent_tx>
wallet2::create_transactions
(
 const std::vector<cryptonote::tx_destination_entry> dsts_vec
 , const size_t fake_outs_count
 , const uint32_t priority
 , const std::vector<uint8_t> extra
 , const uint32_t subaddr_account
 , const std::set<uint32_t> subaddr_indices_
 ) const
{
  return wallet::logic::controller::wallet::create_transactions
    (
     dsts_vec
     , fake_outs_count
     , priority
     , extra
     , subaddr_account
     , subaddr_indices_
     , m_nettype
     , m_received_txs
     , m_pending_sent_txs
     , get_blockchain_size()
     , m_ignore_fractional_outputs
     , m_merge_destinations
     , m_rpc_client
     , m_account.get_keys()
     , m_subaddresses
     , unlocked_balance(subaddr_account)
     );
}

} // tools



