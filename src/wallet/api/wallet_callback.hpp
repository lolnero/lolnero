/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/


#pragma once

#include "cryptonote/basic/functional/base.hpp"
#include "math/blockchain/functional/subaddress_index.hpp"

namespace tools
{
  class i_wallet2_callback
  {
  public:
    // Full wallet callbacks
    virtual void on_new_block
    (
     uint64_t block_index
     , const cryptonote::raw_block& block
     ) {}

    virtual void on_money_received
    (
     uint64_t block_index
     , const crypto::hash &txid
     , const cryptonote::transaction& tx
     , uint64_t amount
     , const cryptonote::subaddress_index& subaddr_index
     ) {}

    virtual void on_un_mined_money_received
    (
     uint64_t block_index
     , const crypto::hash &txid
     , const cryptonote::transaction& tx
     , uint64_t amount
     , const cryptonote::subaddress_index& subaddr_index
     ) {}

    virtual void on_money_spent
    (
     uint64_t block_index
     , const crypto::hash &txid
     , const cryptonote::transaction& in_tx
     , uint64_t amount
     , const cryptonote::transaction& spend_tx
     , const cryptonote::subaddress_index& subaddr_index
     ) {}

    virtual void on_skip_transaction
    (
     uint64_t block_index
     , const crypto::hash &txid
     , const cryptonote::transaction& tx
     ) {}

    // Common callbacks
    virtual void on_pool_tx_removed(const crypto::hash &txid) {}
    virtual ~i_wallet2_callback() {}
  };

}
