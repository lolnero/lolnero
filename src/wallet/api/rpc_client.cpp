/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/

#include "rpc_client.h"

#include "wallet_errors.h"

#include "tools/epee/include/net/http_abstract_invoke.h"
#include "network/rpc/core_rpc_server_commands_defs.hpp"
#include "cryptonote/basic/functional/format_utils.hpp"

#include "math/blockchain/controller/gamma_ring.hpp"
#include "wallet/logic/type/transfer.hpp"


#define RETURN_ON_RPC_RESPONSE_ERROR(r, error, res, method) \
  do { \
    ASSERT_OR_LOG_ERROR_AND_RETURN(error.code == 0, error.message, error.message); \
    ASSERT_OR_LOG_ERROR_AND_RETURN(r, std::string("Failed to connect to daemon"), "Failed to connect to daemon"); \
    /* empty string -> not connection */ \
    ASSERT_OR_LOG_ERROR_AND_RETURN(!res.status.empty(), res.status, "No connection to daemon"); \
    ASSERT_OR_LOG_ERROR_AND_RETURN(res.status != CORE_RPC_STATUS_BUSY, res.status, "Daemon busy"); \
    ASSERT_OR_LOG_ERROR_AND_RETURN(res.status == CORE_RPC_STATUS_OK, res.status, "Error calling " + std::string(method) + " daemon RPC"); \
  } while(0)

using namespace cryptonote;
using namespace wallet::logic::type::transfer;

namespace tools
{

std::vector<wallet::logic::type::get_tx_outputs_entry>
RPC_Client::get_decoys
(
 const wallet::logic::type::transfer::received_tx_details td
 ) const
{
  cryptonote::COMMAND_RPC_GET_DECOYS::request req{};
  cryptonote::COMMAND_RPC_GET_DECOYS::response res{};

  LOG_DEBUG("get decoy for: " + td.m_output_public_key.to_str());
  req.output_public_key = td.m_output_public_key;

  try
  {
    bool r = invoke_http_json_rpc("get_decoys", req, res);
    THROW_ON_RPC_RESPONSE_ERROR_GENERIC
      (r, {}, res, "/get_decoys");
  }
  catch(...)
  {
    return {};
  }

  std::vector<wallet::logic::type::get_tx_outputs_entry> ring;
  std::transform
    (
     res.decoys.begin()
     , res.decoys.end()
     , std::back_inserter(ring)
     , [](const auto& x) -> wallet::logic::type::get_tx_outputs_entry {

       const auto maybe_public_key =
         crypto::preview_safe_point(x.public_key);

       THROW_WALLET_EXCEPTION_IF
         (
          !maybe_public_key
          , tools::error::wallet_internal_error
          , "Invalid decoy public key: " + x.public_key.to_str()
          );

       const crypto::public_key public_key =
         crypto::p2pk(*maybe_public_key);

       const auto maybe_commit =
         crypto::preview_safe_point(x.commit);

       THROW_WALLET_EXCEPTION_IF
         (
          !maybe_commit
          , tools::error::wallet_internal_error
          , "Invalid decoy mask: " + x.commit.to_str()
          );

       const auto commit = *maybe_commit;

       return
         {
           x.output_index
           , public_key
           , commit
         };
     }
     );

  std::sort
    (
     ring.begin()
     , ring.end()
     , [](const auto& a, const auto& b) {
       return std::get<0>(a) < std::get<0>(b);
     }
     );

  return ring;
}
std::vector<std::vector<wallet::logic::type::get_tx_outputs_entry>>
RPC_Client::get_tx_outputs
(
 const std::vector<wallet::logic::type::transfer::received_tx_details> tds
 , const size_t decoy_size
 ) const
{
  LOG_VERBOSE("decoy_size: " + std::to_string(decoy_size));

  std::vector<std::vector<wallet::logic::type::get_tx_outputs_entry>> outV;

  std::transform
    (
     tds.begin()
     , tds.end()
     , std::back_inserter(outV)
     , [this](const auto& td) {
       return get_decoys(td);
     }
     );
  return outV;
}


} // tools
