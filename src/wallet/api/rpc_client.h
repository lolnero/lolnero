/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/

#pragma once

#include "network/rpc/core_rpc_server_commands_defs.hpp"
#include "tools/epee/include/net/http_abstract_invoke.h"

#include "wallet/logic/type/typedef.hpp"
#include "wallet/logic/type/wallet.hpp"
#include "wallet/logic/type/transfer.hpp"


namespace tools
{

  struct RPC_Client
  {
    std::string m_host{config::lol::DAEMON_RPC_DEFAULT_IP};
    uint16_t m_port = cryptonote::mainnet.RPC_DEFAULT_PORT;

    std::vector<std::vector<wallet::logic::type::get_tx_outputs_entry>>
    get_tx_outputs
    (
     const std::vector<wallet::logic::type::transfer::received_tx_details> tds
     , const size_t decoy_size
     ) const;

    template<class t_request, class t_response>
    bool invoke_http_json
    (const std::string_view uri, const t_request& req, t_response& res) const
    {
      return epee::net_utils::invoke_http_json
        (m_host, m_port, uri, req, res);
    }

    template<class t_request, class t_response>
    bool invoke_http_json_rpc
    (const std::string& method_name, const t_request& req, t_response& res) const
    {
      return epee::net_utils::invoke_http_json_rpc
        (m_host, m_port, "/json_rpc", method_name, req, res);
    }

    std::vector<wallet::logic::type::get_tx_outputs_entry>
    get_decoys
    (
     const wallet::logic::type::transfer::received_tx_details td
     ) const;

  };


}
