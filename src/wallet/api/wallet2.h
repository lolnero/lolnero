/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/


#pragma once

#include "wallet_callback.hpp"

#include "wallet/api/rpc_client.h"
#include "wallet/api/wallet_errors.h"

#include "wallet/logic/type/typedef.hpp"
#include "wallet/logic/type/payment.hpp"
#include "wallet/logic/type/transfer.hpp"
#include "wallet/logic/type/tx.hpp"
#include "wallet/logic/type/wallet.hpp"

#include <atomic>

namespace tools
{
  using namespace wallet::logic::type::wallet;
  using namespace wallet::logic::type::payment;
  using namespace wallet::logic::type::transfer;
  using namespace wallet::logic::type::tx;

  struct wallet2
  {
    cryptonote::network_type m_nettype = cryptonote::MAINNET;
    std::string m_daemon_host{config::lol::DAEMON_RPC_DEFAULT_IP};
    uint16_t m_daemon_port = cryptonote::mainnet.RPC_DEFAULT_PORT;
    bool m_offline = false;

    uint32_t m_subaddress_lookahead_major =
      config::lol::SUBADDRESS_LOOKAHEAD_MAJOR;

    uint32_t m_subaddress_lookahead_minor =
      config::lol::SUBADDRESS_LOOKAHEAD_MINOR;


    cryptonote::account_base m_account;
    RPC_Client m_rpc_client{};

    uint64_t m_scan_from_block_index = 0;
    bool m_merge_destinations = false;
    bool m_ignore_fractional_outputs = true;


  private:
    std::vector<crypto::hash> m_blockchain_hashes;
    std::map<crypto::hash, un_mined_sent_tx_details>
    m_pending_sent_txs;

    std::map<crypto::hash, mined_sent_tx_details>
    m_sent_txs;

    std::list<received_tx_details_2> m_received_txs_format_2;


    wallet::logic::type::wallet::received_tx_details_container
    m_received_txs;


    std::map<crypto::key_image, size_t>
    m_received_txs_map_by_output_key_image;

    std::map<crypto::public_key, size_t>
    m_received_txs_map_by_output_pk;

    cryptonote::spend_view_public_keys m_spend_view_public_keys{};

    std::map
    <crypto::public_key, cryptonote::subaddress_index> m_subaddresses;

    std::vector<std::vector<std::string>> m_subaddress_labels;

  public:
    wallet2() = default;

    wallet2
    (
     const cryptonote::network_type nettype
     , const std::string daemon_host
     , const uint16_t daemon_port
     , const crypto::secret_key recovery_key
     , const uint64_t refresh_height
     , const uint32_t max_account
     , const uint32_t max_address
     );

    // Subaddress scheme
    cryptonote::spend_view_public_keys
    get_subaddress(const cryptonote::subaddress_index& index) const;

    cryptonote::spend_view_public_keys
    get_address() const { return get_subaddress({0,0}); }

    std::optional<cryptonote::subaddress_index>
    get_subaddress_index
    (const cryptonote::spend_view_public_keys& address) const;

    crypto::public_key get_subaddress_spend_public_key
    (const cryptonote::subaddress_index& index) const;

    std::vector<crypto::public_key> get_subaddress_spend_public_keys
    (uint32_t account, uint32_t begin, uint32_t end) const;

    std::string get_subaddress_as_str
    (const cryptonote::subaddress_index& index) const;

    std::string get_address_as_str() const {
      return get_subaddress_as_str({0, 0});
    }

    void add_subaddress_account(const std::string& label);

    size_t get_num_subaddress_accounts() const {
      return m_subaddress_labels.size();
    }

    size_t get_num_subaddresses(uint32_t index_major) const {
      return index_major < m_subaddress_labels.size()
        ? m_subaddress_labels[index_major].size()
        : 0
        ;
    }

    // throws when index is out of bound
    void add_subaddress
    (uint32_t index_major, const std::string& label); 

    void expand_subaddresses
    (const cryptonote::subaddress_index& index);

    void create_one_off_subaddress
    (const cryptonote::subaddress_index& index);

    void set_subaddress_lookahead
    (size_t major, size_t minor);

    std::pair<size_t, size_t> get_subaddress_lookahead() const {
      return {
        m_subaddress_lookahead_major
        , m_subaddress_lookahead_minor
      };
    }

    std::pair<uint64_t, bool> refresh();

    uint64_t balance(uint32_t subaddr_index_major) const;
    uint64_t unlocked_balance(uint32_t subaddr_index_major) const;

    std::map<uint32_t, uint64_t> balance_per_subaddress
    (uint32_t subaddr_index_major) const;

    std::map
    <
      uint32_t
      , std::pair<uint64_t, std::pair<uint64_t, uint64_t>>
      >
    unlocked_balance_per_subaddress
    (uint32_t subaddr_index_major) const;

    uint64_t balance_all() const;
    uint64_t unlocked_balance_all();

    void commit_tx(const pending_sent_tx& ptx_vector);

    std::vector<wallet::logic::type::tx::pending_sent_tx>
    create_transactions
    (
     const std::vector<cryptonote::tx_destination_entry> dsts_vec
     , const size_t fake_outs_count
     , const uint32_t priority
     , const std::vector<uint8_t> extra
     , const uint32_t subaddr_account
     , const std::set<uint32_t> subaddr_indices_
     ) const;

    wallet::logic::type::wallet::received_tx_details_container
    get_txs() const;

    std::list
    <
      wallet::logic::type::payment::received_tx_details_2 
      >
    get_received_txs
    (
     const uint64_t min_height
     , const uint64_t max_height = (uint64_t)-1
     , const std::optional<uint32_t> subaddr_account = {}
     , const std::set<uint32_t> subaddr_indices = {}
     ) const;

    std::list
    <
      std::pair
      <
        crypto::hash
        , wallet::logic::type::transfer::mined_sent_tx_details
        >>
    get_sent_txs
    (
     const uint64_t min_height
     , const uint64_t max_height = (uint64_t)-1
     , const std::optional<uint32_t> subaddr_account = {}
     , const std::set<uint32_t> subaddr_indices = {}
     ) const;

    std::list
    <
      std::pair
      <
        crypto::hash
        , wallet::logic::type::transfer::un_mined_sent_tx_details
        >>
    get_pending_sent_txs
    (
     const std::optional<uint32_t> subaddr_account = {}
     , const std::set<uint32_t> subaddr_indices = {}
     ) const;


    uint64_t get_blockchain_size() const {
      return m_blockchain_hashes.size();
    }

    uint64_t get_top_block_index() const {
      return get_blockchain_size() - 1;
    }

    i_wallet2_callback* m_callback = 0;

    
  private:
    void process_new_transaction
    (
     const crypto::hash &txid
     , const cryptonote::transaction& tx
     , const uint64_t block_index
     , const uint8_t block_version
     , const uint64_t ts
     , const bool miner_tx
     );

    void process_new_blockchain_entry
    (
     const cryptonote::raw_block& b
     , const cryptonote::block_t& block
     , const parsed_block &parsed_block
     , const crypto::hash& bl_id
     , const uint64_t block_index
     );

    void delete_blocks_after_and_including_block_index
    (uint64_t block_index);

    std::list<crypto::hash> get_short_chain_history() const;

    std::optional<uint64_t>
    pull_blocks();

    void finish_pending_sent_txs
    (
     const crypto::hash txid
     , const cryptonote::transaction tx
     , const uint64_t block_index
     );

    void process_outgoing
    (
     const crypto::hash txid
     , const cryptonote::transaction tx
     , const uint64_t block_index
     , const uint64_t ts
     , const uint64_t spent
     , const uint64_t received
     , const uint32_t subaddr_account
     , const std::set<uint32_t> subaddr_indices
     );

    bool should_expand
    (const cryptonote::subaddress_index &index) const;
  };
}
