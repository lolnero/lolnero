/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include "wallet/mnemonics/electrum-words.h"
#include "math/crypto/functional/key.hpp"
#include "tools/epee/functional/hex.hpp"

#include <iostream>
#include <string>

int main() {
  std::string seed;
  for (std::string line; std::getline(std::cin, line);) {
    seed = seed + " " + line;
  }

  // std::cout << "seed " << seed << std::endl;

  crypto::secret_key spend_key{};
  const bool convert_result =
    crypto::ElectrumWords::words_to_sk(seed, spend_key);

  if (!convert_result) {
    return 1;
  }

  std::cout << epee::hex::encode_to_hex(spend_key.data) << std::endl;
  
  return 0;
}
