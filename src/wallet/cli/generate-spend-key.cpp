/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include "math/crypto/controller/keyGen.hpp"
#include "math/crypto/controller/init.hpp"
#include "tools/epee/functional/hex.hpp"

#include <iostream>
#include <string>

int main() {
  crypto::init();

  const auto spend_key = crypto::randomScalar();
  std::cout << epee::hex::encode_to_hex(spend_key.data) << std::endl;
  return 0;
}
