/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "wallet_rpc_server.hpp"

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio.hpp>

#include <chrono>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <string>
#include <functional>

namespace beast = boost::beast;
namespace http = beast::http;
using tcp = boost::asio::ip::tcp;

namespace cryptonote {

  void wallet_http_connection::json_response()
    {
      LOG_DEBUG(std::string(request_.target()));
      LOG_DEBUG(request_.body());

      try {

        if(request_.target() == "/json_rpc")
          {
            json_rpc_response();
          }
        else
          {
            response_.result(http::status::not_found);
          }
      }

      catch (std::exception& e) {
        LOG_WARNING(e.what());
        response_.result(http::status::bad_request);
      }

      catch (...) {
        response_.result(http::status::bad_request);
      }
    }


  void wallet_http_connection::json_rpc_response()
    {
      const std::string body_ = request_.body();
      const json json_body = json::parse(body_);

      LOG_DEBUG("json_body: " + json_body.dump());

      if (!json_body.contains("method")) {
        epee::json_rpc::error_response rsp{};
        rsp.jsonrpc = "2.0";
        rsp.error.code = -32600;
        rsp.error.message = "Invalid Request";

        const json json_error = rsp;

        beast::ostream(response_.body()) << json_error;

        return;
      }

      const std::string callback_name = json_body["method"];
      const uint64_t req_id = json_body.value("id", 0);

      using namespace wallet_rpc;

      if (false) {}

#define JSON_RPC(method, callback, request_t)                     \
      else if(callback_name == method) {                          \
        process_json_rpc<request_t::request, request_t::response> \
          ( json_body.value("params", request_t::request{})       \
            , req_id                                              \
            , std::bind_front                                     \
            ( &wallet_rpc_handler::callback                       \
              , &rpc_ ));                                         \
      }

      JSON_RPC
        ("rpc_version"
         , on_get_version
         , wallet_rpc::COMMAND_RPC_GET_VERSION)

      JSON_RPC
        ("balance"
         , on_get_balance
         , COMMAND_RPC_GET_BALANCE)

      JSON_RPC
        ("addresses"
         , on_get_addresses
         , COMMAND_RPC_GET_ADDRESS)

      JSON_RPC
        ("query_address_index"
         , on_get_address_index
         , COMMAND_RPC_GET_ADDRESS_INDEX)

      JSON_RPC
        ("create_address"
         , on_create_address
         , COMMAND_RPC_CREATE_ADDRESS)

      JSON_RPC
        ("accounts"
         , on_get_accounts
         , COMMAND_RPC_GET_ACCOUNTS)

      JSON_RPC
        ("new_account"
         , on_create_account
         , COMMAND_RPC_CREATE_ACCOUNT)

      JSON_RPC
        ("top_block_index"
         , on_get_top_block_index
         , COMMAND_RPC_GET_TOP_BLOCK_INDEX
         )

      JSON_RPC
        ("transfer"
         , on_transfer
         , COMMAND_RPC_TRANSFER)

      JSON_RPC
        ("txs"
         , on_get_txs
         , COMMAND_RPC_GET_TXS)

      JSON_RPC
        ("refresh"
         , on_refresh
         , COMMAND_RPC_REFRESH)

      JSON_RPC
        ("validate_address"
         , on_validate_address
         , COMMAND_RPC_VALIDATE_ADDRESS)

      JSON_RPC
        ("get_tx_by_txid"
         , on_get_tx_by_txid
         , COMMAND_RPC_GET_TX_BY_ID)

    }


  // "Lop" forever accepting new connections.
  void wallet_rpc_server
  (
   tcp::acceptor& acceptor
   , tcp::socket& socket
   , wallet_rpc_handler& rpc
   )
  {
    acceptor.async_accept
      (
       socket,
       [&](beast::error_code ec)
       {
         if(!ec)
           std::make_shared<wallet_http_connection>
             (
              std::move(socket)
              , rpc
              )
             ->start()
             ;
         wallet_rpc_server(acceptor, socket, rpc);
       });
  }

  void start_wallet_rpc_server
  (
   const std::string_view ip
   , const uint16_t port
   , boost::asio::io_context& ioc
   , wallet_rpc_handler& rpc
   )
  {
    LOG_TRACE("start_wallet_rpc_server");

    const auto address = boost::asio::ip::make_address(ip);

    tcp::acceptor acceptor{ioc, {address, port}};
    tcp::socket socket{ioc};
    wallet_rpc_server(acceptor, socket, rpc);

    ioc.run();
  }

} // cryptonote
