/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "wallet_rpc_handler.hpp"

#include "daemon/rpc/beast.hpp"


namespace beast = boost::beast;
namespace http = beast::http;
using tcp = boost::asio::ip::tcp;

namespace cryptonote {

  class wallet_http_connection : public http_connection
  {
  public:
    wallet_http_connection
    (tcp::socket socket, wallet_rpc_handler& rpc)
      : http_connection(std::move(socket)), rpc_(rpc) {}

  protected:
    wallet_rpc_handler& rpc_;

    void json_response();
    void json_rpc_response();

  };

  void start_wallet_rpc_server
  (
   const std::string_view ip
   , const uint16_t port
   , boost::asio::io_context& ioc
   , wallet_rpc_handler& rpc
   );

}
