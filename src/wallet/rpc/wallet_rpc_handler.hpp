/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Parts of this file are originally
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/

#pragma  once

#include "wallet/api/wallet2.h"
#include "wallet_rpc_server_commands_defs.h"

#include "tools/epee/include/net/jsonrpc_structs.h"


namespace cryptonote
{
  class wallet_rpc_handler
  {
  public:
    wallet_rpc_handler(std::unique_ptr<tools::wallet2>);
    ~wallet_rpc_handler();

    bool on_get_balance(const wallet_rpc::COMMAND_RPC_GET_BALANCE::request& req, wallet_rpc::COMMAND_RPC_GET_BALANCE::response& res, epee::json_rpc::json_error& er);
    bool on_get_addresses(const wallet_rpc::COMMAND_RPC_GET_ADDRESS::request& req, wallet_rpc::COMMAND_RPC_GET_ADDRESS::response& res, epee::json_rpc::json_error& er);
    bool on_get_address_index(const wallet_rpc::COMMAND_RPC_GET_ADDRESS_INDEX::request& req, wallet_rpc::COMMAND_RPC_GET_ADDRESS_INDEX::response& res, epee::json_rpc::json_error& er);
    bool on_create_address(const wallet_rpc::COMMAND_RPC_CREATE_ADDRESS::request& req, wallet_rpc::COMMAND_RPC_CREATE_ADDRESS::response& res, epee::json_rpc::json_error& er);
    bool on_get_accounts(const wallet_rpc::COMMAND_RPC_GET_ACCOUNTS::request& req, wallet_rpc::COMMAND_RPC_GET_ACCOUNTS::response& res, epee::json_rpc::json_error& er);
    bool on_create_account(const wallet_rpc::COMMAND_RPC_CREATE_ACCOUNT::request& req, wallet_rpc::COMMAND_RPC_CREATE_ACCOUNT::response& res, epee::json_rpc::json_error& er);
    bool on_get_top_block_index
    (
     const wallet_rpc::COMMAND_RPC_GET_TOP_BLOCK_INDEX::request& req
     , wallet_rpc::COMMAND_RPC_GET_TOP_BLOCK_INDEX::response& res
     , epee::json_rpc::json_error& er
     );
    bool on_transfer(const wallet_rpc::COMMAND_RPC_TRANSFER::request& req, wallet_rpc::COMMAND_RPC_TRANSFER::response& res, epee::json_rpc::json_error& er);
    bool on_get_txs(const wallet_rpc::COMMAND_RPC_GET_TXS::request& req, wallet_rpc::COMMAND_RPC_GET_TXS::response& res, epee::json_rpc::json_error& er);
    bool on_get_tx_by_txid(const wallet_rpc::COMMAND_RPC_GET_TX_BY_ID::request& req, wallet_rpc::COMMAND_RPC_GET_TX_BY_ID::response& res, epee::json_rpc::json_error& er);
    bool on_refresh(const wallet_rpc::COMMAND_RPC_REFRESH::request& req, wallet_rpc::COMMAND_RPC_REFRESH::response& res, epee::json_rpc::json_error& er);
    bool on_validate_address(const wallet_rpc::COMMAND_RPC_VALIDATE_ADDRESS::request& req, wallet_rpc::COMMAND_RPC_VALIDATE_ADDRESS::response& res, epee::json_rpc::json_error& er);
    bool on_get_version(const wallet_rpc::COMMAND_RPC_GET_VERSION::request& req, wallet_rpc::COMMAND_RPC_GET_VERSION::response& res, epee::json_rpc::json_error& er);

    // helpers
    void fill_generic_tx_entry(wallet_rpc::generic_tx_entry &entry, const crypto::hash &txid, const wallet::logic::type::payment::received_tx_details_2  &pd);
    void fill_generic_tx_entry(wallet_rpc::generic_tx_entry &entry, const crypto::hash &txid, const wallet::logic::type::transfer::mined_sent_tx_details &pd);
    void fill_generic_tx_entry(wallet_rpc::generic_tx_entry &entry, const crypto::hash &txid, const wallet::logic::type::transfer::un_mined_sent_tx_details &pd);

    bool not_open(epee::json_rpc::json_error& er);
    void handle_rpc_exception(const std::exception_ptr& e, epee::json_rpc::json_error& er, int default_error_code);


    bool validate_transfer(const std::list<wallet_rpc::tx_construction_destination>& destinations, std::vector<cryptonote::tx_destination_entry>& dsts, std::vector<uint8_t>& extra, bool at_least_one_destination, epee::json_rpc::json_error& er);

    std::unique_ptr<tools::wallet2> m_wallet;
  };
}
