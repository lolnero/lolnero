/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Parts of this file are originally
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/

#pragma once

#include "wallet_rpc_server_error_codes.h"

#include "tools/serialization/json.hpp"

#define EMPTY_REQUEST                                   \
  struct request {};                                    \
  inline void from_json(const json& j, request& p) {};  \
  inline void to_json(json& j, const request& p) {};    \

#define EMPTY_RESPONSE                          \
  struct response : rpc_response_base {};       \
                                                \
  JSON_LAYOUT                                   \
  ( response                                    \
    , status                                    \
    )

namespace cryptonote
{
  constexpr uint32_t WALLET_RPC_VERSION_MAJOR = 2;
  constexpr uint32_t WALLET_RPC_VERSION_MINOR = 0;
  constexpr uint32_t WALLET_RPC_VERSION =
    WALLET_RPC_VERSION_MAJOR << 16 | WALLET_RPC_VERSION_MINOR;

  using namespace tools;

  JSON_LAYOUT
    (
     subaddress_index
     , major
     , minor
     );

  namespace wallet_rpc
  {
    namespace COMMAND_RPC_GET_BALANCE
    {
      struct request
      {
        uint32_t account_index;
        std::set<uint32_t> address_indices;
        bool all_accounts = false;
      };

      JSON_LAYOUT
        (
         request
         , account_index
         , address_indices
         , all_accounts
         );

      struct per_subaddress_info
      {
        uint32_t account_index;
        uint32_t address_index;
        std::string address;
        uint64_t balance;
        uint64_t unlocked_balance;
        uint64_t num_unspent_outputs;
      };

      JSON_LAYOUT
        (
         per_subaddress_info
         , account_index
         , address_index
         , address
         , balance
         , unlocked_balance
         , num_unspent_outputs
         );

      struct response
      {
        uint64_t 	 balance;
        uint64_t 	 unlocked_balance;
        std::vector<per_subaddress_info> per_subaddress;
      };

      JSON_LAYOUT
        (
         response
         , balance
         , unlocked_balance
         , per_subaddress
         );
    };

    namespace COMMAND_RPC_GET_ADDRESS
    {
      struct request
      {
        uint32_t account_index;
        std::vector<uint32_t> address_indices;
      };

      JSON_LAYOUT
        (
         request
         , account_index
         , address_indices
         );
       

      struct address_info
      {
        std::string address;
        uint32_t address_index;
        bool used = false;
      };

      JSON_LAYOUT
        (
         address_info
         , address
         , address_index
         , used
         );

      struct response
      {
        // to remain compatible with older RPC format
        std::string address;
        std::vector<address_info> addresses;
      };

      JSON_LAYOUT
        (
         response
         , address
         , addresses
         );
    };

    namespace COMMAND_RPC_GET_ADDRESS_INDEX
    {
      struct request
      {
        std::string address;
      };

      JSON_LAYOUT
        (
         request
         , address
         );

      struct response
      {
        cryptonote::subaddress_index index;
      };

      JSON_LAYOUT
        (
         response
         , index
         );
    };

    namespace COMMAND_RPC_CREATE_ADDRESS
    {
      struct request
      {
        uint32_t    account_index;
        uint32_t    count;
      };

      JSON_LAYOUT
        (
         request
         , account_index
         , count
         );

      struct response
      {
        std::string              address;
        uint32_t                 address_index;
        std::vector<std::string> addresses;
        std::vector<uint32_t>    address_indices;
      };

      JSON_LAYOUT
        (
         response
         , address
         , address_index
         , addresses
         , address_indices
         );
    };

    namespace COMMAND_RPC_GET_ACCOUNTS
    {
      EMPTY_REQUEST;

      struct subaddress_account_info
      {
        uint32_t account_index;
        std::string base_address;
        uint64_t balance;
        uint64_t unlocked_balance;
      };

      JSON_LAYOUT
        (
         subaddress_account_info
         , account_index
         , base_address
         , balance
         , unlocked_balance
         );

      struct response
      {
        uint64_t total_balance;
        uint64_t total_unlocked_balance;
        std::vector<subaddress_account_info> subaddress_accounts;
      };

      JSON_LAYOUT
        (
         response
         , total_balance
         , total_unlocked_balance
         , subaddress_accounts
         );
         
    };

    namespace COMMAND_RPC_CREATE_ACCOUNT
    {
      EMPTY_REQUEST;

      struct response
      {
        uint32_t account_index;
        std::string address;      // the 0-th address for convenience
      };

      JSON_LAYOUT
        (
         response
         , account_index
         , address
         );
    };

    namespace COMMAND_RPC_GET_TOP_BLOCK_INDEX
    {
      EMPTY_REQUEST;

      struct response
      {
        uint64_t top_block_index;
      };

      JSON_LAYOUT
        (
         response
         , top_block_index
         );
    };

    struct tx_construction_destination
    {
      uint64_t amount;
      std::string address;
    };

    JSON_LAYOUT
      (
       tx_construction_destination
       , amount
       , address
       );

    namespace COMMAND_RPC_TRANSFER
    {
      struct request
      {
        std::list<tx_construction_destination> destinations;
        uint32_t account_index;
        std::set<uint32_t> subaddr_indices;
        uint32_t priority;
        bool get_tx_hex = false;
      };

      JSON_LAYOUT
        (
         request
         , destinations
         , account_index
         , subaddr_indices
         , priority
         , get_tx_hex
         );

      struct response
      {
        crypto::hash tx_hash;
        std::vector<crypto::secret_key> output_ecdh_secret_keys;
        uint64_t amount;
        uint64_t fee;
        uint64_t weight;
        std::string tx_blob;
        std::string unsigned_txset;
      };

      JSON_LAYOUT
        (
         response
         , tx_hash
         , output_ecdh_secret_keys
         , amount
         , fee
         , weight
         , tx_blob
         , unsigned_txset
         );
    };

    struct generic_tx_entry
    {
      crypto::hash txid;
      uint64_t block_index;
      uint64_t timestamp;
      uint64_t amount;
      amounts_container amounts;
      uint64_t fee;
      std::string note;
      std::list<tx_construction_destination> destinations;
      std::string type;
      bool locked;
      cryptonote::subaddress_index subaddr_index;
      std::vector<cryptonote::subaddress_index> subaddr_indices;
      std::string address;
      uint64_t confirmations = 0;
      uint64_t suggested_confirmations_threshold = 0;
    };


    JSON_LAYOUT
      (
       generic_tx_entry
       , txid
       , block_index
       , timestamp
       , amount
       , amounts
       , fee
       , note
       , destinations
       , type
       , locked
       , subaddr_index
       , subaddr_indices
       , address
       , confirmations
       , suggested_confirmations_threshold
       );


    namespace COMMAND_RPC_GET_TXS
    {
      struct request
      {
        bool in;
        bool out;
        bool pending;
        bool failed;

        bool filter_by_height;
        uint64_t min_height;
        uint64_t max_height = (uint64_t)CRYPTONOTE_MAX_BLOCK_NUMBER;
        uint32_t account_index;
        std::set<uint32_t> subaddr_indices;
        bool all_accounts = false;
      };


      JSON_LAYOUT
        (
         request
         , in
         , out
         , pending
         , failed
         , filter_by_height
         , min_height
         , max_height
         , account_index
         , subaddr_indices
         , all_accounts
         );

      struct response
      {
        std::list<generic_tx_entry> in;
        std::list<generic_tx_entry> out;
        std::list<generic_tx_entry> pending;
        std::list<generic_tx_entry> failed;
      };

      JSON_LAYOUT
        (
         response
         , in
         , out
         , pending
         , failed
         );
    };

    namespace COMMAND_RPC_GET_TX_BY_ID
    {
      struct request
      {
        crypto::hash txid;
        uint32_t account_index;
      };

      JSON_LAYOUT
        (
         request
         , txid
         , account_index
         );

      struct response
      {
        generic_tx_entry transfer;
        std::list<generic_tx_entry> transfers;
      };

      JSON_LAYOUT
        (
         response
         , transfer
         , transfers
         );
    };

    namespace COMMAND_RPC_REFRESH
    {
      EMPTY_REQUEST;

      struct response
      {
        uint64_t blocks_fetched;
        bool received_money;
      };

      JSON_LAYOUT
        (
         response
         , blocks_fetched
         , received_money
         );
    };

    namespace COMMAND_RPC_GET_VERSION
    {
      EMPTY_REQUEST;

      struct response
      {
        uint32_t version;
      };

      JSON_LAYOUT
        (
         response
         , version
         );
    };

    namespace COMMAND_RPC_VALIDATE_ADDRESS
    {
      struct request
      {
        std::string address;
        bool any_net_type = false;
      };

      JSON_LAYOUT
        (
         request
         , address
         , any_net_type
         );

      struct response
      {
        bool valid;
        bool subaddress;
        std::string nettype;
      };

      JSON_LAYOUT
        (
         response
         , valid
         , subaddress
         );
    };

  }
}
