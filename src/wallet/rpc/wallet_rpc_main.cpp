/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Parts of this file are originally
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/


#include "wallet_rpc_server.hpp"

#include "wallet/logic/functional/fee.hpp"
#include "wallet/logic/functional/wallet.hpp"

#include "tools/epee/include/string_file.hpp"
#include "tools/epee/include/logging.hpp"
#include "tools/common/signal.hpp"

#include "math/consensus/consensus.hpp"
#include "math/crypto/controller/init.hpp"

#include "config/version/version.hpp"

#include <boost/algorithm/string.hpp> // trim
#include <boost/program_options.hpp>

#include <filesystem>

namespace cryptonote
{

bool open_wallet
(
 const std::string spend_key_file_path
 , const uint64_t scan_from_block_index
 , const uint32_t max_account
 , const uint32_t max_address
 , const uint16_t rpc_port
 , const std::string rpc_bind_ip
 , const std::string daemon_host
 , const uint16_t daemon_port
 , const bool testnet
 )
{

  const auto maybe_spend_key_str =
    epee::string_file::load_file_to_string(spend_key_file_path);
  

  if (!maybe_spend_key_str) {
    std::cout
      << "failed to load spend key: " + spend_key_file_path
      << std::endl
      ;
    
    return false;
  }

  auto spend_key_str = *maybe_spend_key_str;

  boost::trim(spend_key_str);

  const auto maybe_unsafe_scalar =
    epee::string_tools::hex_to_pod<crypto::ec_scalar_unnormalized>
    (spend_key_str);

  if (!maybe_unsafe_scalar) {
    std::cout
      << "failed to parse spend key: " + spend_key_str
      << std::endl
      ;
  }

  const auto unsafe_scalar = *maybe_unsafe_scalar;

  if (!crypto::is_reduced(unsafe_scalar)) {
    std::cout
      << "invalid spend key: " + spend_key_str
      << std::endl
      ;
  }

  const auto spend_key = crypto::s2sk(crypto::reduce(unsafe_scalar));

  const network_type nettype = testnet ? TESTNET : MAINNET;

  auto m_wallet = std::make_unique<tools::wallet2>
    (
     nettype
     , daemon_host
     , daemon_port
     , spend_key
     , scan_from_block_index
     , max_account
     , max_address
     );

  if (!m_wallet)
  {

    std::cout << "Error creating wallet" << std::endl;
    return false;
  }

  LOG_GLOBAL("Scanning wallet ...");

  m_wallet->refresh();
 

  const std::string beast_rpc_description =
    "Lolnero Beast wallet RPC server";

  boost::asio::io_context ioc{1};

  tools::signal_handler_install
    ([&ioc, beast_rpc_description](int type) {
      LOG_INFO
        ("Wallet interrupted with signal: " + std::to_string(type));

      LOG_INFO
        ("Stopping " + beast_rpc_description + " ...");

      ioc.stop();
    });

  boost::asio::signal_set signals(ioc, SIGINT, SIGTERM);
  signals.async_wait(tools::signal_handler);



  const std::string rpc_ip_str = rpc_bind_ip;

  //  , boost::asio::io_context& ioc
  wallet_rpc_handler rpc{std::move(m_wallet)};

  LOG_GLOBAL
    (
     "Starting "
     + beast_rpc_description
     + " on "
     + rpc_ip_str
     + ":"
     + std::to_string(rpc_port)
     + " ..."
     );

  try {
    start_wallet_rpc_server
      (
       rpc_ip_str
       , rpc_port
       , ioc
       , rpc
       );

    LOG_GLOBAL(beast_rpc_description + " stopped");
  } catch (std::exception& e) {
    LOG_FATAL(e.what());
    return false;
  }

  return true;
}

} // cryptonote

int main(const int argc, const char** argv) {

  crypto::init();

  namespace po = boost::program_options;

  uint64_t scan_from_block_index = 0;
  uint32_t log_level = 0;
  uint16_t rpc_port = 0;
  std::string rpc_bind_ip = "";
  uint32_t max_account = 0;
  uint32_t max_address = 0;
  std::string daemon_host = "";
  uint16_t daemon_port = 0;


  // Declare the supported options.
  po::options_description command_options("Commands");
  command_options.add_options()
    ("help", "")
    ("version", "")
    (
     "open"
     , po::value<std::string>()
     , "Open a wallet using the spend key file at <arg>"
     )
    ;

  po::options_description setting_options("Settings");
  setting_options.add_options()
    (
     "log-level"
     , po::value<uint32_t>(&log_level)->default_value(0)
     , "0 default, 1 info, 2 verbose, 3 debug, 4 trace"
     )
    (
     "scan-from-index"
     , po::value<uint64_t>(&scan_from_block_index)->default_value(0ull)
     , "Skip scanning blocks before block index"
     )
    (
     "max-account"
     , po::value<uint32_t>(&max_account)
     ->default_value(config::lol::SUBADDRESS_LOOKAHEAD_MAJOR)
     , "Maximum number of accounts to scan"
     )
    (
     "max-address"
     , po::value<uint32_t>(&max_address)
     ->default_value(config::lol::SUBADDRESS_LOOKAHEAD_MINOR)
     , "Maximum number of addresses to scan"
     )
    (
     "ip"
     , po::value<std::string>(&rpc_bind_ip)
     ->default_value(std::string(config::lol::WALLET_RPC_DEFAULT_IP))
     , "RPC bind IP"
     )
    (
     "port"
     , po::value<uint16_t>(&rpc_port)
     ->default_value(config::lol::WALLET_RPC_DEFAULT_PORT)
     , "RPC port"
     )
    (
     "daemon-host"
     , po::value<std::string>(&daemon_host)
     ->default_value
     (std::string(config::lol::DAEMON_RPC_DEFAULT_IP))
     , "Lolnero daemon host"
     )
    (
     "daemon-port"
     , po::value<uint16_t>(&daemon_port)
     ->default_value(cryptonote::mainnet.RPC_DEFAULT_PORT)
     , "Lolnero daemon port"
     )
    ("testnet", "")
    ;

  po::options_description cmdline_options;
  cmdline_options
    .add(command_options)
    .add(setting_options)
    ;

  po::variables_map vm;

  std::string parsed_spend_key_file;

  try {
    po::store
      (po::parse_command_line(argc, argv, cmdline_options), vm);
    po::notify(vm);
  }
  catch (std::exception& e) {
    std::cout << e.what() << std::endl;
    return 1;
  }

  if (vm.count("help")) {
    std::cout << cmdline_options << "\n";
    return 0;
  }

  if (vm.count("version")) {
    std::cout << cryptonote::get_version_string() << std::endl;
    return 0;
  }

  const bool testnet = vm.count("testnet");

  epee::set_log_level(log_level);

  if (vm.count("open")) {
    const std::string spend_key_file = vm["open"].as<std::string>();
    parsed_spend_key_file = spend_key_file;
  }

  else {
    std::cout << cmdline_options << "\n";
    return 1;
  }

  LOG_GLOBAL(cryptonote::get_version_string());
  LOG_VERBOSE("Using spend key file: " + parsed_spend_key_file);

  cryptonote::open_wallet
    (
     parsed_spend_key_file
     , scan_from_block_index
     , max_account
     , max_address
     , rpc_port
     , rpc_bind_ip
     , daemon_host
     , daemon_port
     , testnet
     );

  return 0;
}
