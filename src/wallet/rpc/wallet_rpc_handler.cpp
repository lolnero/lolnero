/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Parts of this file are originally
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/


#include "wallet_rpc_handler.hpp"

#include "wallet/logic/functional/fee.hpp"
#include "wallet/logic/functional/wallet.hpp"

#include "math/consensus/consensus.hpp"

#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>

namespace
{
  //------------------------------------------------------------------------------------------------------------------------------
  void set_confirmations
  (
   cryptonote::wallet_rpc::generic_tx_entry &entry
   , uint64_t blockchain_height
   )
  {
    if (entry.block_index + 1 >= blockchain_height
        || (entry.block_index + 1 == 0 &&
            (!strcmp(entry.type.c_str(), "pending") || !strcmp(entry.type.c_str(), "pool"))))
      entry.confirmations = 0;
    else
      entry.confirmations = blockchain_height - (entry.block_index + 1);

    constexpr auto block_reward = consensus::get_block_reward();
    entry.suggested_confirmations_threshold = (entry.amount + block_reward - 1) / block_reward;
  }
}

namespace cryptonote
{
  //------------------------------------------------------------------------------------------------------------------------------
  wallet_rpc_handler::wallet_rpc_handler(std::unique_ptr<tools::wallet2> x)
    :
    m_wallet(std::move(x))
  {
  }
  //------------------------------------------------------------------------------------------------------------------------------
  wallet_rpc_handler::~wallet_rpc_handler()
  {
  }

  //------------------------------------------------------------------------------------------------------------------------------
  bool wallet_rpc_handler::not_open(epee::json_rpc::json_error& er)
  {
      er.code = WALLET_RPC_ERROR_CODE_NOT_OPEN;
      er.message = "No wallet file";
      return false;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  void wallet_rpc_handler::fill_generic_tx_entry(wallet_rpc::generic_tx_entry &entry, const crypto::hash &txid, const wallet::logic::type::payment::received_tx_details_2  &pd)
  {
    entry.txid = pd.m_tx_hash;
    entry.block_index = pd.m_block_index;
    entry.timestamp = pd.m_timestamp;
    entry.amount = pd.m_amount;
    entry.amounts = pd.m_amounts;
    entry.locked = false;
    entry.fee = pd.m_fee;
    entry.type = pd.m_coinbase ? "block" : "in";
    entry.subaddr_index = pd.m_subaddr_index;
    entry.subaddr_indices.push_back(pd.m_subaddr_index);
    entry.address = m_wallet->get_subaddress_as_str(pd.m_subaddr_index);
    set_confirmations(entry, m_wallet->get_blockchain_size());
  }
  //------------------------------------------------------------------------------------------------------------------------------
  void wallet_rpc_handler::fill_generic_tx_entry(wallet_rpc::generic_tx_entry &entry, const crypto::hash &txid, const wallet::logic::type::transfer::mined_sent_tx_details &pd)
  {
    entry.txid = txid;
    entry.block_index = pd.m_block_index;
    entry.timestamp = pd.m_timestamp;
    entry.locked = false;
    entry.fee = pd.m_amount_in - pd.m_amount_out;
    uint64_t change = pd.m_change == (uint64_t)-1 ? 0 : pd.m_change; // change may not be known
    entry.amount = pd.m_amount_in - change - entry.fee;

    for (const auto &d: pd.m_dests) {
      entry.destinations.push_back(wallet_rpc::tx_construction_destination());
      wallet_rpc::tx_construction_destination &td = entry.destinations.back();
      td.amount = d.amount;
      td.address = d.address(m_wallet->m_nettype);
    }

    entry.type = "out";
    entry.subaddr_index = { pd.m_subaddr_account, 0 };
    for (uint32_t i: pd.m_subaddr_indices)
      entry.subaddr_indices.push_back({pd.m_subaddr_account, i});
    entry.address = m_wallet->get_subaddress_as_str({pd.m_subaddr_account, 0});
    set_confirmations(entry, m_wallet->get_blockchain_size());
  }
  //------------------------------------------------------------------------------------------------------------------------------
  void wallet_rpc_handler::fill_generic_tx_entry(wallet_rpc::generic_tx_entry &entry, const crypto::hash &txid, const wallet::logic::type::transfer::un_mined_sent_tx_details &pd)
  {
    bool is_failed = pd.m_state == wallet::logic::type::transfer::un_mined_sent_tx_details::failed;
    entry.txid = txid;
    entry.block_index = 0;
    entry.timestamp = pd.m_timestamp;
    entry.fee = pd.m_amount_in - pd.m_amount_out;
    entry.amount = pd.m_amount_in - pd.m_change - entry.fee;
    entry.locked = true;

    for (const auto &d: pd.m_dests) {
      entry.destinations.push_back(wallet_rpc::tx_construction_destination());
      wallet_rpc::tx_construction_destination &td = entry.destinations.back();
      td.amount = d.amount;
      td.address = d.address(m_wallet->m_nettype);
    }

    entry.type = is_failed ? "failed" : "pending";
    entry.subaddr_index = { pd.m_subaddr_account, 0 };
    for (uint32_t i: pd.m_subaddr_indices)
      entry.subaddr_indices.push_back({pd.m_subaddr_account, i});
    entry.address = m_wallet->get_subaddress_as_str({pd.m_subaddr_account, 0});
    set_confirmations(entry, m_wallet->get_blockchain_size());
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool wallet_rpc_handler::on_get_balance(const wallet_rpc::COMMAND_RPC_GET_BALANCE::request& req, wallet_rpc::COMMAND_RPC_GET_BALANCE::response& res, epee::json_rpc::json_error& er)
  {
    if (!m_wallet) return not_open(er);
    try
    {
      res.balance = req.all_accounts ? m_wallet->balance_all() : m_wallet->balance(req.account_index);
      res.unlocked_balance = req.all_accounts ? m_wallet->unlocked_balance_all() : m_wallet->unlocked_balance(req.account_index);
      std::map<uint32_t, std::map<uint32_t, uint64_t>> balance_per_subaddress_per_account;
      std::map<uint32_t, std::map<uint32_t, std::pair<uint64_t, std::pair<uint64_t, uint64_t>>>> unlocked_balance_per_subaddress_per_account;
      if (req.all_accounts)
      {
        for (uint32_t account_index = 0; account_index < m_wallet->get_num_subaddress_accounts(); ++account_index)
        {
          balance_per_subaddress_per_account[account_index] = m_wallet->balance_per_subaddress(account_index);
          unlocked_balance_per_subaddress_per_account[account_index] = m_wallet->unlocked_balance_per_subaddress(account_index);
        }
      }
      else
      {
        balance_per_subaddress_per_account[req.account_index] = m_wallet->balance_per_subaddress(req.account_index);
        unlocked_balance_per_subaddress_per_account[req.account_index] = m_wallet->unlocked_balance_per_subaddress(req.account_index);
      }
      const std::vector<wallet::logic::type::transfer::received_tx_details> transfers = m_wallet->get_txs();
      for (const auto& p : balance_per_subaddress_per_account)
      {
        uint32_t account_index = p.first;
        std::map<uint32_t, uint64_t> balance_per_subaddress = p.second;
        std::map<uint32_t, std::pair<uint64_t, std::pair<uint64_t, uint64_t>>> unlocked_balance_per_subaddress = unlocked_balance_per_subaddress_per_account[account_index];
        std::set<uint32_t> address_indices;
        if (!req.all_accounts && !req.address_indices.empty())
        {
          address_indices = req.address_indices;
        }
        else
        {
          for (const auto& i : balance_per_subaddress)
            address_indices.insert(i.first);
        }
        for (uint32_t i : address_indices)
        {
          wallet_rpc::COMMAND_RPC_GET_BALANCE::per_subaddress_info info;
          info.account_index = account_index;
          info.address_index = i;
          cryptonote::subaddress_index index = {info.account_index, info.address_index};
          info.address = m_wallet->get_subaddress_as_str(index);
          info.balance = balance_per_subaddress[i];
          info.unlocked_balance = unlocked_balance_per_subaddress[i].first;
          info.num_unspent_outputs = std::count_if
            (
             transfers.begin()
             , transfers.end()
             , [&](const wallet::logic::type::transfer::received_tx_details& td) {
               return !td.is_spent() && td.m_subaddr_index == index;
             }
             );
          res.per_subaddress.emplace_back(info);
        }
      }
    }
    catch (const std::exception& e)
    {
      handle_rpc_exception(std::current_exception(), er, WALLET_RPC_ERROR_CODE_UNKNOWN_ERROR);
      return false;
    }
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool wallet_rpc_handler::on_get_addresses(const wallet_rpc::COMMAND_RPC_GET_ADDRESS::request& req, wallet_rpc::COMMAND_RPC_GET_ADDRESS::response& res, epee::json_rpc::json_error& er)
  {
    if (!m_wallet) return not_open(er);
    try
    {
      THROW_WALLET_EXCEPTION_IF(req.account_index >= m_wallet->get_num_subaddress_accounts(), error::account_index_outofbound);
      res.addresses.clear();
      std::vector<uint32_t> req_address_indices;
      if (req.address_indices.empty())
      {
        for (uint32_t i = 0; i < m_wallet->get_num_subaddresses(req.account_index); ++i)
          req_address_indices.push_back(i);
      }
      else
      {
        req_address_indices = req.address_indices;
      }
      const wallet::logic::type::wallet::received_tx_details_container transfers = m_wallet->get_txs();
      for (uint32_t i : req_address_indices)
      {
        THROW_WALLET_EXCEPTION_IF(i >= m_wallet->get_num_subaddresses(req.account_index), error::address_index_outofbound);
        res.addresses.resize(res.addresses.size() + 1);
        auto& info = res.addresses.back();
        const cryptonote::subaddress_index index = {req.account_index, i};
        info.address = m_wallet->get_subaddress_as_str(index);
        info.address_index = index.minor;
        info.used = std::find_if(transfers.begin(), transfers.end(), [&](const wallet::logic::type::transfer::received_tx_details& td) { return td.m_subaddr_index == index; }) != transfers.end();
      }
      res.address = m_wallet->get_subaddress_as_str({req.account_index, 0});
    }
    catch (const std::exception& e)
    {
      handle_rpc_exception(std::current_exception(), er, WALLET_RPC_ERROR_CODE_UNKNOWN_ERROR);
      return false;
    }
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool wallet_rpc_handler::on_get_address_index(const wallet_rpc::COMMAND_RPC_GET_ADDRESS_INDEX::request& req, wallet_rpc::COMMAND_RPC_GET_ADDRESS_INDEX::response& res, epee::json_rpc::json_error& er)
  {
    if (!m_wallet) return not_open(er);
    const auto maybe_info =
      get_account_address_from_str(m_wallet->m_nettype, req.address);

    if(!maybe_info)
    {
      er.code = WALLET_RPC_ERROR_CODE_WRONG_ADDRESS;
      er.message = "Invalid address";
      return false;
    }
    auto index = m_wallet->get_subaddress_index(maybe_info->address);
    if (!index)
    {
      er.code = WALLET_RPC_ERROR_CODE_WRONG_ADDRESS;
      er.message = "Address doesn't belong to the wallet";
      return false;
    }
    res.index = *index;
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool wallet_rpc_handler::on_create_address(const wallet_rpc::COMMAND_RPC_CREATE_ADDRESS::request& req, wallet_rpc::COMMAND_RPC_CREATE_ADDRESS::response& res, epee::json_rpc::json_error& er)
  {
    if (!m_wallet) return not_open(er);
    try
    {
      if (req.count < 1 || req.count > 64) {
        er.code = WALLET_RPC_ERROR_CODE_UNKNOWN_ERROR;
        er.message = "Count must be between 1 and 64.";
        return false;
      }

      std::vector<std::string> addresses;
      std::vector<uint32_t>    address_indices;

      addresses.reserve(req.count);
      address_indices.reserve(req.count);

      for (uint32_t i = 0; i < req.count; i++) {
        m_wallet->add_subaddress(req.account_index, {});
        uint32_t new_address_index = m_wallet->get_num_subaddresses(req.account_index) - 1;
        address_indices.push_back(new_address_index);
        addresses.push_back(m_wallet->get_subaddress_as_str({req.account_index, new_address_index}));
      }

      res.address = addresses[0];
      res.address_index = address_indices[0];
      res.addresses = addresses;
      res.address_indices = address_indices;
    }
    catch (const std::exception& e)
    {
      handle_rpc_exception(std::current_exception(), er, WALLET_RPC_ERROR_CODE_UNKNOWN_ERROR);
      return false;
    }
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool wallet_rpc_handler::on_get_accounts(const wallet_rpc::COMMAND_RPC_GET_ACCOUNTS::request& req, wallet_rpc::COMMAND_RPC_GET_ACCOUNTS::response& res, epee::json_rpc::json_error& er)
  {
    if (!m_wallet) return not_open(er);
    try
    {
      res.total_balance = 0;
      res.total_unlocked_balance = 0;
      cryptonote::subaddress_index subaddr_index = {0,0};
      for (; subaddr_index.major < m_wallet->get_num_subaddress_accounts(); ++subaddr_index.major)
      {
        wallet_rpc::COMMAND_RPC_GET_ACCOUNTS::subaddress_account_info info;
        info.account_index = subaddr_index.major;
        info.base_address = m_wallet->get_subaddress_as_str(subaddr_index);
        info.balance = m_wallet->balance(subaddr_index.major);
        info.unlocked_balance = m_wallet->unlocked_balance(subaddr_index.major);
        res.subaddress_accounts.push_back(info);
        res.total_balance += info.balance;
        res.total_unlocked_balance += info.unlocked_balance;
      }
    }
    catch (const std::exception& e)
    {
      handle_rpc_exception(std::current_exception(), er, WALLET_RPC_ERROR_CODE_UNKNOWN_ERROR);
      return false;
    }
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool wallet_rpc_handler::on_create_account(const wallet_rpc::COMMAND_RPC_CREATE_ACCOUNT::request& req, wallet_rpc::COMMAND_RPC_CREATE_ACCOUNT::response& res, epee::json_rpc::json_error& er)
  {
    if (!m_wallet) return not_open(er);
    try
    {
      m_wallet->add_subaddress_account({});
      res.account_index = m_wallet->get_num_subaddress_accounts() - 1;
      res.address = m_wallet->get_subaddress_as_str({res.account_index, 0});
    }
    catch (const std::exception& e)
    {
      handle_rpc_exception(std::current_exception(), er, WALLET_RPC_ERROR_CODE_UNKNOWN_ERROR);
      return false;
    }
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool wallet_rpc_handler::on_get_top_block_index
  (
   const wallet_rpc::COMMAND_RPC_GET_TOP_BLOCK_INDEX::request& req
   , wallet_rpc::COMMAND_RPC_GET_TOP_BLOCK_INDEX::response& res
   , epee::json_rpc::json_error& er
   )
  {
    if (!m_wallet) return not_open(er);
    try
    {
      res.top_block_index =
        m_wallet->get_top_block_index();
    }
    catch (const std::exception& e)
    {
      handle_rpc_exception(std::current_exception(), er, WALLET_RPC_ERROR_CODE_UNKNOWN_ERROR);
      return false;
    }
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool wallet_rpc_handler::validate_transfer(const std::list<wallet_rpc::tx_construction_destination>& destinations, std::vector<cryptonote::tx_destination_entry>& dsts, std::vector<uint8_t>& extra, bool at_least_one_destination, epee::json_rpc::json_error& er)
  {
    std::string extra_nonce;
    for (auto it = destinations.begin(); it != destinations.end(); it++)
    {
      const auto maybe_info =
        get_account_address_from_str(m_wallet->m_nettype, it->address);

      cryptonote::tx_destination_entry de;
      er.message = "";
      if(!maybe_info)
      {
        er.code = WALLET_RPC_ERROR_CODE_WRONG_ADDRESS;
        if (er.message.empty())
          er.message = std::string("WALLET_RPC_ERROR_CODE_WRONG_ADDRESS: ") + it->address;
        return false;
      }

      const auto info = *maybe_info;

      de.original = it->address;
      de.addr = info.address;
      de.is_subaddress = info.is_subaddress;
      de.amount = it->amount;
      dsts.push_back(de);
    }

    if (at_least_one_destination && dsts.empty())
    {
      er.code = WALLET_RPC_ERROR_CODE_ZERO_DESTINATION;
      er.message = "No destinations for this transfer";
      return false;
    }

    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  template<typename T> static bool is_error_value(const T &val) { return false; }
  static bool is_error_value(const std::string &s) { return s.empty(); }
  //------------------------------------------------------------------------------------------------------------------------------
  template<typename T, typename V>
  static bool fill(T &where, const V s)
  {
    if (is_error_value(s)) return false;
    where = s;
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  static uint64_t total_amount(const wallet::logic::type::tx::pending_sent_tx &ptx)
  {
    uint64_t amount = 0;
    for (const auto &dest: ptx.dests) amount += dest.amount;
    return amount;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool wallet_rpc_handler::on_transfer(const wallet_rpc::COMMAND_RPC_TRANSFER::request& req, wallet_rpc::COMMAND_RPC_TRANSFER::response& res, epee::json_rpc::json_error& er)
  {

    std::vector<cryptonote::tx_destination_entry> dsts;
    std::vector<uint8_t> extra;

    LOG_PRINT_L3("on_transfer starts");
    if (!m_wallet) return not_open(er);

    // validate the transfer requested and populate dsts & extra
    if (!validate_transfer(req.destinations, dsts, extra, true, er))
    {
      return false;
    }

    try
    {
      const uint32_t priority = req.priority;
      std::vector<wallet::logic::type::tx::pending_sent_tx> ptx_vector = m_wallet->create_transactions(dsts, config::lol::mixin, priority, extra, req.account_index, req.subaddr_indices);

      if (ptx_vector.empty())
      {
        er.code = WALLET_RPC_ERROR_CODE_TX_NOT_POSSIBLE;
        er.message = "No transaction created";
        return false;
      }

      // reject proposed transactions if there are more than one.  see on_transfer_split below.
      if (ptx_vector.size() != 1)
      {
        er.code = WALLET_RPC_ERROR_CODE_TX_TOO_LARGE;
        er.message = "Transaction would be too large.  try /transfer_split.";
        return false;
      }

      for (const auto & ptx : ptx_vector)
        {
          std::transform
            (
             ptx.output_secret_keys.begin()
             , ptx.output_secret_keys.end()
             , std::back_inserter(res.output_ecdh_secret_keys)
             , [](const auto& x) { return x; }
             );

          // Compute amount leaving wallet in tx. By convention dests does not include change outputs
          fill(res.amount, total_amount(ptx));
          fill(res.fee, ptx.fee);
          fill(res.weight, cryptonote::get_transaction_size(ptx.tx));
        }

      for (const auto& x: ptx_vector) {
        m_wallet->commit_tx(x);
      }

      // populate response with tx hashes
      for (auto & ptx : ptx_vector)
      {
        bool r = fill(res.tx_hash, cryptonote::get_transaction_hash(ptx.tx));
        r = r && (!req.get_tx_hex || fill(res.tx_blob, epee::string_tools::buff_to_hex_nodelimer(tx_to_blob(ptx.tx))));
        if (!r)
        {
          er.code = WALLET_RPC_ERROR_CODE_UNKNOWN_ERROR;
          er.message = "Failed to save tx info";
          return false;
        }
      }
    }
    catch (const std::exception& e)
    {
      handle_rpc_exception(std::current_exception(), er, WALLET_RPC_ERROR_CODE_GENERIC_TRANSFER_ERROR);
      return false;
    }
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool wallet_rpc_handler::on_get_txs(const wallet_rpc::COMMAND_RPC_GET_TXS::request& req, wallet_rpc::COMMAND_RPC_GET_TXS::response& res, epee::json_rpc::json_error& er)
  {
    if (!m_wallet) return not_open(er);

    uint64_t min_height = 0, max_height = CRYPTONOTE_MAX_BLOCK_NUMBER;
    if (req.filter_by_height)
    {
      min_height = req.min_height;
      max_height = req.max_height <= max_height ? req.max_height : max_height;
    }

    std::optional<uint32_t> account_index = req.account_index;
    std::set<uint32_t> subaddr_indices = req.subaddr_indices;
    if (req.all_accounts)
    {
      account_index = std::nullopt;
      subaddr_indices.clear();
    }

    if (req.in)
    {
      const auto received_txs =
        m_wallet->get_received_txs
        (min_height, max_height, account_index, subaddr_indices);

      for (const auto& x: received_txs) {
        res.in.push_back(wallet_rpc::generic_tx_entry());
        fill_generic_tx_entry(res.in.back(), x.m_tx_hash, x);
      }
    }

    if (req.out)
    {
      const auto sent_txs =
        m_wallet->get_sent_txs
        (min_height, max_height, account_index, subaddr_indices);

      for (const auto& x: sent_txs) {
        res.out.push_back(wallet_rpc::generic_tx_entry());
        fill_generic_tx_entry(res.out.back(), x.first, x.second);
      }
    }

    if (req.pending || req.failed) {
      const auto pending_sent_txs =
        m_wallet->get_pending_sent_txs
        (account_index, subaddr_indices);

      for (const auto& x: pending_sent_txs) {
        const auto &pd = x.second;
        const bool is_failed = pd.m_state == wallet::logic::type::transfer::un_mined_sent_tx_details::failed;

        if (!((req.failed && is_failed) || (!is_failed && req.pending)))
          continue;

        std::list<wallet_rpc::generic_tx_entry> &entries = is_failed ? res.failed : res.pending;

        entries.push_back(wallet_rpc::generic_tx_entry());
        fill_generic_tx_entry(entries.back(), x.first, x.second);
      }
    }

    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool wallet_rpc_handler::on_get_tx_by_txid(const wallet_rpc::COMMAND_RPC_GET_TX_BY_ID::request& req, wallet_rpc::COMMAND_RPC_GET_TX_BY_ID::response& res, epee::json_rpc::json_error& er)
  {
    if (!m_wallet) return not_open(er);

    const crypto::hash txid = req.txid;

    if(txid == crypto::null_hash)
    {
      er.code = WALLET_RPC_ERROR_CODE_WRONG_TXID;
      er.message = "Transaction ID has invalid format";
      return false;
    }

    if (req.account_index >= m_wallet->get_num_subaddress_accounts())
    {
      er.code = WALLET_RPC_ERROR_CODE_ACCOUNT_INDEX_OUT_OF_BOUNDS;
      er.message = "Account index is out of bound";
      return false;
    }

    const auto received_txs =
      m_wallet->get_received_txs
      (0, (uint64_t)-1, req.account_index);

    for (const auto& x: received_txs) {
      if (x.m_tx_hash == txid)
      {
        res.transfers.resize(res.transfers.size() + 1);
        fill_generic_tx_entry
          (res.transfers.back(), x.m_tx_hash, x);
      }
    }

    const auto sent_txs =
      m_wallet->get_sent_txs
      (0, (uint64_t)-1, req.account_index);

    for (const auto& x: sent_txs) {
      if (x.first == txid)
      {
        res.transfers.resize(res.transfers.size() + 1);
        fill_generic_tx_entry(res.transfers.back(), x.first, x.second);
      }
    }

    const auto pending_sent_txs =
      m_wallet->get_pending_sent_txs
      (req.account_index);

    for (const auto& x: pending_sent_txs) {
      if (x.first == txid)
      {
        res.transfers.resize(res.transfers.size() + 1);
        fill_generic_tx_entry(res.transfers.back(), x.first, x.second);
      }
    }

    if (!res.transfers.empty())
    {
      res.transfer = res.transfers.front(); // backward compat
      return true;
    }

    er.code = WALLET_RPC_ERROR_CODE_WRONG_TXID;
    er.message = "Transaction not found.";
    return false;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool wallet_rpc_handler::on_refresh(const wallet_rpc::COMMAND_RPC_REFRESH::request& req, wallet_rpc::COMMAND_RPC_REFRESH::response& res, epee::json_rpc::json_error& er)
  {
    if (!m_wallet) return not_open(er);
    try
    {
      std::tie
        (
         res.blocks_fetched
         , res.received_money
         ) = m_wallet->refresh();
      return true;
    }
    catch (const std::exception& e)
    {
      handle_rpc_exception(std::current_exception(), er, WALLET_RPC_ERROR_CODE_UNKNOWN_ERROR);
      return false;
    }
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  void wallet_rpc_handler::handle_rpc_exception(const std::exception_ptr& e, epee::json_rpc::json_error& er, int default_error_code) {
    try
    {
      std::rethrow_exception(e);
    }
    catch (const tools::error::no_connection_to_daemon& e)
    {
      er.code = WALLET_RPC_ERROR_CODE_NO_DAEMON_CONNECTION;
      er.message = e.what();
    }
    catch (const tools::error::daemon_busy& e)
    {
      er.code = WALLET_RPC_ERROR_CODE_DAEMON_IS_BUSY;
      er.message = e.what();
    }
    catch (const tools::error::zero_destination& e)
    {
      er.code = WALLET_RPC_ERROR_CODE_ZERO_DESTINATION;
      er.message = e.what();
    }
    catch (const tools::error::not_enough_money& e)
    {
      er.code = WALLET_RPC_ERROR_CODE_NOT_ENOUGH_MONEY;
      er.message = e.what();
    }
    catch (const tools::error::not_enough_unlocked_money& e)
    {
      er.code = WALLET_RPC_ERROR_CODE_NOT_ENOUGH_UNLOCKED_MONEY;
      er.message = e.what();
    }
    catch (const tools::error::tx_not_possible& e)
    {
      er.code = WALLET_RPC_ERROR_CODE_TX_NOT_POSSIBLE;
      er.message =
        (
         boost::format("Transaction not possible. Available only %s, transaction amount %s = %s + %s (fee)") %
         cryptonote::print_money(e.available()) %
         cryptonote::print_money(e.tx_amount() + e.fee())  %
         cryptonote::print_money(e.tx_amount()) %
         cryptonote::print_money(e.fee())
         ).str();
      er.message = e.what();
    }
    catch (const error::file_exists& e)
    {
      er.code = WALLET_RPC_ERROR_CODE_WALLET_ALREADY_EXISTS;
      er.message = "Cannot create wallet. Already exists.";
    }
    catch (const error::account_index_outofbound& e)
    {
      er.code = WALLET_RPC_ERROR_CODE_ACCOUNT_INDEX_OUT_OF_BOUNDS;
      er.message = e.what();
    }
    catch (const error::address_index_outofbound& e)
    {
      er.code = WALLET_RPC_ERROR_CODE_ADDRESS_INDEX_OUT_OF_BOUNDS;
      er.message = e.what();
    }
    catch (const error::signature_check_failed& e)
    {
        er.code = WALLET_RPC_ERROR_CODE_WRONG_SIGNATURE;
        er.message = e.what();
    }
    catch (const std::exception& e)
    {
      er.code = default_error_code;
      er.message = e.what();
    }
    catch (...)
    {
      er.code = WALLET_RPC_ERROR_CODE_UNKNOWN_ERROR;
      er.message = "WALLET_RPC_ERROR_CODE_UNKNOWN_ERROR";
    }
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool wallet_rpc_handler::on_validate_address(const wallet_rpc::COMMAND_RPC_VALIDATE_ADDRESS::request& req, wallet_rpc::COMMAND_RPC_VALIDATE_ADDRESS::response& res, epee::json_rpc::json_error& er)
  {
    static const struct { cryptonote::network_type type; const char *stype; } net_types[] = {
      { cryptonote::MAINNET, "mainnet" },
      { cryptonote::TESTNET, "testnet" },
    };
    if (!req.any_net_type && !m_wallet) return not_open(er);
    for (const auto &net_type: net_types)
    {
      if (!req.any_net_type
          && (!m_wallet || net_type.type != m_wallet->m_nettype)
          ) {
        continue;
      }

      const auto maybe_info =
        cryptonote::get_account_address_from_str
        (net_type.type, req.address);

      if (maybe_info)
      {
        res.subaddress = maybe_info->is_subaddress;
        res.nettype = net_type.stype;
        res.valid = true;

        return true;
      }
    }

    res.valid = false;
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------------
  bool wallet_rpc_handler::on_get_version(const wallet_rpc::COMMAND_RPC_GET_VERSION::request& req, wallet_rpc::COMMAND_RPC_GET_VERSION::response& res, epee::json_rpc::json_error& er)
  {
    res.version = WALLET_RPC_VERSION;
    return true;
  }

}
