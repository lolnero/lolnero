/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#pragma once

#include <span>
#include <vector>
#include <algorithm>
#include <ranges>

namespace epee
{

namespace algorithm
{
  // need copy by value, since std::nth_element
  // will modify the element in place
  inline std::optional<uint64_t> median(const std::vector<uint64_t> v_in)
  {
    auto v = v_in;
    std::ranges::sort(v);

    if(v.empty())
      return {};

    if(v.size() == 1)
      return v.front();

    const size_t n = v.size() / 2;

    std::nth_element(v.begin(), std::next(v.begin(), n), v.end());
    return v[n];
  }

}

}
