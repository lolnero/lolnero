/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "base64.hpp"

#include "boost/beast/core/detail/base64.hpp"
#include "tools/epee/include/logging.hpp"

namespace epee
{
namespace base64
{
  std::string encode_to_base64
  (const std::span<const std::uint8_t> src) {

  const size_t base64_size =
    boost::beast::detail::base64::encoded_size(src.size());

    std::string base64_str;
    base64_str.resize(base64_size);

    const auto written =
      boost::beast::detail::base64::encode
      (
       base64_str.data()
       , src.data()
       , src.size()
       );

    base64_str.resize(written);

    // LOG_DEBUG("encoded base64: " + base64_str);

    return base64_str;

  }

  std::optional<epee::blob::data> decode_from_base64_to_blob
  (const std::string_view src) {
    // LOG_DEBUG("decoding base64: " + std::string(src));

    const auto chars_expected =
      boost::beast::detail::base64::decoded_size(src.size());

    epee::blob::data x;
    x.resize(chars_expected);

    const auto [written, read] =
      boost::beast::detail::base64::decode
      (
       x.data()
       , src.data()
       , src.size()
       );

    x.resize(written);

    return x;
  }

}
}
