/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#pragma once

#include "tools/serialization/json.hpp"

namespace epee
{
  namespace json_rpc
  {
    template<typename t_param>
    struct request
    {
      std::string jsonrpc;
      std::string method;
      uint64_t id;
      t_param     params;
    };

    template<typename t_param>
    void to_json(json& j, const request<t_param>& x) {
      j = json
        {
          {"jsonrpc", x.jsonrpc}
          , {"method", x.method}
          , {"id", x.id}
          , {"params", x.params}
        };
    }

    template<typename t_param>
    void from_json(const json& j, request<t_param>& x) {
      j.at("jsonrpc").get_to(x.jsonrpc);
      j.at("method").get_to(x.method);
      j.at("id").get_to(x.id);
      j.at("params").get_to(x.params);
    }


    struct json_error
    {
      int64_t code;
      std::string message;
    };

    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE
    (
     json_error
     , code
     , message
     );

    struct dummy_error
    {
    };

    template<typename t_param, typename t_error>
    struct response
    {
      std::string jsonrpc;
      t_param     result;
      uint64_t id;
      t_error     error;
    };

    template<typename t_param>
    struct ok_response
    {
      std::string jsonrpc;
      t_param     result;
      uint64_t id;
    };

    template<typename t_param>
    void to_json(json& j, const ok_response<t_param>& x) {
      j = json
        {
          {"jsonrpc", x.jsonrpc}
          , {"result", x.result}
          , {"id", x.id}
        };
    }

    template<typename t_param>
    void from_json(const json& j, ok_response<t_param>& x) {
      j.at("jsonrpc").get_to(x.jsonrpc);
      j.at("result").get_to(x.result);
      j.at("id").get_to(x.id);
    }

    struct error_response
    {
      std::string jsonrpc;
      json_error     error;
      uint64_t id;
    };

    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE
    (
     error_response
     , jsonrpc
     , error
     , id
     );
  }
}
