/**
@file
@author from CrypoNote (see copyright below; Andrey N. Sabelnikov)
@monero rfree
@brief the connection templated-class for one peer connection
*/
// Copyright (c) 2006-2013, Andrey N. Sabelnikov, www.sabelnikov.net
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// * Neither the name of the Andrey N. Sabelnikov nor the
// names of its contributors may be used to endorse or promote products
// derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER  BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


#pragma once




#include <boost/uuid/random_generator.hpp>





#define AGGRESSIVE_TIMEOUT_THRESHOLD 120 // sockets
#define NEW_CONNECTION_TIMEOUT_LOCAL 1200000 // 2 minutes
#define NEW_CONNECTION_TIMEOUT_REMOTE 10000 // 10 seconds
#define DEFAULT_TIMEOUT_MS_LOCAL 1800000 // 30 minutes
#define DEFAULT_TIMEOUT_MS_REMOTE 300000 // 5 minutes
#define TIMEOUT_EXTRA_MS_PER_BYTE 0.2


namespace epee
{
namespace net_utils
{
  template<typename T>
  T& check_and_get(std::shared_ptr<T>& ptr)
  {
    LOG_ERROR_AND_THROW_UNLESS(bool(ptr), "shared_state cannot be null");
    return *ptr;
  }

  /************************************************************************/
  /*                                                                      */
  /************************************************************************/
  template<class t_protocol_handler>
  connection<t_protocol_handler>::connection( boost::asio::io_service& io_service,
                std::shared_ptr<shared_state> state,
		t_connection_type connection_type
	)
	: connection(boost::asio::ip::tcp::socket{io_service}, std::move(state), connection_type)
  {
  }

  template<class t_protocol_handler>
  connection<t_protocol_handler>::connection( boost::asio::ip::tcp::socket&& sock,
                std::shared_ptr<shared_state> state,
		t_connection_type connection_type
	)
	:
		connection_basic(std::move(sock), state),
		m_protocol_handler(this, check_and_get(state), context),
		m_connection_type( connection_type ),
		m_timer(GET_IO_SERVICE(socket_)),
		m_local(false),
		m_ready_to_close(false)
  {
    LOG_DEBUG_MUTE
      (
       "test, connection constructor set m_connection_type="
       + m_connection_type
       );
  }

  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  connection<t_protocol_handler>::~connection() noexcept(false)
  {
    if(!m_was_shutdown)
    {
      _dbg3
      (
      "[sock "
      + std::to_string(socket().native_handle())
      + "] Socket destroyed without shutdown."
      );
      shutdown();
    }
    else {
        _dbg3
        (
        "[sock "
        + std::to_string(socket().native_handle())
        + "] Socket destroyed"
        );
    }
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  std::shared_ptr<connection<t_protocol_handler> > connection<t_protocol_handler>::safe_shared_from_this()
  {
    try
    {
      return connection<t_protocol_handler>::shared_from_this();
    }
    catch (const boost::bad_weak_ptr&)
    {
      // It happens when the connection is being deleted
      return std::shared_ptr<connection<t_protocol_handler> >();
    }
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  bool connection<t_protocol_handler>::start(bool is_income, bool is_multithreaded)
  {
    TRY_ENTRY();

    boost::system::error_code ec;
    auto remote_ep = socket().remote_endpoint(ec);
    ASSERT_OR_LOG_AND_RETURN
      (
       LogLevel::Warning
       , !ec
       , false
       , std::string()
       + "Failed to get remote endpoint: "
       + ec.message()
       + ':'
       + std::to_string(ec.value())
       );
    ASSERT_OR_LOG_AND_RETURN(LogLevel::Warning, remote_ep.address().is_v4() || remote_ep.address().is_v6(), false, "only IPv4 and IPv6 supported here");

    if (remote_ep.address().is_v4())
    {
      const unsigned long ip_ = boost::asio::detail::socket_ops::host_to_network_long(remote_ep.address().to_v4().to_ulong());
      return start(is_income, is_multithreaded, ipv4_network_address{uint32_t(ip_), remote_ep.port()});
    }
    else
    {
      const auto ip_ = remote_ep.address().to_v6();
      return start(is_income, is_multithreaded, ipv6_network_address{ip_, remote_ep.port()});
    }
    CATCH_ENTRY_L0("connection<t_protocol_handler>::start()", false);
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  bool connection<t_protocol_handler>::start(bool is_income, bool is_multithreaded, network_address real_remote)
  {
    TRY_ENTRY();

    // Use safe_shared_from_this, because of this is public method and it can be called on the object being deleted
    auto self = safe_shared_from_this();
    if(!self)
      return false;

    m_is_multithreaded = is_multithreaded;
    m_local = real_remote.is_loopback() || real_remote.is_local();

    // create a random uuid, we don't need crypto strength here
    const boost::uuids::uuid random_uuid = boost::uuids::random_generator()();

    context = t_connection_context{};
    context.set_details(random_uuid, std::move(real_remote), is_income);

    boost::system::error_code ec;
    auto local_ep = socket().local_endpoint(ec);
    ASSERT_OR_LOG_AND_RETURN
      (
       LogLevel::Warning
       , !ec
       , false
       , std::string()
       + "Failed to get local endpoint: "
       + ec.message()
       + ':'
       + std::to_string(ec.value())
       );

    _dbg3
      (
       "[sock "
       + std::to_string(socket_.native_handle())
       + "] new connection from "
       + print_connection_context_short(context)
       + " to "
       + local_ep.address().to_string()
       + ':'
       + std::to_string(local_ep.port())
       + ", total sockets objects "
       + std::to_string(get_state().sock_count)
       );

    if
      (
       static_cast<shared_state&>(get_state())
       .pfilter &&
       !static_cast<shared_state&>(get_state())
       .pfilter->is_remote_host_allowed
       (context.m_remote_address)
       )
    {
      _dbg2
        (
         "[sock "
         + std::to_string(socket().native_handle())
         + "] host denied "
         + context.m_remote_address.host_str()
         + ", shutdowning connection"
         );
      close();
      return false;
    }

    m_host = context.m_remote_address.host_str();
    try { host_count(m_host, 1); } catch(...) { /* ignore */ }

    m_protocol_handler.after_init_connection();

    reset_timer(std::chrono::milliseconds(m_local ? NEW_CONNECTION_TIMEOUT_LOCAL : NEW_CONNECTION_TIMEOUT_REMOTE));

    // first read on the raw socket to detect SSL for the server
    socket().async_read_some
      (
       boost::asio::buffer(buffer_)
       , strand_.wrap
       (
        std::bind_front
        (&connection<t_protocol_handler>::handle_read, self
         )
        )
       );

    boost::asio::ip::tcp::no_delay noDelayOption(false);
    socket().set_option(noDelayOption);

    return true;

    CATCH_ENTRY_L0("connection<t_protocol_handler>::start()", false);
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  bool connection<t_protocol_handler>::request_callback()
  {
    TRY_ENTRY();
    _dbg2
      (
       "["
       + print_connection_context_short(context)
       + "] request_callback"
       );
    // Use safe_shared_from_this, because of this is public method and it can be called on the object being deleted
    auto self = safe_shared_from_this();
    if(!self)
      return false;

    strand_.post(std::bind(&connection<t_protocol_handler>::call_back_starter, self));
    CATCH_ENTRY_L0("connection<t_protocol_handler>::request_callback()", false);
    return true;
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  boost::asio::io_service& connection<t_protocol_handler>::get_io_service()
  {
    return GET_IO_SERVICE(socket());
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  bool connection<t_protocol_handler>::add_ref()
  {
    TRY_ENTRY();

    // Use safe_shared_from_this, because of this is public method and it can be called on the object being deleted
    auto self = safe_shared_from_this();
    if(!self)
      return false;
    //_dbg3("[sock " << socket().native_handle() << "] add_ref, m_peer_number=" << mI->m_peer_number);
    LOCK_RECURSIVE_MUTEX(self->m_self_refs_lock);
    //_dbg3("[sock " << socket().native_handle() << "] add_ref 2, m_peer_number=" << mI->m_peer_number);
    if(m_was_shutdown)
      return false;
    ++m_reference_count;
    m_self_ref = std::move(self);
    return true;
    CATCH_ENTRY_L0("connection<t_protocol_handler>::add_ref()", false);
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  bool connection<t_protocol_handler>::release()
  {
    TRY_ENTRY();
    std::shared_ptr<connection<t_protocol_handler> >  back_connection_copy;
    LOG_TRACE
      (
       context.to_str()
       + "[sock "
       + std::to_string(socket().native_handle())
       + "] release"
       );
    {
      LOCK_RECURSIVE_MUTEX(m_self_refs_lock);
      ASSERT_OR_LOG_ERROR_AND_RETURN
        (
         m_reference_count
         , false
         , "[sock "
         + std::to_string(socket().native_handle())
         + "] m_reference_count already at 0 at connection<t_protocol_handler>::release() call"
         );
      // is this the last reference?
      if (--m_reference_count == 0) {
          // move the held reference to a local variable, keeping the object alive until the function terminates
          std::swap(back_connection_copy, m_self_ref);
      }
    }
    return true;
    CATCH_ENTRY_L0("connection<t_protocol_handler>::release()", false);
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  void connection<t_protocol_handler>::call_back_starter()
  {
    TRY_ENTRY();
    _dbg2
      (
       "["
       + print_connection_context_short(context)
       + "] fired_callback"
       );
    m_protocol_handler.handle_qued_callback();
    CATCH_ENTRY_L0("connection<t_protocol_handler>::call_back_starter()", void());
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  void connection<t_protocol_handler>::save_dbg_log()
  {
    std::string address, port;
    boost::system::error_code e;

    boost::asio::ip::tcp::endpoint endpoint = socket().remote_endpoint(e);
    if (e)
    {
      address = "<not connected>";
      port = "<not connected>";
    }
    else
    {
      address = endpoint.address().to_string();
      port = boost::lexical_cast<std::string>(endpoint.port());
    }
    LOG_DEBUG_MUTE
      (
       " connection type "
       + to_string( m_connection_type )
       + " "
       + socket().local_endpoint().address().to_string()
       + ":"
       + socket().local_endpoint().port()
       + " <--> "
       + context.m_remote_address.str()
       + " (via "
       + address
       + ":"
       + port
       + ")"
       );
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  void connection<t_protocol_handler>::handle_read(const boost::system::error_code& e,
    std::size_t bytes_transferred)
  {
    TRY_ENTRY();
    //_info("[sock " << socket().native_handle() << "] Async read calledback.");

    if (m_was_shutdown)
        return;

    if (!e)
    {
      //_info("[sock " << socket().native_handle() << "] RECV " << bytes_transferred);
      context.m_last_recv = time(NULL);
      context.m_recv_cnt += bytes_transferred;
      m_ready_to_close = false;
      bool recv_res = m_protocol_handler.handle_recv(buffer_.data(), bytes_transferred);
      if(!recv_res)
      {
        //_info("[sock " << socket().native_handle() << "] protocol_want_close");
        //some error in protocol, protocol handler ask to close connection
        m_want_close_connection = true;
        bool do_shutdown = false;
        {
          LOCK_RECURSIVE_MUTEX(m_send_que_lock);
          if(!m_send_que.size())
            do_shutdown = true;
        }
        if(do_shutdown)
          shutdown();
      }else
      {
        reset_timer(get_timeout_from_bytes_read(bytes_transferred));
        socket().async_read_some
          (
           boost::asio::buffer(buffer_)
           , strand_.wrap
           (
            std::bind_front
            (
             &connection<t_protocol_handler>::handle_read
             , connection<t_protocol_handler>::shared_from_this()
             )
            )
           );
        //_info("[sock " << socket().native_handle() << "]Async read requested.");
      }
    }else
    {
      _dbg3
        (
         "[sock "
         + std::to_string(socket().native_handle())
         + "] Some not success at read: "
         + e.message()
         + ':'
         + std::to_string(e.value())
         );
      if(e.value() != 2)
      {
        _dbg3
          (
           "[sock "
           + std::to_string(socket().native_handle())
           + "] Some problems at read: "
           + e.message()
           + ':'
           + std::to_string(e.value())
           );
        shutdown();
      }
      else
      {
        _dbg3
          (
           "[sock "
           + std::to_string(socket().native_handle())
           + "] peer closed connection"
           );
        bool do_shutdown = false;
        {
          LOCK_RECURSIVE_MUTEX(m_send_que_lock);
          if(!m_send_que.size())
            do_shutdown = true;
        }
        if (m_ready_to_close || do_shutdown)
          shutdown();
      }
      m_ready_to_close = true;
    }
    // If an error occurs then no new asynchronous operations are started. This
    // means that all shared_ptr references to the connection object will
    // disappear and the object will be destroyed automatically after this
    // handler returns. The connection class's destructor closes the socket.
    CATCH_ENTRY_L0("connection<t_protocol_handler>::handle_read", void());
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  void connection<t_protocol_handler>::handle_receive(const boost::system::error_code& e,
    std::size_t bytes_transferred)
  {
    TRY_ENTRY();

    if (m_was_shutdown) return;

    if (e)
    {
      // offload the error case
      handle_read(e, bytes_transferred);
      return;
    }

    {
      handle_read(e, bytes_transferred);
      return;
    }

    async_read_some
      (
       boost::asio::buffer(buffer_)
       , strand_.wrap
       (
        std::bind_front
        (
         &connection<t_protocol_handler>::handle_read
         , connection<t_protocol_handler>::shared_from_this()
         )
        )
       );

    // If an error occurs then no new asynchronous operations are started. This
    // means that all shared_ptr references to the connection object will
    // disappear and the object will be destroyed automatically after this
    // handler returns. The connection class's destructor closes the socket.
    CATCH_ENTRY_L0("connection<t_protocol_handler>::handle_receive", void());
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  bool connection<t_protocol_handler>::call_run_once_service_io()
  {
    TRY_ENTRY();
    if(!m_is_multithreaded)
    {
      //single thread model, we can wait in blocked call
      size_t cnt = GET_IO_SERVICE(socket()).run_one();
      if(!cnt)//service is going to quit
        return false;
    }else
    {
      //multi thread model, we can't(!) wait in blocked call
      //so we make non blocking call and releasing CPU by calling sleep(0);
      //if no handlers were called
      //TODO: Maybe we need to have have critical section + event + callback to upper protocol to
      //ask it inside(!) critical region if we still able to go in event wait...
      size_t cnt = GET_IO_SERVICE(socket()).poll_one();
      if(!cnt)
        epee::time_helper::sleep_no_w(1);
    }

    return true;
    CATCH_ENTRY_L0("connection<t_protocol_handler>::call_run_once_service_io", false);
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  bool connection<t_protocol_handler>::do_send(const epee::blob::data chunk)
  {
    TRY_ENTRY();
    // Use safe_shared_from_this, because of this is public method and it can be called on the object being deleted
    auto self = safe_shared_from_this();
    if(!self)
      return false;
    if(m_was_shutdown)
      return false;

    //_info("[sock " << socket().native_handle() << "] SEND " << cb);
    context.m_last_send = time(NULL);
    context.m_send_cnt += chunk.size();
    //some data should be wrote to stream
    //request complete
    // No sleeping here; sleeping is done once and for all in "handle_write"

    m_send_que_lock.lock(); // *** critical ***
    const auto scope_exit_handler = epee::misc_utils::create_scope_leave_handler([&]{m_send_que_lock.unlock();});

    m_send_que.push(chunk);

    if(m_send_que.size() > 1)
    { // active operation should be in progress, nothing to do, just wait last operation callback
        auto size_now = m_send_que.back().size();
        LOG_DEBUG
          (
           "do_send() NOW just queues: packet="
           + std::to_string(size_now)
           + " B, is added to queue-size="
           + std::to_string(m_send_que.size())
           );
        //do_send_handler_delayed( ptr , size_now ); // (((H))) // empty function

      LOG_TRACE
        (
         context.to_str()
         + "[sock "
         + std::to_string(socket().native_handle())
         + "] Async send requested "
         + std::to_string(m_send_que.front().size())
         );
    }
    else
    { // no active operation

        if(m_send_que.size()!=1)
        {
            _erro("Looks like no active operations, but send que size != 1!!");
            return false;
        }

        auto size_now = m_send_que.front().size();
        LOG_DEBUG
          (
           "do_send() NOW SENSD: packet="
           + std::to_string(size_now)
           + " B"
           );

        ASSERT_OR_LOG_ERROR_AND_RETURN( size_now == m_send_que.front().size(), false, "Unexpected queue size");
        reset_timer(get_default_timeout());

        boost::asio::async_write
          (
           socket()
           , boost::asio::buffer(m_send_que.front().data(), size_now )
           , strand_.wrap
           (
            std::bind_front
            (
             &connection<t_protocol_handler>::handle_write
             , self
             )
            )
           );

        //_dbg3("(chunk): " << size_now);
        //logger_handle_net_write(size_now);
        //_info("[sock " << socket().native_handle() << "] Async send requested " << m_send_que.front().size());
    }

    //do_send_handler_stop( ptr , cb ); // empty function

    return true;

    CATCH_ENTRY_L0("connection<t_protocol_handler>::do_send", false);
  } // do_send
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  std::chrono::milliseconds connection<t_protocol_handler>::get_default_timeout()
  {
    unsigned count;
    try { count = host_count(m_host); } catch (...) { count = 0; }
    const unsigned shift = get_state().sock_count > AGGRESSIVE_TIMEOUT_THRESHOLD ? std::min(std::max(count, 1u) - 1, 8u) : 0;
    std::chrono::milliseconds timeout;
    if (m_local)
      timeout = std::chrono::milliseconds(DEFAULT_TIMEOUT_MS_LOCAL >> shift);
    else
      timeout = std::chrono::milliseconds(DEFAULT_TIMEOUT_MS_REMOTE >> shift);
    return timeout;
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  std::chrono::milliseconds connection<t_protocol_handler>::get_timeout_from_bytes_read(size_t bytes)
  {
    std::chrono::milliseconds ms = std::chrono::milliseconds((unsigned)(bytes * TIMEOUT_EXTRA_MS_PER_BYTE));
    const std::chrono::milliseconds cur = std::chrono::duration_cast<
      std::chrono::milliseconds>(m_timer.expiry() - std::chrono::steady_clock::now());

    if (cur.count() > 0)
      ms += cur;

    if (ms > get_default_timeout())
      ms = get_default_timeout();
    return ms;
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  unsigned int connection<t_protocol_handler>::host_count(const std::string &host, int delta)
  {
    static std::mutex hosts_mutex;
    LOCK_MUTEX(hosts_mutex);
    static std::map<std::string, unsigned int> hosts;
    unsigned int &val = hosts[host];
    if (delta > 0)
      LOG_TRACE
      (
      "New connection from host "
      + host
      + ": "
      + std::to_string(val)
      );
    else if (delta < 0)
      LOG_TRACE
      (
      "Closed connection from host "
      + host
      + ": "
      + std::to_string(val)
      );
    LOG_ERROR_AND_THROW_UNLESS(delta >= 0 || val >= (unsigned)-delta, "Count would go negative");
    LOG_ERROR_AND_THROW_UNLESS(delta <= 0 || val <= std::numeric_limits<unsigned int>::max() - (unsigned)delta, "Count would wrap");
    val += delta;
    return val;
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  void connection<t_protocol_handler>::reset_timer(std::chrono::milliseconds ms)
  {
    const auto tms = ms.count();
    if (tms < 0)
    {
      LOG_WARNING("Ignoring negative timeout " + std::to_string(tms));
      return;
    }
    LOG_TRACE("Setting" + std::string(" ") + std::to_string(tms) + " expiry");
    auto self = safe_shared_from_this();
    if(!self)
    {
      LOG_ERROR("Resetting timer on a dead object");
      return;
    }
    if (m_was_shutdown)
    {
      LOG_ERROR("Setting timer on a shut down object");
      return;
    }
    m_timer.expires_after(ms);
    m_timer.async_wait([=, this](const boost::system::error_code& ec)
    {
      if(ec == boost::asio::error::operation_aborted)
        return;
      LOG_DEBUG(context.to_str() + "connection timeout, closing");
      self->close();
    });
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  bool connection<t_protocol_handler>::shutdown()
  {
    {
      LOCK_RECURSIVE_MUTEX(m_shutdown_lock);
      if (m_was_shutdown)
        return true;
      m_was_shutdown = true;
      // Initiate graceful connection closure.
      m_timer.cancel();
      boost::system::error_code ignored_ec;
      socket().shutdown(boost::asio::ip::tcp::socket::shutdown_both, ignored_ec);
      if (!m_host.empty())
      {
        try { host_count(m_host, -1); } catch (...) { /* ignore */ }
        m_host = "";
      }
    }
    m_protocol_handler.release_protocol();
    return true;
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  bool connection<t_protocol_handler>::close()
  {
    TRY_ENTRY();
    auto self = safe_shared_from_this();
    if(!self)
      return false;
    //_info("[sock " << socket().native_handle() << "] Que Shutdown called.");
    m_timer.cancel();
    size_t send_que_size = 0;
    {
      LOCK_RECURSIVE_MUTEX(m_send_que_lock);
      send_que_size = m_send_que.size();
    }
    m_want_close_connection = true;
    if(!send_que_size)
    {
      shutdown();
    }

    return true;
    CATCH_ENTRY_L0("connection<t_protocol_handler>::close", false);
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  bool connection<t_protocol_handler>::send_done()
  {
    if (m_ready_to_close)
      return close();
    m_ready_to_close = true;
    return true;
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  bool connection<t_protocol_handler>::cancel()
  {
    return close();
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  void connection<t_protocol_handler>::handle_write(const boost::system::error_code& e, size_t cb)
  {
    TRY_ENTRY();
    LOG_TRACE
    (
     context.to_str()
    + "[sock "
    + std::to_string(socket().native_handle())
    + "] Async send calledback "
    + std::to_string(cb)
    );

    if (e)
    {
      _dbg1
        (
         "[sock "
         + std::to_string(socket().native_handle())
         + "] Some problems at write: "
         + e.message()
         + ':'
         + std::to_string(e.value())
         );
      shutdown();
      return;
    }

                // The single sleeping that is needed for correctly handling "out" speed throttling

    bool do_shutdown = false;
    {
      LOCK_RECURSIVE_MUTEX(m_send_que_lock);
      if(m_send_que.empty())
      {
        _erro
          (
           "[sock "
           + std::to_string(socket().native_handle())
           + "] m_send_que.size() == 0 at handle_write!"
           );
        return;
      }

      m_send_que.pop();
      if(m_send_que.empty())
      {
        if(m_want_close_connection)
        {
          do_shutdown = true;
        }
      }else
      {
        //have more data to send
      reset_timer(get_default_timeout());
      auto size_now = m_send_que.front().size();
      LOG_DEBUG
        (
         "handle_write() NOW SENDS: packet="
         + std::to_string(size_now)
         + " B"
         + ", from  queue size="
         + std::to_string(m_send_que.size())
         );
      ASSERT_OR_LOG_ERROR_AND_RETURN( size_now == m_send_que.front().size(), void(), "Unexpected queue size");
      boost::asio::async_write
        (
        socket()
        , boost::asio::buffer(m_send_que.front().data(), size_now)
        , strand_.wrap
        (
         std::bind_front
         (
          &connection<t_protocol_handler>::handle_write
          , connection<t_protocol_handler>::shared_from_this()
          )
         )
         );
        //_dbg3("(normal)" << size_now);
      }
    }

    if(do_shutdown)
    {
      shutdown();
    }
    CATCH_ENTRY_L0("connection<t_protocol_handler>::handle_write", void());
  }

  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  void connection<t_protocol_handler>::setRpcStation()
  {
    m_connection_type = e_connection_type_RPC;
    LOG_DEBUG("set m_connection_type = RPC ");
  }

  /************************************************************************/
  /*                                                                      */
  /************************************************************************/

  template<class t_protocol_handler>
  boosted_tcp_server<t_protocol_handler>::boosted_tcp_server( t_connection_type connection_type ) :
    m_state(std::make_shared<typename connection<t_protocol_handler>::shared_state>()),
    m_io_service_local_instance(new worker()),
    io_service_(m_io_service_local_instance->io_service),
    acceptor_(io_service_),
    acceptor_ipv6(io_service_),
    default_remote(),
    m_stop_signal_sent(false), m_port(0),
    m_threads_count(0),
		m_connection_type( connection_type ),
    new_connection_(),
    new_connection_ipv6()
  {
    create_server_type_map();
    m_thread_name_prefix = "NET";
  }

  template<class t_protocol_handler>
  boosted_tcp_server<t_protocol_handler>::boosted_tcp_server(boost::asio::io_service& extarnal_io_service, t_connection_type connection_type) :
    m_state(std::make_shared<typename connection<t_protocol_handler>::shared_state>()),
    io_service_(extarnal_io_service),
    acceptor_(io_service_),
    acceptor_ipv6(io_service_),
    default_remote(),
    m_stop_signal_sent(false), m_port(0),
    m_threads_count(0),
		m_connection_type(connection_type),
    new_connection_(),
    new_connection_ipv6()
  {
    create_server_type_map();
    m_thread_name_prefix = "NET";
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  boosted_tcp_server<t_protocol_handler>::~boosted_tcp_server()
  {
    this->send_stop_signal();
    wait_server_stop();
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  void boosted_tcp_server<t_protocol_handler>::create_server_type_map()
  {
		server_type_map["NET"] = e_connection_type_NET;
		server_type_map["RPC"] = e_connection_type_RPC;
		server_type_map["P2P"] = e_connection_type_P2P;
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
    bool boosted_tcp_server<t_protocol_handler>::init_server(uint32_t port,  const std::string& address,
	uint32_t port_ipv6, const std::string& address_ipv6, bool use_ipv6, bool require_ipv4)
  {
    TRY_ENTRY();
    m_stop_signal_sent = false;
    m_port = port;
    m_port_ipv6 = port_ipv6;
    m_address = address;
    m_address_ipv6 = address_ipv6;
    m_use_ipv6 = use_ipv6;
    m_require_ipv4 = require_ipv4;

    std::string ipv4_failed = "";
    std::string ipv6_failed = "";
    try
    {
      boost::asio::ip::tcp::resolver resolver(io_service_);
      boost::asio::ip::tcp::resolver::query query(address, boost::lexical_cast<std::string>(port), boost::asio::ip::tcp::resolver::query::canonical_name);
      boost::asio::ip::tcp::endpoint endpoint = *resolver.resolve(query);
      acceptor_.open(endpoint.protocol());
      acceptor_.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
      acceptor_.bind(endpoint);
      acceptor_.listen();
      boost::asio::ip::tcp::endpoint binded_endpoint = acceptor_.local_endpoint();
      m_port = binded_endpoint.port();
      LOG_DEBUG("start accept (IPv4)");
      new_connection_.reset(new connection<t_protocol_handler>(io_service_, m_state, m_connection_type));
      acceptor_.async_accept
        (
         new_connection_->socket(),
         std::bind_front
         (
          &boosted_tcp_server<t_protocol_handler>::handle_accept_ipv4,
          this
          ));
    }
    catch (const std::exception &e)
    {
      ipv4_failed = e.what();
    }

    if (ipv4_failed != "")
    {
      LOG_ERROR("Failed to bind IPv4: " + ipv4_failed);
      if (require_ipv4)
      {
        throw std::runtime_error("Failed to bind IPv4 (set to required)");
      }
    }

    if (use_ipv6)
    {
      try
      {
        if (port_ipv6 == 0) port_ipv6 = port; // default arg means bind to same port as ipv4
        boost::asio::ip::tcp::resolver resolver(io_service_);
        boost::asio::ip::tcp::resolver::query query(address_ipv6, boost::lexical_cast<std::string>(port_ipv6), boost::asio::ip::tcp::resolver::query::canonical_name);
        boost::asio::ip::tcp::endpoint endpoint = *resolver.resolve(query);
        acceptor_ipv6.open(endpoint.protocol());
        acceptor_ipv6.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
        acceptor_ipv6.set_option(boost::asio::ip::v6_only(true));
        acceptor_ipv6.bind(endpoint);
        acceptor_ipv6.listen();
        boost::asio::ip::tcp::endpoint binded_endpoint = acceptor_ipv6.local_endpoint();
        m_port_ipv6 = binded_endpoint.port();
        LOG_DEBUG("start accept (IPv6)");
        new_connection_ipv6.reset(new connection<t_protocol_handler>(io_service_, m_state, m_connection_type));

        acceptor_ipv6.async_accept
          (new_connection_ipv6->socket(),
           std::bind_front
           (
            &boosted_tcp_server<t_protocol_handler>::handle_accept_ipv6,
            this
            ));
      }
      catch (const std::exception &e)
      {
        ipv6_failed = e.what();
      }
    }

      if (use_ipv6 && ipv6_failed != "")
      {
        LOG_ERROR("Failed to bind IPv6: " + ipv6_failed);
        if (ipv4_failed != "")
        {
          throw std::runtime_error("Failed to bind IPv4 and IPv6");
        }
      }

    return true;
    }
    catch (const std::exception &e)
    {
      LOG_FATAL("Error starting server: " + std::string(e.what()));
      return false;
    }
    catch (...)
    {
      LOG_FATAL("Error starting server");
      return false;
    }
  }
  //-----------------------------------------------------------------------------
  template<class t_protocol_handler>
  bool boosted_tcp_server<t_protocol_handler>::init_server(const std::string port,  const std::string& address,
      const std::string port_ipv6, const std::string address_ipv6, bool use_ipv6, bool require_ipv4)
  {
    uint32_t p = 0;
    uint32_t p_ipv6 = 0;

    if (port.size() && !string_tools::get_xtype_from_string(p, port)) {
      LOG_ERROR("Failed to convert port no = " + port);
      return false;
    }

    if (port_ipv6.size() && !string_tools::get_xtype_from_string(p_ipv6, port_ipv6)) {
      LOG_ERROR("Failed to convert port no = " + port_ipv6);
      return false;
    }
    return this->init_server(p, address, p_ipv6, address_ipv6, use_ipv6, require_ipv4);
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  bool boosted_tcp_server<t_protocol_handler>::worker_thread(const size_t index)
  {
    TRY_ENTRY();
    uint32_t local_thr_index = index;
    std::string thread_name = std::string("[") + m_thread_name_prefix;
    thread_name += std::to_string(local_thr_index) + "]";
    //   _fact("Thread name: " << m_thread_name_prefix);
    while(!m_stop_signal_sent)
    {
      try
      {
        io_service_.run();
        return true;
      }
      catch(const std::exception& ex)
      {
        _erro("Exception at server worker thread, what=" + std::string(ex.what()));
      }
      catch(...)
      {
        _erro("Exception at server worker thread, unknown execption");
      }
    }
    //_info("Worker thread finished");
    return true;
    CATCH_ENTRY_L0("boosted_tcp_server<t_protocol_handler>::worker_thread", false);
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  void boosted_tcp_server<t_protocol_handler>::set_connection_filter(i_connection_filter* pfilter)
  {
    assert(m_state != nullptr); // always set in constructor
    m_state->pfilter = pfilter;
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  bool boosted_tcp_server<t_protocol_handler>::run_server(size_t threads_count, bool wait)
  {
    TRY_ENTRY();
    m_threads_count = threads_count;
    m_main_thread_id = std::this_thread::get_id();
    while(!m_stop_signal_sent)
    {

      // Create a pool of threads to run all of the io_services.
      {
        LOCK_RECURSIVE_MUTEX(m_threads_lock);
        for (std::size_t i = 0; i < threads_count; ++i)
        {
          std::shared_ptr<std::thread> thread =
            std::make_shared<std::thread>(std::bind(&boosted_tcp_server<t_protocol_handler>::worker_thread, this, i));
            _note("Run server thread name: " + m_thread_name_prefix);
          m_threads.push_back(thread);
        }
      }
      // Wait for all threads in the pool to exit.
      if (wait)
      {
		_fact("JOINING all threads");
        for (std::size_t i = 0; i < m_threads.size(); ++i) {
			m_threads[i]->join();
         }
         _fact("JOINING all threads - almost");
        m_threads.clear();
        _fact("JOINING all threads - DONE");

      }
      else {
		_dbg1("Reiniting OK.");
        return true;
      }

      if(wait && !m_stop_signal_sent)
      {
        //some problems with the listening socket ?..
        _dbg1("Net service stopped without stop request, restarting...");
        if(!this->init_server(m_port, m_address, m_port_ipv6, m_address_ipv6, m_use_ipv6, m_require_ipv4))
        {
          _dbg1("Reiniting service failed, exit.");
          return false;
        }else
        {
          _dbg1("Reiniting OK.");
        }
      }
    }
    return true;
    CATCH_ENTRY_L0("boosted_tcp_server<t_protocol_handler>::run_server", false);
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  bool boosted_tcp_server<t_protocol_handler>::is_thread_worker()
  {
    TRY_ENTRY();
    LOCK_RECURSIVE_MUTEX(m_threads_lock);
    for (const auto& thp:  m_threads)
    {
      if(thp->get_id() == std::this_thread::get_id())
        return true;
    }
    if(m_threads_count == 1 && std::this_thread::get_id() == m_main_thread_id)
      return true;
    return false;
    CATCH_ENTRY_L0("boosted_tcp_server<t_protocol_handler>::is_thread_worker", false);
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  bool boosted_tcp_server<t_protocol_handler>::wait_server_stop()
  {
    TRY_ENTRY();

    for (std::size_t i = 0; i < m_threads.size(); ++i)
    {
      if(m_threads[i]->joinable())
      {
        m_threads[i]->join();
      }
    }

    return true;
    CATCH_ENTRY_L0("boosted_tcp_server<t_protocol_handler>::wait_server_stop", false);
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  void boosted_tcp_server<t_protocol_handler>::send_stop_signal()
  {
    m_stop_signal_sent = true;
    typename connection<t_protocol_handler>::shared_state *state = static_cast<typename connection<t_protocol_handler>::shared_state*>(m_state.get());
    state->stop_signal_sent = true;
    TRY_ENTRY();
    connections_mutex.lock();
    for (auto &c: connections_)
    {
      c->cancel();
    }
    connections_.clear();
    connections_mutex.unlock();
    io_service_.stop();
    CATCH_ENTRY_L0("boosted_tcp_server<t_protocol_handler>::send_stop_signal()", void());
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  void boosted_tcp_server<t_protocol_handler>::handle_accept_ipv4(const boost::system::error_code& e)
  {
    this->handle_accept(e, false);
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  void boosted_tcp_server<t_protocol_handler>::handle_accept_ipv6(const boost::system::error_code& e)
  {
    this->handle_accept(e, true);
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  void boosted_tcp_server<t_protocol_handler>::handle_accept(const boost::system::error_code& e, bool ipv6)
  {
    LOG_DEBUG("handle_accept");

    boost::asio::ip::tcp::acceptor* current_acceptor = &acceptor_;
    connection_ptr* current_new_connection = &new_connection_;
    auto accept_function_pointer = &boosted_tcp_server<t_protocol_handler>::handle_accept_ipv4;
    if (ipv6)
    {
      current_acceptor = &acceptor_ipv6;
      current_new_connection = &new_connection_ipv6;
      accept_function_pointer = &boosted_tcp_server<t_protocol_handler>::handle_accept_ipv6;
    }

    try
    {
    if (!e)
    {
      if (m_connection_type == e_connection_type_RPC) {
        LOG_DEBUG("New server for RPC connections");
        (*current_new_connection)->setRpcStation(); // hopefully this is not needed actually
      }
      connection_ptr conn(std::move((*current_new_connection)));
      (*current_new_connection).reset(new connection<t_protocol_handler>(io_service_, m_state, m_connection_type));
      current_acceptor->async_accept((*current_new_connection)->socket(),
                                     std::bind_front(accept_function_pointer, this));

      boost::asio::socket_base::keep_alive opt(true);
      conn->socket().set_option(opt);

      bool res;
      if (default_remote.get_type_id() == epee::net_utils::address_type::invalid)
        res = conn->start(true, 1 < m_threads_count);
      else
        res = conn->start(true, 1 < m_threads_count, default_remote);
      if (!res)
      {
        conn->cancel();
        return;
      }
      conn->save_dbg_log();
      return;
    }
    else
    {
      LOG_ERROR
        (
         "Error in boosted_tcp_server<t_protocol_handler>::handle_accept: "
         + std::string(e.message())
         );
    }
    }
    catch (const std::exception &e)
    {
      LOG_ERROR("Exception in boosted_tcp_server<t_protocol_handler>::handle_accept: " + std::string(e.what()));
    }

    // error path, if e or exception
    assert(m_state != nullptr); // always set in constructor
    _erro
      (
       "Some problems at accept: "
       + e.message()
       + ", connections_count = "
       + std::to_string(m_state->sock_count)
       );
    epee::time_helper::sleep_no_w(100);
    (*current_new_connection).reset(new connection<t_protocol_handler>(io_service_, m_state, m_connection_type));
    current_acceptor->async_accept((*current_new_connection)->socket(),
                                   std::bind_front(accept_function_pointer, this));
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  bool boosted_tcp_server<t_protocol_handler>::add_connection(t_connection_context& out, boost::asio::ip::tcp::socket&& sock, network_address real_remote)
  {
    if(std::addressof(get_io_service()) == std::addressof(GET_IO_SERVICE(sock)))
    {
      connection_ptr conn(new connection<t_protocol_handler>(std::move(sock), m_state, m_connection_type));
      if(conn->start(false, 1 < m_threads_count, std::move(real_remote)))
      {
        conn->get_context(out);
        conn->save_dbg_log();
        return true;
      }
    }
    else
    {
      LOG_WARNING(out.to_str() + " was not added, socket/io_service mismatch");
    }
    return false;
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  typename boosted_tcp_server<t_protocol_handler>::try_connect_result_t boosted_tcp_server<t_protocol_handler>::try_connect(connection_ptr new_connection_l, const std::string& adr, const std::string& port, boost::asio::ip::tcp::socket &sock_, const boost::asio::ip::tcp::endpoint &remote_endpoint, const std::string &bind_ip, uint32_t conn_timeout)
  {
    TRY_ENTRY();

    sock_.open(remote_endpoint.protocol());
    if(bind_ip != "0.0.0.0" && bind_ip != "0" && bind_ip != "" )
    {
      boost::asio::ip::tcp::endpoint local_endpoint(boost::asio::ip::address::from_string(bind_ip.c_str()), 0);
      boost::system::error_code ec;
      sock_.bind(local_endpoint, ec);
      if (ec)
      {
        LOG_ERROR("Error binding to " + bind_ip + ": " + ec.message());
        if (sock_.is_open())
          sock_.close();
        return CONNECT_FAILURE;
      }
    }

    /*
    NOTICE: be careful to make sync connection from event handler: in case if all threads suddenly do sync connect, there will be no thread to dispatch events from io service.
    */

    boost::system::error_code ec = boost::asio::error::would_block;

    //have another free thread(s), work in wait mode, without event handling
    struct local_async_context
    {
      boost::system::error_code ec;
      std::mutex connect_mut;
      std::condition_variable cond;
    };

    std::shared_ptr<local_async_context> local_shared_context(new local_async_context());
    local_shared_context->ec = boost::asio::error::would_block;
    std::unique_lock<std::mutex> lock(local_shared_context->connect_mut);
    auto connect_callback = [](boost::system::error_code ec_, std::shared_ptr<local_async_context> shared_context)
    {
      shared_context->connect_mut.lock(); shared_context->ec = ec_; shared_context->cond.notify_one(); shared_context->connect_mut.unlock();
    };

    sock_.async_connect
    (
     remote_endpoint
     , std::bind<void>
     (connect_callback, std::placeholders::_1, local_shared_context)
     );
    while(local_shared_context->ec == boost::asio::error::would_block)
    {
      bool r = std::cv_status::no_timeout ==
        local_shared_context->cond.wait_for(lock, std::chrono::milliseconds(conn_timeout));
      if (m_stop_signal_sent)
      {
        if (sock_.is_open())
          sock_.close();
        return CONNECT_FAILURE;
      }
      if(local_shared_context->ec == boost::asio::error::would_block && !r)
      {
        //timeout
        sock_.close();
        _dbg3
          (
           "Failed to connect to "
           + adr
           + ":"
           + port
           + ", because of timeout ("
           + std::to_string(conn_timeout)
           + ")"
           );
        return CONNECT_FAILURE;
      }
    }
    ec = local_shared_context->ec;

    if (ec || !sock_.is_open())
    {
      _dbg3("Some problems at connect, message: " + ec.message());
      if (sock_.is_open())
        sock_.close();
      return CONNECT_FAILURE;
    }

    _dbg3("Connected success to " + adr + ':' + port);

    return CONNECT_SUCCESS;

    CATCH_ENTRY_L0("boosted_tcp_server<t_protocol_handler>::try_connect", CONNECT_FAILURE);
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler>
  bool boosted_tcp_server<t_protocol_handler>::connect(const std::string& adr, const std::string& port, uint32_t conn_timeout, t_connection_context& conn_context, const std::string& bind_ip)
  {
    TRY_ENTRY();

    connection_ptr new_connection_l(new connection<t_protocol_handler>(io_service_, m_state, m_connection_type) );
    connections_mutex.lock();
    connections_.insert(new_connection_l);
    LOG_DEBUG("connections_ size now " + std::to_string(connections_.size()));
    connections_mutex.unlock();
    const auto scope_exit_handler = epee::misc_utils::create_scope_leave_handler([&]{ LOCK_MUTEX(connections_mutex); connections_.erase(new_connection_l); });
    boost::asio::ip::tcp::socket&  sock_ = new_connection_l->socket();

    bool try_ipv6 = false;

    boost::asio::ip::tcp::resolver resolver(io_service_);
    boost::asio::ip::tcp::resolver::query query(boost::asio::ip::tcp::v4(), adr, port, boost::asio::ip::tcp::resolver::query::canonical_name);
    boost::system::error_code resolve_error;
    boost::asio::ip::tcp::resolver::iterator iterator;
    try
    {
      //resolving ipv4 address as ipv6 throws, catch here and move on
      iterator = resolver.resolve(query, resolve_error);
    }
    catch (const boost::system::system_error& e)
    {
      if (!m_use_ipv6 || (resolve_error != boost::asio::error::host_not_found &&
            resolve_error != boost::asio::error::host_not_found_try_again))
      {
        throw;
      }
      try_ipv6 = true;
    }
    catch (...)
    {
      throw;
    }

    std::string bind_ip_to_use;

    boost::asio::ip::tcp::resolver::iterator end;
    if(iterator == end)
    {
      if (!m_use_ipv6)
      {
        _erro("Failed to resolve " + adr);
        return false;
      }
      else
      {
        try_ipv6 = true;
        LOG_INFO("Resolving address as IPv4 failed, trying IPv6");
      }
    }
    else
    {
      bind_ip_to_use = bind_ip;
    }

    if (try_ipv6)
    {
      boost::asio::ip::tcp::resolver::query query6(boost::asio::ip::tcp::v6(), adr, port, boost::asio::ip::tcp::resolver::query::canonical_name);

      iterator = resolver.resolve(query6, resolve_error);

      if(iterator == end)
      {
        _erro("Failed to resolve " + adr);
        return false;
      }
      else
      {
        if (bind_ip == "0.0.0.0")
        {
          bind_ip_to_use = "::";
        }
        else
        {
          bind_ip_to_use = "";
        }

      }

    }

    LOG_DEBUG
      (
       "Trying to connect to "
       + adr
       + ":"
       + port
       + ", bind_ip = "
       + bind_ip_to_use
       );

    //boost::asio::ip::tcp::endpoint remote_endpoint(boost::asio::ip::address::from_string(addr.c_str()), port);
    boost::asio::ip::tcp::endpoint remote_endpoint(*iterator);

    auto try_connect_result = try_connect(new_connection_l, adr, port, sock_, remote_endpoint, bind_ip_to_use, conn_timeout);
    if (try_connect_result == CONNECT_FAILURE)
      return false;

    // start adds the connection to the config object's list, so we don't need to have it locally anymore
    connections_mutex.lock();
    connections_.erase(new_connection_l);
    connections_mutex.unlock();
    bool r = new_connection_l->start(false, 1 < m_threads_count);
    if (r)
    {
      new_connection_l->get_context(conn_context);
      //new_connection_l.reset(new connection<t_protocol_handler>(io_service_, m_config, m_sock_count, m_pfilter));
    }
    else
    {
      assert(m_state != nullptr); // always set in constructor
      _erro
        (
         "[sock "
         + std::to_string(new_connection_l->socket().native_handle())
         + "] Failed to start connection, connections_count = "
         + std::to_string(m_state->sock_count)
         );
    }

	new_connection_l->save_dbg_log();

    return r;

    CATCH_ENTRY_L0("boosted_tcp_server<t_protocol_handler>::connect", false);
  }
  //---------------------------------------------------------------------------------
  template<class t_protocol_handler> template<class t_callback>
  bool boosted_tcp_server<t_protocol_handler>::connect_async(const std::string& adr, const std::string& port, uint32_t conn_timeout, const t_callback &cb, const std::string& bind_ip)
  {
    TRY_ENTRY();
    connection_ptr new_connection_l(new connection<t_protocol_handler>(io_service_, m_state, m_connection_type) );
    connections_mutex.lock();
    connections_.insert(new_connection_l);
    LOG_DEBUG("connections_ size now " + std::to_string(connections_.size()));
    connections_mutex.unlock();
    const auto scope_exit_handler = epee::misc_utils::create_scope_leave_handler([&]{ LOCK_MUTEX(connections_mutex); connections_.erase(new_connection_l); });
    boost::asio::ip::tcp::socket&  sock_ = new_connection_l->socket();

    bool try_ipv6 = false;

    boost::asio::ip::tcp::resolver resolver(io_service_);
    boost::asio::ip::tcp::resolver::query query(boost::asio::ip::tcp::v4(), adr, port, boost::asio::ip::tcp::resolver::query::canonical_name);
    boost::system::error_code resolve_error;
    boost::asio::ip::tcp::resolver::iterator iterator;
    try
    {
      //resolving ipv4 address as ipv6 throws, catch here and move on
      iterator = resolver.resolve(query, resolve_error);
    }
    catch (const boost::system::system_error& e)
    {
      if (!m_use_ipv6 || (resolve_error != boost::asio::error::host_not_found &&
            resolve_error != boost::asio::error::host_not_found_try_again))
      {
        throw;
      }
      try_ipv6 = true;
    }
    catch (...)
    {
      throw;
    }

    boost::asio::ip::tcp::resolver::iterator end;
    if(iterator == end)
    {
      if (!try_ipv6)
      {
        _erro("Failed to resolve " + adr);
        return false;
      }
      else
      {
        LOG_INFO("Resolving address as IPv4 failed, trying IPv6");
      }
    }

    if (try_ipv6)
    {
      boost::asio::ip::tcp::resolver::query query6(boost::asio::ip::tcp::v6(), adr, port, boost::asio::ip::tcp::resolver::query::canonical_name);

      iterator = resolver.resolve(query6, resolve_error);

      if(iterator == end)
      {
        _erro("Failed to resolve " + adr);
        return false;
      }
    }


    boost::asio::ip::tcp::endpoint remote_endpoint(*iterator);

    sock_.open(remote_endpoint.protocol());
    if(bind_ip != "0.0.0.0" && bind_ip != "0" && bind_ip != "" )
    {
      boost::asio::ip::tcp::endpoint local_endpoint(boost::asio::ip::address::from_string(bind_ip.c_str()), 0);
      boost::system::error_code ec;
      sock_.bind(local_endpoint, ec);
      if (ec)
      {
        LOG_ERROR("Error binding to " + bind_ip + ": " + ec.message());
        if (sock_.is_open())
          sock_.close();
        return false;
      }
    }

    std::shared_ptr<boost::asio::steady_timer> sh_deadline(new boost::asio::steady_timer(io_service_));
    //start deadline
    sh_deadline->expires_after(std::chrono::milliseconds(conn_timeout));
    sh_deadline->async_wait([=](const boost::system::error_code& error)
      {
          if(error != boost::asio::error::operation_aborted)
          {
            _dbg3
              (
               "Failed to connect to "
               + adr
               + ':'
               + port
               + ", because of timeout ("
               + std::to_string(conn_timeout)
               + ")"
               );
            new_connection_l->socket().close();
          }
      });
    //start async connect
    sock_.async_connect(remote_endpoint, [=, this](const boost::system::error_code& ec_)
      {
        t_connection_context conn_context{};
        boost::system::error_code ignored_ec;
        boost::asio::ip::tcp::socket::endpoint_type lep = new_connection_l->socket().local_endpoint(ignored_ec);
        if(!ec_)
        {//success
          if(!sh_deadline->cancel())
          {
            cb(conn_context, boost::asio::error::operation_aborted);//this mean that deadline timer already queued callback with cancel operation, rare situation
          }else
          {
            _dbg3
              (
               "[sock "
               + std::to_string(new_connection_l->socket().native_handle())
               + "] Connected success to "
               + adr
               + ':'
               + port
               + " from "
               + lep.address().to_string()
               + ':'
               + std::to_string(lep.port())
               );

            // start adds the connection to the config object's list, so we don't need to have it locally anymore
            connections_mutex.lock();
            connections_.erase(new_connection_l);
            connections_mutex.unlock();
            bool r = new_connection_l->start(false, 1 < m_threads_count);
            if (r)
            {
              new_connection_l->get_context(conn_context);
              cb(conn_context, ec_);
            }
            else
            {
              _dbg3
                (
                 "[sock "
                 + std::to_string(new_connection_l->socket().native_handle())
                 + "] Failed to start connection to "
                 + adr
                 + ':'
                 + port
                 );
              cb(conn_context, boost::asio::error::fault);
            }
          }
        }else
        {
          _dbg3
            (
             "[sock "
             + std::to_string(new_connection_l->socket().native_handle())
             + "] Failed to connect to "
             + adr
             + ':'
             + port 
             + " from "
             + lep.address().to_string()
             + ':'
             + std::to_string(lep.port())
             + ": "
             + ec_.message()
             + ':'
             + std::to_string(ec_.value())
             );
          cb(conn_context, ec_);
        }
      });
    return true;
    CATCH_ENTRY_L0("boosted_tcp_server<t_protocol_handler>::connect_async", false);
  }

} // namespace
} // namespace
