/*

  Copyright 2021 fuwa

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#pragma once

#include "jsonrpc_structs.h"

#include "tools/epee/include/storages/portable_storage_template_helper.h"

namespace epee
{
  namespace net_utils
  {
    std::optional<std::string> beast_http_json
    (
     const std::string host
     , const uint16_t port 
     , const std::string uri
     , const std::string request_body
     );

    template<class t_request, class t_response>
    bool invoke_http_json
    (
     const std::string host
     , const uint16_t port 
     , const std::string_view uri
     , const t_request& request_struct
     , t_response& result_struct
     )
    {
      const json json_param = request_struct;

      const std::optional<std::string> response_str =
        beast_http_json(host, port, std::string(uri), json_param.dump());

      if (!response_str) {
        return false;
      }

      const json json_response = json::parse(*response_str);
      result_struct = json_response;

      return true;
    }

    template<class t_request, class t_response>
    bool invoke_http_json_rpc
    (
     const std::string host
     , const uint16_t port 
     , const std::string_view uri
     , const std::string method_name
     , const t_request& request_struct
     , t_response& result_struct
     )
    {
      const uint64_t req_id = 0;
      epee::json_rpc::request<t_request> req_t{};
      req_t.jsonrpc = "2.0";
      req_t.id = req_id;
      req_t.method = method_name;
      req_t.params = request_struct;

      const json json_param = req_t;

      const std::optional<std::string> response_str =
        beast_http_json(host, port, std::string(uri), json_param.dump());

      if (!response_str) {
        return false;
      }

      const json json_response = json::parse(*response_str);

      // left, error
      if (json_response.contains("error")) {
        const json_rpc::error_response error_response = json_response;
        const auto error = error_response.error;
        LOG_ERROR
          (
           "RPC call of \""
           + req_t.method
           + "\" returned error: "
           + std::to_string(error.code)
           + ", message: "
           + error.message
           );
        return false;
      }
      else if (json_response.contains("result")) {
        const json_rpc::ok_response<t_response> response = json_response;
        result_struct = response.result;
        return true;
      }
      else {
        LOG_ERROR("Invalid RPC response: " + json_response.dump());
        return false;
      }

    }

  }
}
