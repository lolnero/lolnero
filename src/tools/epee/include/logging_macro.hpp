/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/


#pragma once

#define LOG_AND_RETURN_IF(level, expr, fail_ret_val, x) \
  do {                                                  \
    if(expr) {                                          \
      LOG_CATEGORY                                      \
        (                                               \
         level                                          \
         , DEFAULT_CAT                                  \
         , x );                                         \
      return fail_ret_val;                              \
    };                                                  \
  } while(0)

#define ASSERT_OR_LOG_AND_RETURN(level, expr, fail_ret_val, x) \
  LOG_AND_RETURN_IF(level, !(expr), fail_ret_val, x)

#define ASSERT_OR_LOG_ERROR_AND_RETURN(expr, fail_ret_val, x)            \
  ASSERT_OR_LOG_AND_RETURN(epee::LogLevel::Error, expr, fail_ret_val, x)

#define ASSERT_OR_LOG_TRACE_AND_RETURN(expr, fail_ret_val, x)  \
  ASSERT_OR_LOG_AND_RETURN                                     \
  (epee::LogLevel::Trace, expr, fail_ret_val, x)



#define TRY_ENTRY()   try {

#define CATCH_ENTRY(location, return_val) }     \
    catch(const std::exception& ex)             \
      {                                         \
        (void)(ex);                             \
        LOG_ERROR                               \
          (                                     \
           "Exception at ["                     \
           + std::string(location)              \
           + "], what="                         \
           + std::string(ex.what())) ;          \
        return return_val;                      \
      }                                         \
    catch(...)                                  \
      {                                         \
        LOG_ERROR                               \
          (                                     \
           "Exception at ["                     \
           + std::string(location)              \
           + "], generic exception \"...\"") ;  \
        return return_val;                      \
      }

#define CATCH_ENTRY_L0(lacation, return_val)    \
  CATCH_ENTRY(lacation, return_val)
