// Copyright (c) 2006-2013, Andrey N. Sabelnikov, www.sabelnikov.net
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// * Neither the name of the Andrey N. Sabelnikov nor the
// names of its contributors may be used to endorse or promote products
// derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER  BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#pragma once


#include "portable_storage.h"

#include "tools/epee/functional/string_tools.hpp"

namespace epee
{
  namespace serialization
  {
    //-----------------------------------------------------------------------------------------------------------
    template<class t_struct>
    std::optional<t_struct> load_t_from_binary
    (
     const std::span<const uint8_t> binary_buff
     , const epee::serialization::portable_storage::limits_t *limits = NULL
     )
    {
      portable_storage ps;
      if (!ps.load_from_binary(binary_buff, limits)) {
        return {};
      }

      t_struct out;
      if (!out.load(ps)) {
        return {};
      }

      return out;
    }

    //-----------------------------------------------------------------------------------------------------------
    template<class t_struct>
    epee::blob::data store_t_to_binary
    (
     const t_struct& x
     )
    {
      portable_storage ps;
      x.store(ps);
      return epee::string_tools::string_to_blob(ps.store_to_binary());
    }
  }
}
