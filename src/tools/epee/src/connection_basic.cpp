/// @file
/// @author rfree (current maintainer in monero.cc project)
/// @brief base for connection, contains e.g. the ratelimit hooks

// Copyright (c) 2014-2020, The Monero Project
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this list of
//    conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this list
//    of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may be
//    used to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
// THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
// THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

/* rfree: implementation for the non-template base, can be used by connection<> template class in abstract_tcp_server2 file  */



#include "tools/epee/include/net/connection_basic.hpp"


#define GET_IO_SERVICE(s) ((boost::asio::io_context&)(s).get_executor().context())



// ################################################################################################
// local (TU local) headers
// ################################################################################################

namespace epee
{
namespace net_utils
{

  std::string to_string(t_connection_type type)
  {
	  if (type == e_connection_type_NET)
		return std::string("NET");
	  else if (type == e_connection_type_RPC)
	    return std::string("RPC");
	  else if (type == e_connection_type_P2P)
	    return std::string("P2P");

	  return std::string("UNKNOWN");
  }


/* ============================================================================ */

class connection_basic_pimpl {
	public:
		connection_basic_pimpl(const std::string &name);

		int m_peer_number; // e.g. for debug/stats
};


} // namespace
} // namespace

// ################################################################################################
// The implementation part
// ################################################################################################

namespace epee
{
namespace net_utils
{

// ================================================================================================
// connection_basic_pimpl
// ================================================================================================

connection_basic_pimpl::connection_basic_pimpl(const std::string &name) : m_peer_number(0) { }

// ================================================================================================
// connection_basic
// ================================================================================================

// methods:
connection_basic::connection_basic(boost::asio::ip::tcp::socket&& sock, std::shared_ptr<connection_basic_shared_state> state)
	:
	m_state(std::move(state)),
	mI( std::make_unique<connection_basic_pimpl>("peer") ),
	strand_(GET_IO_SERVICE(sock)),
	socket_(GET_IO_SERVICE(sock))
{
	// add nullptr checks if removed
	assert(m_state != nullptr); // release runtime check in get_context

  socket_ = std::move(sock);

  ++(m_state->sock_count); // increase the global counter
	mI->m_peer_number = m_state->sock_number.fetch_add(1); // use, and increase the generated number

	std::string remote_addr_str = "?";
	try { boost::system::error_code e; remote_addr_str = socket().remote_endpoint(e).address().to_string(); } catch(...){} ;

	_note
    (
     "Spawned connection #"
     + std::to_string(mI->m_peer_number)
     + " to "
     + remote_addr_str
     + " currently we have sockets count:"
     + std::to_string(m_state->sock_count)
     );
}

connection_basic::~connection_basic() noexcept(false) {
	--(m_state->sock_count);

	std::string remote_addr_str = "?";
	try { boost::system::error_code e; remote_addr_str = socket().remote_endpoint(e).address().to_string(); } catch(...){} ;
	_note
    (
     "Destructing connection #"
     + std::to_string(mI->m_peer_number)
     + " to "
     + remote_addr_str
     );
}

void connection_basic::do_send_handler_write(const void* ptr , size_t cb ) {
        // No sleeping here; sleeping is done once and for all in connection<t_protocol_handler>::handle_write
	LOG_TRACE
    (
     "handler_write (direct) - before ASIO write, for packet="
     + std::to_string(cb)
     + " B (after sleep)"
     );
}

void connection_basic::do_send_handler_write_from_queue( const boost::system::error_code& e, size_t cb, int q_len ) {
        // No sleeping here; sleeping is done once and for all in connection<t_protocol_handler>::handle_write
	LOG_TRACE
    (
     "handler_write (after write, from queue="
     + std::to_string(q_len)
     + ") - before ASIO write, for packet="
     + std::to_string(cb)
     + " B (after sleep)"
     );
}

} // namespace
} // namespace

