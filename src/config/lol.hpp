/*

Copyright (c) 2020-2021, The Lolnero Project

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#pragma once

#include "math/consensus/constant.hpp"

#include <chrono>
#include <string_view>
#include <span>
#include <numeric> // fix for debian

namespace constant
{
  // COIN - number of smallest units in one coin

  constexpr uint64_t DIFFICULTY_TARGET_IN_SECONDS = 300;
  constexpr uint64_t DIFFICULTY_WINDOW_IN_BLOCKS = 144;
  constexpr size_t DIFFICULTY_BLOCKS_COUNT =
    static_cast<size_t>(DIFFICULTY_WINDOW_IN_BLOCKS) + 1;


  constexpr size_t BLOCKCHAIN_TIMESTAMP_CHECK_WINDOW_V2 = 11;
  constexpr uint64_t CRYPTONOTE_BLOCK_FUTURE_TIME_LIMIT_V2 = 300*2;

  constexpr uint64_t CRYPTONOTE_COINBASE_BLOB_RESERVED_SIZE = 600;

  constexpr uint64_t COIN = consensus::get_coin_amount();

  constexpr uint64_t FEE_PER_BYTE = 300000;

  constexpr size_t AMOUNT_BIT_WIDTH = 64;



  //by default, blocks ids count in synchronizing
  constexpr size_t BLOCKS_IDS_SYNCHRONIZING_DEFAULT_COUNT = 10000;
  constexpr size_t BLOCKS_IDS_SYNCHRONIZING_PEER_BATCH_SIZE = 2000;
  //max blocks ids count in synchronizing
  constexpr size_t BLOCKS_IDS_SYNCHRONIZING_MAX_COUNT = 25000;

  constexpr size_t BLOCKS_SYNCHRONIZING_SIZE = 100;

  constexpr size_t COMMAND_RPC_GET_BLOCKS_FAST_MAX_COUNT = 1000;

  //Bender's nightmare
  constexpr uint64_t LEVIN_SIGNATURE = 0x0101010101012101LL;

  constexpr size_t LEVIN_DEFAULT_TIMEOUT_PRECONFIGURED = 0;

  constexpr uint64_t LEVIN_INITIAL_MAX_PACKET_SIZE = 256*1024;
  // 256 KiB before handshake

  constexpr uint64_t LEVIN_DEFAULT_MAX_PACKET_SIZE = 100000000;
  //100MB by default after handshake

  constexpr uint64_t CRYPTONOTE_MINED_MONEY_UNLOCK_WINDOW =
    consensus::get_coinbase_unlock_time();

  constexpr auto p2p_sleep_duration = std::chrono::seconds(10);

}

namespace config
{
  // Hash domain separators
  constexpr std::string_view HASH_KEY_BULLETPROOF_EXPONENT =
    "bulletproof";

  constexpr std::string_view HASH_KEY_SUBADDRESS = "SubAddr";

  constexpr std::string_view HASH_KEY_CLSAG_ROUND = "CLSAG_round";
  constexpr std::string_view HASH_KEY_CLSAG_AGG_0 = "CLSAG_agg_0";
  constexpr std::string_view HASH_KEY_CLSAG_AGG_1 = "CLSAG_agg_1";

  constexpr std::string_view ecdhHashPrefix = "amount";
  constexpr std::string_view commitmentMaskPrefix =
    "commitment_mask";

  namespace lol
  {
    constexpr std::string_view CRYPTONOTE_NAME = "lolnero";

    constexpr std::string_view DAEMON_P2P_DEFAULT_IP = "0.0.0.0";
    constexpr std::string_view DAEMON_P2P_DEFAULT_IP_V6 = "::";

    constexpr std::string_view DAEMON_RPC_DEFAULT_IP = "127.0.0.1";

    constexpr std::string_view WALLET_RPC_DEFAULT_IP= "127.0.0.1";
    constexpr uint16_t WALLET_RPC_DEFAULT_PORT = 45680;

    constexpr size_t mixin = 31;
    constexpr size_t ring_size = 32;

    constexpr uint8_t constant_hf_version = 17;
    constexpr uint64_t constant_hf_height = 0;
    constexpr size_t constant_transaction_version = 2;
    constexpr time_t constant_hf_time = 1600576524;

    constexpr size_t max_connections_per_address = 2;
    constexpr uint64_t max_tx_weight = 128 * 1024; // 128 kB

    constexpr size_t tx_version = 2;
    constexpr size_t miner_tx_version = 2;

    constexpr uint32_t SUBADDRESS_LOOKAHEAD_MAJOR = 8;
    constexpr uint32_t SUBADDRESS_LOOKAHEAD_MINOR = 128;

    constexpr auto rpc_timeout = std::chrono::seconds(10);

    constexpr std::string_view hash_sep = "####### ";
    constexpr std::string_view dash_sep = "------- ";
    constexpr std::string_view plus_sep = "+++++++ ";
    constexpr std::string_view x_sep    = "xxxxxxx ";
    constexpr std::string_view tab_sep  = "        ";

    constexpr std::string_view unicode_gem_stone = "💎";
    constexpr std::string_view unicode_money = "λ";

    constexpr size_t reorg_buffer = 3;

    constexpr uint64_t tx_locked_one_year_away_in_blocks = 288 * 365;

    constexpr std::string_view mem_db_file_name = "mem_db.json";

  }
}
