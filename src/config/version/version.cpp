#include "version.hpp"

constexpr std::string_view LOLNERO_VERSION = "0.9.14.11";
constexpr std::string_view LOLNERO_RELEASE_NAME = "Saber";


namespace cryptonote {
  std::string get_version_string() {
    return "Lolnero '"
      + std::string(LOLNERO_RELEASE_NAME)
      + "' (v"
      + std::string(LOLNERO_VERSION_FULL)
      + ")"
      ;
  }
}
