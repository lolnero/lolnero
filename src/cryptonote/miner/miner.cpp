/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE


Parts of this file are originally 
copyright (c) 2012-2013 The Cryptonote developers

*/


#include "miner.hpp"

#include "tools/epee/include/string_tools.h"
#include "tools/epee/functional/time_helper.hpp"

#include "math/crypto/controller/random.hpp"

#include "cryptonote/tx/pseudo_functional/tx_utils.hpp"
#include "cryptonote/basic/functional/format_utils.hpp"

#ifdef OpenCL
#include "math/hash/opencl/sha3.hpp"
#endif


#include <execution>


namespace cryptonote
{
  miner::miner
  (
   i_miner_handler& phandler
   , const network_type nettype
   , const std::string mining_address
   , const uint32_t opencl_mining_threads
   )
    : m_phandler(phandler)
    , m_threads_total(opencl_mining_threads)
  {
    if (mining_address.empty()) {
      return;
    }

    LOG_GLOBAL("MINER address init: " + mining_address);

    const auto maybe_info =
       cryptonote::get_account_address_from_str
       (
        nettype
        , mining_address
        );

    if(!maybe_info) {
      LOG_FATAL
        (
         "Target account address "
         + mining_address
         + " has invalid format, starting daemon canceled"
         );
    }
    else if(maybe_info->is_subaddress) {
      LOG_FATAL
        (
         "Target account address "
         + mining_address
         + " has to be a mining address (main address)"
         " , starting daemon canceled"
         );
    }
    else {
      m_mine_address = maybe_info->address;
      m_do_mining = true;
    }
  }

  miner::~miner()
  {
    try { stop(); }
    catch (...) { /* ignore */ }
  }

  bool miner::set_block_template
  (
   const raw_block& bl
   , const diff_t& di
   , uint64_t block_reward
   )
  {
    const std::scoped_lock<std::mutex> lock(m_template_lock);
    m_template = bl;
    m_diff = di;
    m_block_reward = block_reward;
    ++m_template_no;
    m_starter_nonce = crypto::rand<uint64_t>();
    return true;
  }

  bool miner::on_block_chain_update()
  {
    if(!is_mining())
      return true;

    return request_block_template();
  }

  bool miner::request_block_template()
  {
    if (!m_mine_address) return false;

    const auto maybe_block_template =
      m_phandler.create_block_template(*m_mine_address);

    if (!maybe_block_template)
      {
        LOG_ERROR("Failed to get_block_template(), stopping mining");
        return false;
      }

    const auto [block, diff, reward] = *maybe_block_template;

    set_block_template(block, diff, reward);
    return true;
  }

  bool miner::on_idle()
  {
    m_update_block_template_interval.do_call([&]{
      if(is_mining())request_block_template();
      return true;
    });

    m_update_merge_hr_interval.do_call([&]{
      merge_hr();
      return true;
    });

    return true;
  }

  void miner::merge_hr()
  {
    if(m_last_hr_merge_time && is_mining())
      {
        m_current_hash_rate =
          m_hashes * 1000 /
          (
           epee::misc_utils::get_tick_count()
           - m_last_hr_merge_time
           + 1
           );
        {
          const std::scoped_lock<std::mutex> lock(m_last_hash_rates_lock);
          m_last_hash_rates.push_back(m_current_hash_rate);
          if(m_last_hash_rates.size() > 19)
            m_last_hash_rates.pop_front();
        }

        constexpr auto m_do_print_hashrate = false;
        if(m_do_print_hashrate)
          {
            const uint64_t total_hr =
              std::accumulate
              (m_last_hash_rates.begin(), m_last_hash_rates.end(), 0);

            const float hr =
              static_cast<float>(total_hr)
              /
              static_cast<float>(m_last_hash_rates.size());

            const auto flags = std::cout.flags();
            const auto precision = std::cout.precision();
            std::cout
              << "hashrate: "
              << std::setprecision(4)
              << std::fixed
              << hr
              << std::setiosflags(flags)
              << std::setprecision(precision)
              << std::endl;
          }
      }
    m_last_hr_merge_time = epee::misc_utils::get_tick_count();
    m_hashes = 0;
  }


  bool miner::is_mining() const
  {
    return !m_stop;
  }

  const spend_view_public_keys miner::get_mining_address() const
  {
    return m_mine_address.value_or(spend_view_public_keys{});
  }

  uint32_t miner::get_threads_count() const {
    return m_threads_total;
  }

  bool miner::start
  (const spend_view_public_keys& adr, size_t threads_count)
  {
    m_block_reward = 0;
    m_mine_address = adr;
    m_threads_total =
      std::max(1u, static_cast<uint32_t>(threads_count));

    m_starter_nonce = crypto::rand<uint64_t>();
    const std::scoped_lock<std::mutex> lock(m_thread_lock);
    if(is_mining())
      {
        LOG_ERROR("Starting miner but it's already started");
        return false;
      }

    if(m_thread)
      {
        LOG_ERROR
          (
           "Unable to start miner because "
           "there are active mining threads"
           );
        return false;
      }

    request_block_template();//lets update block template

    m_stop = false;



#ifdef OpenCL
    const auto maybeGpu = opencl::getGPU();

    if (!maybeGpu) {
      LOG_WARNING("No OpenCL capable GPUs");
    } else {
      const auto device = *maybeGpu;
      m_thread = {std::thread(&miner::opencl_miner, this, device)};
      return true;
    }
#endif
    m_thread = {std::thread(&miner::worker_thread, this)};
    LOG_GLOBAL("CPU Mining has started with 1 thread");

    return true;
  }

  uint64_t miner::get_speed() const
  {
    if(is_mining()) {
      return m_current_hash_rate;
    }
    else {
      return 0;
    }
  }

  void miner::send_stop_signal()
  {
    m_stop = true;
  }

  bool miner::stop()
  {
    LOG_TRACE("Miner has received stop signal");

    if (!m_thread)
      {
        LOG_TRACE("Not mining - nothing to stop" );
        return true;
      }

    send_stop_signal();

    m_thread->join();

    LOG_INFO("Mining has been stopped");

    m_thread = {};
    return true;
  }

  void miner::on_synchronized()
  {
    if (m_mine_address) {
      if(m_do_mining)
        {
          start(*m_mine_address, m_threads_total);
        }
    }
  }

  void miner::pause()
  {
    m_pauser = true;
    LOG_DEBUG("miner::pause");
  }

  void miner::resume()
  {
    m_pauser = false;
    LOG_DEBUG("miner::resume");
  }

  bool miner::worker_thread()
  {
    LOG_GLOBAL("Miner thread was started");
    uint64_t nonce = m_starter_nonce;
    raw_block b = m_template;
    uint32_t local_template_ver = 0;

    while(!m_stop)
      {
        if(m_pauser)
          {
            epee::time_helper::sleep_no_w(100);
            continue;
          }

        if(local_template_ver != m_template_no)
        {
          const std::scoped_lock<std::mutex> lock(m_template_lock);
          local_template_ver = m_template_no;
          b = m_template;
          nonce = m_starter_nonce;
        }

        const diff_t local_diff = m_diff;

        const boost::multiprecision::uint512_t max_int =
            max_int_for_diff(local_diff);

        const epee::blob::data head_full =
            epee::string_tools::string_to_blob
            (get_mining_blob_head(b));

        const epee::blob::data hashing_blob_head
            = head_full.substr
            (0, head_full.length() - sizeof(nonce));

        const epee::blob::data hashing_blob_tail =
            epee::string_tools::string_to_blob
            (cryptonote::get_mining_blob_tail(b));

        if(!local_template_ver)//no any set_block_template call
          {
            LOG_PRINT_L2("Block template not set yet");
            epee::time_helper::sleep_no_w(1000);
            continue;
          }

        const epee::blob::data nonceData =
          epee::blob::data((const uint8_t*)(&nonce), sizeof(nonce));

        const crypto::hash h = crypto::sha3
          (
           hashing_blob_head
           + nonceData
           + hashing_blob_tail
           );

        const bool valid_hash = hash_to_int(h) <= max_int;

        if(valid_hash && check_hash(h, local_diff))
          {
            raw_block mined_block = b;
            mined_block.nonce = nonce;

            log_mined_block
              (
               get_block_index(mined_block)
               , get_block_hash(mined_block)
               );

            m_phandler.handle_block_found(mined_block);
          }
        nonce ++;
        m_hashes ++;
      }

    LOG_GLOBAL("Miner thread stopped");
    return true;
  }

#ifdef OpenCL
  bool miner::opencl_miner(cl::Device device)
  {
    const cl::Context context(device);

    const auto maybeProgram = opencl::getSha3Program(device, context);

    if (!maybeProgram) {
      std::cerr << "Failed to build sha3 OpenCL kernel" << std::endl;
      return false;
    }
    const auto [program, queue] = *maybeProgram;

    LOG_GLOBAL("OpenCL Miner was started");
    uint64_t nonce = m_starter_nonce;
    diff_t local_diff = 0;
    uint32_t local_template_ver = 0;

    raw_block b;

    epee::blob::data hashing_blob_head;
    epee::blob::data hashing_blob_tail;
    opencl::cl_mining_template mining_template{};

    const size_t gpu_worker_scale = 256;
    const size_t gpu_loop_size = 256;
    const size_t worker_size = m_threads_total * gpu_worker_scale;

    boost::multiprecision::uint512_t max_int;

    while(!m_stop)
      {
        if(m_pauser)
          {
            epee::time_helper::sleep_no_w(100);
            continue;
          }

        if(local_template_ver != m_template_no)
          {
            const std::scoped_lock<std::mutex> lock(m_template_lock);
            b = m_template;
            local_diff = m_diff;
            max_int = max_int_for_diff(local_diff);
            local_template_ver = m_template_no;
            nonce = m_starter_nonce;

            const epee::blob::data head_full =
              epee::string_tools::string_to_blob
              (get_mining_blob_head(b));

            hashing_blob_head =
              head_full.substr(0, head_full.length() - sizeof(nonce));

            hashing_blob_tail = epee::string_tools::string_to_blob
              (cryptonote::get_mining_blob_tail(b));

            opencl::sha3_ctx_t sha3_ctx;
            opencl::sha3_init(&sha3_ctx);
            opencl::sha3_update
              (
               &sha3_ctx
               , hashing_blob_head.data()
               , hashing_blob_head.size()
               );

            mining_template.sha3_ctx = sha3_ctx;

            std::ranges::copy
              (
               hashing_blob_tail
               , mining_template.tail.begin()
               );

            mining_template.tailSize = hashing_blob_tail.size();
            mining_template.hashBound = int_to_hash(max_int).data;
            mining_template.loopSize = gpu_loop_size;
          }

        if(!local_template_ver)//no any set_block_template call
          {
            LOG_PRINT_L2("Block template not set yet");
            epee::time_helper::sleep_no_w(1000);
            continue;
          }

        using namespace opencl;

        mining_template.nonce = nonce;

        // LOG_GLOBAL("Mining opencl sha3 on nonce: " << nonce);

        const auto hashes = opencl_sha3_mining
          (
           mining_template
           , worker_size
           , context
           , program
           , queue
           );

        const auto r = std::find_if
          (
           hashes.begin()
           , hashes.end()
           , [](const auto& x) {
             return x.valid;
           }
           );

        if(r != hashes.end()) {
          const uint64_t nonce = r->nonce;

          const epee::blob::data nonceData =
            epee::blob::data
            ((const uint8_t*)(&nonce), sizeof(uint64_t));

          const crypto::hash h = crypto::sha3
            (
             hashing_blob_head
             + nonceData
             + hashing_blob_tail
             );

          if(check_hash(h, local_diff))
            {
              raw_block mined_block = b;
              mined_block.nonce = nonce;

              log_mined_block
                (
                 get_block_index(mined_block)
                 , get_block_hash(mined_block)
                 );

              m_phandler.handle_block_found(mined_block);
            }
          else {
            LOG_FATAL
              (
               "Failed to verify nonce: "
               + std::to_string(nonce)
               );
          }
        }

        nonce += worker_size * gpu_loop_size;
        m_hashes += worker_size * gpu_loop_size;

      }
    LOG_GLOBAL("OpenCL Miner thread stopped");

    return true;
  }

#endif

  void log_mined_block
  (
   const uint64_t block_index
   , const crypto::hash hash
   ) {
    LOG_CATEGORY_COLOR
      (
       epee::LogLevel::Info
       , epee::GLOBAL_CATEGORY
       , epee::green
       , std::string()
       + "Found "
       + std::string(config::lol::unicode_gem_stone)
       + " "
       + std::to_string(block_index)
       + ": "
       + hash.to_str()
       );
  }

}
