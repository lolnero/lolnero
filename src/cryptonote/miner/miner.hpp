/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE


Parts of this file are originally 
copyright (c) 2012-2013 The Cryptonote developers

*/

#pragma once

#include "cryptonote/basic/functional/base.hpp"
#include "cryptonote/basic/type/verification_context.hpp"
#include "math/blockchain/functional/difficulty.hpp"
#include "cryptonote/basic/type/string_blob_type.hpp"

#include "tools/epee/include/time_helper.h"

#ifdef OpenCL

#include "math/hash/opencl/cl_version.hpp"

#endif

namespace cryptonote
{

  struct i_miner_handler
  {
    virtual bool handle_block_found(const raw_block& b) = 0;

    virtual
    std::optional
    <std::tuple
     < raw_block
       , diff_t
       , uint64_t
       >>
    create_block_template
    (
     const spend_view_public_keys& miner_address
     ) = 0;
  };

  class miner
  {
  public:
    miner
    (
     i_miner_handler& phandler
     , const network_type nettype
     , const std::string mining_address
     , const uint32_t opencl_mining_threads
     );

    ~miner();

    bool set_block_template
    (
     const raw_block& bl
     , const diff_t& diffic
     , uint64_t block_reward
     );

    bool on_block_chain_update();
    bool start
    (const spend_view_public_keys& adr, size_t threads_count);

    uint64_t get_speed() const;
    uint32_t get_threads_count() const;
    void send_stop_signal();
    bool stop();
    bool is_mining() const;
    const spend_view_public_keys get_mining_address() const;
    bool on_idle();
    void on_synchronized();
    void pause();
    void resume();
    uint64_t get_block_reward() const { return m_block_reward; }

  private:
    bool worker_thread();

#ifdef OpenCL
    bool opencl_miner
    (
     const cl::Device device
     );
#endif

    bool request_block_template();
    void  merge_hr();

    i_miner_handler& m_phandler;
    std::optional<spend_view_public_keys> m_mine_address;
    std::atomic<uint32_t> m_threads_total = 0;

    std::atomic<bool> m_stop = true;
    std::mutex m_template_lock;
    std::mutex m_thread_lock;

    raw_block m_template{};
    std::atomic<uint32_t> m_template_no = 0;
    std::atomic<uint64_t> m_starter_nonce = 0;
    diff_t m_diff = 0;
    std::atomic<bool> m_pauser = false;

    std::optional<std::thread> m_thread;

    epee::time_helper::once_a_time_seconds<10>
    m_update_block_template_interval;

    epee::time_helper::once_a_time_seconds<2>
    m_update_merge_hr_interval;

    std::atomic<uint64_t> m_last_hr_merge_time = 0;
    std::atomic<uint64_t> m_hashes = 0;
    std::atomic<uint64_t> m_current_hash_rate = 0;
    std::mutex m_last_hash_rates_lock;
    std::list<uint64_t> m_last_hash_rates;
    bool m_do_mining = false;
    std::atomic<uint64_t> m_block_reward = 0;
  };

  void log_mined_block
  (
   const uint64_t block_index
   , const crypto::hash hash
   );
}
