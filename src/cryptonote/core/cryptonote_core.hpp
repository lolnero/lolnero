/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE


Parts of this file are originally 
copyright (c) 2012-2013 The Cryptonote developers

*/

#pragma once


#include "tools/common/util.h"
#include "tools/epee/include/syncobj.h"
#include "math/blockchain/controller/tx_pool.hpp"

#include "math/blockchain/functional/coinbase_tx.hpp"
#include "math/blockchain/functional/ringct.hpp"
#include "math/blockchain/controller/tx_pool.hpp"


#include "cryptonote/protocol/cryptonote_protocol_handler_common.h"

#include "cryptonote/basic/type/events.hpp"
#include "cryptonote/miner/miner.hpp"
#include "math/blockchain/controller/blockchain_mem.hpp"


namespace cryptonote
{
  class core final: public i_miner_handler
  {

  public:
    const network_type m_nettype;
    const bool m_offline;
    std::optional<uint64_t> m_target_top_block_index;
    blocktree_t m_blocktree;

  private:
    i_cryptonote_protocol& m_pprotocol;
    const std::string m_config_folder;

    miner m_miner;

    std::atomic<bool> m_starter_message_showed = false;

    const bool m_ignore_checkpoints = false;
    const std::optional<diff_t> m_fixed_difficulty;

  public:

    core
    (
     i_cryptonote_protocol& pprotocol
     , const network_type nettype
     , const bool offline
     , const std::string data_dir
     , const std::string mining_address
     , const uint32_t opencl_mining_threads
     , const bool ignore_checkpoints
     , const std::optional<uint64_t> maybe_fix_difficulty
     );

    bool init();
    bool deinit();

    void stop();

    bool on_idle();

    i_cryptonote_protocol& get_protocol() { return m_pprotocol; }

    //-------------------- i_miner_handler -----------------------

    virtual bool handle_block_found(const raw_block& b) override;

    virtual
    std::optional
    <std::tuple
     < raw_block
       , diff_t
       , uint64_t
       >>
    create_block_template
    (const spend_view_public_keys& miner_address) override;

    miner& get_miner() { return m_miner; }
    const miner& get_miner() const { return m_miner; }

    blocktree_t& get_tree() { return m_blocktree; }
    const blocktree_t& get_tree() const { return m_blocktree; };

    void on_synchronized();
  };

}
