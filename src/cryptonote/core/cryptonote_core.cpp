/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Parts of this file are originally
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE


Parts of this file are originally
copyright (c) 2012-2013 The Cryptonote developers

*/

#include "cryptonote_core.hpp"

#include "math/blockchain/functional/tx_check.hpp"
#include "cryptonote/tx/functional/tx_utils.hpp"
#include "cryptonote/basic/functional/format_utils.hpp"

#include "math/ringct/functional/ringCT.hpp"

#include <boost/uuid/nil_generator.hpp>

#include <filesystem>
#include <ranges>
#include <iostream>
#include <fstream>

namespace cryptonote
{
  std::mutex m_incoming_tx_lock;

  core::core
    (
     i_cryptonote_protocol& pprotocol
     , const network_type nettype
     , const bool offline
     , const std::string data_dir
     , const std::string mining_address
     , const uint32_t opencl_mining_threads
     , const bool ignore_checkpoints
     , const std::optional<uint64_t> maybe_fix_difficulty
     )
      :
      m_nettype(nettype)
      , m_offline(offline)
      , m_pprotocol(pprotocol)
      , m_config_folder(data_dir)
      , m_miner
      (
       *this
       , nettype
       , mining_address
       , opencl_mining_threads
       )
      , m_ignore_checkpoints(ignore_checkpoints)
      , m_fixed_difficulty(maybe_fix_difficulty)
    {
    }

  bool core::init()
  {
    std::filesystem::path folder(m_config_folder);
    if (m_nettype == FAKECHAIN)
      folder /= "fake";

    // make sure the data directory exists, and try to lock it
    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       std::filesystem::exists(folder)
       || std::filesystem::create_directories(folder)
       , false
       , std::string("Failed to create directory ")
       + folder.string()
       );

    const std::string filename = folder.string();

    const auto json_file_path =
      std::filesystem::path(m_config_folder)
      / config::lol::mem_db_file_name;


    const auto maybe_block_tree =
      blocktree_t::load(json_file_path.string());

    if (maybe_block_tree) {
      m_blocktree = *maybe_block_tree;
    }

    m_blocktree.set_fixed_difficulty(m_fixed_difficulty);
    m_blocktree.set_ignore_checkpoints(m_ignore_checkpoints);

    return true;
  }


  bool core::deinit()
  {
    m_miner.stop();

    const auto json_file_path = std::filesystem::path(m_config_folder)
      / config::lol::mem_db_file_name;

    m_blocktree.store(json_file_path.string());
    return true;
  }

  void core::stop()
  {
    m_miner.stop();
  }


  std::optional
  <std::tuple
   < raw_block
     , diff_t
     , uint64_t
     >>
  core::create_block_template
  (
   const spend_view_public_keys& miner_address
   )
  {
    raw_block b{};
    b.major_version = config::lol::constant_hf_version;
    b.minor_version = config::lol::constant_hf_version;

    b.timestamp = time(NULL);

    if (!get_tree().check_block_timestamp(b.timestamp)) {
      LOG_FATAL("invalid local time");
      return {};
    }

    while (true) {
      const auto current_top_block_hash =
        get_tree().get_top_block().hash();

      const auto current_index =
        get_tree().get_top_block_index();


      b.prev_hash = current_top_block_hash;

      const auto next_block_index = current_index + 1;

      const auto
        [
         picked_txs
         , txs_weight
         , fee
          ] =
        get_tree().pick_txs_for_block_template(next_block_index);

      b.tx_hashes = {picked_txs.begin(), picked_txs.end()};

      const auto miner_tx =
        construct_miner_tx
        (next_block_index, txs_weight, fee, miner_address);

      ASSERT_OR_LOG_ERROR_AND_RETURN
        (
         miner_tx
         , {}
         , "Failed to construct miner tx"
         );

      b.miner_tx = *miner_tx;

      const auto expected_reward =
        consensus::get_block_reward() + fee;

      const diff_t diff =
        m_fixed_difficulty
        ? (*m_fixed_difficulty)
        : get_tree().get_next_diff()
        ;

      if (current_top_block_hash ==
          get_tree().get_top_block().hash()) {

        const std::tuple
          < raw_block
            , diff_t
            , uint64_t
            > r = {b, diff, expected_reward};

        // json j = r;
        // LOG_DEBUG
        //   (
        //    "miner block constructed: "
        //    + j.dump(4)
        //    );

        return r;
      }
    }
  }

  bool core::handle_block_found(const raw_block& b)
  {
    LOG_DEBUG("BLOCK found");

    m_miner.pause();

    const auto pool_txs = get_tree().pool_get_txs(b.tx_hashes);

    std::vector<ringct> txs;

    for (const auto& x: pool_txs) {
      if (!x.has_value()) {
        LOG_ERROR("tx not found in pool");
        m_miner.resume();
        return false;
      }
      txs.push_back(x->tx);
    }

    const auto maybe_block = preview_block(b, txs);

    if (!maybe_block) {
      LOG_ERROR("mined invalid block format");
      m_miner.resume();
      return false;
    }

    const auto block = *maybe_block;

    const auto block_add_result = get_tree().push_block(block);

    switch (block_add_result)
      {
      case block_add_t::rejected:
        LOG_FATAL("mined block failed verification");
        m_miner.resume();
        return false;

      case block_add_t::ignored:
        LOG_FATAL("parellel universe detected, aliens are real");
        m_miner.resume();
        return false;

      case block_add_t::saved:
        LOG_INFO("too slow, need a new GPU");
        m_miner.on_block_chain_update();
        m_miner.resume();
        return true;

      case block_add_t::added:
      default:
        break;
      }

    m_miner.on_block_chain_update();
    m_miner.resume();

    cryptonote_connection_context exclude_context = {};

    NOTIFY_NEW_FLUFFY_BLOCK::request arg{};
    arg.p2p_block_index =
      get_tree().get_size();

    arg.b = review_block_to_block_and_txs_data_t(block);
    arg.b.txs = {};

    m_pprotocol.relay_block(arg, exclude_context);

    return true;
  }


  void core::on_synchronized()
  {
    m_miner.on_synchronized();
  }


  bool core::on_idle()
  {
    if(!m_starter_message_showed)
    {
      if (m_offline) {
        constexpr std::string_view main_message =
          "The daemon is running offline.";

        LOG_CATEGORY_COLOR
          (
           epee::LogLevel::Info
           , epee::GLOBAL_CATEGORY
           , epee::yellow
           , "******************************************************"
           );

        LOG_CATEGORY_COLOR
          (
           epee::LogLevel::Info
           , epee::GLOBAL_CATEGORY
           , epee::yellow
           , main_message
           );

        LOG_CATEGORY_COLOR
          (
           epee::LogLevel::Info
           , epee::GLOBAL_CATEGORY
           , epee::yellow
           , "******************************************************"
           );
      }
      m_starter_message_showed = true;
    }

    m_miner.on_idle();
    return true;
  }
} // cryptonote
