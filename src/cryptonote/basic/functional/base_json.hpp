/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#pragma once

#include "base.hpp"

#include "tools/serialization/json.hpp"

#include "math/json/all.hpp"
#include "tools/serialization/json.hpp"

namespace cryptonote
{ 
  void to_json(json& j, const txin_from_key & x);
  inline void from_json(const json& j, txin_from_key& x) {};


  void to_json(json& j, const txin_v& x);
  inline void from_json(const json& j, txin_v& x) {};


  void to_json(json& j, const txout_target_v& x);
  inline void from_json(const json& j, txout_target_v& x) {}


  void to_json(json& j, const tx_out& x);
  inline void from_json(const json& j, tx_out& x) {}

  JSON_LAYOUT
  (
   raw_block
   // , major_version
   // , minor_version
   , timestamp
   , prev_hash
   , nonce
   , miner_tx
   , tx_hashes
   );


}
