// Copyright (c) 2014-2020, The Monero Project
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, are
//
// permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this list of
//    conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this list
//    of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may be
//    used to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
// THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
// THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Parts of this file are originally copyright (c) 2012-2013 The Cryptonote developers

#pragma once


#include "tools/epee/include/misc_language.h"
#include "tools/epee/include/serialization/keyvalue_serialization.h" // eepe named serialization
#include "tools/epee/include/string_tools.h"

#include "tools/serialization/binary_archive.h"
#include "tools/serialization/crypto.h"
#include "tools/serialization/variant.h"

#include "cryptonote/basic/type/string_blob_type.hpp"

#include "math/crypto/controller/keyGen.hpp"
#include "math/ringct/functional/rctTypesUnsafe.hpp"
#include "math/blockchain/functional/subaddress.hpp"

#include "config/cryptonote.hpp"

#include <boost/variant.hpp>
#include <boost/functional/hash.hpp>


namespace cryptonote
{
  /* outputs */
  struct txout_to_key
  {
    crypto::public_key output_public_key;
  };

  /* inputs */

  struct txin_gen
  {
    size_t block_index;

    BEGIN_SERIALIZE_OBJECT()
      VARINT_FIELD(block_index)
    END_SERIALIZE()
  };

  struct txin_from_key
  {
    uint64_t amount = 0;
    std::vector<uint64_t> output_relative_offsets;
    crypto::key_image output_key_image;      // double spending protection

    BEGIN_SERIALIZE_OBJECT()
      VARINT_FIELD(amount)
      FIELD(output_relative_offsets)
      FIELD(output_key_image)
    END_SERIALIZE()
  };

  using txin_v = boost::variant<txin_gen, txin_from_key>;

  using txout_target_v = boost::variant<txout_to_key>;

  struct tx_out
  {
    uint64_t amount;
    txout_target_v target;

    BEGIN_SERIALIZE_OBJECT()
      VARINT_FIELD(amount)
      FIELD(target)
    END_SERIALIZE()
  };

  struct transaction_prefix
  {
    // tx information
    size_t   version = 1;
    uint64_t unlock_block_index = 0;  //number of block (or time), used as a limitation like: spend this tx not early then block/time

    std::vector<txin_v> vin;
    std::vector<tx_out> vout;
    //extra
    std::vector<uint8_t> extra;

    BEGIN_SERIALIZE()
      VARINT_FIELD(version)
      VARINT_FIELD(unlock_block_index)
      FIELD(vin)
      FIELD(vout)
      FIELD(extra)
    END_SERIALIZE()
  };


  template<typename T> unsigned int getpos(T &ar) {
    return 0;
  }

  inline unsigned int getpos(binary_archive<true> &ar) {
    return ar.stream().tellp();
  }

  inline unsigned int getpos(binary_archive<false> &ar) {
    return ar.stream().tellg();
  }

  struct transaction: public transaction_prefix
  {
    rct::rctData_unsafe ringct;

    // hash cash
    unsigned int prefix_and_ringct_basic_size = 0;
    unsigned int prefix_size = 0;


    BEGIN_SERIALIZE_OBJECT()
    const unsigned int start_pos = getpos(ar);

    FIELDS(*static_cast<transaction_prefix *>(this))

    if (std::is_same<Archive<W>, binary_archive<W>>())
      prefix_size = getpos(ar) - start_pos;

    if (version == 1)
      {
        if (std::is_same<Archive<W>, binary_archive<W>>())
          prefix_and_ringct_basic_size = getpos(ar) - start_pos;

        ar.tag("signatures");
        ar.begin_array();
        ar.end_array();
      }
    else
      {
        ar.tag("ringct");
        if (!vin.empty())
          {
            ar.begin_object();
            bool r = ringct.serialize_rctsig_base(ar, vin.size(), vout.size());
            if (!r || !ar.stream().good()) return false;
            ar.end_object();
            if (std::is_same<Archive<W>, binary_archive<W>>())
              prefix_and_ringct_basic_size = getpos(ar) - start_pos;
            if (ringct.rct_type != rct::RCTTypeNull)
              {
                ar.tag("ringct_prunable");
                ar.begin_object();
                r = ringct.p.serialize_ringct_prunable
                  (
                   ar
                   , ringct.rct_type
                   , vin.size()
                   , vout.size()
                   , vin.size() > 0 && vin[0].type() == typeid(txin_from_key) ?
                   boost::get<txin_from_key>(vin[0]).output_relative_offsets.size() - 1
                   : 0
                   );
                if (!r || !ar.stream().good()) return false;
                ar.end_object();
              }
          }
      }

    END_SERIALIZE()

    template<bool W, template <bool> class Archive>
    bool serialize_base(Archive<W> &ar)
    {
      FIELDS(*static_cast<transaction_prefix *>(this))

        if (version != 1)
          {
            ar.tag("ringct");
            if (!vin.empty())
              {
                ar.begin_object();
                bool r = ringct.serialize_rctsig_base(ar, vin.size(), vout.size());
                if (!r || !ar.stream().good()) return false;
                ar.end_object();
              }
          }
      return ar.stream().good();
    }
  };

  /************************************************************************/
  /*                                                                      */
  /************************************************************************/
  struct block_header
  {
    uint8_t major_version = 0;
    uint8_t minor_version = 0;  // now used as a voting mechanism, rather than how this particular block is built
    uint64_t timestamp;
    crypto::hash prev_hash;
    uint64_t nonce;

    BEGIN_SERIALIZE()
      VARINT_FIELD(major_version)
      VARINT_FIELD(minor_version)
      VARINT_FIELD(timestamp)
      FIELD(prev_hash)
      FIELD(nonce)
    END_SERIALIZE()
  };

  struct raw_block: public block_header
  {
  public:
    transaction miner_tx;
    std::vector<crypto::hash> tx_hashes;

    // hash cash

    BEGIN_SERIALIZE_OBJECT()
      FIELDS(*static_cast<block_header *>(this))
      FIELD(miner_tx)
      FIELD(tx_hashes)
    END_SERIALIZE()
  };



  /************************************************************************/
  /*                                                                      */
  /************************************************************************/
  struct keypair
  {
    crypto::secret_key sec;
    crypto::public_key pub;

    static inline keypair generate()
    {
      const auto sec = crypto::s2sk(crypto::randomScalar());
      return keypair {sec, crypto::to_pk(sec)};
    }
  };
  //---------------------------------------------------------------

  std::optional<txin_gen> is_coinbase(const transaction& tx);

  bool operator ==(const cryptonote::transaction& a, const cryptonote::transaction& b);
  bool operator ==(const cryptonote::raw_block& a, const cryptonote::raw_block& b);

  std::string get_account_address_as_str
  (
   const network_type nettype
   , const bool subaddress
   , const spend_view_public_keys& adr
   );


  struct public_address_outer_blob
  {
    uint8_t m_ver;
    spend_view_public_keys m_address;
    uint8_t check_sum;
  };

  struct address_parse_info
  {
    spend_view_public_keys address;
    bool is_subaddress;
  };

  uint8_t get_account_address_checksum(const public_address_outer_blob& bl);


  struct block_and_txs_data_t
  {
    string_blob block;
    std::vector<string_blob> txs;
    BEGIN_KV_SERIALIZE_MAP()
      KV_SERIALIZE(block)
      KV_SERIALIZE(txs)
    END_KV_SERIALIZE_MAP()
  };

}


BLOB_SERIALIZER(cryptonote::txout_to_key);

VARIANT_TAG(binary_archive, cryptonote::txin_gen, 0xff);
VARIANT_TAG(binary_archive, cryptonote::txin_from_key, 0x2);
VARIANT_TAG(binary_archive, cryptonote::txout_to_key, 0x2);
