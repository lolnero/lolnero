/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Parts of this file are originally
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE


Parts of this file are originally
copyright (c) 2012-2013 The Cryptonote developers

*/

#pragma once

#include "cryptonote/basic/functional/base.hpp"

#include "cryptonote/basic/functional/account.hpp"

namespace cryptonote
{

  struct subaddress_receive_info
  {
    const subaddress_index index;
    const crypto::ecdh_shared_secret tx_output_shared_secret;
  };


  std::optional<std::pair<keypair, crypto::key_image>>
  derive_with_internal_checking_output_key_pair_and_key_image
  (
   const account_keys ack
   , const std::map<crypto::public_key, subaddress_index>&
   subaddresses
   , const crypto::public_key out_key
   , const std::optional<crypto::public_key> tx_public_key
   , const std::span<const crypto::public_key> output_public_keys
   , const size_t real_output_index
   );


  std::optional<std::pair<keypair, crypto::key_image>>
  derive_output_key_pair_and_key_image
  (
   const account_keys ack
   , const crypto::public_key out_key
   , const crypto::ecdh_shared_secret recv_tx_output_shared_secret
   , const size_t real_output_index
   , const subaddress_index received_index
   );


  std::optional<subaddress_receive_info> check_output_for_subaddresses
  (
   const std::map<crypto::public_key, subaddress_index>& subaddresses
   , const crypto::public_key tx_out_key
   , const crypto::ecdh_shared_secret tx_output_shared_secret
   , const size_t output_index
   );

}
