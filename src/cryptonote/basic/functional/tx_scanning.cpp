/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE


Parts of this file are originally 
copyright (c) 2012-2013 The Cryptonote developers

*/

#include "tx_scanning.hpp"


namespace cryptonote
{
  std::optional<std::pair<keypair, crypto::key_image>>
  derive_with_internal_checking_output_key_pair_and_key_image
  (
   const account_keys ack
   , const std::map<crypto::public_key, subaddress_index>&
   subaddresses
   , const crypto::public_key out_key
   , const std::optional<crypto::public_key> tx_public_key
   , const std::span<const crypto::public_key> output_public_keys
   , const size_t real_output_index
   )
  {
    const std::optional<crypto::ecdh_shared_secret>
      output_shared_secret = 
      (
       real_output_index >= 0
       && real_output_index < output_public_keys.size()
       )
      ? std::make_optional
      (
       crypto::derive_tx_output_ecdh_shared_secret
       (
        output_public_keys[real_output_index]
        , ack.m_view_secret_key
        )
       )
      : tx_public_key
      ? std::make_optional
      (crypto::derive_tx_output_ecdh_shared_secret
       (*tx_public_key, ack.m_view_secret_key))
      : std::nullopt
      ;

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       output_shared_secret
       , {}
       , "failed to generate a valid output shared secret"
       );

    std::optional<subaddress_receive_info> subaddr_recv_info =
      check_output_for_subaddresses
      (
       subaddresses, out_key, *output_shared_secret, real_output_index
       );

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       subaddr_recv_info
       , {}
       , "key image helper: "
       "given output pubkey doesn't seem to belong to this address"
       );

    return derive_output_key_pair_and_key_image
      (
       ack
       , out_key
       , subaddr_recv_info->tx_output_shared_secret
       , real_output_index
       , subaddr_recv_info->index
       );
  }


  std::optional<std::pair<keypair, crypto::key_image>>
  derive_output_key_pair_and_key_image
  (
   const account_keys account_keys
   , const crypto::public_key out_key
   , const crypto::ecdh_shared_secret recv_tx_output_shared_secret
   , const size_t real_output_index
   , const subaddress_index received_index
   )
  {
    const crypto::secret_key spend_sk = get_subaddress_spend_secret_key
      (account_keys.get_spend_view_secret_keys(), received_index);
    const crypto::secret_key output_secret_key =
      compute_output_secret_key_from_subaddress_spend_sk
      (recv_tx_output_shared_secret, real_output_index, spend_sk);

    const keypair output_key_pair =
      {
        output_secret_key
        , to_pk(output_secret_key)
      };

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       output_key_pair.pub == out_key
       , {}
       , "key image helper precomp: "
       "given output pubkey doesn't match the derived one"
       );

    const crypto::key_image ki =
      crypto::derive_key_image(output_key_pair.sec);

    return {{output_key_pair, ki}};
  }


  std::optional<subaddress_receive_info> check_output_for_subaddresses
  (
   const std::map<crypto::public_key, subaddress_index>& subaddresses
   , const crypto::public_key tx_output_public_key
   , const crypto::ecdh_shared_secret tx_output_shared_secret
   , const size_t output_index
   )
  {
    // try additional tx pubkeys if available
    const auto spend_pk_1 =
      crypto::compute_subaddress_spend_pk_from_output_public_key
      (tx_output_shared_secret, output_index, tx_output_public_key);

    if (spend_pk_1) {
      const auto found_1 = subaddresses.find(*spend_pk_1);

      if (found_1 != subaddresses.end()) {
        return subaddress_receive_info
          { found_1->second
            , tx_output_shared_secret
          };
      }
    }
    return {};
  }


}
