/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include "base_json.hpp"

#include "cryptonote/basic/functional/tx_extra.hpp"
#include "cryptonote/basic/functional/format_utils.hpp"

namespace cryptonote
{ 
  void to_json(json& j, const txin_from_key & x) {
    j = x.output_key_image;
  }

  void to_json(json& j, const txin_v& x) {
    x.type() == typeid(txin_gen)
      ? j["gen"] = boost::get<txin_gen>(x).block_index
      : j["key_image"] = boost::get<txin_from_key>(x)
      ;
  }

  void to_json(json& j, const txout_target_v& x) {
    to_json(j, boost::get<txout_to_key>(x).output_public_key);
  }

  void to_json(json& j, const tx_out& x) {
    if (x.amount > 0) {
      j["amount"] = x.amount;
    }
    j["public_key"] = x.target;
  }

}
