// Copyright (c) 2014-2020, The Monero Project
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this list of
//    conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this list
//    of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may be
//    used to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
// THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
// THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Parts of this file are originally copyright (c) 2012-2013 The Cryptonote developers

#include "base.hpp"

#include "cryptonote/basic/functional/format_utils.hpp"

#include "tools/common/base58.h"

#include "math/consensus/consensus.hpp"

namespace cryptonote
{
  //-----------------------------------------------------------------------
  std::optional<txin_gen> is_coinbase(const transaction& tx)
  {
    if (!consensus::
        rule_28_coinbase_tx_should_have_rct_null_type_in_rct_data
        (tx.ringct.rct_type)
        ) {
      return {};
    }

    const auto maybe_vin = consensus::rule_22_coinbase_tx_should_have_only_one_input(tx.vin);
    if(!maybe_vin) {
      return {};
    }

    return consensus::rule_23_coinbase_tx_input_type_should_be_gen(*maybe_vin);
  }

  //--------------------------------------------------------------------------------
  bool operator ==(const cryptonote::transaction& a, const cryptonote::transaction& b) {
    return cryptonote::get_transaction_hash(a) == cryptonote::get_transaction_hash(b);
  }

  bool operator ==(const cryptonote::raw_block& a, const cryptonote::raw_block& b) {
    return cryptonote::get_block_hash(a) == cryptonote::get_block_hash(b);
  }

  //------------------------------------------------------------------------------------
  std::string get_account_address_as_str
  (
   const network_type nettype
   , const bool subaddress
   , const spend_view_public_keys & adr
   )
  {
    uint64_t address_prefix = subaddress ? get_config(nettype).CRYPTONOTE_PUBLIC_SUBADDRESS_BASE58_PREFIX : get_config(nettype).CRYPTONOTE_PUBLIC_ADDRESS_BASE58_PREFIX;

    return tools::base58::encode_addr(address_prefix, t_serializable_object_to_blob(adr));
  }

  //------------------------------------------------------------------------------------
  uint8_t get_account_address_checksum(const public_address_outer_blob& bl)
  {
    const auto buf = epee::pod_to_span(bl);
    const auto buf_data = buf.subspan(0, buf.size() - 1);

    return std::reduce(buf_data.begin(), buf_data.end());
  }

}
