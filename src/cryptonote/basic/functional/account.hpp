/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/

#pragma once

#include "cryptonote/basic/functional/base.hpp"
#include "math/blockchain/functional/subaddress.hpp"

namespace cryptonote
{
  struct account_keys
  {
    crypto::secret_key   m_spend_secret_key;
    crypto::secret_key   m_view_secret_key;

    spend_view_public_keys get_spend_view_public_keys() const {
      return
        {
          crypto::to_pk(m_spend_secret_key)
          , crypto::to_pk(m_view_secret_key)
        };
    }
                                                              
    spend_view_secret_keys get_spend_view_secret_keys() const {
      return {m_spend_secret_key, m_view_secret_key};
    }
  };

  struct account_base
  {
    account_base() = default;

    account_base(const crypto::secret_key);

    const account_keys& get_keys() const;

    std::string
    get_public_address_str(const network_type nettype) const;

  private:
    account_keys m_keys{};
  };


  std::optional<address_parse_info> get_account_address_from_str
  (
   const network_type nettype
   , const std::string_view str
   );
}
