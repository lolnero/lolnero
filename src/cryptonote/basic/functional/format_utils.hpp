/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Parts of this file are originally
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE


Parts of this file are originally
copyright (c) 2012-2013 The Cryptonote developers

*/

#pragma once

#include "object_hash.hpp"

#include "cryptonote/basic/functional/base.hpp"

#include "cryptonote/basic/type/string_blob_type.hpp"
#include "cryptonote/basic/type/tx_extra.hpp"
#include "cryptonote/basic/functional/base_json.hpp"

#include "tools/epee/functional/blob.hpp"
#include "tools/epee/functional/wipeable_string.hpp"
#include "tools/serialization/json.hpp"
#include "tools/serialization/binary_utils.h"

namespace cryptonote
{
  template <typename T>
  std::string obj_to_json_str(const T& obj)
  {
    const json x = obj;
    return x.dump();
  }


  uint64_t get_transaction_size(const transaction &tx);

  uint64_t get_block_index(const raw_block& b);

  bool check_output_amount_sum_overflow(const transaction& tx);

  std::vector<uint64_t> relative_output_offsets_to_absolute
  (const std::span<const uint64_t> off);

  std::vector<uint64_t> absolute_output_offsets_to_relative
  (const std::span<const uint64_t> off);

  std::optional<transaction> preview_tx_from_blob
  (const string_blob_view tx_blob);

  std::optional<std::pair<transaction, crypto::hash>>
  maybe_tx_and_hash_from_blob(const string_blob_view tx_blob);

  std::optional<raw_block> preview_block_from_blob
  (const string_blob_view b_blob);

  std::optional<std::pair<raw_block, crypto::hash>>
  maybe_block_and_hash_from_blob(const string_blob_view b_blob);

  std::string print_money(const uint64_t amount);

  block_t lol_genesis_block();

}


#define DEFINE_WITH_TX_INPUT_OR_RETURN(in, var, r)  \
                                                    \
  ASSERT_OR_LOG_ERROR_AND_RETURN                       \
  ( in.type() == typeid(txin_from_key)              \
    , r                                             \
    , std::string()                                 \
    + "wrong variant type: "                        \
    + in.type().name()                              \
    + ", expected txin_from_key");                  \
                                                    \
  const auto& var =                                 \
    boost::get<txin_from_key>(in);
