/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Parts of this file are originally
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE


Parts of this file are originally
copyright (c) 2012-2013 The Cryptonote developers

*/

#pragma once

#include "cryptonote/basic/functional/base.hpp"

#include "cryptonote/basic/type/string_blob_type.hpp"
#include "cryptonote/basic/type/tx_extra.hpp"

#include "tools/epee/functional/blob.hpp"
#include "tools/epee/functional/wipeable_string.hpp"
#include "tools/serialization/binary_utils.h"

namespace cryptonote
{
  crypto::hash get_transaction_prefix_hash
  (const transaction_prefix& tx);


  crypto::hash get_blob_hash(const string_blob_view blob);


  crypto::hash calculate_transaction_prunable_hash
  (
   const transaction& t
   , const cryptonote::string_blob_view blob
   );


  template<class t_object>
  string_blob t_serializable_object_to_blob(const t_object& to)
  {
    std::ostringstream ss;
    binary_archive<true> ba(ss);
    const bool r =
      serialization::serialize(ba, const_cast<t_object&>(to));
    if (!r) {
      throw std::runtime_error("failed to serialize object to blob");
    }
    return ss.str();
  }


  template<class t_object>
  std::optional<t_object> preview_object_from_blob
  (const string_blob_view b_blob)
  {
    t_object x{};
    const bool r = ::serialization::parse_binary(b_blob, x);
    if (r) {
      return x;
    } else {
      return {};
    }
  }


  template<class t_object>
  crypto::hash get_object_hash(const t_object& o)
  {
    return get_blob_hash(t_serializable_object_to_blob(o));
  }

  crypto::hash get_tx_tree_hash
  (const std::span<const crypto::hash> tx_hashes);

  crypto::hash get_block_tree_hash(const raw_block& b);

  crypto::hash get_block_hash(const raw_block& b);

  crypto::hash get_transaction_hash(const transaction& t);

  crypto::hash get_mining_hash(const raw_block& b);

  string_blob get_mining_blob(const raw_block& b);
  string_blob get_mining_blob_head(const raw_block& b);
  string_blob get_mining_blob_tail(const raw_block& b);

  string_blob block_to_blob(const raw_block& b);
  string_blob tx_to_blob(const transaction& b);

}
