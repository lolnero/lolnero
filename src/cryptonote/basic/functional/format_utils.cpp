/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE


Parts of this file are originally 
copyright (c) 2012-2013 The Cryptonote developers

*/

#include "format_utils.hpp"

#include "tools/epee/include/string_tools.h"

#include "math/hash/functional/tree-hash.hpp"
#include "math/ringct/functional/ringCT.hpp"
#include "math/ringct/functional/rctOps.hpp"
#include "math/blockchain/functional/ringct.hpp"
#include "math/blockchain/functional/block.hpp"
#include "math/consensus/consensus.hpp"

#include <boost/algorithm/string.hpp>


namespace cryptonote
{
  uint64_t get_transaction_size(const transaction &tx)
  {
    return t_serializable_object_to_blob(tx).size();
  }


  uint64_t get_block_index(const raw_block& b)
  {
    return
      is_coinbase(b.miner_tx).value_or(txin_gen{}).block_index - 1;
  }


  bool check_output_amount_sum_overflow(const transaction& tx)
  {
    std::vector<uint64_t> output_amount;
    std::transform
      (
       tx.vout.begin()
       , tx.vout.end()
       , std::back_inserter(output_amount)
       , [](const auto&x ) { return x.amount; }
       );

    return consensus::
      rule_24_coinbase_output_amount_sum_should_not_overflow_amount_t
      (output_amount).has_value();
  }

  std::vector<uint64_t> relative_output_offsets_to_absolute
  (const std::span<const uint64_t> xs)
  {
    const std::basic_string<uint64_t> ys =
      std::accumulate
      (
       xs.begin()
       , xs.end()
       , std::basic_string<uint64_t>{0}
       , [](const auto& x, const auto& y)
       -> std::basic_string<uint64_t> {
         return x + std::basic_string<uint64_t>{x.back() + y};
       }
       );

    return {std::next(ys.begin()), ys.end()};
  }


  std::vector<uint64_t> absolute_output_offsets_to_relative
  (const std::span<const uint64_t> xs)
  {
    const auto ys =
      std::basic_string<uint64_t>{0}
    + std::basic_string<uint64_t>{xs.begin(), xs.end()};

    std::list<uint64_t> zs;

    std::transform
      (
       xs.begin()
       , xs.end()
       , ys.begin()
       , std::back_inserter(zs)
       , std::minus()
       );

    return {zs.begin(), zs.end()};
  }

  std::optional<transaction> preview_tx_from_blob
  (const string_blob_view tx_blob)
  {
    const auto maybeTx =
      preview_object_from_blob<transaction>(tx_blob);

    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       maybeTx
       , {}
       , "Failed to parse transaction from blob"
       );

    const auto tx = *maybeTx;

    return tx;
  }


  std::optional<std::pair<transaction, crypto::hash>>
  maybe_tx_and_hash_from_blob(const string_blob_view tx_blob)
  {
    const auto maybeTx = preview_tx_from_blob(tx_blob);
    if (maybeTx) {
      return {{*maybeTx, get_transaction_hash(*maybeTx)}};
    }
    else {
      return {};
    }
  }


  std::optional<raw_block>
  preview_block_from_blob(const string_blob_view b_blob)
  {
    return preview_object_from_blob<raw_block>(b_blob);
  }


  std::optional<std::pair<raw_block, crypto::hash>>
  maybe_block_and_hash_from_blob(const string_blob_view b_blob)
  {
    const auto maybeBlock = preview_block_from_blob(b_blob);
    if (maybeBlock) {
      const auto& b = *maybeBlock;
      const auto block_hash = get_block_hash(b);
      return {{b, block_hash}};
    } else {
      return {};
    }
  }


  std::string print_money(const uint64_t amount) {
    std::stringstream out;
    out << std::scientific << static_cast<double>(amount);
    return out.str();
  }

  block_t lol_genesis_block() {
    const auto tx_extra_blob =
      epee::hex::decode_from_hex_to_blob
      (
       "01bc63d4bc04d6454c00408ecdcd677cf"
       "77c0227fbf21f58eec1e8df0ed680a335"
       ).value_or(epee::blob::data())
      ;

    const tx_extra genesis_coinbase_tx_extra =
      {
        { tx_extra_blob.begin(), tx_extra_blob.end() }
      };

    const tx_common genesis_tx_common = 
      {
        61
        , genesis_coinbase_tx_extra
      };

    const std::vector<coinbase_output> genesis_coinbase_tx_outputs
      =
      {
        {
          consensus::get_block_reward()
          , crypto::preview_point_from_base64
          ("0sUgQllmTDXDbG03QxSTWfSUSA/7aLnJiF2YWSAf7MU=")
          .value_or(crypto::identity)
        }
      };

    const coinbase_tx genesis_coinbase =
      {
        genesis_tx_common
        , 1
        , crypto::preview_point_from_base64
        ("vGPUvATWRUwAQI7NzWd893wCJ/vyH1juwejfDtaAozU=")
        .value_or(crypto::identity)
        , genesis_coinbase_tx_outputs
      };

    const crypto::hash old_genesis_hash = 
      crypto::preview_hash_from_base64
      (
       "/gZ4wKKeNMxFXANXkUAp0cNO+2gk5zC9nvRs0PDc6c4="
       ).value_or(crypto::hash{});


    const block_t genesis =
      {
        config::lol::constant_hf_version
        , config::lol::constant_hf_version
        , 1602437136
        , old_genesis_hash
        , 13196859321458624913ull
        , genesis_coinbase
        , {}
      };

    return genesis;
  }

}

