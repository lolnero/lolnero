/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE


Parts of this file are originally 
copyright (c) 2012-2013 The Cryptonote developers

*/

#include "object_hash.hpp"

#include "tools/epee/include/string_tools.h"
#include "tools/serialization/string.h" // don't remove, or face core dump

#include "math/hash/functional/tree-hash.hpp"

namespace cryptonote
{
  crypto::hash
  get_transaction_prefix_hash(const transaction_prefix& tx)
  {
    const string_blob bd = t_serializable_object_to_blob(tx);
    return crypto::sha3(epee::string_tools::string_to_blob(bd));
  }

  crypto::hash get_blob_hash(const string_blob_view blob)
  {
    return crypto::sha3
      (epee::string_tools::string_view_to_blob_view(blob));
  }


  crypto::hash calculate_transaction_prunable_hash
  (
   const transaction& t
   , const cryptonote::string_blob_view blob
   )
  {
    const unsigned int prefix_and_ringct_basic_size =
      t.prefix_and_ringct_basic_size;

    LOG_ERROR_AND_THROW_UNLESS
      (
       prefix_and_ringct_basic_size <= blob.size()
       , "Inconsistent transaction unprunable and blob sizes"
       );

    return cryptonote::get_blob_hash
      (
       string_blob_view
       (
        blob.data() + prefix_and_ringct_basic_size
        , blob.size() - prefix_and_ringct_basic_size
        )
       );
  }

  
  crypto::hash get_transaction_hash(const transaction& t)
  {
    const auto prefix_hash = get_transaction_prefix_hash(t);

    if (is_coinbase(t)) {

      const crypto::hash sha3_rct_type_null_0 =
        crypto::preview_hash_from_hex
        (
         "5d53469f20fef4f8eab52b88044ede69"
         "c77a6a68a60728609fc4a65ff531e7d0"
         ).value_or(crypto::null_hash);

      return crypto::sha3
        (
         prefix_hash.blob()
         + sha3_rct_type_null_0.blob()
         + crypto::null_hash.blob()
         );
    }

    const string_blob blob = tx_to_blob(t);
    const unsigned int prefix_and_ringct_basic_size =
      t.prefix_and_ringct_basic_size;

    const unsigned int prefix_size = t.prefix_size;

    // base rct
    if (!
        (
         prefix_size <= prefix_and_ringct_basic_size
           && prefix_and_ringct_basic_size <= blob.size()
         )) {
      LOG_FATAL
        (
         "Inconsistent transaction prefix"
         ", unprunable and blob sizes");
    }

    const auto ringct_basic_hash = cryptonote::get_blob_hash
      (blob.substr
       (prefix_size, prefix_and_ringct_basic_size - prefix_size));

    // prunable rct
    return crypto::sha3
      (
       prefix_hash.blob()
       + ringct_basic_hash.blob()
       + calculate_transaction_prunable_hash(t, blob).blob()
       );
  }


  crypto::hash get_tx_tree_hash
  (const std::span<const crypto::hash> tx_hashes)
  {
    return tree_hash(tx_hashes).value_or(crypto::null_hash);
  }


  crypto::hash get_block_tree_hash(const raw_block& b)
  {
    const crypto::hash h = get_transaction_hash(b.miner_tx);

    std::vector<crypto::hash> txs = {h};

    std::ranges::copy
      (
       b.tx_hashes
       , std::back_inserter(txs)
       );

    return get_tx_tree_hash(txs);
  }

  crypto::hash get_block_hash(const raw_block& b)
  {
    return get_object_hash(get_mining_blob(b));
  }

  crypto::hash get_mining_hash(const raw_block& b)
  {
    return crypto::sha3
      (epee::string_tools::string_to_blob(get_mining_blob(b)));
  }

  string_blob get_mining_blob_tail(const raw_block& b)
  {
    const auto tree_root_hash = get_block_tree_hash(b);

    return
      epee::string_tools::blob_to_string(tree_root_hash.data)
      + tools::get_varint_data(b.tx_hashes.size()+1)
      ;
  }


  string_blob get_mining_blob_head(const raw_block& b)
  {
    return
      t_serializable_object_to_blob(static_cast<block_header>(b));
  }


  string_blob get_mining_blob(const raw_block& b)
  {
    return get_mining_blob_head(b) + get_mining_blob_tail(b);
  }


  string_blob block_to_blob(const raw_block& b)
  {
    return t_serializable_object_to_blob(b);
  }


  string_blob tx_to_blob(const transaction& tx)
  {
    return t_serializable_object_to_blob(tx);
  }

}
