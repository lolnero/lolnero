/*

Copyright 2021 fuwa

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Parts of this file are originally 
Copyright (c) 2014-2020, The Monero Project

see: etc/other-licenses/monero/LICENSE

*/

#include "account.hpp"

#include "cryptonote/basic/functional/format_utils.hpp"

#include "tools/common/base58.h"


namespace cryptonote
{
  //-----------------------------------------------------------------
  account_base::account_base
  (
   const crypto::secret_key x
   )
    : m_keys
    {
      x
      , crypto::s2sk(crypto::hash_to_scalar(x.data))
    }
  {}

  //-----------------------------------------------------------------
  const account_keys& account_base::get_keys() const
  {
    return m_keys;
  }
  //-----------------------------------------------------------------
  std::string account_base::get_public_address_str
  (const network_type nettype) const
  {
    return get_account_address_as_str
      (nettype, false, m_keys.get_spend_view_public_keys());
  }

  std::optional<address_parse_info> get_account_address_from_str
  (
   const network_type nettype
   , const std::string_view str
   )
  {
    address_parse_info info{};

    const uint64_t address_prefix =
      get_config(nettype).CRYPTONOTE_PUBLIC_ADDRESS_BASE58_PREFIX;

    const uint64_t subaddress_prefix =
      get_config(nettype).CRYPTONOTE_PUBLIC_SUBADDRESS_BASE58_PREFIX;

    string_blob data;
    uint64_t prefix;

    if
      (
       !tools::base58::decode_addr
       (
        std::string(str)
        , prefix
        , data
        )
       )
      {
        LOG_PRINT_L2("Invalid address format");
        return {};
      }
    else if (address_prefix == prefix)
      {
        info.is_subaddress = false;
      }
    else if (subaddress_prefix == prefix)
      {
        info.is_subaddress = true;
      }
    else {
      LOG_PRINT_L1
        (
         "Wrong address prefix: "
         + std::to_string(prefix)
         + ", expected "
         + std::to_string(address_prefix)
         + " or "
         + std::to_string(subaddress_prefix)
         );
      return {};
    }

    const auto maybe_unsafe_address =
      preview_object_from_blob<spend_view_public_keys_unsafe>(data);

    if (!maybe_unsafe_address)
      {
        LOG_PRINT_L1("Account public address keys can't be parsed");
        return {};
      }

    const auto maybe_safe_address =
      maybe_safe_spend_view_public_keys(*maybe_unsafe_address);

    if (!maybe_safe_address) {
      LOG_PRINT_L1("Failed to validate address keys");
      return {};
    }

    info.address = *maybe_safe_address;

    return info;
  }
}
