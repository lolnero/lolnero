/// @file
/// @author rfree (current maintainer/user in monero.cc project - most of code is from CryptoNote)
/// @brief This is the original cryptonote protocol network-events handler, modified by us

// Copyright (c) 2014-2020, The Monero Project
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this list of
//    conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this list
//    of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may be
//    used to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
// THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
// THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Parts of this file are originally copyright (c) 2012-2013 The Cryptonote developers

// (may contain code and/or modifications by other developers)
// developer rfree: this code is caller of our new network code, and is modded; e.g. for rate limiting


#include "network/p2p/net_node.h"

#include "cryptonote/basic/functional/format_utils.hpp"

#include <boost/uuid/uuid_io.hpp>

#include <ranges>
#include <execution>


#define LOG_P2P_MESSAGE(x)                      \
  LOG_CATEGORY                                  \
  (                                             \
   epee::LogLevel::Verbose, "net.p2p.msg"       \
   , context.to_str()                           \
   + x)

#define LOG_PEER_STATE(x) \
  LOG_VERBOSE \
  ( \
   context.to_str() + "state: " + x + " in state " +  \
   cryptonote::get_protocol_state_string(context.m_state) \
    )

#define IDLE_PEER_KICK_TIME (240 * 1000000) // microseconds
#define NON_RESPONSIVE_PEER_KICK_TIME (20 * 1000000) // microseconds
#define DROP_PEERS_ON_SCORE -2

constexpr uint64_t p2p_bi_to_bi(const uint64_t x) {
  return x - 2;
}

constexpr uint64_t bi_to_p2p_bi(const uint64_t x) {
  return x + 2;
}

namespace cryptonote
{
  //-----------------------------------------------------------------------------------------------------------------------

  t_cryptonote_protocol_handler::t_cryptonote_protocol_handler
  (
   t_core& rcore
   , nodetool::i_p2p_endpoint<connection_context>& p_net_layout
   , bool offline
   )
    :
    m_core(rcore)
    , m_p2p(p_net_layout)
    , m_synchronized(offline)
  {
  }
  //-----------------------------------------------------------------------------------------------------------------------

  bool t_cryptonote_protocol_handler::init()
  {
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------

  bool t_cryptonote_protocol_handler::deinit()
  {
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------

  bool t_cryptonote_protocol_handler::on_callback(cryptonote_connection_context& context)
  {
    LOG_PRINT_CCONTEXT_L2("callback fired");
    ASSERT_OR_LOG_ERROR_AND_RETURN
      (
       context.m_callback_request_count > 0
       , false
       , std::string()
       + "["
       + epee::net_utils::print_connection_context_short(context)
       + "]"
       + "false callback fired, but context.m_callback_request_count="
       + std::to_string(context.m_callback_request_count)
       );

    --context.m_callback_request_count;

    if(context.m_state == cryptonote_connection_context::state_synchronizing
       && context.m_last_request_time == std::chrono::time_point<std::chrono::system_clock>::min())
    {
      NOTIFY_REQUEST_CHAIN::request r = {};
      context.m_needed_blocks.clear();
      r.block_ids =
        m_core.get_tree().get_short_chain_history();
      context.m_last_request_time = std::chrono::system_clock::now();
      context.m_expect_response = NOTIFY_RESPONSE_CHAIN_ENTRY::ID;
      LOG_P2P_MESSAGE
        (
         "-->>NOTIFY_REQUEST_CHAIN: m_block_ids.size()="
         + std::to_string(r.block_ids.size())
         );
      post_notify<NOTIFY_REQUEST_CHAIN>(r, context);
      LOG_PEER_STATE("requesting chain");
    }

    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------
  // Returns a list of connection_info objects describing each open p2p connection
  //------------------------------------------------------------------------------------------------------------------------

  std::list<connection_info> t_cryptonote_protocol_handler::get_connections()
  {
    std::list<connection_info> connections;

    m_p2p.for_each_connection([&](const connection_context& cntxt, nodetool::peerid_type peer_id, uint32_t support_flags)
    {
      connection_info cnx{};
      auto timestamp = time(NULL);

      cnx.incoming = cntxt.m_is_income ? true : false;

      cnx.address = cntxt.m_remote_address.str();
      cnx.host = cntxt.m_remote_address.host_str();
      cnx.ip = "";
      cnx.port = "";
      if (cntxt.m_remote_address.get_type_id() == epee::net_utils::ipv4_network_address::get_type_id())
      {
        cnx.ip = cnx.host;
        cnx.port = std::to_string(cntxt.m_remote_address.as<epee::net_utils::ipv4_network_address>().port());
      }

      cnx.peer_id = nodetool::peerid_to_string(peer_id);

      cnx.support_flags = support_flags;

      cnx.recv_count = cntxt.m_recv_cnt;
      cnx.recv_idle_time = timestamp - std::max(cntxt.m_started, cntxt.m_last_recv);

      cnx.send_count = cntxt.m_send_cnt;
      cnx.send_idle_time = timestamp - std::max(cntxt.m_started, cntxt.m_last_send);

      cnx.state = get_protocol_state_string(cntxt.m_state);

      cnx.live_time = timestamp - cntxt.m_started;

      cnx.localhost = cntxt.m_remote_address.is_loopback();
      cnx.local_ip = cntxt.m_remote_address.is_local();

      auto connection_time = time(NULL) - cntxt.m_started;
      if (connection_time == 0)
      {
        cnx.avg_download = 0;
        cnx.avg_upload = 0;
      }

      else
      {
        cnx.avg_download = cntxt.m_recv_cnt / connection_time / 1024;
        cnx.avg_upload = cntxt.m_send_cnt / connection_time / 1024;
      }

      cnx.current_download = cntxt.m_current_speed_down / 1024;
      cnx.current_upload = cntxt.m_current_speed_up / 1024;

      cnx.connection_id = epee::string_tools::pod_to_hex(cntxt.m_connection_id);

      cnx.top_block_index = p2p_bi_to_bi(cntxt.m_remote_p2p_block_index);
      cnx.address_type = (uint8_t)cntxt.m_remote_address.get_type_id();

      connections.push_back(cnx);

      return true;
    });

    return connections;
  }
  //------------------------------------------------------------------------------------------------------------------------

  bool t_cryptonote_protocol_handler::process_payload_sync_data(const CORE_SYNC_DATA& hshd, cryptonote_connection_context& context, bool is_inital)
  {
    if(context.m_state == cryptonote_connection_context::state_before_handshake && !is_inital)
      return true;

    if(context.m_state == cryptonote_connection_context::state_synchronizing)
      return true;

    if (hshd.p2p_block_index < context.m_remote_p2p_block_index)
    {
      LOG_INFO
        (
         context.to_str()
         + "Claims "
         + std::to_string(hshd.p2p_block_index)
         + ", claimed "
         + std::to_string(context.m_remote_p2p_block_index)
         + " before"
         );
      hit_score(context, 1);
    }
    context.m_remote_p2p_block_index = hshd.p2p_block_index;

    const auto maybe_target_top_block_index =
      m_core.m_target_top_block_index;

    const uint64_t local_top_block_index =
      m_core.get_tree().get_top_block_index();

    const uint64_t known_top_block_index =
      maybe_target_top_block_index
      ? *maybe_target_top_block_index
      : local_top_block_index
      ;

    const uint64_t remote_top_block_index =
      p2p_bi_to_bi(hshd.p2p_block_index);

    if(m_core.get_tree().get_block_by_hash(hshd.top_id))
    {
      context.m_state = cryptonote_connection_context::state_normal;
      if
        (
         is_inital
         && remote_top_block_index == known_top_block_index
         && known_top_block_index == local_top_block_index
         )
        on_connection_synchronized();
      return true;
    }

    const diff_pair_t remote_diff =
      { hshd.cumulative_difficulty_top64, hshd.cumulative_difficulty};

    if (pair_to_diff(remote_diff) <= m_core.get_tree().get_top_cumulative_diff()) {
      context.m_state = cryptonote_connection_context::state_normal;
      return true;
    }

    if (remote_top_block_index > known_top_block_index)
      {
        LOG_CATEGORY_COLOR
          (
           is_inital ? epee::LogLevel::Info : epee::LogLevel::Debug
           , epee::GLOBAL_CATEGORY
           , epee::console_colors::yellow
           , context.to_str()
           + "Sync data returned a new top block candidate: "
           + std::to_string(known_top_block_index)
           + " -> "
           + std::to_string(remote_top_block_index)
           + " [Your node is "
           + std::to_string(remote_top_block_index - local_top_block_index)
           + " blocks behind"
           + "]"
           );

        LOG_CATEGORY_COLOR
          (
           is_inital ? epee::LogLevel::Info : epee::LogLevel::Debug
           , epee::GLOBAL_CATEGORY
           , epee::console_colors::yellow
           , "SYNCHRONIZATION started"
           );

        m_core.m_target_top_block_index = remote_top_block_index;
      }

    LOG_INFO
      (
       context.to_str()
       + "Remote blockchain top block index: "
       + std::to_string(remote_top_block_index)
       + ", id: "
       + hshd.top_id.to_str()
       );

    context.m_state = cryptonote_connection_context::state_synchronizing;

    //let the socket to send response to handshake, but request callback, to let send request data after response
    LOG_PRINT_CCONTEXT_L2("requesting callback");
    ++context.m_callback_request_count;
    m_p2p.request_callback(context);
    LOG_PEER_STATE("requesting callback");
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------

  bool t_cryptonote_protocol_handler::get_payload_sync_data(CORE_SYNC_DATA& hshd)
  {
    hshd.top_id = m_core.get_tree().get_top_block().hash();
    hshd.p2p_block_index =
      m_core.get_tree().get_p2p_index();


    const auto top_cumulative_diff =
      diff_to_pair(m_core.get_tree().get_top_cumulative_diff());

    hshd.cumulative_difficulty = top_cumulative_diff.low;
    hshd.cumulative_difficulty_top64 = top_cumulative_diff.high;

    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------

    bool t_cryptonote_protocol_handler::get_payload_sync_data
    (string_blob& data)
  {
    CORE_SYNC_DATA hsd = {};
    get_payload_sync_data(hsd);
    data = epee::string_tools::blob_to_string
      (epee::serialization::store_t_to_binary(hsd));
    return true;
  }

  //------------------------------------------------------------------------------------------------------------------------

  int t_cryptonote_protocol_handler::handle_notify_new_fluffy_block
  (const int command, const NOTIFY_NEW_FLUFFY_BLOCK::request& arg, cryptonote_connection_context& context)
  {
    const auto r = maybe_block_and_hash_from_blob(arg.b.block);
    if (r) {
      const auto& [block, hash] = *r;
      LOG_P2P_MESSAGE
        (
         "Received NOTIFY_NEW_FLUFFY_BLOCK "
         + hash.to_str()
         + " (block index "
         + std::to_string(p2p_bi_to_bi(arg.p2p_block_index))
         + ", "
         + std::to_string(arg.b.txs.size())
         + " txes)"
         );
    }

    if(context.m_state != cryptonote_connection_context::state_normal)
      return 1;

    if(!is_synchronized()) // can happen if a peer connection goes to normal but another thread still hasn't finished adding queued blocks
    {
      LOG_DEBUG_CC(context, "Received new block while syncing, ignored");
      return 1;
    }

    m_core.get_miner().pause();


    const auto maybeFluffyBlock = preview_block_from_blob(arg.b.block);

    if(!maybeFluffyBlock)
    {
      LOG_ERROR_CCONTEXT
      (
        "sent wrong block: failed to parse and validate block: "
        + epee::string_tools::buff_to_hex_nodelimer(arg.b.block)
        + ", dropping connection"
      );

      m_core.get_miner().resume();
      drop_connection(context, false, false);

      return 1;
    }

    const raw_block fluffy_block = *maybeFluffyBlock;

    {
      const json j = fluffy_block;
      LOG_DEBUG("Received fluffy block: " + j.dump(4));
    }

    {
      const auto hash = get_block_hash(fluffy_block);
      if (m_core.get_tree().get_block_by_hash(hash)) {
        LOG_DEBUG("New fluffy block already found in chain");
        m_core.get_miner().resume();
        return 1;
      }
    }

    const auto pool_maybe_txs =
      m_core.get_tree().pool_get_txs(fluffy_block.tx_hashes);

    if ((!fluffy_block.tx_hashes.empty()) && arg.b.txs.empty()) {

      // inital empty block
      if (!std::ranges::all_of
          (
           pool_maybe_txs
           , [](const auto& x) { return x.has_value(); }
           ))
        {
          // init fluffy txs request
          LOG_DEBUG
            (
             "We are missing txes for this fluffy block"
             );

          std::list<uint64_t> missing_tx_indices;
          size_t tx_idx = 0;
          for(const auto& maybe_pool_tx: pool_maybe_txs)
            {
              if (!maybe_pool_tx)
                {
                  {
                    LOG_DEBUG
                      (
                       "Tx "
                       + std::to_string(tx_idx)
                       + " not found in pool"
                       );
                    missing_tx_indices.push_back(tx_idx);
                  }
                }

              ++tx_idx;
            }

          NOTIFY_REQUEST_FLUFFY_MISSING_TX::request missing_tx_req{};

          missing_tx_req.block_hash = get_block_hash(fluffy_block);
          missing_tx_req.p2p_block_index = arg.p2p_block_index;
          missing_tx_req.missing_tx_indices =
            { missing_tx_indices.begin(), missing_tx_indices.end()} ;

          m_core.get_miner().resume();
          LOG_P2P_MESSAGE
            (
             "-->>NOTIFY_REQUEST_FLUFFY_MISSING_TX: missing_tx_indices.size()="
             + std::to_string(missing_tx_req.missing_tx_indices.size())
             );
          post_notify<NOTIFY_REQUEST_FLUFFY_MISSING_TX>(missing_tx_req, context);
          return 1;
        }
    }

    // after fluffy txs requst, need to combine with pool txs
    tx_memory_pool tmp_pool;

    for(auto& tx_blob: arg.b.txs)
      {
        const auto maybeTx = preview_ringct_from_blob(tx_blob);
        if (!maybeTx) {
          {
            LOG_ERROR_CCONTEXT
              (
               "sent wrong tx: failed to parse and validate transaction: "
               + epee::string_tools::buff_to_hex_nodelimer(tx_blob)
               + ", dropping connection"
               );

            drop_connection(context, false, false);
            m_core.get_miner().resume();
            return 1;
          }
        }
        tmp_pool.add_tx(*maybeTx);
      }

    for (auto& maybe_pool_tx: pool_maybe_txs) {
      if (maybe_pool_tx) {
        tmp_pool.add_tx(maybe_pool_tx->tx);
      }
    }

    // need to preserve order
    const auto tmp_maybe_txs =
      tmp_pool.get_txs(fluffy_block.tx_hashes);

    if (!std::ranges::all_of
        (tmp_maybe_txs
         , [](const auto& x) { return x.has_value(); }
         ))
      {
        LOG_PRINT_CCONTEXT_L0
          ("Can't get all txs in fluffy block, dropping connection");
        drop_connection_with_score(context, 1, false);
        return 1;
      }

    LOG_DEBUG("We have all needed txes for this fluffy block");

    const auto tmp_txs_view =
      tmp_maybe_txs
      | std::views::transform([](const auto& x) { return x->tx; })
      ;

    const std::vector<ringct> tmp_txs =
      { tmp_txs_view.begin(), tmp_txs_view.end() };

    // const json fluffy_block_j = fluffy_block;
    // const json txs_j = txs;
    // LOG_DEBUG("Constructing full block: " + fluffy_block_j.dump(4));
    // LOG_DEBUG("with txs: " + txs_j.dump(4));

    const auto maybe_block_t = preview_block
      (
       fluffy_block
       , tmp_txs
       );

    if (!maybe_block_t) {
      LOG_PRINT_CCONTEXT_L0
        ("Block parsing failed, dropping connection");
      drop_connection_with_score(context, 1, false);

      m_core.get_miner().resume();
      return 1;
    }

    const auto new_block = *maybe_block_t;

    LOG_DEBUG("block constructed");

    const auto block_add_result =
      m_core.get_tree().push_block(new_block);

    switch (block_add_result)
      {
      case block_add_t::added:
        {
          LOG_VERBOSE("Block added: " + new_block.hash().to_str());

          m_core.get_miner().on_block_chain_update();
          m_core.get_miner().resume();


          NOTIFY_NEW_FLUFFY_BLOCK::request req{};
          req.p2p_block_index = arg.p2p_block_index;
          req.b = review_block_to_block_and_txs_data_t(new_block);
          req.b.txs = {};
          relay_block(req, context);
          break;
        }
      case block_add_t::saved:
        {
          m_core.get_miner().resume();
          LOG_VERBOSE("Block saved: " + new_block.hash().to_str());
          break;
        }
      case block_add_t::rejected:
        {
          m_core.get_miner().resume();
          LOG_VERBOSE("Block rejected: " + new_block.hash().to_str());
          LOG_PRINT_CCONTEXT_L0("dropping connection");
          drop_connection_with_score(context, 1, false);
          break;
        }
      case block_add_t::ignored:
        {
          m_core.get_miner().resume();
          LOG_VERBOSE("Block ignored: " + new_block.hash().to_str());
          break;
        }
      default:
        break;
      }

    return 1;
  }
  //------------------------------------------------------------------------------------------------------------------------

  int t_cryptonote_protocol_handler::handle_request_fluffy_missing_tx(const int command, const NOTIFY_REQUEST_FLUFFY_MISSING_TX::request& arg, cryptonote_connection_context& context)
  {
    LOG_P2P_MESSAGE
      (
       "Received NOTIFY_REQUEST_FLUFFY_MISSING_TX ("
       + std::to_string(arg.missing_tx_indices.size())
       + " txes), block hash "
       + arg.block_hash.to_str()
       );
    if (context.m_state == cryptonote_connection_context::state_before_handshake)
    {
      LOG_ERROR_CCONTEXT("Requested fluffy tx before handshake, dropping connection");
      drop_connection(context, false, false);
      return 1;
    }

    const auto maybe_block =
      m_core.get_tree().get_block_by_hash(arg.block_hash);

    if (!maybe_block)
      {
        LOG_ERROR_CCONTEXT
          (
           "failed to find block: "
           + arg.block_hash.to_str()
           + ", dropping connection"
           );
        drop_connection(context, false, false);
        return 1;
      }

    const auto b = *maybe_block;

    if (!std::ranges::all_of
        (
         arg.missing_tx_indices
         , [&b](const auto& x) {
           return x < b.txs.size();
         }
         ))
      {
        LOG_ERROR_CCONTEXT
          (
           "missing tx id out of bound"
           );

        drop_connection(context, false, false);
        return 1;
      }

    const auto unique_missing_tx_indices =
      std::set(arg.missing_tx_indices.begin(), arg.missing_tx_indices.end());

    if (unique_missing_tx_indices.size() != arg.missing_tx_indices.size()) {
      LOG_ERROR_CCONTEXT
        (
         "duplicated missing tx indices"
         );

      drop_connection(context, false, false);
      return 1;
    }

    NOTIFY_NEW_FLUFFY_BLOCK::request fluffy_response{};
    fluffy_response.b.block = block_to_blob(review_block(b));
    fluffy_response.p2p_block_index = arg.p2p_block_index;

    std::transform
      (
       arg.missing_tx_indices.begin()
       , arg.missing_tx_indices.end()
       , std::back_inserter(fluffy_response.b.txs)
       , [&b](const auto& x) {
         return review_ringct_to_blob(b.txs.at(x));
       }
       );

    LOG_P2P_MESSAGE
      (
       "-->>NOTIFY_RESPONSE_FLUFFY_MISSING_TX: "
       + ", txs.size()="
       + std::to_string(fluffy_response.b.txs.size())
       + ", rsp.p2p_block_index="
       + std::to_string(fluffy_response.p2p_block_index)
       );

    post_notify<NOTIFY_NEW_FLUFFY_BLOCK>(fluffy_response, context);
    return 1;
  }
  //------------------------------------------------------------------------------------------------------------------------

  int t_cryptonote_protocol_handler::handle_notify_get_tx_pool_complement(const int command, const NOTIFY_GET_TXPOOL_COMPLEMENT::request& arg, cryptonote_connection_context& context)
  {
    LOG_P2P_MESSAGE
      (
       "Received NOTIFY_GET_TXPOOL_COMPLEMENT ("
       + std::to_string(arg.hashes.size())
       + " txes)"
       );
    if(context.m_state != cryptonote_connection_context::state_normal)
      return 1;

    std::vector<std::pair<cryptonote::string_blob, raw_block>> local_blocks;
    std::vector<cryptonote::string_blob> local_txs;

    const auto pool_txs =
      m_core.get_tree().pool_get_complement(arg.hashes);

    const auto txs =
      pool_txs
      | std::views::transform
      (
       [](const auto& x) {
         return review_ringct_to_blob(x.tx);
       }
       )
      ;

    NOTIFY_NEW_TRANSACTIONS::request new_txes{};
    new_txes.txs = { txs.begin(), txs.end() };

    LOG_P2P_MESSAGE
      (
       "-->>NOTIFY_NEW_TRANSACTIONS: "
       + ", txs.size()="
       + std::to_string(new_txes.txs.size())
       );

    post_notify<NOTIFY_NEW_TRANSACTIONS>(new_txes, context);
    return 1;
  }
  //------------------------------------------------------------------------------------------------------------------------

  int t_cryptonote_protocol_handler::handle_notify_new_transactions
  (
   const int command
   , const NOTIFY_NEW_TRANSACTIONS::request& arg
   , cryptonote_connection_context& context)
  {
    LOG_P2P_MESSAGE
      (
       "Received NOTIFY_NEW_TRANSACTIONS ("
       + std::to_string(arg.txs.size())
       + " txes)"
       );

    if(context.m_state != cryptonote_connection_context::state_normal)
      return 1;

    // while syncing, core will lock for a long time, so we ignore
    // those txes as they aren't really needed anyway, and avoid a
    // long block before replying
    if(!is_synchronized())
    {
      LOG_DEBUG_CC(context, "Received new tx while syncing, ignored");
      return 1;
    }

    std::list<cryptonote::string_blob> new_txs;

    for (const auto& x: arg.txs)
    {
      const auto maybe_tx = preview_ringct_from_blob(x);

      if(!maybe_tx)
      {
        LOG_PRINT_CCONTEXT_L1("Tx parsing failed");
        drop_connection(context, false, false);
        return 1;
      }

      const auto tx = *maybe_tx;

      // racy
      if(!m_core.get_tree().pool_contains_tx(tx.hash())) {
        const bool tx_added = m_core.get_tree().pool_add_tx(tx);

        if (!tx_added)
          {
            LOG_PRINT_CCONTEXT_L1("Tx verification failed, dropping connection");
            drop_connection(context, false, false);
            return 1;
          }

        new_txs.push_back(x);
      }
    }

    if(!new_txs.empty())
    {
      auto arg_ = arg;
      arg_.txs = new_txs;

      relay_transactions
        (
         arg_
         , context.m_connection_id
         );
    }

    return 1;
  }
  //------------------------------------------------------------------------------------------------------------------------

  int t_cryptonote_protocol_handler::handle_request_get_blocks(const int command, const NOTIFY_REQUEST_GET_BLOCKS::request& arg, cryptonote_connection_context& context)
  {
    if (context.m_state == cryptonote_connection_context::state_before_handshake)
    {
      LOG_ERROR_CCONTEXT("Requested objects before handshake, dropping connection");
      drop_connection(context, false, false);
      return 1;
    }
    LOG_P2P_MESSAGE
      (
       "Received NOTIFY_REQUEST_GET_BLOCKS ("
       + std::to_string(arg.blocks.size())
       + " blocks)"
       );
    if (arg.blocks.size() > CURRENCY_PROTOCOL_MAX_OBJECT_REQUEST_COUNT)
      {
        LOG_ERROR_CCONTEXT
          (
            "Requested objects count is too big ("
            + std::to_string(arg.blocks.size())
            + ") expected not more then "
            + std::to_string(CURRENCY_PROTOCOL_MAX_OBJECT_REQUEST_COUNT)
           );
        drop_connection(context, false, false);
        return 1;
      }

    const auto blocks = m_core.get_tree().get_blocks(arg.blocks);


    NOTIFY_RESPONSE_GET_BLOCKS::request rsp{};

    for (size_t i = 0; i < blocks.size(); i++) {
      if (blocks[i]) {
        const auto x = *blocks[i];

        const auto block_blob = block_to_blob(review_block(x));
        const auto txs =
           x.txs
           | std::views::transform(review_ringct_to_blob)
           ;

        rsp.blocks.push_back
          ( { block_blob, { txs.begin(), txs.end() } });
      } else {
        rsp.missed_ids.push_back(arg.blocks[i]);
      }
    }

    rsp.p2p_block_index =
      m_core.get_tree().get_p2p_index();

    context.m_last_request_time = std::chrono::system_clock::now();
    LOG_P2P_MESSAGE
      (
       "-->>NOTIFY_RESPONSE_GET_BLOCKS: blocks.size()="
       + std::to_string(rsp.blocks.size())
       + ", rsp.p2p_block_index="
       + std::to_string(rsp.p2p_block_index)
       + ", missed_ids.size()="
       + std::to_string(rsp.missed_ids.size())
       );
    post_notify<NOTIFY_RESPONSE_GET_BLOCKS>(rsp, context);
    return 1;
  }
  //------------------------------------------------------------------------------------------------------------------------



  int t_cryptonote_protocol_handler::handle_response_get_blocks(const int command, const NOTIFY_RESPONSE_GET_BLOCKS::request& arg, cryptonote_connection_context& context)
  {
    LOG_P2P_MESSAGE
      (
       "Received NOTIFY_RESPONSE_GET_BLOCKS ("
       + std::to_string(arg.blocks.size())
       + " blocks)"
       );
    LOG_PEER_STATE("received objects");

    // calculate size of request
    if(arg.blocks.empty())
    {
      LOG_ERROR_CCONTEXT("sent wrong NOTIFY_HAVE_OBJECTS: no blocks");
      drop_connection(context, true, false);
      return 1;
    }

    const auto maybe_target_top_block_index =
      m_core.m_target_top_block_index;

    const bool remote_has_higher_index =
      maybe_target_top_block_index
      ? context.m_remote_p2p_block_index >
      *maybe_target_top_block_index + 1
      : true
      ;

    if(remote_has_higher_index) {
      m_core.m_target_top_block_index =
        p2p_bi_to_bi(context.m_remote_p2p_block_index);
    }


    const uint64_t previous_top_block_index =
      m_core.get_tree().get_top_block_index();

    const std::chrono::time_point<std::chrono::system_clock> start =
      std::chrono::system_clock::now();

    LOG_COLOR
      (
       epee::yellow
       , epee::LogLevel::Debug
       , context.to_str()
       + " Got NEW BLOCKS inside of "
       + std::string(__FUNCTION__)
       + ": size: "
       + std::to_string(arg.blocks.size())
       );

    const auto blocks = arg.blocks;

    {
      const auto maybeBlock =
        preview_block_from_block_and_txs_data_t(blocks.front());

      if (!maybeBlock)
        {
          LOG_ERROR(context.to_str() + "Failed to parse block");
          drop_connection(context, true, false);
          return 1;
        }

      const auto& first_block_prev_hash = maybeBlock->prev_hash;

      const bool prev_block_known =
        m_core.get_tree().block_known(first_block_prev_hash);

      if (!prev_block_known)
        {
          LOG_ERROR(context.to_str() + "Failed to found parent");
          drop_connection(context, true, false);
          return 1;
        }
    }

    std::vector<std::optional<block_t>> new_maybe_blocks(blocks.size());
    std::transform
      (
       std::execution::par_unseq
       , blocks.begin()
       , blocks.end()
       , new_maybe_blocks.begin()
       , preview_block_from_block_and_txs_data_t
       );

    if (
        !std::ranges::all_of
        (
         new_maybe_blocks
         , [](const auto& x) { return x.has_value(); }
         )
        )
      {
        LOG_ERROR(context.to_str() + "Failed to parse some blocks");
        drop_connection(context, true, false);
        return 1;
      }

    const auto new_blocks =
      new_maybe_blocks
      | std::views::transform([](const auto& x) { return *x; })
      ;

    const auto add_result = m_core.get_tree().push_blocks
      ( std::vector<block_t>{ new_blocks.begin(), new_blocks.end() });

    if (add_result == block_add_t::rejected) {
        LOG_ERROR(context.to_str() + "Failed to add some blocks");
        drop_connection(context, true, false);
        return 1;
    }

    if (maybe_target_top_block_index) {
      const auto target_top_block_index =
        *maybe_target_top_block_index;

      const uint64_t local_top_block_index
        = m_core.get_tree().get_top_block_index();

      const auto dt =
        std::chrono::duration_cast<std::chrono::microseconds>
        (std::chrono::system_clock::now() - start);

      const uint64_t completion_percent =
        (local_top_block_index * 100 / target_top_block_index);

      if (completion_percent != 100
          && add_result == block_add_t::added) {

        const std::string progress_message =
          std::string()
          + "["
          + std::to_string(completion_percent)
          + "%]"
          ;

        const auto sync_rate =
          (local_top_block_index - previous_top_block_index)
          * 1e6 / dt.count()
          ;

        const std::string timing_message = std::string(" \t")
          + std::to_string(static_cast<uint32_t>(sync_rate))
          + " blocks/sec";

        LOG_CATEGORY_COLOR
          (
           epee::LogLevel::Info
           , epee::GLOBAL_CATEGORY
           , epee::yellow
           , std::string()
           + progress_message
           + " "
           + std::to_string(local_top_block_index)
           + "/"
           + std::to_string(target_top_block_index)
           + timing_message
           );
      }
    }

    LOG_DEBUG
      (
       "needed blocks: "
       + std::to_string(context.m_needed_blocks.size())
       + ", arg.blocks: "
       + std::to_string(blocks.size())
       );

    if (context.m_needed_blocks.size() <= blocks.size()) {


      // current design will always try to send short chain history
      // from the main chain, which will result in an infinite loop if
      // reorg is deeper than BLOCKS_IDS_SYNCHRONIZING_PEER_BATCH_SIZE
      if
        (
         !m_core.get_tree()
         .get_block_by_hash(new_blocks.back().hash())
         )
        {
          LOG_ERROR_CCONTEXT("dropping connection with deep reorg");
          drop_connection_with_score(context, 5, false);
          return 1;
        }

      context.m_needed_blocks.clear();

      NOTIFY_REQUEST_CHAIN::request r = {};
      r.block_ids =
        m_core.get_tree().get_short_chain_history();
      context.m_last_request_time = std::chrono::system_clock::now();
      context.m_expect_response = NOTIFY_RESPONSE_CHAIN_ENTRY::ID;
      LOG_P2P_MESSAGE
        (
         "Requesting more block hashes"
         );
      post_notify<NOTIFY_REQUEST_CHAIN>(r, context);

      return 1;
    }

    const auto new_needed_blocks =
      std::span(context.m_needed_blocks).subspan(blocks.size());

    context.m_needed_blocks =
      {new_needed_blocks.begin(), new_needed_blocks.end()};

    return request_missing_blocks_2(context);
  }

  //------------------------------------------------------------------------------------------------------------------------

  int t_cryptonote_protocol_handler::handle_request_chain(const int command, const NOTIFY_REQUEST_CHAIN::request& arg, cryptonote_connection_context& context)
  {
    LOG_P2P_MESSAGE
      (
       "Received NOTIFY_REQUEST_CHAIN ("
       + std::to_string(arg.block_ids.size())
       + " blocks"
       );
    if (context.m_state == cryptonote_connection_context::state_before_handshake)
      {
        LOG_ERROR_CCONTEXT("Requested chain before handshake, dropping connection");
        drop_connection(context, false, false);
        return 1;
      }

    NOTIFY_RESPONSE_CHAIN_ENTRY::request r{};

    // uint64_t start_block_index;
    // uint64_t p2p_block_index; // top_block_index + 1
    // uint64_t cumulative_difficulty;
    // uint64_t cumulative_difficulty_top64 = 0;
    // std::vector<crypto::hash> m_block_ids;
    // std::vector<uint64_t> m_block_weights;

    const auto [block_hashes, common_index] =
      m_core.get_tree()
      .get_hashes_by_short_chain_history(arg.block_ids);

    if(block_hashes.empty())
      {
        LOG_ERROR_CCONTEXT("Failed to handle NOTIFY_REQUEST_CHAIN.");
        return 1;
      }

    r.m_block_ids = { block_hashes.begin(), block_hashes.end() };
    r.start_block_index = common_index;
    r.p2p_block_index = m_core.get_tree().get_p2p_index();

    const auto diff_pair =
      diff_to_pair
      (
       m_core.get_tree().get_top_cumulative_diff()
       );

    r.cumulative_difficulty = diff_pair.low;
    r.cumulative_difficulty_top64 = diff_pair.high;

    LOG_P2P_MESSAGE
      (
       "-->>NOTIFY_RESPONSE_CHAIN_ENTRY: m_start_block_index ="
       + std::to_string(r.start_block_index)
       + ", m_p2p_block_index="
       + std::to_string(r.p2p_block_index)
       + ", m_block_ids.size()="
       + std::to_string(r.m_block_ids.size())
       );
    post_notify<NOTIFY_RESPONSE_CHAIN_ENTRY>(r, context);
    return 1;
  }
  //------------------------------------------------------------------------------------------------------------------------

  bool t_cryptonote_protocol_handler::request_missing_blocks_2
  (
   cryptonote_connection_context& context
   )
  {
    const auto block_hashes = std::span(context.m_needed_blocks);

    NOTIFY_REQUEST_GET_BLOCKS::request req{};
    const size_t count_limit = constant::BLOCKS_SYNCHRONIZING_SIZE;

    const auto hashes =
      block_hashes.size() > count_limit
      ? block_hashes.subspan(0, count_limit)
      : block_hashes
      ;

    req.blocks = std::vector<crypto::hash>(hashes.begin(), hashes.end());

    post_notify<NOTIFY_REQUEST_GET_BLOCKS>(req, context);
    LOG_PEER_STATE("requesting objects");

    // for (const auto& h: block_hashes) {
    //   LOG_GLOBAL("request missing block: " + h.to_str());
    // }
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------

  bool t_cryptonote_protocol_handler::on_connection_synchronized()
  {
    bool val_expected = false;
    if(m_synchronized.compare_exchange_strong(val_expected, true))
    {
      LOG_CATEGORY_COLOR
        (
         epee::LogLevel::Info
         , epee::GLOBAL_CATEGORY
         , epee::yellow
         , "**********************************************************************"
         );

      LOG_CATEGORY_COLOR
        (
         epee::LogLevel::Info
         , epee::GLOBAL_CATEGORY
         , epee::yellow
         , "You are now synchronized with the network."
         );

      LOG_CATEGORY_COLOR
        (
         epee::LogLevel::Info
         , epee::GLOBAL_CATEGORY
         , epee::yellow
         , "**********************************************************************"
         );

      m_core.on_synchronized();
    }

    // ask for tx_pool complement from any suitable node if we did not yet
    val_expected = true;
    if (m_ask_for_tx_pool_complement.compare_exchange_strong(val_expected, false))
    {
      m_p2p.for_each_connection([&](cryptonote_connection_context& context, nodetool::peerid_type peer_id, uint32_t support_flags)->bool
      {
        if(context.m_state < cryptonote_connection_context::state_synchronizing)
        {
          LOG_DEBUG(context.to_str() +  "not ready, ignoring");
          return true;
        }
        if (!request_tx_pool_complement(context))
        {
          LOG_ERROR(context.to_str() + "Failed to request tx_pool complement");
          return true;
        }
        return false;
      });
    }

    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------

  size_t t_cryptonote_protocol_handler::get_synchronizing_connections_count()
  {
    size_t count = 0;
    m_p2p.for_each_connection([&](cryptonote_connection_context& context, nodetool::peerid_type peer_id, uint32_t support_flags)->bool{
      if(context.m_state == cryptonote_connection_context::state_synchronizing)
        ++count;
      return true;
    });
    return count;
  }
  //------------------------------------------------------------------------------------------------------------------------

  int t_cryptonote_protocol_handler::handle_response_chain_entry(const int command, const NOTIFY_RESPONSE_CHAIN_ENTRY::request& arg, cryptonote_connection_context& context)
  {
    LOG_P2P_MESSAGE
      (
       "Received NOTIFY_RESPONSE_CHAIN_ENTRY: m_block_ids.size()="
       + std::to_string(arg.m_block_ids.size())
       + ", m_start_block_index ="
       + std::to_string(arg.start_block_index)
       + ", m_p2p_block_index ="
       + std::to_string(arg.p2p_block_index)
       );
    LOG_PEER_STATE("received chain");

    if (context.m_expect_response != NOTIFY_RESPONSE_CHAIN_ENTRY::ID)
    {
      LOG_ERROR_CCONTEXT("Got NOTIFY_RESPONSE_CHAIN_ENTRY out of the blue, dropping connection");
      drop_connection(context, true, false);
      return 1;
    }
    context.m_expect_response = 0;

    context.m_last_request_time = std::chrono::system_clock::time_point::min();

    if(!arg.m_block_ids.size())
    {
      LOG_ERROR_CCONTEXT("sent empty m_block_ids, dropping connection");
      drop_connection(context, true, false);
      return 1;
    }

    LOG_DEBUG
      (
       context.to_str()
       + "first block hash "
       + arg.m_block_ids.front().to_str()
       + ", last "
       + arg.m_block_ids.back().to_str()
       );

    if (arg.p2p_block_index >= CRYPTONOTE_MAX_BLOCK_NUMBER || arg.m_block_ids.size() > BLOCKS_IDS_SYNCHRONIZING_MAX_COUNT)
    {
      LOG_ERROR_CCONTEXT
        (
         "sent wrong NOTIFY_RESPONSE_CHAIN_ENTRY, with total_height="
         + std::to_string(arg.p2p_block_index)
         + ", m_block_ids.size()="
         + std::to_string(arg.m_block_ids.size())
         );
      drop_connection(context, false, false);
      return 1;
    }

    if (arg.p2p_block_index < context.m_remote_p2p_block_index)
    {
      LOG_INFO
        (
         context.to_str()
         + "Claims "
         + std::to_string(arg.p2p_block_index)
         + ", claimed "
         + std::to_string(context.m_remote_p2p_block_index)
         + " before"
         );
      hit_score(context, 1);
    }

    context.m_remote_p2p_block_index = arg.p2p_block_index;

    // racy
    // doesn't handle alt chain
    auto missing_objects =
      arg.m_block_ids
      | std::views::drop_while
      ([&](const auto& x) {
        return m_core.get_tree().get_block_by_hash(x)
          .has_value();
      })
      | std::views::take
      (constant::BLOCKS_IDS_SYNCHRONIZING_PEER_BATCH_SIZE)
      ;

    if (missing_objects.empty()) {
      context.m_state = cryptonote_connection_context::state_normal;

      if (m_core.m_target_top_block_index) {
          const auto target_top_block_index =
            *m_core.m_target_top_block_index;

          if (context.m_remote_p2p_block_index >= target_top_block_index + 1) {
            if (m_core.get_tree().get_top_block_index()
                >= target_top_block_index)
              {
                LOG_CATEGORY_COLOR
                  (
                   epee::LogLevel::Info
                   , DEFAULT_CAT
                   , epee::green
                   , "SYNCHRONIZED OK"
                   );
                on_connection_synchronized();
              }
          }
        }

      return 1;
    }

    context.m_needed_blocks =
      { missing_objects.begin(), missing_objects.end() };

    if (!request_missing_blocks_2(context))
    // if (!request_missing_blocks(context, false))
    {
      LOG_ERROR_CCONTEXT("Failed to request missing blocks, dropping connection");
      drop_connection(context, false, false);
      return 1;
    }

    if (arg.p2p_block_index
        > m_core.m_target_top_block_index.value_or(0ull)
        )

        p2p_bi_to_bi(arg.p2p_block_index);

    return 1;
  }
  //------------------------------------------------------------------------------------------------------------------------

  bool t_cryptonote_protocol_handler::relay_block
  (
   const NOTIFY_NEW_FLUFFY_BLOCK::request& fluffy_arg
   , cryptonote_connection_context& exclude_context
   )
  {
    LOG_DEBUG("Relay block");
    // sort peers between fluffy ones and others
    std::vector<std::pair<epee::net_utils::zone, boost::uuids::uuid>>
      fluffyConnections;

    m_p2p.for_each_connection
      (
       [
        &exclude_context
        , &fluffyConnections
        ]
       (
        connection_context& context
        , nodetool::peerid_type peer_id
        , uint32_t support_flags
        )
    {
      // peer_id also filters out connections before handshake
      if (peer_id && exclude_context.m_connection_id != context.m_connection_id)
      {
        LOG_DEBUG_CC(context, "PEER SUPPORTS FLUFFY BLOCKS - RELAYING THIN/COMPACT WHATEVER BLOCK");
        fluffyConnections.push_back({context.m_remote_address.get_zone(), context.m_connection_id});
      }
      return true;
    });

    // send fluffy ones first, we want to encourage people to run that
    if (!fluffyConnections.empty())
    {
      const auto fluffyBlob =
        epee::serialization::store_t_to_binary(fluffy_arg);

      LOG_DEBUG("Sending fluffy block");

      m_p2p.relay_notify_to_list
        (
         NOTIFY_NEW_FLUFFY_BLOCK::ID
         , fluffyBlob
         , fluffyConnections
         );
    }

    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------

  bool t_cryptonote_protocol_handler::relay_transactions
  (
   const NOTIFY_NEW_TRANSACTIONS::request& arg
   , const boost::uuids::uuid& source_connection_id
   )
  {
    /* Push all outgoing transactions to this function. The behavior needs to
       identify how the transaction is going to be relayed, and then update the
       local mempool before doing the relay. The code was already updating the
       DB twice on received transactions - it is difficult to workaround this
       due to the internal design. */
    return m_p2p.send_txs
      (
       arg.txs
       , source_connection_id
       );
  }
  //------------------------------------------------------------------------------------------------------------------------

  bool t_cryptonote_protocol_handler::request_tx_pool_complement(cryptonote_connection_context &context)
  {
    NOTIFY_GET_TXPOOL_COMPLEMENT::request r = {};

    const auto txs = m_core.get_tree().pool_get_all_txs();

    for (const auto& [h, x]: txs) {
      r.hashes.push_back(h);
    }

    LOG_P2P_MESSAGE
      (
       "-->>NOTIFY_GET_TXPOOL_COMPLEMENT: hashes.size()="
       + std::to_string(r.hashes.size())
       );
    post_notify<NOTIFY_GET_TXPOOL_COMPLEMENT>(r, context);
    LOG_PEER_STATE("requesting tx_pool complement");
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------

  void t_cryptonote_protocol_handler::hit_score(cryptonote_connection_context &context, int32_t score)
  {
    if (score <= 0)
    {
      LOG_ERROR("Negative score hit");
      return;
    }
    context.m_score -= score;
    if (context.m_score <= DROP_PEERS_ON_SCORE)
      drop_connection_with_score(context, 5, false);
  }
  //------------------------------------------------------------------------------------------------------------------------

  std::string t_cryptonote_protocol_handler::get_peers_overview() const
  {
    std::stringstream ss;
    const std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
    m_p2p.for_each_connection([&](const connection_context &ctx, nodetool::peerid_type peer_id, uint32_t support_flags) {
      char state_char = cryptonote::get_protocol_state_char(ctx.m_state);
      ss << state_char;
      if (ctx.m_last_request_time != std::chrono::system_clock::time_point::min()) {
        const auto dt = std::chrono::duration_cast<std::chrono::microseconds>(now - ctx.m_last_request_time);
        ss << ((dt.count() > IDLE_PEER_KICK_TIME) ? "!" : "?");
      }
      ss <<  + " ";
      return true;
    });
    return ss.str();
  }
  //------------------------------------------------------------------------------------------------------------------------

  bool t_cryptonote_protocol_handler::needs_new_sync_connections() const
  {
    const auto maybe_target_top_block_index =
      m_core.m_target_top_block_index;

    const uint64_t local_top_block_index =
      m_core.get_tree().get_top_block_index();

    if (maybe_target_top_block_index) {
      if (*maybe_target_top_block_index <= local_top_block_index) {
        return false;
      }
    }

    size_t n_out_peers = 0;
    m_p2p.for_each_connection([&](cryptonote_connection_context& ctx, nodetool::peerid_type peer_id, uint32_t support_flags)->bool{
      if (!ctx.m_is_income)
        ++n_out_peers;
      return true;
    });
    if (n_out_peers >= m_max_out_peers)
      return false;
    return true;
  }
  //------------------------------------------------------------------------------------------------------------------------

  bool t_cryptonote_protocol_handler::is_busy_syncing()
  {
    const std::unique_lock<std::mutex> sync{m_sync_lock, std::try_to_lock};
    return !sync.owns_lock();
  }
  //------------------------------------------------------------------------------------------------------------------------

  void t_cryptonote_protocol_handler::drop_connection_with_score(cryptonote_connection_context &context, unsigned score, bool flush_all_spans)
  {
    LOG_DEBUG_CC
      (
       context
       , "dropping connection id "
       + boost::uuids::to_string(context.m_connection_id)
       + ", score "
       + std::to_string(score)
       + ", flush_all_spans "
       + std::to_string(flush_all_spans)
       );

    // copy since dropping the connection will invalidate the context, and thus the address
    const auto remote_address = context.m_remote_address;

    m_p2p.drop_connection(context);

    if (score > 0)
      m_p2p.add_host_fail(remote_address, score);
  }
  //------------------------------------------------------------------------------------------------------------------------

  void t_cryptonote_protocol_handler::drop_connection(cryptonote_connection_context &context, bool add_fail, bool flush_all_spans)
  {
    return drop_connection_with_score(context, add_fail ? 1 : 0, flush_all_spans);
  }
  //------------------------------------------------------------------------------------------------------------------------

  void t_cryptonote_protocol_handler::drop_bad_connections(const epee::net_utils::network_address address)
  {
    LOG_WARNING("dropping bad connections to " + address.str());

    m_p2p.add_host_fail(address, 5);

    std::vector<boost::uuids::uuid> drop;
    m_p2p.for_each_connection([&](const connection_context& cntxt, nodetool::peerid_type peer_id, uint32_t support_flags) {
      if (address.is_same_host(cntxt.m_remote_address))
        drop.push_back(cntxt.m_connection_id);
      return true;
    });
    for (const boost::uuids::uuid &id: drop)
    {
      m_p2p.for_connection(id, [&](cryptonote_connection_context& context, nodetool::peerid_type peer_id, uint32_t f)->bool{
        drop_connection(context, true, false);
        return true;
      });
    }
  }
  //------------------------------------------------------------------------------------------------------------------------

  void t_cryptonote_protocol_handler::on_connection_new(cryptonote_connection_context &context)
  {
    context.set_max_bytes(nodetool::COMMAND_HANDSHAKE_T<cryptonote::CORE_SYNC_DATA>::ID, 65536);
    context.set_max_bytes(nodetool::COMMAND_TIMED_SYNC_T<cryptonote::CORE_SYNC_DATA>::ID, 65536);
    context.set_max_bytes(nodetool::COMMAND_PING::ID, 4096);
    context.set_max_bytes(nodetool::COMMAND_SUPPORT_FLAGS::ID, 4096);

    context.set_max_bytes(cryptonote::NOTIFY_NEW_TRANSACTIONS::ID, 8e6);
    context.set_max_bytes(cryptonote::NOTIFY_REQUEST_GET_BLOCKS::ID, 2e6);
    context.set_max_bytes(cryptonote::NOTIFY_RESPONSE_GET_BLOCKS::ID, 1e8);

    context.set_max_bytes(cryptonote::NOTIFY_REQUEST_CHAIN::ID, 1e6);
    context.set_max_bytes(cryptonote::NOTIFY_RESPONSE_CHAIN_ENTRY::ID, 4e6);
    context.set_max_bytes(cryptonote::NOTIFY_NEW_FLUFFY_BLOCK::ID, 1e6);
    context.set_max_bytes(cryptonote::NOTIFY_REQUEST_FLUFFY_MISSING_TX::ID, 1e6);
    context.set_max_bytes(cryptonote::NOTIFY_GET_TXPOOL_COMPLEMENT::ID, 8e6);
  }
  //------------------------------------------------------------------------------------------------------------------------

  void t_cryptonote_protocol_handler::on_connection_close(cryptonote_connection_context &context)
  {
    std::optional<uint64_t> maybe_target_top_block_index;
    m_p2p.for_each_connection([&](const connection_context& cntxt, nodetool::peerid_type peer_id, uint32_t support_flags) {
      if (cntxt.m_state >= cryptonote_connection_context::state_synchronizing && cntxt.m_connection_id != context.m_connection_id)
        {

          const std::optional<uint64_t> remote_target_block_index =
            cntxt.m_remote_p2p_block_index == 0
            ? std::nullopt
            : std::make_optional<uint64_t>
            (p2p_bi_to_bi(cntxt.m_remote_p2p_block_index))
            ;

          if (remote_target_block_index) {
            if (maybe_target_top_block_index) {
              maybe_target_top_block_index =
                std::max
                (
                 *maybe_target_top_block_index
                 , *remote_target_block_index
                 );
            } else {
              maybe_target_top_block_index = remote_target_block_index;
            }
          }

        }

      return true;
    });

    if (m_stopping) {
      LOG_PEER_STATE("closed");
      return;
    }

    const auto maybe_previous_target_top_block_index
      = m_core.m_target_top_block_index;

    m_core.m_target_top_block_index = maybe_target_top_block_index;

    LOG_VERBOSE
      (
       "Previous target: "
       + std::to_string(maybe_previous_target_top_block_index.value_or(0))
       + ", Current target: "
       + std::to_string(maybe_target_top_block_index.value_or(0))
       );


    if (maybe_target_top_block_index) {
      const auto target_top_block_index =
        *maybe_target_top_block_index;

      if (maybe_previous_target_top_block_index) {
        const auto previous_target_top_block_index =
          *maybe_previous_target_top_block_index;

        if (target_top_block_index < previous_target_top_block_index)
          {
            LOG_INFO
              (
               "Target height decreasing from "
               + std::to_string(previous_target_top_block_index)
               + " to "
               + std::to_string(target_top_block_index)
               );
          }
      }
    }
    else {
      if
        (
         context.m_state
         > cryptonote_connection_context::state_before_handshake
         && !m_stopping)
        {
          LOG_INFO("lolnerod is disconnected from the network");
          m_core.m_target_top_block_index = {};

          m_ask_for_tx_pool_complement = true;
        }
    }

    LOG_PEER_STATE("closed");
  }

  //------------------------------------------------------------------------------------------------------------------------

  void t_cryptonote_protocol_handler::stop()
  {
    m_stopping = true;
    m_core.stop();
  }
} // namespace
