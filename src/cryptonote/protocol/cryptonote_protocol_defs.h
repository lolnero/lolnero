// Copyright (c) 2014-2020, The Monero Project
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this list of
//    conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this list
//    of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may be
//    used to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
// THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
// THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Parts of this file are originally copyright (c) 2012-2013 The Cryptonote developers

#pragma once

#include "cryptonote/basic/functional/base.hpp"
#include "cryptonote/basic/type/string_blob_type.hpp"



namespace cryptonote
{

  constexpr int BC_COMMANDS_POOL_BASE = 2000;

  /************************************************************************/
  /* P2P connection info, serializable to json                            */
  /************************************************************************/
  struct connection_info
  {
    bool incoming;
    bool localhost;
    bool local_ip;

    std::string address;
    std::string host;
    std::string ip;
    std::string port;
    uint16_t rpc_port = 0;

    std::string peer_id;

    uint64_t recv_count;
    uint64_t recv_idle_time;

    uint64_t send_count;
    uint64_t send_idle_time;

    std::string state;

    uint64_t live_time;

    uint64_t avg_download;
    uint64_t current_download;

    uint64_t avg_upload;
    uint64_t current_upload;

    uint32_t support_flags;

    std::string connection_id;

    uint64_t top_block_index;

    uint8_t address_type;
  };

  /************************************************************************/
  /*                                                                      */
  /************************************************************************/

  /************************************************************************/
  /*                                                                      */
  /************************************************************************/
  struct NOTIFY_NEW_TRANSACTIONS
  {
    constexpr static int ID = BC_COMMANDS_POOL_BASE + 2;

    struct request
    {
      std::list<string_blob>   txs;
      std::string _; // padding

      BEGIN_KV_SERIALIZE_MAP()
        KV_SERIALIZE(txs)
        KV_SERIALIZE(_)
      END_KV_SERIALIZE_MAP()
    };
    
  };
  /************************************************************************/
  /*                                                                      */
  /************************************************************************/
  struct NOTIFY_REQUEST_GET_BLOCKS
  {
    constexpr static int ID = BC_COMMANDS_POOL_BASE + 3;

    struct request
    {
      std::vector<crypto::hash> blocks;

      BEGIN_KV_SERIALIZE_MAP()
        KV_SERIALIZE_CONTAINER_POD_AS_BLOB(blocks)
      END_KV_SERIALIZE_MAP()
    };
    
  };

  struct NOTIFY_RESPONSE_GET_BLOCKS
  {
    constexpr static int ID = BC_COMMANDS_POOL_BASE + 4;

    struct request
    {
      std::vector<block_and_txs_data_t>  blocks;
      std::vector<crypto::hash>          missed_ids;
      uint64_t                           p2p_block_index;

      BEGIN_KV_SERIALIZE_MAP()
        KV_SERIALIZE(blocks)
        KV_SERIALIZE_CONTAINER_POD_AS_BLOB(missed_ids)
           KV_SERIALIZE_N(p2p_block_index, "current_blockchain_height")
      END_KV_SERIALIZE_MAP()
    };
    
  };


  struct CORE_SYNC_DATA
  {
    uint64_t p2p_block_index;
    uint64_t cumulative_difficulty;
    uint64_t cumulative_difficulty_top64 = 0;
    crypto::hash  top_id;

    BEGIN_KV_SERIALIZE_MAP()
    KV_SERIALIZE_N(p2p_block_index, "current_height")
      KV_SERIALIZE(cumulative_difficulty)
      KV_SERIALIZE(cumulative_difficulty_top64)
      KV_SERIALIZE_VAL_POD_AS_BLOB(top_id)
    END_KV_SERIALIZE_MAP()
  };

  struct NOTIFY_REQUEST_CHAIN
  {
    constexpr static int ID = BC_COMMANDS_POOL_BASE + 6;

    struct request
    {
      std::list<crypto::hash> block_ids; /*IDs of the first 10 blocks are sequential, next goes with pow(2,n) offset, like 2, 4, 8, 16, 32, 64 and so on, and the last one is always genesis block */

      BEGIN_KV_SERIALIZE_MAP()
        KV_SERIALIZE_CONTAINER_POD_AS_BLOB(block_ids)
      END_KV_SERIALIZE_MAP()
    };
    
  };

  struct NOTIFY_RESPONSE_CHAIN_ENTRY
  {
    constexpr static int ID = BC_COMMANDS_POOL_BASE + 7;

    struct request
    {
      uint64_t start_block_index;
      uint64_t p2p_block_index; // top_block_index + 1
      uint64_t cumulative_difficulty;
      uint64_t cumulative_difficulty_top64 = 0;
      std::vector<crypto::hash> m_block_ids;
      std::vector<uint64_t> m_block_weights;

      BEGIN_KV_SERIALIZE_MAP()
        KV_SERIALIZE_N(start_block_index, "start_height")
        KV_SERIALIZE_N(p2p_block_index, "total_height")
        KV_SERIALIZE(cumulative_difficulty)
        KV_SERIALIZE(cumulative_difficulty_top64)
        KV_SERIALIZE_CONTAINER_POD_AS_BLOB(m_block_ids)
        KV_SERIALIZE_CONTAINER_POD_AS_BLOB(m_block_weights)
      END_KV_SERIALIZE_MAP()
    };
    
  };

  /************************************************************************/
  /*                                                                      */
  /************************************************************************/
  struct NOTIFY_NEW_FLUFFY_BLOCK
  {
    constexpr static int ID = BC_COMMANDS_POOL_BASE + 8;

    struct request
    {
      block_and_txs_data_t b;
      uint64_t p2p_block_index;

      BEGIN_KV_SERIALIZE_MAP()
        KV_SERIALIZE(b)
        KV_SERIALIZE_N(p2p_block_index, "current_blockchain_height")
      END_KV_SERIALIZE_MAP()
    };
    
  };

  /************************************************************************/
  /*                                                                      */
  /************************************************************************/
  struct NOTIFY_REQUEST_FLUFFY_MISSING_TX
  {
    constexpr static int ID = BC_COMMANDS_POOL_BASE + 9;

    struct request
    {
      crypto::hash block_hash;
      uint64_t p2p_block_index;
      std::vector<uint64_t> missing_tx_indices;

      BEGIN_KV_SERIALIZE_MAP()
        KV_SERIALIZE_VAL_POD_AS_BLOB(block_hash)
           KV_SERIALIZE_N(p2p_block_index, "current_blockchain_height")
        KV_SERIALIZE_CONTAINER_POD_AS_BLOB(missing_tx_indices)
      END_KV_SERIALIZE_MAP()
    };
    
  };

  /************************************************************************/
  /*                                                                      */
  /************************************************************************/
  struct NOTIFY_GET_TXPOOL_COMPLEMENT
  {
    constexpr static int ID = BC_COMMANDS_POOL_BASE + 10;

    struct request
    {
      std::vector<crypto::hash> hashes;

      BEGIN_KV_SERIALIZE_MAP()
        KV_SERIALIZE_CONTAINER_POD_AS_BLOB(hashes)
      END_KV_SERIALIZE_MAP()
    };
    
  };

}
