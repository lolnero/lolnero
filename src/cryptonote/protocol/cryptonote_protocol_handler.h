/// @file
/// @author rfree (current maintainer/user in monero.cc project - most of code is from CryptoNote)
/// @brief This is the original cryptonote protocol network-events handler, modified by us

// Copyright (c) 2014-2020, The Monero Project
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this list of
//    conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this list
//    of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may be
//    used to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
// THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
// THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Parts of this file are originally copyright (c) 2012-2013 The Cryptonote developers

#pragma once

#include "cryptonote_protocol_handler_common.h"

#include "cryptonote/core/cryptonote_core.hpp"

#include "tools/epee/include/net/levin_abstract_invoke2.h"
#include "tools/epee/include/net/levin_abstract_invoke2_macro.h"





#define LOCALHOST_INT 2130706433
#define CURRENCY_PROTOCOL_MAX_OBJECT_REQUEST_COUNT 100

static_assert(CURRENCY_PROTOCOL_MAX_OBJECT_REQUEST_COUNT >= constant::BLOCKS_SYNCHRONIZING_SIZE,
                "Invalid CURRENCY_PROTOCOL_MAX_OBJECT_REQUEST_COUNT");

namespace cryptonote
{
	class cryptonote_protocol_handler_base {
  public:
    virtual ~cryptonote_protocol_handler_base() = default;
	};

  class t_cryptonote_protocol_handler:  public i_cryptonote_protocol, cryptonote_protocol_handler_base
  {
  public:
    typedef core t_core;
    typedef cryptonote_connection_context connection_context;
    typedef t_cryptonote_protocol_handler cryptonote_protocol_handler;
    typedef CORE_SYNC_DATA payload_type;

    t_cryptonote_protocol_handler
    (
     t_core& rcore
     , nodetool::i_p2p_endpoint<connection_context>& p_net_layout
     , bool offline = false
     );

    BEGIN_INVOKE_MAP2()
      HANDLE_NOTIFY_T2(NOTIFY_NEW_TRANSACTIONS, &cryptonote_protocol_handler::handle_notify_new_transactions)
      HANDLE_NOTIFY_T2(NOTIFY_REQUEST_GET_BLOCKS, &cryptonote_protocol_handler::handle_request_get_blocks)
      HANDLE_NOTIFY_T2(NOTIFY_RESPONSE_GET_BLOCKS, &cryptonote_protocol_handler::handle_response_get_blocks)
      HANDLE_NOTIFY_T2(NOTIFY_REQUEST_CHAIN, &cryptonote_protocol_handler::handle_request_chain)
      HANDLE_NOTIFY_T2(NOTIFY_RESPONSE_CHAIN_ENTRY, &cryptonote_protocol_handler::handle_response_chain_entry)
      HANDLE_NOTIFY_T2(NOTIFY_NEW_FLUFFY_BLOCK, &cryptonote_protocol_handler::handle_notify_new_fluffy_block)
      HANDLE_NOTIFY_T2(NOTIFY_REQUEST_FLUFFY_MISSING_TX, &cryptonote_protocol_handler::handle_request_fluffy_missing_tx)
      HANDLE_NOTIFY_T2(NOTIFY_GET_TXPOOL_COMPLEMENT, &cryptonote_protocol_handler::handle_notify_get_tx_pool_complement)
    END_INVOKE_MAP2()

    bool on_idle() { return m_core.on_idle(); };
    bool init();
    bool deinit();
    //bool process_handshake_data(const string_blob& data, cryptonote_connection_context& context);
    bool process_payload_sync_data(const CORE_SYNC_DATA& hshd, cryptonote_connection_context& context, bool is_inital);
    bool get_payload_sync_data(string_blob& data);
    bool get_payload_sync_data(CORE_SYNC_DATA& hshd);
    bool on_callback(cryptonote_connection_context& context);
    t_core& get_core(){return m_core;}
    bool is_synchronized(){return m_synchronized;}
    std::list<connection_info> get_connections();
    void stop();
    void on_connection_new(cryptonote_connection_context &context);
    void on_connection_close(cryptonote_connection_context &context);
    void set_max_out_peers(unsigned int max) { m_max_out_peers = max; }
    std::string get_peers_overview() const;
    bool needs_new_sync_connections() const;
    bool is_busy_syncing();

  private:
    //----------------- commands handlers ----------------------------------------------
    int handle_notify_new_transactions(const int command, const NOTIFY_NEW_TRANSACTIONS::request& arg, cryptonote_connection_context& context);
    int handle_request_get_blocks(const int command, const NOTIFY_REQUEST_GET_BLOCKS::request& arg, cryptonote_connection_context& context);
    int handle_response_get_blocks(const int command, const NOTIFY_RESPONSE_GET_BLOCKS::request& arg, cryptonote_connection_context& context);
    int handle_request_chain(const int command, const NOTIFY_REQUEST_CHAIN::request& arg, cryptonote_connection_context& context);
    int handle_response_chain_entry(const int command, const NOTIFY_RESPONSE_CHAIN_ENTRY::request& arg, cryptonote_connection_context& context);
    int handle_notify_new_fluffy_block(const int command, const NOTIFY_NEW_FLUFFY_BLOCK::request& arg, cryptonote_connection_context& context);
    int handle_request_fluffy_missing_tx(const int command, const NOTIFY_REQUEST_FLUFFY_MISSING_TX::request& arg, cryptonote_connection_context& context);
    int handle_notify_get_tx_pool_complement(const int command, const NOTIFY_GET_TXPOOL_COMPLEMENT::request& arg, cryptonote_connection_context& context);

    //----------------- i_bc_protocol_layout ---------------------------------------
    virtual bool relay_block
    (
     const NOTIFY_NEW_FLUFFY_BLOCK::request& arg
     , cryptonote_connection_context& exclude_context
     );

    virtual bool relay_transactions
    (
     const NOTIFY_NEW_TRANSACTIONS::request& arg
     , const boost::uuids::uuid& source_connection_id
     );
    //----------------------------------------------------------------------------------
    bool request_missing_blocks_2
    (
     cryptonote_connection_context& context
     );

    size_t get_synchronizing_connections_count();
    bool on_connection_synchronized();
    void drop_connection(cryptonote_connection_context &context, bool add_fail, bool flush_all_spans);
    void drop_connection_with_score(cryptonote_connection_context &context, unsigned int score, bool flush_all_spans);
    void drop_bad_connections(const epee::net_utils::network_address address);
    void notify_new_stripe(cryptonote_connection_context &context, uint32_t stripe);
    bool request_tx_pool_complement(cryptonote_connection_context &context);
    void hit_score(cryptonote_connection_context &context, int32_t score);

    t_core& m_core;

    nodetool::i_p2p_endpoint<connection_context>& m_p2p;
    std::atomic<uint32_t> m_syncronized_connections_count = 0;
    std::atomic<bool> m_synchronized;
    std::atomic<bool> m_stopping = false;
    std::atomic<bool> m_ask_for_tx_pool_complement = true;
    std::mutex m_sync_lock;
    std::atomic<unsigned int> m_max_out_peers;
    uint64_t m_last_add_end_time;

    std::mutex m_buffer_mutex;

    template<class t_parameter>
      bool post_notify(typename t_parameter::request& arg, cryptonote_connection_context& context)
      {
        LOG_VERBOSE
          (
           "["
           + epee::net_utils::print_connection_context_short(context)
           + "] post "
           + std::string(typeid(t_parameter).name())
           + " -->"
           );

        const auto blob = epee::serialization::store_t_to_binary(arg);
        //handler_response_blocks_now(blob.size()); // XXX
        return m_p2p.invoke_notify_to_peer(t_parameter::ID, blob, context);
      }
  };

} // namespace
