1. Private
2. ASIC friendly
3. Linear emission

There is no premine and no dev tax.

Specifications
==============

* Proof of Work: [SHA-3-256][4]
* Max supply: ∞
* Block reward: 300
* Block time: 5 minutes
* Block size limit: Block height bytes (linearly increasing)
* Range proof: [Bulletproofs][5]
* Ring signature: [CLSAG (Concise Linkable Spontaneous Anonymous
  Group)][6]
* Ring size: 32

[Build](doc/BUILD.md)
=====================

Connect
=======

Pick a [Seed node][7]
---------------------

Public
------

```
lolnerod-rpc --add-peer SOME_SEED_NODE_IP
```

Tor
---

```
lolnerod-rpc --add-peer SOME_TOR_NODE_ADDRESS --proxy tor,127.0.0.1:9050
```

[Use a wallet](doc/wallet-rpc.md)
=================================

[4]: https://en.wikipedia.org/wiki/SHA-3
[5]: https://eprint.iacr.org/2017/1066.pdf
[6]: https://eprint.iacr.org/2019/654.pdf
[7]: https://gitlab.com/lolnero/lolnero/-/wikis/Seed-nodes
