# Create a new wallet

```
lolnero-generate-spend-key > spend_key_file
```

# Open a wallet

```
lolnero-wallet-rpc --open spend_key_file --log-level 1
```

# Converting a seed

```
cat some_seed_file | lolnero-seed-to-spend-key > spend_key_file
```

# Use wallet scripts

## Try various `lolnero-wallet-scripts` in the `build/bin` directory

For example `lolnero-wallet-balance`.

## Sending LOL

To send 5 LOL, one can use

`lolnero-wallet-transfer SOME_ADDRESS 5e11`

since 1 LOL = 1e11 basic unit, or 10 ^ 11 in uint64_t.
