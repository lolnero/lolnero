# Supported

## [Nix](https://nixos.org/download.html)

### Install Nix

```
sh <(curl -L https://nixos.org/nix/install) --no-daemon
```

### Build lolnero

```
nix --extra-experimental-features 'nix-command flakes' build gitlab:lolnero/lolnero
```

Generated binaries will be in `./result/bin/`.

# Unsupported

## Alpine (not working, no GCC12)

```
sudo apk add build-base cmake git
sudo apk add boost-dev libsodium-dev nlohmann-json libtbb
sudo apk add httpie jq

git clone https://gitlab.com/lolnero/lolnero.git

mkdir lolnero/build
cd lolnero/build

cmake .. && make
```

Generated binaries will be in `./bin/`.


## Arch

```
sudo pacman -S base-devel cmake git
sudo pacman -S boost libsodium nlohmann-json tbb
sudo pacman -S httpie jq

git clone https://gitlab.com/lolnero/lolnero.git

mkdir lolnero/build
cd lolnero/build

cmake .. && make
```

Generated binaries will be in `./bin/`.


## Gentoo

```
sudo emerge \
dev-util/cmake \
dev-vcs/git

sudo emerge \
dev-libs/boost \
dev-libs/libsodium \
dev-cpp/nlohmann-json \
dev-cpp/tbb

sudo emerge \
net-misc/httpie \
app-misc/jq

git clone https://gitlab.com/lolnero/lolnero.git

mkdir lolnero/build
cd lolnero/build

cmake .. && make
```

Generated binaries will be in `./bin/`.



## Build for the built-in OpenCL miner

1. Install [`opencl-headers`][1]
2. Install [`opencl-clhpp`][5]
2. Install [`ocl-icd`][2]
3. For AMD GPUs, install [`rocm-opencl-runtime`][3]. For NVIDIA GPUs, install [`opencl-nvidia`][4].
4. In the last command that involves `cmake`, do 

        cmake -DUSE_OPENCL=ON .. && make

### [Tutorial for Manjaro Linux][6]

[1]: https://archlinux.org/packages/extra/any/opencl-headers/
[2]: https://archlinux.org/packages/extra/x86_64/ocl-icd/
[3]: https://aur.archlinux.org/packages/rocm-opencl-runtime/
[4]: https://archlinux.org/packages/extra/x86_64/opencl-nvidia/
[5]: https://archlinux.org/packages/extra/any/opencl-clhpp/
[6]: https://bitcointalk.org/index.php?topic=5280570.msg58294497#msg58294497
