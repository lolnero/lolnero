# ChangeLog

## [0.9.12.27] - 2022-05-27

### Removed

* `unlock-height` after block `200000`

## Added

* `lolnero-classic` after block `200000` for those who want to be more conservative and stick with `unlock-height`

## [0.9.11.36] - 2022-05-01

### Added

* `lolnero-wallet-rpc`
* `lolnero-wallet-scripts`

### Removed

* `lolnero`

### Added

* `lolnero-seed-to-spend-key`

### Removed

* `--restore` and `--electrum-seed`

## [0.9.11.21] - 2022-04-07

### Added

* dependency `nlohmann_json`

## [0.9.10.75] - 2022-04-07

### Removed
* binary `lolnerod`

### Added
* script `lolnerod-status`
* script `lolnerod-block 1`
* script `LOLNEROD_RPC_PORT=4400 lolnerod-diff`
* etc

### Changed
* New dependency: [HTTPie][1] 
* New dependency: [jq][2]

## [0.9.10.55] - 2022-03-15

### Removed
* command `set-in-peers`
* command `set-out-peers`
* command `save`
* command `alt-chain-info`

## [0.9.10.54] - 2022-03-15

### Removed
* command `pool-statss`
* command `peer-list-stats`
* command `blockchain-stats`
* command `blockchain`

## [0.9.10.52] - 2022-03-14

### Added
* Colored output for logging 

### Removed
* command `set-log-level`

#### `lolnerod-rpc`
* arg `--seed-node`, use `--add-peer` instead
* arg `--tos-flag`

#### `lolnero`
* command `get-output-ecdh-secret-keys` / `verify-output-ecdh-secret-keys`
* command `sign` / `verify`
* command `info`
* command `welcome`


## [0.9.10.26] - 2022-02-18

### Changed
* `lolnero*-cpp` -> `lolnero*`
* `lolnerod-cli` -> `lolnerod`


## [0.9.10.22] - 2022-02-18

### Added
* binary `lolnerod-cli-cpp`

### Changed
* `lolnerod-cpp` -> `lolnerod-rpc-cpp`

### Removed

* interactive mode of `lolnerod-rpc-cpp`

    
	

[1]: https://httpie.io/
[2]: https://stedolan.github.io/jq/
